#!/usr/bin/env bash

set -e

cd $(dirname $0)/..
echo "work directory: $(pwd)"

DIRECTORY="src/main/resources/build"

function buildfile {
  echo "$DIRECTORY/$1"
}

function installCloc {
	apk add cloc
}

function ifCommandExists {
	which "$1" || echo ""
}

function ensureCommand {
	NAME="$1"
	INSTALL="$2"

	COMMAND_PATH="$(ifCommandExists $NAME)"
	if [ -z "$COMMAND_PATH" ]; then
		echo "Missing command $NAME, installing ..."
		if [ ! -d "./bin" ]; then
			mkdir -p "./bin"
			export PATH="$PATH:./bin"
			echo "Created a bin directory for commands ..."
		fi
		$INSTALL
	# else
		# echo "Command detected: $PATH"
	fi
}

# target
echo "Preparing target directory ..."
mkdir -p "$DIRECTORY"

# cloc
ensureCommand cloc installCloc
cloc --json src/main/java > `buildfile loc.json`

# hash
echo "Running hash ..."
git rev-parse HEAD > `buildfile hash.txt`

# branch
echo "Running branch ..."
git rev-parse --abbrev-ref HEAD > `buildfile branch.txt`

# time
echo "Running time ..."
date -Iseconds > `buildfile time.txt`

# summary
echo "Generated:"
ls "$DIRECTORY"

echo "Done!"

