/**************************************************************************\
* EnderCrypt's Challenge                                                   *
* Copyright (C) 2023 by EnderCrypt                                         *
*                                                                          *
* This program is free software: you can redistribute it and/or modify     *
* it under the terms of the GNU Affero General Public License as           *
* published by the Free Software Foundation, either version 3 of           *
* the License, or (at your option) any later version.                      *
*                                                                          *
* This program is distributed in the hope that it will be useful,          *
* but WITHOUT ANY WARRANTY; without even the implied warranty of           *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
* GNU Affero General Public License for more details.                      *
*                                                                          *
* You should have received a copy of the GNU Affero General Public License *
* along with this program in its root directory.                           *
* If not, see <http://www.gnu.org/licenses/>.                              *
\**************************************************************************/

package endercrypt.endercryptschallenge.modules.data;


import endercrypt.library.commons.RandomEntry;
import endercrypt.library.commons.data.DataSource;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;


/**
 * @author EnderCrypt
 */
public class DataMessageCollection
{
	public static DataMessageCollection read(String path) throws IOException
	{
		List<String> messages = DataSource.RESOURCES.read(path)
			.split("###")
			.stream()
			.map(String::strip)
			.collect(Collectors.toList());
		
		return new DataMessageCollection(messages);
	}
	
	private List<String> messages;
	
	public DataMessageCollection(List<String> messages)
	{
		this.messages = Collections.unmodifiableList(new ArrayList<>(messages));
	}
	
	public List<String> getMessages()
	{
		return messages;
	}
	
	public String getRandom()
	{
		return RandomEntry.from(messages);
	}
}
