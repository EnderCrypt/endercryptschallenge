/**************************************************************************\
* EnderCrypt's Challenge                                                   *
* Copyright (C) 2023 by EnderCrypt                                         *
*                                                                          *
* This program is free software: you can redistribute it and/or modify     *
* it under the terms of the GNU Affero General Public License as           *
* published by the Free Software Foundation, either version 3 of           *
* the License, or (at your option) any later version.                      *
*                                                                          *
* This program is distributed in the hope that it will be useful,          *
* but WITHOUT ANY WARRANTY; without even the implied warranty of           *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
* GNU Affero General Public License for more details.                      *
*                                                                          *
* You should have received a copy of the GNU Affero General Public License *
* along with this program in its root directory.                           *
* If not, see <http://www.gnu.org/licenses/>.                              *
\**************************************************************************/

package endercrypt.endercryptschallenge.modules.data;


import endercrypt.endercryptschallenge.core.Core;
import endercrypt.endercryptschallenge.core.modules.CoreModule;

import java.io.IOException;


/**
 * @author EnderCrypt
 */
public class DataManager extends CoreModule
{
	private DataMessages messages;
	
	public DataManager(Core core)
	{
		super(core);
	}
	
	@Override
	protected void onStart() throws Exception
	{
		messages = new DataMessages();
	}
	
	public DataMessages getMessages()
	{
		return messages;
	}
	
	public static class DataMessages
	{
		private DataMessageCollection leave;
		private DataMessageCollection acg;
		private DataMessageCollection acgWarning;
		
		private DataMessages() throws IOException
		{
			leave = DataMessageCollection.read("messages/leave.txt");
			acg = DataMessageCollection.read("messages/acg.txt");
			acgWarning = DataMessageCollection.read("messages/acgWarning.txt");
		}
		
		public DataMessageCollection getLeave()
		{
			return leave;
		}
		
		public DataMessageCollection getAcg()
		{
			return acg;
		}
		
		public DataMessageCollection getAcgWarning()
		{
			return acgWarning;
		}
	}
}
