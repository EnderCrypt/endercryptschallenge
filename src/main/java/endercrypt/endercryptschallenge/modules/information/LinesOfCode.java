/**************************************************************************\
* EnderCrypt's Challenge                                                   *
* Copyright (C) 2023 by EnderCrypt                                         *
*                                                                          *
* This program is free software: you can redistribute it and/or modify     *
* it under the terms of the GNU Affero General Public License as           *
* published by the Free Software Foundation, either version 3 of           *
* the License, or (at your option) any later version.                      *
*                                                                          *
* This program is distributed in the hope that it will be useful,          *
* but WITHOUT ANY WARRANTY; without even the implied warranty of           *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
* GNU Affero General Public License for more details.                      *
*                                                                          *
* You should have received a copy of the GNU Affero General Public License *
* along with this program in its root directory.                           *
* If not, see <http://www.gnu.org/licenses/>.                              *
\**************************************************************************/

package endercrypt.endercryptschallenge.modules.information;


import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;


public class LinesOfCode
{
	private final Summary summary;
	
	private List<Language> languages = new ArrayList<>();
	
	public LinesOfCode(JsonObject root)
	{
		summary = new Summary(root.get("SUM").getAsJsonObject());
		
		for (java.util.Map.Entry<String, JsonElement> entry : root.entrySet())
		{
			String name = entry.getKey();
			if (name.equals("SUM") == false && name.equals("header") == false)
			{
				JsonObject json = entry.getValue().getAsJsonObject();
				languages.add(new Language(name, json));
			}
		}
	}
	
	public Summary getSummary()
	{
		return summary;
	}
	
	public List<Language> getLanguages()
	{
		return Collections.unmodifiableList(languages);
	}
	
	public Optional<Language> getLanguage(String language)
	{
		return languages.stream().filter(l -> l.getName().equalsIgnoreCase(language)).findFirst();
	}
	
	public class Language extends Summary
	{
		private final String name;
		
		public Language(String name, JsonObject root)
		{
			super(root);
			this.name = Objects.requireNonNull(name, "name");
		}
		
		public String getName()
		{
			return name;
		}
	}
	
	public class Summary
	{
		private final int files;
		
		private final int lines;
		private final int blank;
		private final int comment;
		
		public Summary(JsonObject root)
		{
			this.files = root.get("nFiles").getAsInt();
			this.lines = root.get("code").getAsInt();
			this.blank = root.get("blank").getAsInt();
			this.comment = root.get("comment").getAsInt();
		}
		
		public int getFiles()
		{
			return files;
		}
		
		public int getLines()
		{
			return lines;
		}
		
		public int getBlank()
		{
			return blank;
		}
		
		public int getComment()
		{
			return comment;
		}
	}
}
