/**************************************************************************\
* EnderCrypt's Challenge                                                   *
* Copyright (C) 2023 by EnderCrypt                                         *
*                                                                          *
* This program is free software: you can redistribute it and/or modify     *
* it under the terms of the GNU Affero General Public License as           *
* published by the Free Software Foundation, either version 3 of           *
* the License, or (at your option) any later version.                      *
*                                                                          *
* This program is distributed in the hope that it will be useful,          *
* but WITHOUT ANY WARRANTY; without even the implied warranty of           *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
* GNU Affero General Public License for more details.                      *
*                                                                          *
* You should have received a copy of the GNU Affero General Public License *
* along with this program in its root directory.                           *
* If not, see <http://www.gnu.org/licenses/>.                              *
\**************************************************************************/

package endercrypt.endercryptschallenge.modules.information;


import endercrypt.endercryptschallenge.utility.Utility;
import endercrypt.library.commons.data.DataSource;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Paths;
import java.time.ZonedDateTime;
import java.util.Optional;

import com.google.gson.JsonObject;

import lombok.Getter;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;


/**
 * @author EnderCrypt
 */
@Log4j2
@RequiredArgsConstructor
@Getter
@NonNull
public class BuildInformation
{
	public static Optional<BuildInformation> generate()
	{
		try
		{
			LinesOfCode clocReport = generateLinesOfCode();
			String hash = generateHash();
			String branch = generateBranch();
			ZonedDateTime time = generateTime();
			return Optional.of(new BuildInformation(clocReport, hash, branch, time));
		}
		catch (FileNotFoundException e)
		{
			return Optional.empty();
		}
		catch (IOException e)
		{
			log.error("Failed to read resource " + e.getMessage(), e);
			return Optional.empty();
		}
	}
	
	private static LinesOfCode generateLinesOfCode() throws IOException
	{
		String text = DataSource.RESOURCES.read("build/loc.json").asString();
		JsonObject json = Utility.gson.fromJson(text, JsonObject.class);
		return new LinesOfCode(json);
	}
	
	private static String generateHash() throws IOException
	{
		return DataSource.RESOURCES.read(Paths.get("build/hash.txt")).splitNewline().stream().findFirst().orElseThrow();
	}
	
	private static String generateBranch() throws IOException
	{
		return DataSource.RESOURCES.read(Paths.get("build/branch.txt")).splitNewline().stream().findFirst().orElseThrow();
	}
	
	private static ZonedDateTime generateTime() throws IOException
	{
		String text = DataSource.RESOURCES.read("build/time.txt").splitNewline().stream().findFirst().orElseThrow();
		return ZonedDateTime.parse(text);
	}
	
	private final LinesOfCode linesOfCode;
	private final String hash;
	private final String branch;
	private final ZonedDateTime time;
	
	@Override
	public String toString()
	{
		StringBuilder sb = new StringBuilder();
		sb.append("Version: ").append(getBranch()).append(" - ").append(getHash()).append("\n");
		sb.append("Build time: ").append(getTime()).append("\n");
		sb.append("Lines of code: ").append(getLinesOfCode().getSummary().getLines());
		return sb.toString();
	}
}
