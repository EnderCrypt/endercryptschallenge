/**************************************************************************\
* EnderCrypt's Challenge                                                   *
* Copyright (C) 2023 by EnderCrypt                                         *
*                                                                          *
* This program is free software: you can redistribute it and/or modify     *
* it under the terms of the GNU Affero General Public License as           *
* published by the Free Software Foundation, either version 3 of           *
* the License, or (at your option) any later version.                      *
*                                                                          *
* This program is distributed in the hope that it will be useful,          *
* but WITHOUT ANY WARRANTY; without even the implied warranty of           *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
* GNU Affero General Public License for more details.                      *
*                                                                          *
* You should have received a copy of the GNU Affero General Public License *
* along with this program in its root directory.                           *
* If not, see <http://www.gnu.org/licenses/>.                              *
\**************************************************************************/

package endercrypt.endercryptschallenge.modules.timer;


import endercrypt.endercryptschallenge.core.Core;
import endercrypt.endercryptschallenge.core.modules.CoreModule;
import endercrypt.endercryptschallenge.modules.features.OnSecond;
import endercrypt.endercryptschallenge.modules.features.OnTick;
import endercrypt.library.commons.interfaces.Code;

import java.util.logging.Level;

import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.scheduler.BukkitTask;


/**
 * @author EnderCrypt
 */
public class TimerManager extends CoreModule
{
	private final BukkitTask tickTimerTask;
	private final BukkitTask secondTimerTask;
	
	public TimerManager(Core core)
	{
		super(core);
		
		// tick timer task
		tickTimerTask = new TickRunner().runTaskTimer(core.getPlugin(), 0L, 1L);
		
		// second timer task
		secondTimerTask = new SecondRunner().runTaskTimer(core.getPlugin(), 0L, 20L);
	}
	
	public BukkitRunnable schedule(int ticks, Code code)
	{
		BukkitRunnable runnable = new BukkitRunnable()
		{
			@Override
			public void run()
			{
				try
				{
					code.run();
				}
				catch (Exception e)
				{
					getLogger().log(Level.SEVERE, "exception in " + code, e);
				}
			}
		};
		runnable.runTaskLater(core.getPlugin(), ticks);
		return runnable;
	}
	
	@Override
	public void onShutdown()
	{
		if (tickTimerTask != null)
		{
			tickTimerTask.cancel();
		}
		
		if (secondTimerTask != null)
		{
			secondTimerTask.cancel();
		}
	}
	
	private class TickRunner extends BukkitRunnable
	{
		@Override
		public void run()
		{
			core.getEventManager().trigger(OnTick.class).onTick();
		}
	}
	
	private class SecondRunner extends BukkitRunnable
	{
		@Override
		public void run()
		{
			core.getEventManager().trigger(OnSecond.class).onSecond();
		}
	}
}
