/**************************************************************************\
* EnderCrypt's Challenge                                                   *
* Copyright (C) 2023 by EnderCrypt                                         *
*                                                                          *
* This program is free software: you can redistribute it and/or modify     *
* it under the terms of the GNU Affero General Public License as           *
* published by the Free Software Foundation, either version 3 of           *
* the License, or (at your option) any later version.                      *
*                                                                          *
* This program is distributed in the hope that it will be useful,          *
* but WITHOUT ANY WARRANTY; without even the implied warranty of           *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
* GNU Affero General Public License for more details.                      *
*                                                                          *
* You should have received a copy of the GNU Affero General Public License *
* along with this program in its root directory.                           *
* If not, see <http://www.gnu.org/licenses/>.                              *
\**************************************************************************/

package endercrypt.endercryptschallenge.modules.players;


import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import org.reflections.Reflections;


/**
 * @author EnderCrypt
 */
public class PlayerValueFactory
{
	private final Set<Class<? extends PlayerValue>> playerValueClasses;
	
	public PlayerValueFactory(String packagePath)
	{
		Reflections reflections = new Reflections(packagePath);
		playerValueClasses = reflections.getSubTypesOf(PlayerValue.class);
		if (playerValueClasses.isEmpty())
		{
			throw new IllegalArgumentException("Found no player values!");
		}
	}
	
	public Map<Class<? extends PlayerValue>, PlayerValue> build(PlayerProfile profile)
	{
		Map<Class<? extends PlayerValue>, PlayerValue> result = new HashMap<>();
		for (Class<? extends PlayerValue> playerValueClass : playerValueClasses)
		{
			PlayerValue value = buildValue(playerValueClass, profile);
			result.put(playerValueClass, value);
		}
		return result;
	}
	
	private <T extends PlayerValue> T buildValue(Class<T> valueType, PlayerProfile profile)
	{
		try
		{
			Constructor<T> constructor = valueType.getConstructor(PlayerProfile.class);
			return constructor.newInstance(profile);
		}
		catch (NoSuchMethodException | SecurityException | InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException e)
		{
			throw new PlayerException("failed to create " + valueType + " for " + profile.getPlayer(), e);
		}
	}
}
