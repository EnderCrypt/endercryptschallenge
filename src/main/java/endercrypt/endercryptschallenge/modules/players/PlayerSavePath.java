/**************************************************************************\
* EnderCrypt's Challenge                                                   *
* Copyright (C) 2023 by EnderCrypt                                         *
*                                                                          *
* This program is free software: you can redistribute it and/or modify     *
* it under the terms of the GNU Affero General Public License as           *
* published by the Free Software Foundation, either version 3 of           *
* the License, or (at your option) any later version.                      *
*                                                                          *
* This program is distributed in the hope that it will be useful,          *
* but WITHOUT ANY WARRANTY; without even the implied warranty of           *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
* GNU Affero General Public License for more details.                      *
*                                                                          *
* You should have received a copy of the GNU Affero General Public License *
* along with this program in its root directory.                           *
* If not, see <http://www.gnu.org/licenses/>.                              *
\**************************************************************************/

package endercrypt.endercryptschallenge.modules.players;


import endercrypt.endercryptschallenge.core.Core;
import endercrypt.endercryptschallenge.core.CoreCollective;
import endercrypt.endercryptschallenge.modules.players.data.PlayerData;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import org.bukkit.entity.Player;


/**
 * @author EnderCrypt
 */
public class PlayerSavePath extends CoreCollective
{
	private final Player player;
	private final Path directory;
	private final Path file;
	
	public PlayerSavePath(Core core, Player player)
	{
		super(core);
		this.player = player;
		this.directory = core.getPlugin().getDataFolder().toPath().resolve("players");
		this.file = directory.resolve(player.getUniqueId().toString() + ".sav");
	}
	
	public Player getPlayer()
	{
		return player;
	}
	
	public Path getDirectory()
	{
		return directory;
	}
	
	public Path getFile()
	{
		return file;
	}
	
	private void setup()
	{
		try
		{
			Files.createDirectories(directory);
		}
		catch (IOException e)
		{
			throw new PlayerException("failed to create directory for " + getPlayer(), e);
		}
	}
	
	public boolean exists()
	{
		return Files.exists(getFile());
	}
	
	public void write(PlayerProfile profile)
	{
		setup();
		try (ObjectOutputStream output = new ObjectOutputStream(new FileOutputStream(getFile().toFile())))
		{
			Map<String, Object> values = profile.getData().getValues();
			output.writeInt(values.size());
			for (Entry<String, Object> entry : values.entrySet())
			{
				String key = entry.getKey();
				Object value = entry.getValue();
				output.writeUTF(key);
				output.writeObject(value);
			}
		}
		catch (IOException e)
		{
			throw new PlayerException("failed to save " + player, e);
		}
		logger.info("Saved " + profile + " to " + getFile());
	}
	
	public PlayerProfile read()
	{
		try (ObjectInputStream input = new ObjectInputStream(new FileInputStream(getFile().toFile())))
		{
			Map<String, Object> values = new HashMap<>();
			int count = input.readInt();
			for (int i = 0; i < count; i++)
			{
				String key = input.readUTF();
				Object value = input.readObject();
				values.put(key, value);
			}
			PlayerData data = new PlayerData(values);
			PlayerProfile profile = new PlayerProfile(core, player, data);
			logger.info("Loaded " + profile);
			return profile;
		}
		catch (IOException | ClassNotFoundException e)
		{
			throw new PlayerException("failed to load " + player, e);
		}
	}
	
	public PlayerProfile createNew()
	{
		return new PlayerProfile(core, player);
	}
	
	public PlayerProfile generate()
	{
		if (exists())
		{
			return read();
		}
		
		PlayerProfile profile = createNew();
		write(profile);
		return profile;
	}
}
