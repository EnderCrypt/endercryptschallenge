/**************************************************************************\
* EnderCrypt's Challenge                                                   *
* Copyright (C) 2023 by EnderCrypt                                         *
*                                                                          *
* This program is free software: you can redistribute it and/or modify     *
* it under the terms of the GNU Affero General Public License as           *
* published by the Free Software Foundation, either version 3 of           *
* the License, or (at your option) any later version.                      *
*                                                                          *
* This program is distributed in the hope that it will be useful,          *
* but WITHOUT ANY WARRANTY; without even the implied warranty of           *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
* GNU Affero General Public License for more details.                      *
*                                                                          *
* You should have received a copy of the GNU Affero General Public License *
* along with this program in its root directory.                           *
* If not, see <http://www.gnu.org/licenses/>.                              *
\**************************************************************************/

package endercrypt.endercryptschallenge.modules.players;


import endercrypt.endercryptschallenge.core.Core;
import endercrypt.endercryptschallenge.core.modules.CoreModule;
import endercrypt.endercryptschallenge.modules.features.OnSecond;
import endercrypt.endercryptschallenge.modules.features.OnTick;
import endercrypt.library.commons.iterator.PurifyingIterator;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.bukkit.World;
import org.bukkit.entity.Player;


/**
 * @author EnderCrypt
 */
public class PlayerManager extends CoreModule implements OnSecond, OnTick, Iterable<PlayerProfile>
{
	private Map<Player, PlayerProfile> profiles = new HashMap<>();
	
	public PlayerManager(Core core)
	{
		super(core);
	}
	
	@Override
	public Iterator<PlayerProfile> iterator()
	{
		return new PurifyingIterator<>(profiles.values(), PlayerProfile::isValid)
			.addPurifyListener(PlayerProfile::onDestroy);
	}
	
	@Override
	public void onSecond()
	{
		iterator().forEachRemaining(PlayerProfile::onSecond);
	}
	
	@Override
	public void onTick()
	{
		for (World world : server.getWorlds())
		{
			world.getPlayers().forEach(this::resolve);
		}
		iterator().forEachRemaining(PlayerProfile::onTick);
	}
	
	public PlayerProfile resolve(Player player)
	{
		PlayerProfile profile = profiles.get(player);
		if (profile == null)
		{
			try
			{
				PlayerSavePath path = new PlayerSavePath(core, player);
				profile = path.generate();
				profiles.put(player, profile);
			}
			catch (PlayerException e)
			{
				throw new PlayerException("failed to resolve player " + player.getUniqueId(), e);
			}
		}
		return profile;
	}
	
	@Override
	protected void onShutdown() throws Exception
	{
		iterator().forEachRemaining(PlayerProfile::onDestroy);
	}
}
