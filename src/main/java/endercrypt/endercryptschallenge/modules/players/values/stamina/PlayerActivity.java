/**************************************************************************\
* EnderCrypt's Challenge                                                   *
* Copyright (C) 2023 by EnderCrypt                                         *
*                                                                          *
* This program is free software: you can redistribute it and/or modify     *
* it under the terms of the GNU Affero General Public License as           *
* published by the Free Software Foundation, either version 3 of           *
* the License, or (at your option) any later version.                      *
*                                                                          *
* This program is distributed in the hope that it will be useful,          *
* but WITHOUT ANY WARRANTY; without even the implied warranty of           *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
* GNU Affero General Public License for more details.                      *
*                                                                          *
* You should have received a copy of the GNU Affero General Public License *
* along with this program in its root directory.                           *
* If not, see <http://www.gnu.org/licenses/>.                              *
\**************************************************************************/

package endercrypt.endercryptschallenge.modules.players.values.stamina;

/**
 * @author EnderCrypt
 */
public enum PlayerActivity
{
	ROAMING(1.0), // walking
	SPRINTING(0.5), // running
	STANDING(1.5), // started standing still
	RESTING(2.5), // stood still for a while
	SLEEPING(10.0); // laying in bed
	
	private double recoveryModifier;
	
	private PlayerActivity(double recoveryModifier)
	{
		this.recoveryModifier = recoveryModifier;
	}
	
	public double getRecoveryModifier()
	{
		return recoveryModifier;
	}
}
