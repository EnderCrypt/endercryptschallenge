/**************************************************************************\
* EnderCrypt's Challenge                                                   *
* Copyright (C) 2023 by EnderCrypt                                         *
*                                                                          *
* This program is free software: you can redistribute it and/or modify     *
* it under the terms of the GNU Affero General Public License as           *
* published by the Free Software Foundation, either version 3 of           *
* the License, or (at your option) any later version.                      *
*                                                                          *
* This program is distributed in the hope that it will be useful,          *
* but WITHOUT ANY WARRANTY; without even the implied warranty of           *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
* GNU Affero General Public License for more details.                      *
*                                                                          *
* You should have received a copy of the GNU Affero General Public License *
* along with this program in its root directory.                           *
* If not, see <http://www.gnu.org/licenses/>.                              *
\**************************************************************************/

package endercrypt.endercryptschallenge.modules.players.values.stamina;


import endercrypt.endercryptschallenge.features.popup.deathmessage.DeathMessage;
import endercrypt.endercryptschallenge.modules.players.PlayerProfile;
import endercrypt.endercryptschallenge.modules.players.PlayerValue;
import endercrypt.endercryptschallenge.modules.players.data.PlayerDataKey;
import endercrypt.endercryptschallenge.modules.players.data.PlayerDataValue;
import endercrypt.endercryptschallenge.utility.Utility;
import endercrypt.endercryptschallenge.utility.difficulty.Difficulties;
import endercrypt.endercryptschallenge.utility.difficulty.DifficultyRating;
import endercrypt.library.commons.misc.Ender;

import java.util.function.DoubleUnaryOperator;

import org.bukkit.Difficulty;
import org.bukkit.EntityEffect;
import org.bukkit.entity.Boat;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;


/**
 * @author EnderCrypt
 */
public class StaminaPlayerValue extends PlayerValue
{
	public static PlayerDataKey<Double> keyStamina = new PlayerDataKey<>("stamina.stamina", 1.0);
	private final PlayerDataValue<Double> stamina = resolve(keyStamina);
	
	public static PlayerDataKey<Integer> keyRestTimer = new PlayerDataKey<>("stamina.rest_timer", 0);
	private final PlayerDataValue<Integer> restTimer = resolve(keyRestTimer);
	
	public static PlayerDataKey<Double> keySugar = new PlayerDataKey<>("stamina.sugar", 0.0);
	private final PlayerDataValue<Double> sugar = resolve(keySugar);
	
	private final PlayerStaminaBar bar;
	private int idleTimer = 0;
	
	public StaminaPlayerValue(PlayerProfile profile)
	{
		super(profile);
		this.bar = new PlayerStaminaBar(profile);
	}
	
	public PlayerActivity getPlayerActivity()
	{
		// sprinting
		if (player.isSprinting())
		{
			return PlayerActivity.SPRINTING;
		}
		
		// sleeping
		if (player.isSleeping())
		{
			return PlayerActivity.SLEEPING;
		}
		
		// resting
		if (restTimer.get() > 100)
		{
			return PlayerActivity.RESTING;
		}
		
		// standing still
		if (restTimer.get() > 5)
		{
			return PlayerActivity.STANDING;
		}
		
		// anything else
		return PlayerActivity.ROAMING;
	}
	
	private double getRecoveryRate()
	{
		double health = player.getHealth() * 0.5;
		int food = (int) (player.getFoodLevel() * 2.0);
		double modifier = 1.0 / (10 + 30) * (health + food); // should be 0 to 1
		
		modifier += retrieveSugar(0.002);
		
		return modifier * modifier * modifier * 0.075;
	}
	
	private double getGameModeModifier()
	{
		switch (player.getGameMode())
		{
		case CREATIVE:
			return 10.0;
		default:
			return 1.0;
		}
	}
	
	public double getRecoveryBoost()
	{
		double base = restTimer.get() * 0.0035;
		double boost = 1 + (base * base * base);
		return Math.min(boost, 2.5);
	}
	
	private double getRegainAmount()
	{
		double regain = getRecoveryRate() * getRecoveryBoost() * 0.003;
		regain *= getPlayerActivity().getRecoveryModifier() * getGameModeModifier();
		return regain;
	}
	
	public boolean isWeak()
	{
		return getStamina() <= 0.05;
	}
	
	// EVENTS //
	
	public boolean onSwingArm(boolean attack)
	{
		restTimer.set(0);
		
		double modifier = attack ? 10.0 : 1.0;
		if (burnStamina(0.0010 * modifier, 0.0013 * modifier, 0.0016 * modifier) == false)
		{
			player.sendMessage("You are too tired do swing your arm!");
			player.addPotionEffect(new PotionEffect(PotionEffectType.MINING_FATIGUE, 10, 1));
			return false;
		}
		
		return true;
	}
	
	public void onPlayerJump()
	{
		restTimer.set(0);
		
		if (burnStaminaByWeight(0.010, 0.020, 0.025) == false)
		{
			DifficultyRating difficulty = Difficulties.getFor(player);
			double damage = difficulty.asDoubleValue(1.0, 2.0, 3.0);
			
			player.sendMessage("Forcing your exhausted body to jump hurts!");
			player.playEffect(EntityEffect.HURT);
			double rawHealth = Math.ceil(player.getHealth()) - damage;
			double newHealth = Math.max(0, rawHealth);
			
			core.getMinecraftFeatureManager().getFeature(DeathMessage.class).setKilledBy(player, "exhaustion");
			
			player.setHealth(newHealth);
		}
	}
	
	public void onPlayerMove()
	{
		restTimer.set(0);
	}
	
	// MANAGE STAMINA //
	
	public void mapStamina(DoubleUnaryOperator operator)
	{
		setStamina(operator.applyAsDouble(getStamina()));
	}
	
	public double getStamina()
	{
		return stamina.get();
	}
	
	public void setStamina(double stamina)
	{
		if (stamina > 1.0)
		{
			stamina = 1.0;
		}
		if (stamina < 0.0)
		{
			stamina = 0.0;
		}
		this.stamina.set(stamina);
	}
	
	// BURN STAMINA //
	
	public boolean burnStaminaByWeight(double easy, double normal, double hard)
	{
		double modifier = Utility.getPlayerWeight(player) * 0.02;
		return burnStamina(easy * modifier, normal * modifier, hard * modifier);
	}
	
	public boolean burnStamina(double easy, double normal, double hard)
	{
		DifficultyRating difficulty = Difficulties.getFor(player);
		return burnStamina(difficulty.asDoubleValue(easy, normal, hard));
	}
	
	public boolean burnStamina(double stamina)
	{
		setStamina(getStamina() - stamina);
		return hasStamina();
	}
	
	public boolean hasStamina()
	{
		return (getStamina() > 0.025);
	}
	
	// SUGAR
	
	public void addSugar(double amount)
	{
		double value = sugar.get();
		value += amount;
		if (value < 0.01)
		{
			value = 0;
		}
		sugar.set(value);
	}
	
	public double retrieveSugar(double ratio)
	{
		double current = sugar.get();
		double retrieved = current * ratio;
		addSugar(-retrieved);
		return retrieved;
	}
	
	public double getSugar()
	{
		return sugar.get();
	}
	
	// OVERRIDES //
	
	@Override
	protected void onTick()
	{
		// vars
		DifficultyRating difficulty = Difficulties.getFor(player);
		
		// increase reseting timer
		restTimer.map(n -> n + 1);
		
		// recover stamina
		burnStamina(-getRegainAmount());
		
		// running
		if (player.isSprinting())
		{
			burnStaminaByWeight(0.001, 0.0015, 0.002);
		}
		
		// in boat
		if (player.getVehicle() instanceof Boat && getPlayerActivity() == PlayerActivity.ROAMING)
		{
			burnStaminaByWeight(0.0012, 0.0015, 0.0020);
		}
		
		// easy debuff
		if (isWeak())
		{
			player.addPotionEffect(new PotionEffect(PotionEffectType.SLOWNESS, 10, 1));
			player.addPotionEffect(new PotionEffect(PotionEffectType.MINING_FATIGUE, 10, 1));
			player.addPotionEffect(new PotionEffect(PotionEffectType.WEAKNESS, 10, 1));
		}
		
		// normal debuff
		if (getStamina() <= 0.2 && difficulty.isHarderThan(Difficulty.EASY))
		{
			player.addPotionEffect(new PotionEffect(PotionEffectType.WEAKNESS, 10, 0));
		}
		
		// hard debuff
		idleTimer++;
		if (getStamina() <= 0.01 && difficulty.isHarderThan(Difficulty.NORMAL))
		{
			player.addPotionEffect(new PotionEffect(PotionEffectType.NAUSEA, Ender.math.randomRange(100, 200), 1));
			player.addPotionEffect(new PotionEffect(PotionEffectType.BLINDNESS, Ender.math.randomRange(100, 200), 1));
			if (idleTimer > 20)
			{
				player.sendMessage("The over-exertion makes you feel extremely ill!");
				idleTimer = 0;
			}
		}
		
		// bar
		bar.update(this);
	}
	
	@Override
	protected void onDestroy()
	{
		bar.destroy();
	}
}
