/**************************************************************************\
* EnderCrypt's Challenge                                                   *
* Copyright (C) 2023 by EnderCrypt                                         *
*                                                                          *
* This program is free software: you can redistribute it and/or modify     *
* it under the terms of the GNU Affero General Public License as           *
* published by the Free Software Foundation, either version 3 of           *
* the License, or (at your option) any later version.                      *
*                                                                          *
* This program is distributed in the hope that it will be useful,          *
* but WITHOUT ANY WARRANTY; without even the implied warranty of           *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
* GNU Affero General Public License for more details.                      *
*                                                                          *
* You should have received a copy of the GNU Affero General Public License *
* along with this program in its root directory.                           *
* If not, see <http://www.gnu.org/licenses/>.                              *
\**************************************************************************/

package endercrypt.endercryptschallenge.modules.players.values.stamina;


import endercrypt.endercryptschallenge.modules.players.PlayerProfile;

import org.bukkit.Bukkit;
import org.bukkit.boss.BarColor;
import org.bukkit.boss.BarStyle;
import org.bukkit.boss.BossBar;


/**
 * @author EnderCrypt
 */
public class PlayerStaminaBar
{
	private final BossBar bar;
	
	public PlayerStaminaBar(PlayerProfile profile)
	{
		this.bar = Bukkit.createBossBar("Stamina", BarColor.WHITE, BarStyle.SEGMENTED_20);
		this.bar.removeAll();
		this.bar.addPlayer(profile.getPlayer());
	}
	
	private BarColor getBossBarColor(StaminaPlayerValue stamina)
	{
		double staminaValue = stamina.getStamina();
		if (staminaValue > 0.99)
			return BarColor.GREEN;
		if (staminaValue > 0.5)
			return BarColor.WHITE;
		if (staminaValue > 0.2)
			return BarColor.YELLOW;
		
		return BarColor.RED;
	}
	
	public void update(StaminaPlayerValue stamina)
	{
		// color
		bar.setColor(getBossBarColor(stamina));
		
		// progress
		bar.setProgress(stamina.getStamina());
	}
	
	public void destroy()
	{
		bar.setVisible(false);
		bar.removeAll();
	}
}
