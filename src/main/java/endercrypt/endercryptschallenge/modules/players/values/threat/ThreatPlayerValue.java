/**************************************************************************\
* EnderCrypt's Challenge                                                   *
* Copyright (C) 2023 by EnderCrypt                                         *
*                                                                          *
* This program is free software: you can redistribute it and/or modify     *
* it under the terms of the GNU Affero General Public License as           *
* published by the Free Software Foundation, either version 3 of           *
* the License, or (at your option) any later version.                      *
*                                                                          *
* This program is distributed in the hope that it will be useful,          *
* but WITHOUT ANY WARRANTY; without even the implied warranty of           *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
* GNU Affero General Public License for more details.                      *
*                                                                          *
* You should have received a copy of the GNU Affero General Public License *
* along with this program in its root directory.                           *
* If not, see <http://www.gnu.org/licenses/>.                              *
\**************************************************************************/

package endercrypt.endercryptschallenge.modules.players.values.threat;


import endercrypt.endercryptschallenge.features.village.endangered.EndangeredCollection;
import endercrypt.endercryptschallenge.features.village.endangered.EndangeredVillager;
import endercrypt.endercryptschallenge.modules.players.PlayerProfile;
import endercrypt.endercryptschallenge.modules.players.PlayerValue;
import endercrypt.endercryptschallenge.modules.players.data.PlayerDataKey;
import endercrypt.endercryptschallenge.modules.players.data.PlayerDataValue;
import endercrypt.endercryptschallenge.utility.difficulty.Difficulties;
import endercrypt.endercryptschallenge.utility.difficulty.DifficultyRating;
import endercrypt.library.commons.misc.Ender;

import java.util.function.UnaryOperator;

import org.bukkit.ChatColor;
import org.bukkit.GameMode;


/**
 * @author EnderCrypt
 */
public class ThreatPlayerValue extends PlayerValue
{
	public static final double MINIMUM = 1.0;
	
	public static PlayerDataKey<Double> keyThreat = new PlayerDataKey<>("threat.value", 0.0);
	
	private final PlayerDataValue<Double> threat = resolve(keyThreat);
	
	public ThreatPlayerValue(PlayerProfile profile)
	{
		super(profile);
	}
	
	public void add(double threat, String reason)
	{
		if (threat <= 0.1)
		{
			return;
		}
		if (player.getGameMode() == GameMode.CREATIVE)
		{
			player.sendMessage(ChatColor.BLUE + "+" + Ender.math.round(threat, 1) + " threat (" + reason + ")");
		}
		map(v -> v + threat);
	}
	
	public void map(UnaryOperator<Double> operator)
	{
		set(operator.apply(get()));
	}
	
	public void set(double value)
	{
		if (value < MINIMUM)
		{
			value = 0.0;
		}
		
		threat.set(value);
	}
	
	public double get()
	{
		return threat.get();
	}
	
	public boolean isThreat()
	{
		return isThreat(0.0);
	}
	
	public boolean isThreat(double heat)
	{
		return get() > heat;
	}
	
	@Override
	protected void onSecond()
	{
		map(this::updateThreat);
	}
	
	private double updateThreat(double threat)
	{
		DifficultyRating difficulty = Difficulties.getFor(player);
		
		threat *= 1.0 - difficulty.asDoubleValue(0.25, 0.025, 0.015, 0.005);
		return threat;
	}
	
	public void apply(EndangeredCollection endangered)
	{
		for (EndangeredVillager endangeredVillager : endangered)
		{
			Double heat = endangeredVillager.getActiveThreats().get(player);
			if (heat != null)
			{
				add(heat, "Kidnapping villager");
			}
		}
	}
}
