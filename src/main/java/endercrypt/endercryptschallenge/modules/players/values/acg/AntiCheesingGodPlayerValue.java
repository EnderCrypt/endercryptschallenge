/**************************************************************************\
* EnderCrypt's Challenge                                                   *
* Copyright (C) 2023 by EnderCrypt                                         *
*                                                                          *
* This program is free software: you can redistribute it and/or modify     *
* it under the terms of the GNU Affero General Public License as           *
* published by the Free Software Foundation, either version 3 of           *
* the License, or (at your option) any later version.                      *
*                                                                          *
* This program is distributed in the hope that it will be useful,          *
* but WITHOUT ANY WARRANTY; without even the implied warranty of           *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
* GNU Affero General Public License for more details.                      *
*                                                                          *
* You should have received a copy of the GNU Affero General Public License *
* along with this program in its root directory.                           *
* If not, see <http://www.gnu.org/licenses/>.                              *
\**************************************************************************/

package endercrypt.endercryptschallenge.modules.players.values.acg;


import endercrypt.endercryptschallenge.modules.players.PlayerProfile;
import endercrypt.endercryptschallenge.modules.players.PlayerValue;
import endercrypt.endercryptschallenge.modules.players.data.PlayerDataKey;
import endercrypt.endercryptschallenge.modules.players.data.PlayerDataValue;


/**
 * @author EnderCrypt
 */
public class AntiCheesingGodPlayerValue extends PlayerValue
{
	private static final PlayerDataKey<Boolean> keyImmidieteStrike = new PlayerDataKey<>("acg.immidiete_strike", false);
	private final PlayerDataValue<Boolean> immidieteStrike = resolve(keyImmidieteStrike);
	
	public AntiCheesingGodPlayerValue(PlayerProfile profile)
	{
		super(profile);
	}
	
	public void setImmidieteStrike(boolean value)
	{
		immidieteStrike.set(value);
	}
	
	public boolean isImmidieteStrike()
	{
		return immidieteStrike.get();
	}
}
