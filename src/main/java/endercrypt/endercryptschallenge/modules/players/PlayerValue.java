/**************************************************************************\
* EnderCrypt's Challenge                                                   *
* Copyright (C) 2023 by EnderCrypt                                         *
*                                                                          *
* This program is free software: you can redistribute it and/or modify     *
* it under the terms of the GNU Affero General Public License as           *
* published by the Free Software Foundation, either version 3 of           *
* the License, or (at your option) any later version.                      *
*                                                                          *
* This program is distributed in the hope that it will be useful,          *
* but WITHOUT ANY WARRANTY; without even the implied warranty of           *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
* GNU Affero General Public License for more details.                      *
*                                                                          *
* You should have received a copy of the GNU Affero General Public License *
* along with this program in its root directory.                           *
* If not, see <http://www.gnu.org/licenses/>.                              *
\**************************************************************************/

package endercrypt.endercryptschallenge.modules.players;


import endercrypt.endercryptschallenge.core.CoreCollective;
import endercrypt.endercryptschallenge.modules.players.data.PlayerData;
import endercrypt.endercryptschallenge.modules.players.data.PlayerDataKey;
import endercrypt.endercryptschallenge.modules.players.data.PlayerDataValue;

import org.bukkit.entity.Player;


/**
 * @author EnderCrypt
 */
public class PlayerValue extends CoreCollective
{
	protected final PlayerProfile profile;
	protected final PlayerData data;
	protected final Player player;
	
	public PlayerValue(PlayerProfile profile)
	{
		super(profile.getCore());
		this.profile = profile;
		this.data = profile.getData();
		this.player = profile.getPlayer();
	}
	
	protected void checkValid()
	{
		if (profile.isValid() == false)
			throw new PlayerException(player + " is not online");
	}
	
	protected <E> PlayerDataValue<E> resolve(PlayerDataKey<E> key)
	{
		return data.resolve(key);
	}
	
	protected void onSecond()
	{
		// optional override
	}
	
	protected void onTick()
	{
		// optional override
	}
	
	protected void onDestroy()
	{
		// optional override
	}
}
