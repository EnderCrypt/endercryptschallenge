/**************************************************************************\
* EnderCrypt's Challenge                                                   *
* Copyright (C) 2023 by EnderCrypt                                         *
*                                                                          *
* This program is free software: you can redistribute it and/or modify     *
* it under the terms of the GNU Affero General Public License as           *
* published by the Free Software Foundation, either version 3 of           *
* the License, or (at your option) any later version.                      *
*                                                                          *
* This program is distributed in the hope that it will be useful,          *
* but WITHOUT ANY WARRANTY; without even the implied warranty of           *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
* GNU Affero General Public License for more details.                      *
*                                                                          *
* You should have received a copy of the GNU Affero General Public License *
* along with this program in its root directory.                           *
* If not, see <http://www.gnu.org/licenses/>.                              *
\**************************************************************************/

package endercrypt.endercryptschallenge.modules.players.data;


import java.util.HashSet;
import java.util.Objects;
import java.util.Set;


/**
 * @author EnderCrypt
 */
public class PlayerDataKey<E>
{
	private static final Set<String> usedIds = new HashSet<>();
	
	private final String id;
	private final E defaultValue;
	
	public PlayerDataKey(String id, E defaultValue)
	{
		this.id = Objects.requireNonNull(id, "id");
		this.defaultValue = Objects.requireNonNull(defaultValue, "defaultValue");
		
		if (usedIds.contains(getId()))
		{
			throw new IllegalArgumentException("id " + getId() + " already exists");
		}
		usedIds.add(getId());
	}
	
	public String getId()
	{
		return id;
	}
	
	public E getDefaultValue()
	{
		return defaultValue;
	}
}
