/**************************************************************************\
* EnderCrypt's Challenge                                                   *
* Copyright (C) 2023 by EnderCrypt                                         *
*                                                                          *
* This program is free software: you can redistribute it and/or modify     *
* it under the terms of the GNU Affero General Public License as           *
* published by the Free Software Foundation, either version 3 of           *
* the License, or (at your option) any later version.                      *
*                                                                          *
* This program is distributed in the hope that it will be useful,          *
* but WITHOUT ANY WARRANTY; without even the implied warranty of           *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
* GNU Affero General Public License for more details.                      *
*                                                                          *
* You should have received a copy of the GNU Affero General Public License *
* along with this program in its root directory.                           *
* If not, see <http://www.gnu.org/licenses/>.                              *
\**************************************************************************/

package endercrypt.endercryptschallenge.modules.players.data;


import java.util.Map;
import java.util.Objects;
import java.util.function.UnaryOperator;


/**
 * @author EnderCrypt
 */
public class PlayerDataValue<E>
{
	private final PlayerDataKey<E> key;
	private final Map<String, Object> values;
	
	public PlayerDataValue(PlayerDataKey<E> key, Map<String, Object> values)
	{
		this.key = Objects.requireNonNull(key, "key");
		this.values = Objects.requireNonNull(values, "values");
		if (get() == null)
		{
			set(getKey().getDefaultValue());
		}
	}
	
	public PlayerDataKey<E> getKey()
	{
		return key;
	}
	
	public void set(E value)
	{
		values.put(getKey().getId(), value);
	}
	
	@SuppressWarnings("unchecked")
	public E get()
	{
		return (E) values.get(getKey().getId());
	}
	
	public void map(UnaryOperator<E> operator)
	{
		set(operator.apply(get()));
	}
}
