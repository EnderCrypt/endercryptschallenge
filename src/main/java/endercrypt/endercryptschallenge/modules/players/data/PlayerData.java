/**************************************************************************\
* EnderCrypt's Challenge                                                   *
* Copyright (C) 2023 by EnderCrypt                                         *
*                                                                          *
* This program is free software: you can redistribute it and/or modify     *
* it under the terms of the GNU Affero General Public License as           *
* published by the Free Software Foundation, either version 3 of           *
* the License, or (at your option) any later version.                      *
*                                                                          *
* This program is distributed in the hope that it will be useful,          *
* but WITHOUT ANY WARRANTY; without even the implied warranty of           *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
* GNU Affero General Public License for more details.                      *
*                                                                          *
* You should have received a copy of the GNU Affero General Public License *
* along with this program in its root directory.                           *
* If not, see <http://www.gnu.org/licenses/>.                              *
\**************************************************************************/

package endercrypt.endercryptschallenge.modules.players.data;


import java.util.Collections;
import java.util.HashMap;
import java.util.Map;


/**
 * @author EnderCrypt
 */
public class PlayerData
{
	private final Map<String, Object> values;
	
	public PlayerData()
	{
		this(new HashMap<>());
	}
	
	public PlayerData(Map<String, Object> values)
	{
		this.values = new HashMap<>(values);
	}
	
	public <E> PlayerDataValue<E> resolve(PlayerDataKey<E> key)
	{
		return new PlayerDataValue<>(key, values);
	}
	
	public Map<String, Object> getValues()
	{
		return Collections.unmodifiableMap(values);
	}
}
