/**************************************************************************\
* EnderCrypt's Challenge                                                   *
* Copyright (C) 2023 by EnderCrypt                                         *
*                                                                          *
* This program is free software: you can redistribute it and/or modify     *
* it under the terms of the GNU Affero General Public License as           *
* published by the Free Software Foundation, either version 3 of           *
* the License, or (at your option) any later version.                      *
*                                                                          *
* This program is distributed in the hope that it will be useful,          *
* but WITHOUT ANY WARRANTY; without even the implied warranty of           *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
* GNU Affero General Public License for more details.                      *
*                                                                          *
* You should have received a copy of the GNU Affero General Public License *
* along with this program in its root directory.                           *
* If not, see <http://www.gnu.org/licenses/>.                              *
\**************************************************************************/

package endercrypt.endercryptschallenge.modules.players;


import endercrypt.endercryptschallenge.core.Core;
import endercrypt.endercryptschallenge.core.CoreCollective;
import endercrypt.endercryptschallenge.modules.features.OnSecond;
import endercrypt.endercryptschallenge.modules.features.OnTick;
import endercrypt.endercryptschallenge.modules.players.data.PlayerData;
import endercrypt.endercryptschallenge.modules.players.values.acg.AntiCheesingGodPlayerValue;
import endercrypt.endercryptschallenge.modules.players.values.stamina.StaminaPlayerValue;
import endercrypt.endercryptschallenge.modules.players.values.threat.ThreatPlayerValue;

import java.util.Map;
import java.util.Objects;
import java.util.function.Consumer;

import org.bukkit.entity.Player;

import net.kyori.adventure.text.serializer.plain.PlainTextComponentSerializer;


/**
 * @author EnderCrypt
 */
public class PlayerProfile extends CoreCollective implements OnSecond, OnTick
{
	private static PlayerValueFactory playerValueFactory = new PlayerValueFactory(PlayerValue.class.getPackageName());
	
	private final Player player;
	private final PlayerData data;
	private final Map<Class<? extends PlayerValue>, PlayerValue> values;
	
	public PlayerProfile(Core core, Player player)
	{
		this(core, player, new PlayerData());
	}
	
	protected PlayerProfile(Core core, Player player, PlayerData data)
	{
		super(core);
		this.player = Objects.requireNonNull(player, "player");
		this.data = Objects.requireNonNull(data, "data");
		this.values = playerValueFactory.build(this);
	}
	
	// VALUES //
	
	public AntiCheesingGodPlayerValue acg()
	{
		return get(AntiCheesingGodPlayerValue.class);
	}
	
	public StaminaPlayerValue stamina()
	{
		return get(StaminaPlayerValue.class);
	}
	
	public ThreatPlayerValue threat()
	{
		return get(ThreatPlayerValue.class);
	}
	
	// GETTERS //
	
	public boolean isValid()
	{
		return getPlayer().isOnline();
	}
	
	public Player getPlayer()
	{
		return player;
	}
	
	public PlayerData getData()
	{
		return data;
	}
	
	// FUNCTIONS //
	
	private void forEachValue(Consumer<PlayerValue> consumer)
	{
		values.values().forEach(consumer);
	}
	
	@SuppressWarnings("unchecked")
	public <T extends PlayerValue> T get(Class<T> valueType)
	{
		T value = (T) values.get(valueType);
		if (value == null)
		{
			throw new PlayerException("unknown player value: " + valueType);
		}
		return value;
	}
	
	public void write()
	{
		PlayerSavePath path = new PlayerSavePath(core, player);
		path.write(this);
	}
	
	// HOOKS //
	
	@Override
	public void onSecond()
	{
		forEachValue(PlayerValue::onSecond);
	}
	
	@Override
	public void onTick()
	{
		forEachValue(PlayerValue::onTick);
	}
	
	public void onDestroy()
	{
		forEachValue(PlayerValue::onDestroy);
		write();
	}
	
	// OVERRIDES //
	
	@Override
	public String toString()
	{
		return PlayerProfile.class.getSimpleName() + " [user=" + PlainTextComponentSerializer.plainText().serialize(player.displayName()) + "]";
	}
}
