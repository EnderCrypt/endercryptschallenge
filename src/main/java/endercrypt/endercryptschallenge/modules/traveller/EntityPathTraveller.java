/**************************************************************************\
* EnderCrypt's Challenge                                                   *
* Copyright (C) 2023 by EnderCrypt                                         *
*                                                                          *
* This program is free software: you can redistribute it and/or modify     *
* it under the terms of the GNU Affero General Public License as           *
* published by the Free Software Foundation, either version 3 of           *
* the License, or (at your option) any later version.                      *
*                                                                          *
* This program is distributed in the hope that it will be useful,          *
* but WITHOUT ANY WARRANTY; without even the implied warranty of           *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
* GNU Affero General Public License for more details.                      *
*                                                                          *
* You should have received a copy of the GNU Affero General Public License *
* along with this program in its root directory.                           *
* If not, see <http://www.gnu.org/licenses/>.                              *
\**************************************************************************/

package endercrypt.endercryptschallenge.modules.traveller;


import endercrypt.endercryptschallenge.modules.features.OnTick;
import endercrypt.endercryptschallenge.modules.pathfinder.PathfinderPath;
import endercrypt.endercryptschallenge.utility.Utility;

import java.util.function.DoubleConsumer;
import java.util.function.DoubleSupplier;

import org.bukkit.Location;
import org.bukkit.block.Block;
import org.bukkit.entity.LivingEntity;
import org.bukkit.util.Vector;

import lombok.Getter;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;


/**
 * @author EnderCrypt
 */
@Getter
@RequiredArgsConstructor
@NonNull
public class EntityPathTraveller implements OnTick
{
	private final LivingEntity entity;
	private final PathfinderPath path;
	
	private boolean cancelled = false;
	private int index = 0;
	private int time = 0;
	
	public void cancel()
	{
		cancelled = true;
	}
	
	private Location getEntityLocation()
	{
		return entity.getLocation()
			.add(0, entity.getHeight() / 2, 0);
	}
	
	@Override
	public void onTick()
	{
		Block targetBlock = path.getCoordinates().get(index);
		Location target = Utility.getBlockCenterLocation(targetBlock);
		Vector offset = target.toVector().subtract(getEntityLocation().toVector());
		Vector speed = getSpeedVector(offset);
		entity.setVelocity(entity.getVelocity().add(speed));
		time++;
		if (getEntityLocation().distance(target) < 0.4)
		{
			index++;
			time = 0;
		}
	}
	
	private static Vector getSpeedVector(Vector offset)
	{
		Vector speed = offset.clone();
		
		stabilizeVectorSpeed(speed::getX, speed::setX, 0.1);
		stabilizeVectorSpeed(speed::getY, speed::setY, 0.15);
		stabilizeVectorSpeed(speed::getZ, speed::setZ, 0.1);
		
		return speed;
	}
	
	private static void stabilizeVectorSpeed(DoubleSupplier getter, DoubleConsumer setter, double speed)
	{
		double value = getter.getAsDouble();
		value = Math.signum(value) * speed;
		setter.accept(value);
	}
	
	public boolean isValid()
	{
		return isCancelled() == false &&
			entity.isValid() &&
			path != null &&
			index < path.getCoordinates().size() &&
			time < 50;
	}
}
