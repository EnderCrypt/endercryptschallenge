/**************************************************************************\
* EnderCrypt's Challenge                                                   *
* Copyright (C) 2023 by EnderCrypt                                         *
*                                                                          *
* This program is free software: you can redistribute it and/or modify     *
* it under the terms of the GNU Affero General Public License as           *
* published by the Free Software Foundation, either version 3 of           *
* the License, or (at your option) any later version.                      *
*                                                                          *
* This program is distributed in the hope that it will be useful,          *
* but WITHOUT ANY WARRANTY; without even the implied warranty of           *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
* GNU Affero General Public License for more details.                      *
*                                                                          *
* You should have received a copy of the GNU Affero General Public License *
* along with this program in its root directory.                           *
* If not, see <http://www.gnu.org/licenses/>.                              *
\**************************************************************************/

package endercrypt.endercryptschallenge.modules.traveller;


import endercrypt.endercryptschallenge.modules.features.OnTick;
import endercrypt.endercryptschallenge.modules.pathfinder.PathfinderPath;
import endercrypt.endercryptschallenge.modules.pathfinder.PathfinderRequest;
import endercrypt.endercryptschallenge.modules.pathfinder.implementation.Pathfinder;
import endercrypt.endercryptschallenge.modules.pathfinder.listener.PathfinderListenerAdapter;
import endercrypt.endercryptschallenge.modules.pathfinder.state.PathfinderState;

import org.bukkit.Location;
import org.bukkit.entity.LivingEntity;
import org.bukkit.util.Vector;


/**
 * @author EnderCrypt
 */
public class EntityPathfindQueuer extends PathfinderListenerAdapter implements OnTick
{
	private final TravellerManager pathfinderTravellerManager;
	private final LivingEntity entity;
	private final PathfinderRequest request;
	private final Location location;
	private boolean alive = true;
	
	public EntityPathfindQueuer(TravellerManager pathfinderTravellerManager, LivingEntity entity, PathfinderRequest request)
	{
		this.pathfinderTravellerManager = pathfinderTravellerManager;
		this.entity = entity;
		this.request = request;
		this.location = entity.getLocation().clone();
	}
	
	@Override
	public void onTick()
	{
		Vector velocity = location.toVector().subtract(entity.getLocation().toVector()).multiply(0.2);
		entity.setVelocity(entity.getVelocity().add(velocity));
	}
	
	public void cancel()
	{
		if (alive)
		{
			alive = false;
			request.getPathfinder().submitFailure(PathfinderState.CANCELLED);
		}
	}
	
	@Override
	public void onChange(Pathfinder pathfinder)
	{
		super.onChange(pathfinder);
		
		pathfinderTravellerManager.queuers.remove(entity);
	}
	
	@Override
	protected void onSuccess(PathfinderPath path)
	{
		if (alive)
		{
			pathfinderTravellerManager.submit(entity, path);
		}
	}
}
