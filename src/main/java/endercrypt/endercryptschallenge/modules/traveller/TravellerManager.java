/**************************************************************************\
* EnderCrypt's Challenge                                                   *
* Copyright (C) 2023 by EnderCrypt                                         *
*                                                                          *
* This program is free software: you can redistribute it and/or modify     *
* it under the terms of the GNU Affero General Public License as           *
* published by the Free Software Foundation, either version 3 of           *
* the License, or (at your option) any later version.                      *
*                                                                          *
* This program is distributed in the hope that it will be useful,          *
* but WITHOUT ANY WARRANTY; without even the implied warranty of           *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
* GNU Affero General Public License for more details.                      *
*                                                                          *
* You should have received a copy of the GNU Affero General Public License *
* along with this program in its root directory.                           *
* If not, see <http://www.gnu.org/licenses/>.                              *
\**************************************************************************/

package endercrypt.endercryptschallenge.modules.traveller;


import endercrypt.endercryptschallenge.core.Core;
import endercrypt.endercryptschallenge.core.modules.CoreModule;
import endercrypt.endercryptschallenge.modules.features.OnTick;
import endercrypt.endercryptschallenge.modules.pathfinder.PathfinderConfiguration;
import endercrypt.endercryptschallenge.modules.pathfinder.PathfinderPath;
import endercrypt.endercryptschallenge.modules.pathfinder.PathfinderRequest;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.bukkit.block.Block;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;


/**
 * @author EnderCrypt
 */
public class TravellerManager extends CoreModule implements OnTick
{
	protected final Map<Entity, EntityPathfindQueuer> queuers = new HashMap<>();
	protected final Map<Entity, EntityPathTraveller> travellers = new HashMap<>();
	
	public TravellerManager(Core core)
	{
		super(core);
	}
	
	public boolean cancelPathFor(Entity entity)
	{
		EntityPathfindQueuer queuer = queuers.get(entity);
		if (queuer != null)
		{
			queuer.cancel();
			return true;
		}
		
		EntityPathTraveller traveller = travellers.get(entity);
		if (traveller != null)
		{
			traveller.cancel();
			return true;
		}
		
		return false;
	}
	
	private void checkValidStartingLocation(LivingEntity entity, PathfinderConfiguration configuration)
	{
		Block entityBlock = entity.getLocation().getBlock();
		Block startingBlock = configuration.getStart();
		if (entityBlock.equals(startingBlock) == false)
		{
			throw new IllegalArgumentException("refusing to submit a travel request for an entity " + entity + ", where the pathfinding starts from a different block than where the entity is (" + entityBlock + " vs " + startingBlock + ")");
		}
	}
	
	public PathfinderRequest submit(LivingEntity entity, PathfinderConfiguration configuration)
	{
		checkValidStartingLocation(entity, configuration);
		cancelPathFor(entity);
		PathfinderRequest request = core.getPathfinderManager().submit(configuration);
		submit(entity, request);
		return request;
	}
	
	public void submit(LivingEntity entity, PathfinderRequest request)
	{
		checkValidStartingLocation(entity, request.getPathfinder().getConfiguration());
		cancelPathFor(entity);
		EntityPathfindQueuer queuer = new EntityPathfindQueuer(this, entity, request);
		queuers.put(entity, queuer);
		request.getPathfinder().addListener(queuer);
	}
	
	protected void submit(LivingEntity entity, PathfinderPath path)
	{
		EntityPathTraveller traveller = new EntityPathTraveller(entity, path);
		travellers.put(traveller.getEntity(), traveller);
	}
	
	@Override
	public void onTick()
	{
		// Listeners
		queuers.values().forEach(EntityPathfindQueuer::onTick);
		
		// Travellers
		Iterator<EntityPathTraveller> iterator = travellers.values().iterator();
		while (iterator.hasNext())
		{
			EntityPathTraveller traveller = iterator.next();
			traveller.onTick();
			if (traveller.isValid() == false)
			{
				queuers.remove(traveller.getEntity());
				iterator.remove();
			}
		}
	}
}
