/**************************************************************************\
* EnderCrypt's Challenge                                                   *
* Copyright (C) 2023 by EnderCrypt                                         *
*                                                                          *
* This program is free software: you can redistribute it and/or modify     *
* it under the terms of the GNU Affero General Public License as           *
* published by the Free Software Foundation, either version 3 of           *
* the License, or (at your option) any later version.                      *
*                                                                          *
* This program is distributed in the hope that it will be useful,          *
* but WITHOUT ANY WARRANTY; without even the implied warranty of           *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
* GNU Affero General Public License for more details.                      *
*                                                                          *
* You should have received a copy of the GNU Affero General Public License *
* along with this program in its root directory.                           *
* If not, see <http://www.gnu.org/licenses/>.                              *
\**************************************************************************/

package endercrypt.endercryptschallenge.modules.settings;


import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.FileConfiguration;

import lombok.AccessLevel;
import lombok.Getter;


/**
 * @author EnderCrypt
 */
@Getter(AccessLevel.PROTECTED)
public abstract class PluginConfigurationCategory
{
	private final FileConfiguration config;
	private final String sectionName;
	private final ConfigurationSection section;
	
	public PluginConfigurationCategory(FileConfiguration config, String sectionName)
	{
		this.config = config;
		this.sectionName = sectionName;
		if (config.isConfigurationSection(getSectionName()) == false)
		{
			config.createSection(getSectionName());
		}
		this.section = config.getConfigurationSection(getSectionName());
		if (section == null)
		{
			throw new IllegalArgumentException("Missing configuration section: " + getSectionName());
		}
	}
	
	protected <T> PluginConfiguration<T> create(ConfigurationType<T> type, String key, T defaultValue)
	{
		getSection().addDefault(key, defaultValue);
		return create(type, key);
	}
	
	private <T> PluginConfiguration<T> create(ConfigurationType<T> type, String key)
	{
		return new PluginConfiguration<>(this, type, key);
	}
}
