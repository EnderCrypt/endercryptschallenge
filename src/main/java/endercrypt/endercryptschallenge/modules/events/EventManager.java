/**************************************************************************\
* EnderCrypt's Challenge                                                   *
* Copyright (C) 2023 by EnderCrypt                                         *
*                                                                          *
* This program is free software: you can redistribute it and/or modify     *
* it under the terms of the GNU Affero General Public License as           *
* published by the Free Software Foundation, either version 3 of           *
* the License, or (at your option) any later version.                      *
*                                                                          *
* This program is distributed in the hope that it will be useful,          *
* but WITHOUT ANY WARRANTY; without even the implied warranty of           *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
* GNU Affero General Public License for more details.                      *
*                                                                          *
* You should have received a copy of the GNU Affero General Public License *
* along with this program in its root directory.                           *
* If not, see <http://www.gnu.org/licenses/>.                              *
\**************************************************************************/

package endercrypt.endercryptschallenge.modules.events;


import endercrypt.endercryptschallenge.core.Core;
import endercrypt.endercryptschallenge.core.modules.CoreModule;
import endercrypt.endercryptschallenge.core.modules.CoreModuleHolder;
import endercrypt.endercryptschallenge.core.modules.CoreModuleState;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.util.HashMap;
import java.util.Map;


/**
 * @author EnderCrypt
 */
public class EventManager extends CoreModule
{
	private InvocationHandler eventInvocationHandler = new EventInvocationHandler();
	private Map<Class<? extends Event>, Event> eventClasses = new HashMap<>();
	
	public EventManager(Core core)
	{
		super(core);
	}
	
	@SuppressWarnings("unchecked")
	private <T extends Event> T generateProxy(Class<T> eventClass)
	{
		return (T) Proxy.newProxyInstance(getClass().getClassLoader(), new Class[] { eventClass }, eventInvocationHandler);
	}
	
	@SuppressWarnings("unchecked")
	public <T extends Event> T trigger(Class<T> eventClass)
	{
		return (T) eventClasses.computeIfAbsent(eventClass, this::generateProxy);
	}
	
	private class EventInvocationHandler implements InvocationHandler
	{
		@SuppressWarnings("unchecked")
		@Override
		public Object invoke(Object proxy, Method method, Object[] args) throws Throwable
		{
			Class<? extends Event> eventClass = (Class<? extends Event>) method.getDeclaringClass();
			for (CoreModuleHolder moduleHolder : core.getModuleManager().getModules())
			{
				if (moduleHolder.getState() != CoreModuleState.ONLINE)
				{
					throw new IllegalStateException("module " + moduleHolder.getName() + " is not available");
				}
				CoreModule module = moduleHolder.getModule();
				if (eventClass.isInstance(module))
				{
					try
					{
						method.invoke(module, args);
					}
					catch (InvocationTargetException e)
					{
						e.getCause().printStackTrace();
					}
				}
			}
			return null;
		}
	}
}
