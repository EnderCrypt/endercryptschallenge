/**************************************************************************\
* EnderCrypt's Challenge                                                   *
* Copyright (C) 2023 by EnderCrypt                                         *
*                                                                          *
* This program is free software: you can redistribute it and/or modify     *
* it under the terms of the GNU Affero General Public License as           *
* published by the Free Software Foundation, either version 3 of           *
* the License, or (at your option) any later version.                      *
*                                                                          *
* This program is distributed in the hope that it will be useful,          *
* but WITHOUT ANY WARRANTY; without even the implied warranty of           *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
* GNU Affero General Public License for more details.                      *
*                                                                          *
* You should have received a copy of the GNU Affero General Public License *
* along with this program in its root directory.                           *
* If not, see <http://www.gnu.org/licenses/>.                              *
\**************************************************************************/

package endercrypt.endercryptschallenge.modules.features.debug;


import endercrypt.endercryptschallenge.modules.features.MinecraftFeature;

import java.util.Objects;


/**
 * @author EnderCrypt
 */
public class FeatureDebugController
{
	private final MinecraftFeature feature;
	private Mode mode = Mode.UNSUPPORTED;
	
	public FeatureDebugController(MinecraftFeature feature)
	{
		this.feature = Objects.requireNonNull(feature, "feature");
	}
	
	public boolean isSupported()
	{
		return mode != Mode.UNSUPPORTED;
	}
	
	protected void requireSupported()
	{
		if (isSupported() == false)
		{
			throw new IllegalArgumentException("debug not supported");
		}
	}
	
	public boolean isEnabled()
	{
		return mode == Mode.ENABLED;
	}
	
	public boolean toggle()
	{
		boolean value = isEnabled() == false;
		set(value);
		return value;
	}
	
	public void set(boolean value)
	{
		requireSupported();
		mode = value ? Mode.ENABLED : Mode.DISABLED;
		feature.getLogger().info(feature.getName() + " debug: " + value);
	}
	
	public void enableDebugSupport()
	{
		if (isSupported())
		{
			throw new IllegalArgumentException("debug has already been enabled");
		}
		mode = Mode.DISABLED;
		feature.getLogger().finer(feature.getName() + " supports debugging");
	}
	
	private enum Mode
	{
		UNSUPPORTED,
		DISABLED,
		ENABLED;
	}
}
