/**************************************************************************\
* EnderCrypt's Challenge                                                   *
* Copyright (C) 2023 by EnderCrypt                                         *
*                                                                          *
* This program is free software: you can redistribute it and/or modify     *
* it under the terms of the GNU Affero General Public License as           *
* published by the Free Software Foundation, either version 3 of           *
* the License, or (at your option) any later version.                      *
*                                                                          *
* This program is distributed in the hope that it will be useful,          *
* but WITHOUT ANY WARRANTY; without even the implied warranty of           *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
* GNU Affero General Public License for more details.                      *
*                                                                          *
* You should have received a copy of the GNU Affero General Public License *
* along with this program in its root directory.                           *
* If not, see <http://www.gnu.org/licenses/>.                              *
\**************************************************************************/

package endercrypt.endercryptschallenge.modules.features;


import endercrypt.endercryptschallenge.Plugin;
import endercrypt.endercryptschallenge.core.Core;
import endercrypt.endercryptschallenge.core.modules.CoreModule;
import endercrypt.endercryptschallenge.features.stamina.cake.CakeStamina;
import endercrypt.endercryptschallenge.modules.features.secondcycler.SecondCycler;
import endercrypt.endercryptschallenge.modules.features.ticker.MinecraftFeatureTicker;
import endercrypt.endercryptschallenge.modules.features.ticker.NamedTickFunction;
import endercrypt.endercryptschallenge.utility.CodeTimer;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.bukkit.plugin.PluginManager;
import org.reflections.Reflections;


/**
 * @author EnderCrypt
 */
public class MinecraftFeatureManager extends CoreModule implements OnTick
{
	private static final Path tickReportPath = Plugin.pluginDirectory.resolve("./endercrypt-tick-report.txt");
	private static final Path secondReportPath = Plugin.pluginDirectory.resolve("./endercrypt-second-report.txt");
	
	private final PluginManager pluginManager = server.getPluginManager();
	private final SecondCycler secondCycler = new SecondCycler(this);
	private List<MinecraftFeature> features = new ArrayList<>();
	
	public MinecraftFeatureManager(Core core)
	{
		super(core);
	}
	
	@Override
	protected void onStart() throws Exception
	{
		logger.fine("Scanning for features ...");
		Reflections reflections = new Reflections("endercrypt.endercryptschallenge.features");
		Set<Class<? extends MinecraftFeature>> minecraftEventListenerClasses = reflections.getSubTypesOf(MinecraftFeature.class);
		
		// HACK: cakestamina not loaded for unknown reasons, manually add in
		minecraftEventListenerClasses.add(CakeStamina.class);
		
		if (minecraftEventListenerClasses.isEmpty())
		{
			logger.warning("Found no features!");
		}
		
		logger.fine("Registering features ...");
		int featureCount = 0;
		for (Class<? extends MinecraftFeature> minecraftFeatureClass : minecraftEventListenerClasses)
		{
			if (register(minecraftFeatureClass))
			{
				featureCount++;
			}
		}
		
		logger.fine("Initializing features ...");
		for (MinecraftFeature feature : features)
		{
			feature.onReady();
		}
		
		logger.info("Installed " + featureCount + " features!");
		
	}
	
	private boolean register(Class<? extends MinecraftFeature> minecraftFeatureClass)
	{
		if (minecraftFeatureClass.isAnnotationPresent(DisabledFeature.class))
		{
			return false;
		}
		try
		{
			Constructor<? extends MinecraftFeature> constructor = minecraftFeatureClass.getConstructor(Core.class);
			MinecraftFeature minecraftEventListener = constructor.newInstance(core);
			register(minecraftEventListener);
			logger.fine("Installed feature: " + minecraftEventListener.getName());
			return true;
		}
		catch (NoSuchMethodException | SecurityException | InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException e)
		{
			throw new MinecraftFeatureException(minecraftFeatureClass, e);
		}
	}
	
	public void register(MinecraftFeature minecraftFeature)
	{
		if (features.contains(minecraftFeature))
		{
			throw new IllegalArgumentException("plugin feature " + minecraftFeature.getClass().getName() + " has already been registered");
		}
		features.add(minecraftFeature);
		pluginManager.registerEvents(minecraftFeature, core.getPlugin());
	}
	
	public List<MinecraftFeature> getFeatures()
	{
		return List.copyOf(features);
	}
	
	public <T extends MinecraftFeature> T getFeature(Class<T> feature)
	{
		return features.stream()
			.filter(feature::isInstance)
			.map(feature::cast)
			.findFirst()
			.orElse(null);
	}
	
	@Override
	public void onTick()
	{
		CodeTimer featuresOnTickTimer = new CodeTimer("features.onTick", logger);
		MinecraftFeatureTicker tickTicker = new MinecraftFeatureTicker(NamedTickFunction.onTick);
		tickTicker.tick(getFeatures());
		tickTicker.generateReportAndSaveIntoFileIfExists(tickReportPath);
		featuresOnTickTimer.check(25);
		
		CodeTimer featuresOnSecondTimer = new CodeTimer("features.onSecond", logger);
		MinecraftFeatureTicker secondTicker = new MinecraftFeatureTicker(NamedTickFunction.onSecond);
		secondCycler.update(secondTicker);
		secondTicker.generateReportAndSaveIntoFileIfExists(secondReportPath);
		featuresOnSecondTimer.check(35);
	}
	
	@Override
	protected void onShutdown() throws Exception
	{
		features.forEach(MinecraftFeature::onShutdown);
	}
}
