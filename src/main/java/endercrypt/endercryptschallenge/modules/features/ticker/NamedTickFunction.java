/**************************************************************************\
* EnderCrypt's Challenge                                                   *
* Copyright (C) 2023 by EnderCrypt                                         *
*                                                                          *
* This program is free software: you can redistribute it and/or modify     *
* it under the terms of the GNU Affero General Public License as           *
* published by the Free Software Foundation, either version 3 of           *
* the License, or (at your option) any later version.                      *
*                                                                          *
* This program is distributed in the hope that it will be useful,          *
* but WITHOUT ANY WARRANTY; without even the implied warranty of           *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
* GNU Affero General Public License for more details.                      *
*                                                                          *
* You should have received a copy of the GNU Affero General Public License *
* along with this program in its root directory.                           *
* If not, see <http://www.gnu.org/licenses/>.                              *
\**************************************************************************/

package endercrypt.endercryptschallenge.modules.features.ticker;


import endercrypt.endercryptschallenge.modules.features.MinecraftFeature;


/**
 * @author EnderCrypt
 */
public interface NamedTickFunction extends TickFunction
{
	public static final NamedTickFunction onTick = create(TickFunction.onTick, "onTick");
	
	public static final NamedTickFunction onSecond = create(TickFunction.onSecond, "onSecond");
	
	private static NamedTickFunction create(TickFunction tickFunction, String name)
	{
		return new NamedTickFunction()
		{
			@Override
			public void tick(MinecraftFeature feature)
			{
				tickFunction.tick(feature);
			}
			
			@Override
			public String getName()
			{
				return name;
			}
		};
	}
	
	public String getName();
}
