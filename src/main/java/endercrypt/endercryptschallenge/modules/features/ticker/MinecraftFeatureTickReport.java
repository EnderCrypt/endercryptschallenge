/**************************************************************************\
* EnderCrypt's Challenge                                                   *
* Copyright (C) 2023 by EnderCrypt                                         *
*                                                                          *
* This program is free software: you can redistribute it and/or modify     *
* it under the terms of the GNU Affero General Public License as           *
* published by the Free Software Foundation, either version 3 of           *
* the License, or (at your option) any later version.                      *
*                                                                          *
* This program is distributed in the hope that it will be useful,          *
* but WITHOUT ANY WARRANTY; without even the implied warranty of           *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
* GNU Affero General Public License for more details.                      *
*                                                                          *
* You should have received a copy of the GNU Affero General Public License *
* along with this program in its root directory.                           *
* If not, see <http://www.gnu.org/licenses/>.                              *
\**************************************************************************/

package endercrypt.endercryptschallenge.modules.features.ticker;


import endercrypt.endercryptschallenge.modules.features.MinecraftFeature;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


/**
 * @author EnderCrypt
 */
public class MinecraftFeatureTickReport implements Comparable<MinecraftFeatureTickReport>
{
	private static final Logger logger = LogManager.getLogger(MinecraftFeatureTicker.class);
	
	public static MinecraftFeatureTickReport perform(MinecraftFeature feature, NamedTickFunction tickFunction)
	{
		long time = System.nanoTime();
		
		try
		{
			tickFunction.tick(feature);
		}
		catch (Exception e)
		{
			logger.error("Failed to tick: " + feature.getName() + " (" + tickFunction.getName() + ")", e);
		}
		
		long delta = (System.nanoTime() - time);
		return new MinecraftFeatureTickReport(feature, delta);
	}
	
	private final MinecraftFeature feature;
	private final long delta;
	
	public MinecraftFeatureTickReport(MinecraftFeature feature, long delta)
	{
		this.feature = feature;
		this.delta = delta;
	}
	
	public MinecraftFeature getFeature()
	{
		return feature;
	}
	
	public long getDelta()
	{
		return delta;
	}
	
	@Override
	public int hashCode()
	{
		final int prime = 31;
		int result = 1;
		result = prime * result + ((feature == null) ? 0 : feature.hashCode());
		return result;
	}
	
	@Override
	public boolean equals(Object obj)
	{
		if (this == obj) return true;
		if (!(obj instanceof MinecraftFeatureTickReport)) return false;
		MinecraftFeatureTickReport other = (MinecraftFeatureTickReport) obj;
		if (feature == null)
		{
			if (other.feature != null) return false;
		}
		else if (!feature.equals(other.feature)) return false;
		return true;
	}
	
	@Override
	public int compareTo(MinecraftFeatureTickReport other)
	{
		return Long.compare(other.delta, delta);
	}
}
