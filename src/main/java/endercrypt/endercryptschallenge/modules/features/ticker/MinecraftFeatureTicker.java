/**************************************************************************\
* EnderCrypt's Challenge                                                   *
* Copyright (C) 2023 by EnderCrypt                                         *
*                                                                          *
* This program is free software: you can redistribute it and/or modify     *
* it under the terms of the GNU Affero General Public License as           *
* published by the Free Software Foundation, either version 3 of           *
* the License, or (at your option) any later version.                      *
*                                                                          *
* This program is distributed in the hope that it will be useful,          *
* but WITHOUT ANY WARRANTY; without even the implied warranty of           *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
* GNU Affero General Public License for more details.                      *
*                                                                          *
* You should have received a copy of the GNU Affero General Public License *
* along with this program in its root directory.                           *
* If not, see <http://www.gnu.org/licenses/>.                              *
\**************************************************************************/

package endercrypt.endercryptschallenge.modules.features.ticker;


import endercrypt.endercryptschallenge.modules.features.MinecraftFeature;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.bukkit.Bukkit;


/**
 * @author EnderCrypt
 */
public class MinecraftFeatureTicker
{
	private static final Logger logger = LogManager.getLogger(MinecraftFeatureTicker.class);
	
	private final NamedTickFunction tickFunction;
	private Set<MinecraftFeatureTickReport> reports = new HashSet<>();
	
	public MinecraftFeatureTicker(NamedTickFunction tickFunction)
	{
		this.tickFunction = tickFunction;
	}
	
	public void tick(List<MinecraftFeature> features)
	{
		features.forEach(this::tick);
	}
	
	public void tick(MinecraftFeature feature)
	{
		reports.add(MinecraftFeatureTickReport.perform(feature, tickFunction));
	}
	
	private List<MinecraftFeatureTickReport> getSortedReports()
	{
		return reports
			.stream()
			.sorted()
			.toList();
	}
	
	public String generateReport()
	{
		StringBuilder builder = new StringBuilder();
		
		int tick = Bukkit.getCurrentTick();
		builder.append(tickFunction.getName()).append(" (tick: ").append(tick).append(")").append('\n');
		
		for (MinecraftFeatureTickReport report : getSortedReports())
		{
			String featureName = report.getFeature().getName();
			long featureDelta = report.getDelta();
			builder.append(featureName).append(" = ").append(featureDelta).append(" ns").append('\n');
		}
		
		builder.append("\n----------------------------\n");
		
		return builder.toString();
	}
	
	public void generateReportAndSaveIntoFileIfExists(Path path)
	{
		if (Files.exists(path) && Files.isRegularFile(path))
		{
			String report = generateReport();
			try
			{
				Files.write(path, report.getBytes(), StandardOpenOption.APPEND);
			}
			catch (IOException e)
			{
				logger.error("Failed to write report to " + path, e);
			}
		}
	}
}
