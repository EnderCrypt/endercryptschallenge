/**************************************************************************\
* EnderCrypt's Challenge                                                   *
* Copyright (C) 2023 by EnderCrypt                                         *
*                                                                          *
* This program is free software: you can redistribute it and/or modify     *
* it under the terms of the GNU Affero General Public License as           *
* published by the Free Software Foundation, either version 3 of           *
* the License, or (at your option) any later version.                      *
*                                                                          *
* This program is distributed in the hope that it will be useful,          *
* but WITHOUT ANY WARRANTY; without even the implied warranty of           *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
* GNU Affero General Public License for more details.                      *
*                                                                          *
* You should have received a copy of the GNU Affero General Public License *
* along with this program in its root directory.                           *
* If not, see <http://www.gnu.org/licenses/>.                              *
\**************************************************************************/

package endercrypt.endercryptschallenge.modules.features.secondcycler;

/**
 * @author EnderCrypt
 */
public class TickIndex
{
	private int index = 0;
	
	public boolean isAtStart()
	{
		return getIndex() == 0;
	}
	
	public int getIndex()
	{
		return index;
	}
	
	public int next()
	{
		index++;
		if (index >= SecondCycler.TICKS)
		{
			index = 0;
		}
		return getIndex();
	}
}
