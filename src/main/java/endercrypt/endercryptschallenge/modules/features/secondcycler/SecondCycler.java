/**************************************************************************\
* EnderCrypt's Challenge                                                   *
* Copyright (C) 2023 by EnderCrypt                                         *
*                                                                          *
* This program is free software: you can redistribute it and/or modify     *
* it under the terms of the GNU Affero General Public License as           *
* published by the Free Software Foundation, either version 3 of           *
* the License, or (at your option) any later version.                      *
*                                                                          *
* This program is distributed in the hope that it will be useful,          *
* but WITHOUT ANY WARRANTY; without even the implied warranty of           *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
* GNU Affero General Public License for more details.                      *
*                                                                          *
* You should have received a copy of the GNU Affero General Public License *
* along with this program in its root directory.                           *
* If not, see <http://www.gnu.org/licenses/>.                              *
\**************************************************************************/

package endercrypt.endercryptschallenge.modules.features.secondcycler;


import endercrypt.endercryptschallenge.modules.features.MinecraftFeature;
import endercrypt.endercryptschallenge.modules.features.MinecraftFeatureManager;
import endercrypt.endercryptschallenge.modules.features.ticker.MinecraftFeatureTicker;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;


/**
 * @author EnderCrypt
 */
public class SecondCycler
{
	public static final int TICKS = 20;
	private final MinecraftFeatureManager featureManager;
	
	private TickIndex index = new TickIndex();
	private TickQueue[] ticks = new TickQueue[TICKS];
	
	public SecondCycler(MinecraftFeatureManager featureManager)
	{
		this.featureManager = Objects.requireNonNull(featureManager, "featureManager");
		for (int i = 0; i < ticks.length; i++)
		{
			ticks[i] = new TickQueue();
		}
	}
	
	public void update(MinecraftFeatureTicker minecraftFeatureTicker)
	{
		if (index.isAtStart())
		{
			fill();
		}
		
		execute(minecraftFeatureTicker);
		
		index.next();
	}
	
	private void fill()
	{
		TickIndex focus = new TickIndex();
		for (MinecraftFeature feature : featureManager.getFeatures())
		{
			ticks[focus.getIndex()].push(feature);
			focus.next();
		}
	}
	
	private void execute(MinecraftFeatureTicker minecraftFeatureTicker)
	{
		ticks[index.getIndex()].execute(minecraftFeatureTicker);
	}
	
	private class TickQueue
	{
		private final List<MinecraftFeature> features = new ArrayList<>();
		
		public void execute(MinecraftFeatureTicker minecraftFeatureTicker)
		{
			minecraftFeatureTicker.tick(features);
			features.clear();
		}
		
		public void push(MinecraftFeature feature)
		{
			features.add(feature);
		}
		
		@Override
		public String toString()
		{
			return features.toString();
		}
	}
}
