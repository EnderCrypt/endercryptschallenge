/**************************************************************************\
* EnderCrypt's Challenge                                                   *
* Copyright (C) 2023 by EnderCrypt                                         *
*                                                                          *
* This program is free software: you can redistribute it and/or modify     *
* it under the terms of the GNU Affero General Public License as           *
* published by the Free Software Foundation, either version 3 of           *
* the License, or (at your option) any later version.                      *
*                                                                          *
* This program is distributed in the hope that it will be useful,          *
* but WITHOUT ANY WARRANTY; without even the implied warranty of           *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
* GNU Affero General Public License for more details.                      *
*                                                                          *
* You should have received a copy of the GNU Affero General Public License *
* along with this program in its root directory.                           *
* If not, see <http://www.gnu.org/licenses/>.                              *
\**************************************************************************/

package endercrypt.endercryptschallenge.modules.features;


import endercrypt.endercryptschallenge.core.Core;
import endercrypt.endercryptschallenge.core.GlobalCoreCollective;
import endercrypt.endercryptschallenge.modules.features.debug.FeatureDebugController;
import endercrypt.library.commons.interfaces.Code;
import endercrypt.library.commons.misc.Ender;

import java.util.Collection;
import java.util.Objects;
import java.util.function.Consumer;
import java.util.logging.Level;

import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;
import org.bukkit.inventory.ItemStack;
import org.bukkit.scheduler.BukkitRunnable;


/**
 * @author EnderCrypt
 */
public abstract class MinecraftFeature extends GlobalCoreCollective implements Listener
{
	private FeatureDebugController debugController = new FeatureDebugController(this);
	
	public MinecraftFeature(Core core)
	{
		super(core);
	}
	
	public void onReady()
	{
		// override me
	}
	
	public FeatureDebugController getDebug()
	{
		return debugController;
	}
	
	public boolean isDebug()
	{
		return getDebug().isEnabled();
	}
	
	private final Consumer<String> debugAll = (text) -> {
		logger.info(text);
		announce(text);
	};
	
	public void debug(Object message)
	{
		debug(message, debugAll);
	}
	
	public interface PlayerDebug
	{
		public Object log(Player player);
	}
	
	public void debug(PlayerDebug playerDebug)
	{
		if (isDebug())
		{
			for (World world : server.getWorlds())
			{
				for (Player player : world.getPlayers())
				{
					debug(player, playerDebug.log(player));
				}
			}
		}
	}
	
	public void debug(Player player, Object message)
	{
		if (player.isOnline())
		{
			debug(message, player::sendMessage);
		}
	}
	
	public void debug(World world, Object message)
	{
		debug(message, (m) -> world.getPlayers().forEach(p -> p.sendMessage(m)));
	}
	
	public void debug(Object message, Consumer<String> messenger)
	{
		if (isDebug())
		{
			messenger.accept("[debug: " + getName() + "] " + Objects.toString(message));
		}
	}
	
	protected <T extends MinecraftFeature> T getFeature(Class<T> featureClass)
	{
		return core.getMinecraftFeatureManager().getFeature(featureClass);
	}
	
	private BukkitRunnable createRunnable(Code code)
	{
		return new BukkitRunnable()
		{
			@Override
			public void run()
			{
				try
				{
					code.run();
				}
				catch (Exception e)
				{
					logger.log(Level.WARNING, "task failed", e);
				}
			}
		};
	}
	
	protected void runOnBukkit(Code code)
	{
		createRunnable(code).runTask(plugin);
	}
	
	protected BukkitRunnable schedule(int delay, Code code)
	{
		BukkitRunnable runnable = createRunnable(code);
		runnable.runTaskLater(plugin, delay);
		return runnable;
	}
	
	protected void announce(String message)
	{
		for (World world : server.getWorlds())
		{
			for (Player player : world.getPlayers())
			{
				player.sendMessage(message);
			}
		}
	}
	
	public String getName()
	{
		return Ender.clazz.getHumanReadableClassName(getClass());
	}
	
	public final void cycleEntities(Collection<Entity> entities)
	{
		entities.forEach(this::onCycleEntity);
	}
	
	@SuppressWarnings("unused")
	public void onCycleEntity(Entity entity)
	{
		// do nothing
	}
	
	@SuppressWarnings("unused")
	public void onCycleInventoryItem(Player player, int index, ItemStack item)
	{
		// do nothing
	}
	
	public void onCycleBlock(@SuppressWarnings("unused") Block block)
	{
		// do nothing
	}
	
	public void onTick()
	{
		// do nothing
	}
	
	public void onSecond()
	{
		// do nothing
	}
	
	public void onShutdown()
	{
		// do nothing
	}
}
