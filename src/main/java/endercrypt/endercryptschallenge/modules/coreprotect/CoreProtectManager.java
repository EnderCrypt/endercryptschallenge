/**************************************************************************\
* EnderCrypt's Challenge                                                   *
* Copyright (C) 2023 by EnderCrypt                                         *
*                                                                          *
* This program is free software: you can redistribute it and/or modify     *
* it under the terms of the GNU Affero General Public License as           *
* published by the Free Software Foundation, either version 3 of           *
* the License, or (at your option) any later version.                      *
*                                                                          *
* This program is distributed in the hope that it will be useful,          *
* but WITHOUT ANY WARRANTY; without even the implied warranty of           *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
* GNU Affero General Public License for more details.                      *
*                                                                          *
* You should have received a copy of the GNU Affero General Public License *
* along with this program in its root directory.                           *
* If not, see <http://www.gnu.org/licenses/>.                              *
\**************************************************************************/

package endercrypt.endercryptschallenge.modules.coreprotect;


import endercrypt.endercryptschallenge.core.Core;
import endercrypt.endercryptschallenge.core.modules.CoreModule;
import endercrypt.endercryptschallenge.utility.Utility;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.data.BlockData;

import net.coreprotect.CoreProtect;
import net.coreprotect.CoreProtectAPI;


/**
 * @author EnderCrypt
 */
public class CoreProtectManager extends CoreModule
{
	private final CoreProtect coreProtect;
	
	public CoreProtectManager(Core core)
	{
		super(core);
		coreProtect = (CoreProtect) core.getExternalPlugin("CoreProtect").orElse(null);
		
		if (isAvailable())
		{
			logger.info("Integrating with CoreProtect (version: " + getApi().APIVersion() + ")");
		}
	}
	
	public boolean isAvailable()
	{
		return coreProtect != null;
	}
	
	private CoreProtectAPI getApi()
	{
		return coreProtect.getAPI();
	}
	
	private void logBlock(String user, Block block, BlockLogger logger)
	{
		Location location = Utility.getBlockFloorLocation(block);
		Material material = block.getType();
		BlockData blockData = block.getBlockData();
		logger.log(user, location, material, blockData);
	}
	
	private interface BlockLogger
	{
		public void log(String user, Location location, Material material, BlockData blockData);
	}
	
	public void logRemoval(String user, Block block)
	{
		if (isAvailable())
		{
			logBlock(user, block, getApi()::logRemoval);
		}
	}
	
	public void logPlace(String user, Block block)
	{
		if (isAvailable())
		{
			logBlock(user, block, getApi()::logPlacement);
		}
	}
}
