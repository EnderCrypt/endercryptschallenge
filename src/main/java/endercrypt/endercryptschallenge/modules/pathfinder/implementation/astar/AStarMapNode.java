/**************************************************************************\
* EnderCrypt's Challenge                                                   *
* Copyright (C) 2023 by EnderCrypt                                         *
*                                                                          *
* This program is free software: you can redistribute it and/or modify     *
* it under the terms of the GNU Affero General Public License as           *
* published by the Free Software Foundation, either version 3 of           *
* the License, or (at your option) any later version.                      *
*                                                                          *
* This program is distributed in the hope that it will be useful,          *
* but WITHOUT ANY WARRANTY; without even the implied warranty of           *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
* GNU Affero General Public License for more details.                      *
*                                                                          *
* You should have received a copy of the GNU Affero General Public License *
* along with this program in its root directory.                           *
* If not, see <http://www.gnu.org/licenses/>.                              *
\**************************************************************************/

package endercrypt.endercryptschallenge.modules.pathfinder.implementation.astar;


import endercrypt.endercryptschallenge.modules.pathfinder.PathfinderConfiguration;
import endercrypt.endercryptschallenge.modules.pathfinder.implementation.MapNode;
import endercrypt.endercryptschallenge.utility.Utility;

import org.bukkit.block.Block;


public class AStarMapNode extends MapNode
{
	private final AStarMapNode previous;
	
	private final double travelDistance;
	private final double distanceToEnd;
	private final double cost;
	
	public AStarMapNode(PathfinderConfiguration configuration, Block block)
	{
		this(configuration, block, null);
	}
	
	public AStarMapNode(Block block, AStarMapNode previous)
	{
		this(previous.getConfiguration(), block, previous);
	}
	
	private AStarMapNode(PathfinderConfiguration configuration, Block block, AStarMapNode previous)
	{
		super(configuration, block);
		this.previous = previous;
		
		this.travelDistance = previous == null ? 0 : previous.calculateTravelDistance(getBlock());
		this.distanceToEnd = Utility.distance(getBlock(), getConfiguration().getEnd());
		
		this.cost = getTravelDistance() + getDistanceToEnd();
	}
	
	public AStarMapNode getPrevious()
	{
		return previous;
	}
	
	private double calculateTravelDistance(Block target)
	{
		return getTravelDistance() + Utility.distance(getBlock(), target);
	}
	
	public double getTravelDistance() // G cost
	{
		return travelDistance;
	}
	
	public double getDistanceToEnd() // H cost
	{
		return distanceToEnd;
	}
	
	public double getCost() // F cost (G + H)
	{
		return cost;
	}
	
	@Override
	public String toString()
	{
		return "MapNode [" + getBlock().getX() + ", " + getBlock().getY() + "]";
	}
}
