/**************************************************************************\
* EnderCrypt's Challenge                                                   *
* Copyright (C) 2023 by EnderCrypt                                         *
*                                                                          *
* This program is free software: you can redistribute it and/or modify     *
* it under the terms of the GNU Affero General Public License as           *
* published by the Free Software Foundation, either version 3 of           *
* the License, or (at your option) any later version.                      *
*                                                                          *
* This program is distributed in the hope that it will be useful,          *
* but WITHOUT ANY WARRANTY; without even the implied warranty of           *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
* GNU Affero General Public License for more details.                      *
*                                                                          *
* You should have received a copy of the GNU Affero General Public License *
* along with this program in its root directory.                           *
* If not, see <http://www.gnu.org/licenses/>.                              *
\**************************************************************************/

package endercrypt.endercryptschallenge.modules.pathfinder.implementation.astar;


import endercrypt.endercryptschallenge.modules.pathfinder.PathfinderConfiguration;
import endercrypt.endercryptschallenge.modules.pathfinder.PathfinderPath;
import endercrypt.endercryptschallenge.modules.pathfinder.implementation.Pathfinder;
import endercrypt.endercryptschallenge.modules.pathfinder.state.PathfinderState;
import endercrypt.endercryptschallenge.modules.pathfinder.state.PathfinderStateType;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import org.bukkit.block.Block;

import lombok.NonNull;


public class AStarPathfinder extends Pathfinder
{
	private final Set<AStarMapNode> nodes = new HashSet<>();
	private final Set<Block> occupied = new HashSet<>();
	private final Map<Block, AStarMapNode> available = new HashMap<>();
	
	public AStarPathfinder(PathfinderConfiguration configuration)
	{
		super(configuration);
		addNode(new AStarMapNode(configuration, configuration.getStart()));
	}
	
	public void addNode(AStarMapNode node)
	{
		if (getState().getType() != PathfinderStateType.SUCCESS && node.getBlock().equals(getConfiguration().getEnd()))
		{
			submitSuccess(generateCompletePath(node));
			return;
		}
		
		nodes.add(node);
		occupied.add(node.getBlock());
		
		for (Block neighbour : getConfiguration().getRules().getNeighbours(node.getBlock()))
		{
			addNeighbourNode(node, neighbour);
		}
	}
	
	private void addNeighbourNode(AStarMapNode node, Block neighbour)
	{
		if (occupied.contains(neighbour))
		{
			return;
		}
		AStarMapNode neighbourNode = new AStarMapNode(neighbour, node);
		
		AStarMapNode old = available.get(neighbour);
		if (old == null || neighbourNode.getCost() < old.getCost())
		{
			available.put(neighbourNode.getBlock(), neighbourNode);
		}
	}
	
	@Override
	public void advance()
	{
		AStarMapNode node = findNextSuitableNode().orElse(null);
		if (node == null)
		{
			submitFailure(PathfinderState.TRAPPED);
			return;
		}
		
		// Utility.sendMessageToAll("node: " + node.getBlock().getX() + ", " + node.getBlock().getY() + ", " + node.getBlock().getZ());
		
		Block block = node.getBlock();
		if (getConfiguration().getRules().canTravelThrough(block))
		{
			addNode(node);
		}
	}
	
	private Optional<AStarMapNode> findNextSuitableNode()
	{
		if (getState().getType() != PathfinderStateType.SEARCHING)
		{
			throw new IllegalStateException("Cannot get a suitable block when not in search mode");
		}
		if (available.isEmpty())
		{
			return Optional.empty();
		}
		
		AStarMapNode best = findNextBestMapNode();
		Block coordinate = best.getBlock();
		occupied.add(coordinate);
		available.remove(coordinate);
		return Optional.of(best);
	}
	
	private @NonNull AStarMapNode findNextBestMapNode()
	{
		AStarMapNode best = null;
		for (AStarMapNode node : available.values())
		{
			if (best == null || isBetter(best, node))
			{
				best = node;
			}
		}
		return best;
	}
	
	private PathfinderPath generateCompletePath(AStarMapNode node)
	{
		List<Block> result = new ArrayList<>();
		while (node != null)
		{
			result.add(node.getBlock());
			node = node.getPrevious();
		}
		Collections.reverse(result);
		
		return new PathfinderPath(getConfiguration(), result);
	}
	
	private static boolean isBetter(AStarMapNode old, AStarMapNode node)
	{
		double cost = old.getCost() - node.getCost();
		if (cost != 0)
		{
			return cost > 0;
		}
		double dist = old.getDistanceToEnd() - node.getDistanceToEnd();
		return dist > 0;
	}
}
