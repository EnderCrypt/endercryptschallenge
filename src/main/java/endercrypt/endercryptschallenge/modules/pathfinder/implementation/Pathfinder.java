/**************************************************************************\
* EnderCrypt's Challenge                                                   *
* Copyright (C) 2023 by EnderCrypt                                         *
*                                                                          *
* This program is free software: you can redistribute it and/or modify     *
* it under the terms of the GNU Affero General Public License as           *
* published by the Free Software Foundation, either version 3 of           *
* the License, or (at your option) any later version.                      *
*                                                                          *
* This program is distributed in the hope that it will be useful,          *
* but WITHOUT ANY WARRANTY; without even the implied warranty of           *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
* GNU Affero General Public License for more details.                      *
*                                                                          *
* You should have received a copy of the GNU Affero General Public License *
* along with this program in its root directory.                           *
* If not, see <http://www.gnu.org/licenses/>.                              *
\**************************************************************************/

package endercrypt.endercryptschallenge.modules.pathfinder.implementation;


import endercrypt.endercryptschallenge.modules.pathfinder.PathfinderConfiguration;
import endercrypt.endercryptschallenge.modules.pathfinder.PathfinderPath;
import endercrypt.endercryptschallenge.modules.pathfinder.listener.PathfinderListenerAdapter;
import endercrypt.endercryptschallenge.modules.pathfinder.state.PathfinderFailureState;
import endercrypt.endercryptschallenge.modules.pathfinder.state.PathfinderState;

import java.util.ArrayList;
import java.util.List;

import lombok.Getter;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;


/**
 * @author EnderCrypt
 */
@RequiredArgsConstructor
@Getter
@NonNull
public abstract class Pathfinder
{
	private final PathfinderConfiguration configuration;
	private PathfinderState state = PathfinderState.SEARCHING;
	private PathfinderPath path = null;
	
	private final List<PathfinderListenerAdapter> listeners = new ArrayList<>();
	
	protected void submitSuccess(@NonNull PathfinderPath path)
	{
		this.path = path;
		setState(PathfinderState.SUCCESS);
	}
	
	public void submitFailure(PathfinderFailureState state)
	{
		setState(state);
	}
	
	private void setState(PathfinderState state)
	{
		this.state = state;
		for (PathfinderListenerAdapter listener : listeners)
		{
			listener.onChange(this);
		}
	}
	
	public void addListener(PathfinderListenerAdapter listener)
	{
		listeners.add(listener);
	}
	
	public abstract void advance();
	
	public interface Factory
	{
		public Pathfinder build(PathfinderConfiguration configuration);
	}
}
