/**************************************************************************\
* EnderCrypt's Challenge                                                   *
* Copyright (C) 2023 by EnderCrypt                                         *
*                                                                          *
* This program is free software: you can redistribute it and/or modify     *
* it under the terms of the GNU Affero General Public License as           *
* published by the Free Software Foundation, either version 3 of           *
* the License, or (at your option) any later version.                      *
*                                                                          *
* This program is distributed in the hope that it will be useful,          *
* but WITHOUT ANY WARRANTY; without even the implied warranty of           *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
* GNU Affero General Public License for more details.                      *
*                                                                          *
* You should have received a copy of the GNU Affero General Public License *
* along with this program in its root directory.                           *
* If not, see <http://www.gnu.org/licenses/>.                              *
\**************************************************************************/

package endercrypt.endercryptschallenge.modules.pathfinder;


import endercrypt.endercryptschallenge.modules.pathfinder.implementation.Pathfinder;
import endercrypt.endercryptschallenge.modules.pathfinder.implementation.astar.AStarPathfinder;

import java.util.Objects;

import org.bukkit.World;
import org.bukkit.block.Block;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;


@Getter
public class PathfinderConfiguration
{
	private final Pathfinder.Factory pathfinderFactory;
	private final Block start;
	private final Block end;
	private final PathfinderRules rules;
	private final int limit;
	private final int timeout;
	private final World world;
	
	public PathfinderConfiguration(Builder builder)
	{
		this.pathfinderFactory = Objects.requireNonNull(builder.pathfinderFactory, "pathfinderFactory");
		this.start = Objects.requireNonNull(builder.start, "start");
		this.end = Objects.requireNonNull(builder.end, "end");
		this.rules = Objects.requireNonNull(builder.rules, "rules");
		this.limit = Objects.requireNonNull(builder.limit, "limit");
		this.timeout = Objects.requireNonNull(builder.timeout, "timeout");
		
		world = start.getWorld();
		if (world.equals(end.getWorld()) == false)
		{
			throw new IllegalArgumentException("start and end cant be in diffrent worlds");
		}
	}
	
	@Setter
	@Accessors(chain = true)
	public static class Builder
	{
		private Pathfinder.Factory pathfinderFactory = AStarPathfinder::new;
		
		private Block start;
		
		private Block end;
		
		private PathfinderRules rules;
		
		private int limit;
		
		private int timeout = Integer.MAX_VALUE;
		
		public PathfinderConfiguration build()
		{
			return new PathfinderConfiguration(this);
		}
	}
}
