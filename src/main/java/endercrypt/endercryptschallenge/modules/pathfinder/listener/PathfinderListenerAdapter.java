/**************************************************************************\
* EnderCrypt's Challenge                                                   *
* Copyright (C) 2023 by EnderCrypt                                         *
*                                                                          *
* This program is free software: you can redistribute it and/or modify     *
* it under the terms of the GNU Affero General Public License as           *
* published by the Free Software Foundation, either version 3 of           *
* the License, or (at your option) any later version.                      *
*                                                                          *
* This program is distributed in the hope that it will be useful,          *
* but WITHOUT ANY WARRANTY; without even the implied warranty of           *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
* GNU Affero General Public License for more details.                      *
*                                                                          *
* You should have received a copy of the GNU Affero General Public License *
* along with this program in its root directory.                           *
* If not, see <http://www.gnu.org/licenses/>.                              *
\**************************************************************************/

package endercrypt.endercryptschallenge.modules.pathfinder.listener;


import endercrypt.endercryptschallenge.modules.pathfinder.PathfinderPath;
import endercrypt.endercryptschallenge.modules.pathfinder.implementation.Pathfinder;
import endercrypt.endercryptschallenge.modules.pathfinder.state.FailureReason;
import endercrypt.endercryptschallenge.modules.pathfinder.state.PathfinderFailureState;
import endercrypt.endercryptschallenge.modules.pathfinder.state.PathfinderStateType;


/**
 * @author EnderCrypt
 */
public abstract class PathfinderListenerAdapter implements PathfinderListener
{
	@Override
	public void onChange(Pathfinder pathfinder)
	{
		if (pathfinder.getState().getType() == PathfinderStateType.SEARCHING)
		{
			throw new IllegalArgumentException("Changed to unexpected pathfinder state: " + pathfinder.getState());
		}
		if (pathfinder.getState().getType() == PathfinderStateType.SUCCESS)
		{
			onSuccess(pathfinder.getPath());
		}
		if (pathfinder.getState() instanceof PathfinderFailureState failureState)
		{
			onFailure(failureState.getReason());
		}
	}
	
	protected void onSuccess(PathfinderPath path)
	{
		// Do nothing
	}
	
	protected void onFailure(FailureReason reason)
	{
		// Do nothing
	}
}
