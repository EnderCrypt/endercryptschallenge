/**************************************************************************\
* EnderCrypt's Challenge                                                   *
* Copyright (C) 2023 by EnderCrypt                                         *
*                                                                          *
* This program is free software: you can redistribute it and/or modify     *
* it under the terms of the GNU Affero General Public License as           *
* published by the Free Software Foundation, either version 3 of           *
* the License, or (at your option) any later version.                      *
*                                                                          *
* This program is distributed in the hope that it will be useful,          *
* but WITHOUT ANY WARRANTY; without even the implied warranty of           *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
* GNU Affero General Public License for more details.                      *
*                                                                          *
* You should have received a copy of the GNU Affero General Public License *
* along with this program in its root directory.                           *
* If not, see <http://www.gnu.org/licenses/>.                              *
\**************************************************************************/

package endercrypt.endercryptschallenge.modules.pathfinder.state;


import lombok.AccessLevel;
import lombok.Getter;
import lombok.RequiredArgsConstructor;


/**
 * @author EnderCrypt
 */
@Getter
@RequiredArgsConstructor(access = AccessLevel.PACKAGE)
public class PathfinderState
{
	public static final PathfinderState SEARCHING = new PathfinderState(PathfinderStateType.SEARCHING); // The pathfinder is either in queue or actively searching for a path
	public static final PathfinderState SUCCESS = new PathfinderState(PathfinderStateType.SUCCESS); // the pathfinder has found a valid path
	public static final PathfinderFailureState TRAPPED = new PathfinderFailureState(FailureReason.TRAPPED);// the pathfinder has found itself in a completely enclosed space and is unable to continue searching for a path
	public static final PathfinderFailureState TIMEOUT = new PathfinderFailureState(FailureReason.TIMEOUT); // the pathfinder ran out of ticks to find a target, either while in queue or while searching
	public static final PathfinderFailureState CANCELLED = new PathfinderFailureState(FailureReason.CANCELLED); // the pathfinder was cancelled either manually by an automated process, such as the traveler having a new pathfinding request for the same entity arrive and thus cancelling the previous one
	
	private final PathfinderStateType type;
	
	@Override
	public String toString()
	{
		return type.toString();
	}
}
