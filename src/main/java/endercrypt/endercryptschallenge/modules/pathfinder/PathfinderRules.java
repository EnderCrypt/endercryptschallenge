/**************************************************************************\
* EnderCrypt's Challenge                                                   *
* Copyright (C) 2023 by EnderCrypt                                         *
*                                                                          *
* This program is free software: you can redistribute it and/or modify     *
* it under the terms of the GNU Affero General Public License as           *
* published by the Free Software Foundation, either version 3 of           *
* the License, or (at your option) any later version.                      *
*                                                                          *
* This program is distributed in the hope that it will be useful,          *
* but WITHOUT ANY WARRANTY; without even the implied warranty of           *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
* GNU Affero General Public License for more details.                      *
*                                                                          *
* You should have received a copy of the GNU Affero General Public License *
* along with this program in its root directory.                           *
* If not, see <http://www.gnu.org/licenses/>.                              *
\**************************************************************************/

package endercrypt.endercryptschallenge.modules.pathfinder;


import endercrypt.endercryptschallenge.utility.Utility;

import java.util.Collection;

import org.bukkit.block.Block;


/**
 * @author EnderCrypt
 */
public interface PathfinderRules
{
	public static final PathfinderRules bat = new PathfinderRules()
	{
		@Override
		public boolean canTravelThrough(Block block)
		{
			return block.isPassable();
		}
		
		@Override
		public Collection<Block> getNeighbours(Block block)
		{
			return Utility.cartesianBlockFaces.stream()
				.map(block::getRelative)
				.toList();
		}
	};
	
	public abstract boolean canTravelThrough(Block block);
	
	public abstract Collection<Block> getNeighbours(Block block);
}
