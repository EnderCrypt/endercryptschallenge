/**************************************************************************\
* EnderCrypt's Challenge                                                   *
* Copyright (C) 2023 by EnderCrypt                                         *
*                                                                          *
* This program is free software: you can redistribute it and/or modify     *
* it under the terms of the GNU Affero General Public License as           *
* published by the Free Software Foundation, either version 3 of           *
* the License, or (at your option) any later version.                      *
*                                                                          *
* This program is distributed in the hope that it will be useful,          *
* but WITHOUT ANY WARRANTY; without even the implied warranty of           *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
* GNU Affero General Public License for more details.                      *
*                                                                          *
* You should have received a copy of the GNU Affero General Public License *
* along with this program in its root directory.                           *
* If not, see <http://www.gnu.org/licenses/>.                              *
\**************************************************************************/

package endercrypt.endercryptschallenge.modules.pathfinder;


import endercrypt.endercryptschallenge.modules.pathfinder.implementation.Pathfinder;
import endercrypt.endercryptschallenge.modules.pathfinder.state.PathfinderState;
import endercrypt.endercryptschallenge.modules.pathfinder.state.PathfinderStateType;
import endercrypt.endercryptschallenge.utility.Utility;

import org.bukkit.Bukkit;

import lombok.Getter;


/**
 * @author EnderCrypt
 */
@Getter
public class PathfinderRequest implements Comparable<PathfinderRequest>
{
	private final int time = Bukkit.getCurrentTick();
	private final Pathfinder pathfinder;
	private final double naiveDistance;
	private boolean done = false;
	
	public PathfinderRequest(PathfinderConfiguration configuration)
	{
		this.pathfinder = configuration.getPathfinderFactory().build(configuration);
		this.naiveDistance = Utility.roughDistance(configuration.getStart(), configuration.getEnd());
	}
	
	protected void update()
	{
		if (Bukkit.getCurrentTick() > time + getPathfinder().getConfiguration().getTimeout())
		{
			pathfinder.submitFailure(PathfinderState.TIMEOUT);
		}
	}
	
	protected void advance()
	{
		update();
		
		if (pathfinder.getState().getType() == PathfinderStateType.SEARCHING)
		{
			pathfinder.advance();
		}
		
		if (pathfinder.getState().getType() != PathfinderStateType.SEARCHING)
		{
			if (done)
			{
				throw new IllegalStateException("cant advance finished pathfinder task, as its already done");
			}
			done = true;
		}
	}
	
	@Override
	public int compareTo(PathfinderRequest other)
	{
		return Double.compare(getNaiveDistance(), other.getNaiveDistance());
	}
}
