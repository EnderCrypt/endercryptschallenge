/**************************************************************************\
* EnderCrypt's Challenge                                                   *
* Copyright (C) 2023 by EnderCrypt                                         *
*                                                                          *
* This program is free software: you can redistribute it and/or modify     *
* it under the terms of the GNU Affero General Public License as           *
* published by the Free Software Foundation, either version 3 of           *
* the License, or (at your option) any later version.                      *
*                                                                          *
* This program is distributed in the hope that it will be useful,          *
* but WITHOUT ANY WARRANTY; without even the implied warranty of           *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
* GNU Affero General Public License for more details.                      *
*                                                                          *
* You should have received a copy of the GNU Affero General Public License *
* along with this program in its root directory.                           *
* If not, see <http://www.gnu.org/licenses/>.                              *
\**************************************************************************/

package endercrypt.endercryptschallenge.modules.pathfinder;


import endercrypt.endercryptschallenge.utility.Utility;

import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;

import org.bukkit.block.Block;

import lombok.Getter;


@Getter
public class PathfinderPath
{
	private final PathfinderConfiguration configuration;
	private final List<Block> coordinates;
	private final double distance;
	
	public PathfinderPath(PathfinderConfiguration configuration, List<Block> coordinates)
	{
		this.configuration = configuration;
		this.coordinates = List.copyOf(coordinates);
		if (coordinates.isEmpty())
		{
			throw new IllegalArgumentException("path cannot be empty");
		}
		this.distance = calculateDistance();
	}
	
	private double calculateDistance()
	{
		Block previous = null;
		double distance = 0;
		for (Iterator<Block> iterator = coordinates.iterator(); iterator.hasNext();)
		{
			Block block = iterator.next();
			
			if (previous != null)
			{
				distance += Utility.distance(previous, block);
			}
			
			previous = block;
		}
		return distance;
	}
	
	@Override
	public String toString()
	{
		return coordinates.stream()
			.map(b -> b.getX() + ", " + b.getY() + ", " + b.getZ())
			.collect(Collectors.joining(" -> "));
	}
}
