/**************************************************************************\
* EnderCrypt's Challenge                                                   *
* Copyright (C) 2023 by EnderCrypt                                         *
*                                                                          *
* This program is free software: you can redistribute it and/or modify     *
* it under the terms of the GNU Affero General Public License as           *
* published by the Free Software Foundation, either version 3 of           *
* the License, or (at your option) any later version.                      *
*                                                                          *
* This program is distributed in the hope that it will be useful,          *
* but WITHOUT ANY WARRANTY; without even the implied warranty of           *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
* GNU Affero General Public License for more details.                      *
*                                                                          *
* You should have received a copy of the GNU Affero General Public License *
* along with this program in its root directory.                           *
* If not, see <http://www.gnu.org/licenses/>.                              *
\**************************************************************************/

package endercrypt.endercryptschallenge.modules.pathfinder;


import endercrypt.endercryptschallenge.core.Core;
import endercrypt.endercryptschallenge.core.modules.CoreModule;
import endercrypt.endercryptschallenge.modules.features.OnTick;
import endercrypt.endercryptschallenge.modules.pathfinder.state.PathfinderStateType;

import java.util.Iterator;
import java.util.PriorityQueue;
import java.util.logging.Level;


/**
 * @author EnderCrypt
 */
public class PathfinderManager extends CoreModule implements OnTick, Iterable<PathfinderRequest>
{
	private static final Level logLevel = Level.FINE;
	private static final int BLOCKS_PER_TICK = 200;
	
	private PriorityQueue<PathfinderRequest> queue = new PriorityQueue<>();
	private PathfinderRequest task = null;
	
	public PathfinderManager(Core core)
	{
		super(core);
	}
	
	public PathfinderRequest submit(PathfinderConfiguration.Builder configuration)
	{
		return submit(configuration.build());
	}
	
	public PathfinderRequest submit(PathfinderConfiguration configuration)
	{
		PathfinderRequest request = new PathfinderRequest(configuration);
		queue.add(request);
		return request;
	}
	
	@Override
	public void onTick()
	{
		updateAllTasks();
		performActiveTask();
	}
	
	private void updateAllTasks()
	{
		for (Iterator<PathfinderRequest> iterator = queue.iterator(); iterator.hasNext();)
		{
			PathfinderRequest request = iterator.next();
			request.update();
			if (request.getPathfinder().getState().getType() != PathfinderStateType.SEARCHING)
			{
				getLogger().log(logLevel, "Inactive pathfinding task " + request + " gave up by itself with status " + request.getPathfinder().getState());
				iterator.remove();
			}
		}
	}
	
	private void performActiveTask()
	{
		for (int i = 0; i < BLOCKS_PER_TICK; i++)
		{
			configureTask();
			
			if (task == null)
				break;
			
			task.advance();
		}
	}
	
	private void configureTask()
	{
		// remove finished task
		if (task != null && task.isDone())
		{
			getLogger().log(logLevel, "Finished pathfinding task  (" + task.getPathfinder().getState() + ")");
			task = null;
		}
		// install new task
		if (task == null)
		{
			task = queue.poll();
			if (task != null)
			{
				getLogger().log(logLevel, "Assigned new pathfinding task: " + task.getPathfinder().getConfiguration().getStart().getLocation() + " -> " + task.getPathfinder().getConfiguration().getEnd().getLocation() + " (~" + (int) Math.round(task.getNaiveDistance()) + " blocks apart)");
			}
		}
	}
	
	@Override
	public Iterator<PathfinderRequest> iterator()
	{
		return queue.iterator();
	}
}
