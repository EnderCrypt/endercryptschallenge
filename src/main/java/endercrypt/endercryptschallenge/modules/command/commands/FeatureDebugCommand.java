/**************************************************************************\
* EnderCrypt's Challenge                                                   *
* Copyright (C) 2023 by EnderCrypt                                         *
*                                                                          *
* This program is free software: you can redistribute it and/or modify     *
* it under the terms of the GNU Affero General Public License as           *
* published by the Free Software Foundation, either version 3 of           *
* the License, or (at your option) any later version.                      *
*                                                                          *
* This program is distributed in the hope that it will be useful,          *
* but WITHOUT ANY WARRANTY; without even the implied warranty of           *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
* GNU Affero General Public License for more details.                      *
*                                                                          *
* You should have received a copy of the GNU Affero General Public License *
* along with this program in its root directory.                           *
* If not, see <http://www.gnu.org/licenses/>.                              *
\**************************************************************************/

package endercrypt.endercryptschallenge.modules.command.commands;


import endercrypt.endercryptschallenge.core.Core;
import endercrypt.endercryptschallenge.modules.command.CoreCommand;
import endercrypt.endercryptschallenge.modules.command.permissions.RequirePermission;
import endercrypt.endercryptschallenge.modules.features.MinecraftFeature;
import endercrypt.endercryptschallenge.modules.features.debug.FeatureDebugController;
import endercrypt.library.intellicommand.annotation.CommandSignature;
import endercrypt.library.intellicommand.annotation.Include;
import endercrypt.library.intellicommand.annotation.Piece;

import org.bukkit.command.CommandSender;


/**
 * @author EnderCrypt
 */
@CommandSignature("endercrypt feature debug")
public class FeatureDebugCommand extends CoreCommand
{
	public FeatureDebugCommand(Core core)
	{
		super(core);
	}
	
	@RequirePermission("endercrypt.feature.debug")
	@CommandSignature("{feature}")
	public void toggle(@Include("sender") CommandSender sender, @Piece MinecraftFeature feature)
	{
		FeatureDebugController debugController = feature.getDebug();
		if (debugController.isSupported() == false)
		{
			sender.sendMessage(feature.getName() + " does not support debug logging");
		}
		
		boolean enabled = debugController.toggle();
		
		String message = "Debug logs " + (enabled ? "enabled" : "disabled") + " for " + feature.getName();
		sender.sendMessage(message);
		server.getLogger().warning(message);
	}
}
