/**************************************************************************\
* EnderCrypt's Challenge                                                   *
* Copyright (C) 2023 by EnderCrypt                                         *
*                                                                          *
* This program is free software: you can redistribute it and/or modify     *
* it under the terms of the GNU Affero General Public License as           *
* published by the Free Software Foundation, either version 3 of           *
* the License, or (at your option) any later version.                      *
*                                                                          *
* This program is distributed in the hope that it will be useful,          *
* but WITHOUT ANY WARRANTY; without even the implied warranty of           *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
* GNU Affero General Public License for more details.                      *
*                                                                          *
* You should have received a copy of the GNU Affero General Public License *
* along with this program in its root directory.                           *
* If not, see <http://www.gnu.org/licenses/>.                              *
\**************************************************************************/

package endercrypt.endercryptschallenge.modules.command.commands;


import endercrypt.endercryptschallenge.core.Core;
import endercrypt.endercryptschallenge.modules.command.CoreCommand;
import endercrypt.endercryptschallenge.modules.command.permissions.RequirePermission;
import endercrypt.endercryptschallenge.modules.features.MinecraftFeature;
import endercrypt.endercryptschallenge.modules.features.MinecraftFeatureManager;
import endercrypt.library.intellicommand.annotation.CommandSignature;
import endercrypt.library.intellicommand.annotation.Include;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import org.bukkit.command.CommandSender;


/**
 * @author EnderCrypt
 */
@CommandSignature("endercrypt feature list")
public class FeatureListCommand extends CoreCommand
{
	public FeatureListCommand(Core core)
	{
		super(core);
	}
	
	@RequirePermission("endercrypt.feature.list")
	@CommandSignature("")
	public void list(@Include("core") Core core, @Include("sender") CommandSender sender)
	{
		StringBuilder builder = new StringBuilder();
		
		List<MinecraftFeature> features = core
			.getModuleManager()
			.fetchModule(MinecraftFeatureManager.class)
			.getFeatures()
			.stream()
			.sorted(new MinecraftFeatureComparator())
			.collect(Collectors.toList());
		
		builder.append("Features (" + features.size() + ")\n");
		
		String featureString = features
			.stream()
			.map(feature -> "- " + getFeatureString(feature))
			.collect(Collectors.joining("\n"));
		builder.append(featureString);
		
		sender.sendMessage(builder.toString());
	}
	
	private static class MinecraftFeatureComparator implements Comparator<MinecraftFeature>
	{
		@Override
		public int compare(MinecraftFeature f1, MinecraftFeature f2)
		{
			String f1Name = f1.getName();
			String f2Name = f2.getName();
			return f1Name.compareTo(f2Name);
		}
	}
	
	private String getFeatureString(MinecraftFeature feature)
	{
		StringBuilder builder = new StringBuilder();
		
		builder.append(feature.getName());
		
		if (feature.isDebug())
		{
			builder.append(" [DEBUG MODE]");
		}
		
		return builder.toString();
	}
}
