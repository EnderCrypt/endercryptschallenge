/**************************************************************************\
* EnderCrypt's Challenge                                                   *
* Copyright (C) 2023 by EnderCrypt                                         *
*                                                                          *
* This program is free software: you can redistribute it and/or modify     *
* it under the terms of the GNU Affero General Public License as           *
* published by the Free Software Foundation, either version 3 of           *
* the License, or (at your option) any later version.                      *
*                                                                          *
* This program is distributed in the hope that it will be useful,          *
* but WITHOUT ANY WARRANTY; without even the implied warranty of           *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
* GNU Affero General Public License for more details.                      *
*                                                                          *
* You should have received a copy of the GNU Affero General Public License *
* along with this program in its root directory.                           *
* If not, see <http://www.gnu.org/licenses/>.                              *
\**************************************************************************/

package endercrypt.endercryptschallenge.modules.command.commands;


import endercrypt.endercryptschallenge.core.Core;
import endercrypt.endercryptschallenge.modules.command.CoreCommand;
import endercrypt.endercryptschallenge.modules.command.permissions.RequirePermission;
import endercrypt.library.commons.RandomEntry;
import endercrypt.library.intellicommand.annotation.CommandPriority;
import endercrypt.library.intellicommand.annotation.CommandSignature;
import endercrypt.library.intellicommand.annotation.Include;
import endercrypt.library.intellicommand.annotation.Piece;

import org.bukkit.Sound;
import org.bukkit.World;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;


/**
 * @author EnderCrypt
 */
@CommandSignature("endercrypt sound")
public class TestSoundCommand extends CoreCommand
{
	public TestSoundCommand(Core core)
	{
		super(core);
	}
	
	@RequirePermission("endercrypt.test.sound")
	@CommandSignature("test random")
	@CommandPriority(1)
	public void testRandomSound(@Include("sender") CommandSender sender)
	{
		Sound sound = RandomEntry.from(Sound.class);
		playSound(sender, sound);
	}
	
	@RequirePermission("endercrypt.test.sound")
	@CommandSignature("test {soundName}")
	public void testSound(@Include("sender") CommandSender sender, @Piece String soundName)
	{
		Sound sound = Sound.valueOf(soundName);
		playSound(sender, sound);
	}
	
	private boolean playSound(CommandSender sender, Sound sound)
	{
		if (sender instanceof Player player)
		{
			player.sendMessage("Playing: " + sound);
			World world = player.getWorld();
			world.playSound(player.getLocation(), sound, 1.0f, 1.0f);
			return true;
		}
		return false;
	}
}
