/**************************************************************************\
* EnderCrypt's Challenge                                                   *
* Copyright (C) 2023 by EnderCrypt                                         *
*                                                                          *
* This program is free software: you can redistribute it and/or modify     *
* it under the terms of the GNU Affero General Public License as           *
* published by the Free Software Foundation, either version 3 of           *
* the License, or (at your option) any later version.                      *
*                                                                          *
* This program is distributed in the hope that it will be useful,          *
* but WITHOUT ANY WARRANTY; without even the implied warranty of           *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
* GNU Affero General Public License for more details.                      *
*                                                                          *
* You should have received a copy of the GNU Affero General Public License *
* along with this program in its root directory.                           *
* If not, see <http://www.gnu.org/licenses/>.                              *
\**************************************************************************/

package endercrypt.endercryptschallenge.modules.command.commands;


import endercrypt.endercryptschallenge.core.Core;
import endercrypt.endercryptschallenge.modules.command.CoreCommand;
import endercrypt.library.intellicommand.annotation.CommandSignature;
import endercrypt.library.intellicommand.annotation.Include;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.command.CommandSender;

import net.kyori.adventure.audience.MessageType;
import net.kyori.adventure.identity.Identity;
import net.kyori.adventure.text.Component;
import net.kyori.adventure.text.JoinConfiguration;
import net.kyori.adventure.text.TextComponent;
import net.kyori.adventure.text.event.ClickEvent;
import net.kyori.adventure.text.format.TextColor;
import net.kyori.adventure.text.format.TextDecoration;


/**
 * @author EnderCrypt
 */
@CommandSignature("endercrypt")
public class InformationCommand extends CoreCommand
{
	private static final TextColor TWITCH_COLOR = TextColor.color(100, 65, 165);
	private static final String URL = "https://discord.gg/pWaxU4zGmV";
	
	public InformationCommand(Core core)
	{
		super(core);
	}
	
	@CommandSignature("")
	public void get(@Include("sender") CommandSender sender)
	{
		List<Component> components = new ArrayList<>();
		components.add(Component.text("-- EnderCrypt's Challenge --"));
		components.add(Component.text("Version:  ").append(Component.text(core.getPlugin().getDescription().getVersion())));
		components.add(Component.text("Discord:  ").append(Component.text(URL).clickEvent(ClickEvent.openUrl(URL))));
		components.add(Component.text("Features: ").append(Component.text(core.getMinecraftFeatureManager().getFeatures().size())));
		components.add(Component.text("Creator:  ").append(Component.text("EnderCrypt")));
		components.add(Component.text(core.getInformationManager().toString()));
		components.add(createWinnerText());
		
		JoinConfiguration joinConfiguration = JoinConfiguration.separator(Component.newline());
		Component text = Component.join(joinConfiguration, components);
		
		sender.sendMessage(Identity.nil(), text, MessageType.CHAT);
	}
	
	private static TextComponent createWinnerText()
	{
		TextComponent lionbigsox = Component.text("LionBigSox")
			.color(TWITCH_COLOR)
			.decorate(TextDecoration.UNDERLINED)
			.clickEvent(ClickEvent.openUrl("https://www.twitch.tv/lionbigsox"));
		
		TextComponent benwyartt = Component.text("Benwyartt")
			.color(TWITCH_COLOR)
			.decorate(TextDecoration.UNDERLINED)
			.clickEvent(ClickEvent.openUrl("https://www.twitch.tv/benwyartt"));
		
		TextComponent soxAndBen = Component.text("")
			.append(lionbigsox)
			.append(Component.text(" and "))
			.append(benwyartt);
		
		TextComponent winnerText = Component.text("Through great effort, pain and adventure ")
			.append(soxAndBen)
			.append(Component.text(" became the first known group to defeat the Enderdragon."));
		return winnerText;
	}
}
