/**************************************************************************\
* EnderCrypt's Challenge                                                   *
* Copyright (C) 2023 by EnderCrypt                                         *
*                                                                          *
* This program is free software: you can redistribute it and/or modify     *
* it under the terms of the GNU Affero General Public License as           *
* published by the Free Software Foundation, either version 3 of           *
* the License, or (at your option) any later version.                      *
*                                                                          *
* This program is distributed in the hope that it will be useful,          *
* but WITHOUT ANY WARRANTY; without even the implied warranty of           *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
* GNU Affero General Public License for more details.                      *
*                                                                          *
* You should have received a copy of the GNU Affero General Public License *
* along with this program in its root directory.                           *
* If not, see <http://www.gnu.org/licenses/>.                              *
\**************************************************************************/

package endercrypt.endercryptschallenge.modules.command.args;


import endercrypt.endercryptschallenge.core.Core;
import endercrypt.endercryptschallenge.core.CoreCollective;
import endercrypt.endercryptschallenge.modules.features.MinecraftFeature;
import endercrypt.library.intellicommand.bundle.Bundle;
import endercrypt.library.intellicommand.mapper.ArgumentMapper;
import endercrypt.library.intellicommand.mapper.MapperConversionFail;


/**
 * @author EnderCrypt
 */
public class MinecraftFeatureMapper extends CoreCollective implements ArgumentMapper<MinecraftFeature>
{
	public MinecraftFeatureMapper(Core core)
	{
		super(core);
	}
	
	@Override
	public MinecraftFeature map(Bundle bundle, Class<? extends MinecraftFeature> targetClass, String text) throws MapperConversionFail
	{
		for (MinecraftFeature feature : core.getMinecraftFeatureManager().getFeatures())
		{
			if (text.equalsIgnoreCase(feature.getClass().getSimpleName()) || text.equalsIgnoreCase(feature.getName()))
			{
				return feature;
			}
		}
		throw new MapperConversionFail();
	}
}
