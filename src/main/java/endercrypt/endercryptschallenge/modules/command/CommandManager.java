/**************************************************************************\
* EnderCrypt's Challenge                                                   *
* Copyright (C) 2023 by EnderCrypt                                         *
*                                                                          *
* This program is free software: you can redistribute it and/or modify     *
* it under the terms of the GNU Affero General Public License as           *
* published by the Free Software Foundation, either version 3 of           *
* the License, or (at your option) any later version.                      *
*                                                                          *
* This program is distributed in the hope that it will be useful,          *
* but WITHOUT ANY WARRANTY; without even the implied warranty of           *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
* GNU Affero General Public License for more details.                      *
*                                                                          *
* You should have received a copy of the GNU Affero General Public License *
* along with this program in its root directory.                           *
* If not, see <http://www.gnu.org/licenses/>.                              *
\**************************************************************************/

package endercrypt.endercryptschallenge.modules.command;


import endercrypt.endercryptschallenge.core.Core;
import endercrypt.endercryptschallenge.core.modules.CoreModule;
import endercrypt.endercryptschallenge.modules.command.args.MinecraftFeatureMapper;
import endercrypt.endercryptschallenge.modules.command.commands.FeatureDebugCommand;
import endercrypt.endercryptschallenge.modules.command.commands.FeatureListCommand;
import endercrypt.endercryptschallenge.modules.command.commands.InformationCommand;
import endercrypt.endercryptschallenge.modules.command.commands.TestSoundCommand;
import endercrypt.endercryptschallenge.modules.command.permissions.PermissionInsufficientException;
import endercrypt.endercryptschallenge.modules.command.permissions.RequiredPermissionIntercetor;
import endercrypt.endercryptschallenge.modules.features.MinecraftFeature;
import endercrypt.library.commons.misc.Ender;
import endercrypt.library.intellicommand.IntelliCommand;
import endercrypt.library.intellicommand.bundle.Bundle;
import endercrypt.library.intellicommand.exception.execution.CommandArgumentMismatchException;
import endercrypt.library.intellicommand.exception.execution.CommandNotFoundException;
import endercrypt.library.intellicommand.exception.execution.MalformedCommandException;
import endercrypt.library.intellicommand.exception.execution.UnderlyingCommandException;

import java.util.Arrays;
import java.util.stream.Collectors;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;


/**
 * @author EnderCrypt
 */
public class CommandManager extends CoreModule
{
	private final IntelliCommand intelliCommand = new IntelliCommand();
	
	public CommandManager(Core core)
	{
		super(core);
		
		// mappers
		intelliCommand.mappers().register(MinecraftFeature.class, new MinecraftFeatureMapper(core));
		
		// commands
		register(new FeatureListCommand(core));
		register(new FeatureDebugCommand(core));
		register(new InformationCommand(core));
		register(new TestSoundCommand(core));
		
		// interceptors
		intelliCommand.interceptors().register(new RequiredPermissionIntercetor());
	}
	
	private void register(CoreCommand command)
	{
		intelliCommand.commands().register(command);
	}
	
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args)
	{
		String commandString = label + " " +
			Arrays.stream(args)
				.map(s -> "\"" + s + "\"")
				.collect(Collectors.joining(" "));
		
		logger.fine("command: " + commandString);
		
		Bundle bundle = new Bundle();
		
		bundle.attach().instance("core", core);
		bundle.attach().instance("sender", sender);
		bundle.attach().instance("command", command);
		bundle.attach().instance("label", label);
		
		try
		{
			intelliCommand.execute(commandString, bundle);
		}
		catch (MalformedCommandException e)
		{
			sender.sendMessage(e.getMessage());
		}
		catch (CommandArgumentMismatchException e)
		{
			sender.sendMessage("did you mean?\n - " + e.getCommands().stream().map(c -> c.getPrimarySignature().asText().toString()).collect(Collectors.joining("\n\t - ")));
		}
		catch (CommandNotFoundException e)
		{
			sender.sendMessage("Command not found");
		}
		catch (UnderlyingCommandException e)
		{
			handleUnderlyingException(sender, e);
		}
		return true;
	}
	
	private void handleUnderlyingException(CommandSender sender, UnderlyingCommandException e)
	{
		Throwable cause = e.getCause();
		if (cause instanceof PermissionInsufficientException permissionInsufficientException)
		{
			sender.sendMessage("Insufficient permissions, " + permissionInsufficientException.getPermissionName() + " is required");
			return;
		}
		String exceptionMessage = Ender.exception.getStackTraceOf(e.getCause());
		logger.warning("Command failed with an exception\n" + exceptionMessage);
	}
}
