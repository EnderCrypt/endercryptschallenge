/**************************************************************************\
* EnderCrypt's Challenge                                                   *
* Copyright (C) 2023 by EnderCrypt                                         *
*                                                                          *
* This program is free software: you can redistribute it and/or modify     *
* it under the terms of the GNU Affero General Public License as           *
* published by the Free Software Foundation, either version 3 of           *
* the License, or (at your option) any later version.                      *
*                                                                          *
* This program is distributed in the hope that it will be useful,          *
* but WITHOUT ANY WARRANTY; without even the implied warranty of           *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
* GNU Affero General Public License for more details.                      *
*                                                                          *
* You should have received a copy of the GNU Affero General Public License *
* along with this program in its root directory.                           *
* If not, see <http://www.gnu.org/licenses/>.                              *
\**************************************************************************/

package endercrypt.endercryptschallenge.modules.cycler;


import endercrypt.endercryptschallenge.core.Core;
import endercrypt.endercryptschallenge.core.modules.CoreModule;
import endercrypt.endercryptschallenge.modules.cycler.cyclers.chunk.ChaoticChunkCycler;
import endercrypt.endercryptschallenge.modules.cycler.cyclers.entity.EntityCycler;
import endercrypt.endercryptschallenge.modules.cycler.cyclers.item.ItemCycler;
import endercrypt.endercryptschallenge.modules.features.OnTick;
import endercrypt.endercryptschallenge.utility.CodeTimer;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;


/**
 * @author EnderCrypt
 */
public class CyclerManager extends CoreModule implements OnTick
{
	private List<Cycler> cyclers = new ArrayList<>();
	
	public CyclerManager(Core core)
	{
		super(core);
		cyclers.add(new ChaoticChunkCycler(core));
		cyclers.add(new EntityCycler(core));
		cyclers.add(new ItemCycler(core));
	}
	
	@Override
	public void onTick()
	{
		cyclers.forEach(this::performCycling);
	}
	
	private void performCycling(Cycler cycler)
	{
		CodeTimer timer = new CodeTimer("Cycling " + cycler.getName(), logger);
		try
		{
			cycler.trigger();
		}
		catch (Exception e)
		{
			logger.log(Level.SEVERE, "Failed to properly cycle " + cycler.getName(), e);
		}
		timer.check(15);
	}
}
