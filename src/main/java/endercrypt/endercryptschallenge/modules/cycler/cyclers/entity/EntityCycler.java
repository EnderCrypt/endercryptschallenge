/**************************************************************************\
* EnderCrypt's Challenge                                                   *
* Copyright (C) 2023 by EnderCrypt                                         *
*                                                                          *
* This program is free software: you can redistribute it and/or modify     *
* it under the terms of the GNU Affero General Public License as           *
* published by the Free Software Foundation, either version 3 of           *
* the License, or (at your option) any later version.                      *
*                                                                          *
* This program is distributed in the hope that it will be useful,          *
* but WITHOUT ANY WARRANTY; without even the implied warranty of           *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
* GNU Affero General Public License for more details.                      *
*                                                                          *
* You should have received a copy of the GNU Affero General Public License *
* along with this program in its root directory.                           *
* If not, see <http://www.gnu.org/licenses/>.                              *
\**************************************************************************/

package endercrypt.endercryptschallenge.modules.cycler.cyclers.entity;


import endercrypt.endercryptschallenge.core.Core;
import endercrypt.endercryptschallenge.modules.cycler.AbstractCycler;
import endercrypt.endercryptschallenge.utility.iterator.BulkIterator;
import endercrypt.endercryptschallenge.utility.iterator.ProgressiveIterator;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import org.bukkit.World;
import org.bukkit.entity.Entity;


/**
 * @author EnderCrypt
 */
public class EntityCycler extends AbstractCycler
{
	private static final double ENTITY_CYCLING_SPEED = 1.0 / 20; // one cycle per 20 ticks
	
	private Map<World, BulkIterator<Entity>> entityCycles = new HashMap<>();
	
	public EntityCycler(Core core)
	{
		super(core);
	}
	
	@Override
	public void trigger()
	{
		super.trigger();
		for (World world : server.getWorlds())
		{
			BulkIterator<Entity> iterator = entityCycles.get(world);
			if (iterator == null || iterator.hasNext() == false)
			{
				Collection<Entity> entities = world.getEntities();
				int speed = (int) Math.max(1, entities.size() * ENTITY_CYCLING_SPEED);
				iterator = new ProgressiveIterator<>(entities, speed);
				entityCycles.put(world, iterator);
			}
			
			Collection<Entity> cycledEntities = iterator.collect();
			getFeatures().forEach(feature -> feature.cycleEntities(cycledEntities));
		}
	}
}
