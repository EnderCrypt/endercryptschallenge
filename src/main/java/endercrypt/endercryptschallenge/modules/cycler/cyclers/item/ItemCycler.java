/**************************************************************************\
* EnderCrypt's Challenge                                                   *
* Copyright (C) 2023 by EnderCrypt                                         *
*                                                                          *
* This program is free software: you can redistribute it and/or modify     *
* it under the terms of the GNU Affero General Public License as           *
* published by the Free Software Foundation, either version 3 of           *
* the License, or (at your option) any later version.                      *
*                                                                          *
* This program is distributed in the hope that it will be useful,          *
* but WITHOUT ANY WARRANTY; without even the implied warranty of           *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
* GNU Affero General Public License for more details.                      *
*                                                                          *
* You should have received a copy of the GNU Affero General Public License *
* along with this program in its root directory.                           *
* If not, see <http://www.gnu.org/licenses/>.                              *
\**************************************************************************/

package endercrypt.endercryptschallenge.modules.cycler.cyclers.item;


import endercrypt.endercryptschallenge.core.Core;
import endercrypt.endercryptschallenge.modules.cycler.AbstractCycler;

import org.bukkit.World;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;


/**
 * @author EnderCrypt
 */
public class ItemCycler extends AbstractCycler
{
	private static final int ITEM_CYCLES = 1; // per tick
	
	public ItemCycler(Core core)
	{
		super(core);
	}
	
	@Override
	public void trigger()
	{
		super.trigger();
		for (World world : server.getWorlds())
		{
			for (Player player : world.getPlayers())
			{
				PlayerInventory inventory = player.getInventory();
				for (int i = 0; i < ITEM_CYCLES; i++)
				{
					int index = (int) (Math.random() * inventory.getSize());
					ItemStack item = inventory.getItem(index);
					if (item == null)
					{
						continue;
					}
					getFeatures().forEach(me -> me.onCycleInventoryItem(player, index, item));
				}
			}
		}
	}
}
