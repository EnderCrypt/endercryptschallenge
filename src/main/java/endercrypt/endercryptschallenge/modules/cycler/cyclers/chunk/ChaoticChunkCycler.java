/**************************************************************************\
* EnderCrypt's Challenge                                                   *
* Copyright (C) 2023 by EnderCrypt                                         *
*                                                                          *
* This program is free software: you can redistribute it and/or modify     *
* it under the terms of the GNU Affero General Public License as           *
* published by the Free Software Foundation, either version 3 of           *
* the License, or (at your option) any later version.                      *
*                                                                          *
* This program is distributed in the hope that it will be useful,          *
* but WITHOUT ANY WARRANTY; without even the implied warranty of           *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
* GNU Affero General Public License for more details.                      *
*                                                                          *
* You should have received a copy of the GNU Affero General Public License *
* along with this program in its root directory.                           *
* If not, see <http://www.gnu.org/licenses/>.                              *
\**************************************************************************/

package endercrypt.endercryptschallenge.modules.cycler.cyclers.chunk;


import endercrypt.endercryptschallenge.core.Core;
import endercrypt.endercryptschallenge.utility.Utility;

import java.util.HashSet;
import java.util.Set;

import org.bukkit.Chunk;
import org.bukkit.World;
import org.bukkit.entity.Player;


/**
 * @author EnderCrypt
 */
public class ChaoticChunkCycler extends AbstractChunkCycler
{
	private static final int CHUNKS_PER_PLAYER = 50;
	private static final int SEEKYNESS = 10; // how far away from the player a chunk may be selected, statistically it'll select nearby chunks more
	
	public ChaoticChunkCycler(Core core)
	{
		super(core);
	}
	
	@Override
	protected int getBlockUpdatesPerChunk()
	{
		return 5;
	}
	
	@Override
	protected void trigger(World world)
	{
		Set<Chunk> visitedChunks = new HashSet<>();
		
		for (Player player : world.getPlayers())
		{
			Chunk playerChunk = player.getChunk();
			if (playerChunk.isLoaded())
			{
				for (int i = 0; i < CHUNKS_PER_PLAYER; i++)
				{
					Chunk targetChunk = seekRandomNearbyChunk(playerChunk);
					if (targetChunk.isLoaded() && visitedChunks.contains(targetChunk) == false)
					{
						trigger(targetChunk);
						visitedChunks.add(targetChunk);
					}
				}
			}
		}
	}
	
	private Chunk seekRandomNearbyChunk(Chunk chunk)
	{
		int x = chunk.getX();
		int z = chunk.getZ();
		for (int i = 0; i < SEEKYNESS; i++)
		{
			x += Utility.randomIntByOne();
			z += Utility.randomIntByOne();
		}
		
		return chunk.getWorld().getChunkAt(x, z);
	}
}
