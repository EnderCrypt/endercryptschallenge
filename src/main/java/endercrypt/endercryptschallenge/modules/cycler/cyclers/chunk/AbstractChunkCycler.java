/**************************************************************************\
* EnderCrypt's Challenge                                                   *
* Copyright (C) 2023 by EnderCrypt                                         *
*                                                                          *
* This program is free software: you can redistribute it and/or modify     *
* it under the terms of the GNU Affero General Public License as           *
* published by the Free Software Foundation, either version 3 of           *
* the License, or (at your option) any later version.                      *
*                                                                          *
* This program is distributed in the hope that it will be useful,          *
* but WITHOUT ANY WARRANTY; without even the implied warranty of           *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
* GNU Affero General Public License for more details.                      *
*                                                                          *
* You should have received a copy of the GNU Affero General Public License *
* along with this program in its root directory.                           *
* If not, see <http://www.gnu.org/licenses/>.                              *
\**************************************************************************/

package endercrypt.endercryptschallenge.modules.cycler.cyclers.chunk;


import endercrypt.endercryptschallenge.core.Core;
import endercrypt.endercryptschallenge.modules.cycler.AbstractCycler;
import endercrypt.endercryptschallenge.modules.features.MinecraftFeature;

import org.bukkit.Chunk;
import org.bukkit.World;
import org.bukkit.block.Block;


/**
 * @author EnderCrypt
 */
public abstract class AbstractChunkCycler extends AbstractCycler
{
	public AbstractChunkCycler(Core core)
	{
		super(core);
	}
	
	@Override
	public final void trigger()
	{
		super.trigger();
		for (World world : server.getWorlds())
		{
			if (world.getPlayerCount() > 0)
			{
				trigger(world);
			}
		}
	}
	
	protected abstract void trigger(World world);
	
	protected void trigger(Chunk chunk)
	{
		final int chunkX = 16;
		final int chunkY = 256;
		final int chunkZ = 16;
		
		for (int cycle = 0; cycle < getBlockUpdatesPerChunk(); cycle++)
		{
			int x = (int) (Math.random() * chunkX);
			int y = (int) (Math.random() * chunkY);
			int z = (int) (Math.random() * chunkZ);
			Block block = chunk.getBlock(x, y, z);
			for (MinecraftFeature feature : getFeatures())
			{
				feature.onCycleBlock(block);
			}
		}
	}
	
	protected abstract int getBlockUpdatesPerChunk();
}
