/**************************************************************************\
* EnderCrypt's Challenge                                                   *
* Copyright (C) 2023 by EnderCrypt                                         *
*                                                                          *
* This program is free software: you can redistribute it and/or modify     *
* it under the terms of the GNU Affero General Public License as           *
* published by the Free Software Foundation, either version 3 of           *
* the License, or (at your option) any later version.                      *
*                                                                          *
* This program is distributed in the hope that it will be useful,          *
* but WITHOUT ANY WARRANTY; without even the implied warranty of           *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
* GNU Affero General Public License for more details.                      *
*                                                                          *
* You should have received a copy of the GNU Affero General Public License *
* along with this program in its root directory.                           *
* If not, see <http://www.gnu.org/licenses/>.                              *
\**************************************************************************/

package endercrypt.endercryptschallenge.modules.cycler.cyclers.chunk;


import endercrypt.endercryptschallenge.core.Core;
import endercrypt.library.commons.misc.Ender;

import org.bukkit.Chunk;
import org.bukkit.World;


/**
 * @author EnderCrypt
 */
@Deprecated
public class GlobalChunkCycler extends AbstractChunkCycler
{
	private static final int CHUNKS_SOFT_LIMIT = 2000;
	
	public GlobalChunkCycler(Core core)
	{
		super(core);
	}
	
	@Override
	protected int getBlockUpdatesPerChunk()
	{
		return 3;
	}
	
	@Override
	protected void trigger(World world)
	{
		Chunk[] chunks = world.getLoadedChunks();
		double chunkChance = 1.0 / chunks.length * CHUNKS_SOFT_LIMIT;
		int updates = 0;
		for (Chunk chunk : chunks)
		{
			if (Math.random() < chunkChance)
			{
				trigger(chunk);
				updates++;
			}
		}
		logger.fine("Loaded: " + chunks.length + " chance: " + Ender.math.round(chunkChance * 100.0, 1) + "% updates: " + updates);
	}
}
