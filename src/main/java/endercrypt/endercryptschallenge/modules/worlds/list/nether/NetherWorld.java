/**************************************************************************\
* EnderCrypt's Challenge                                                   *
* Copyright (C) 2023 by EnderCrypt                                         *
*                                                                          *
* This program is free software: you can redistribute it and/or modify     *
* it under the terms of the GNU Affero General Public License as           *
* published by the Free Software Foundation, either version 3 of           *
* the License, or (at your option) any later version.                      *
*                                                                          *
* This program is distributed in the hope that it will be useful,          *
* but WITHOUT ANY WARRANTY; without even the implied warranty of           *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
* GNU Affero General Public License for more details.                      *
*                                                                          *
* You should have received a copy of the GNU Affero General Public License *
* along with this program in its root directory.                           *
* If not, see <http://www.gnu.org/licenses/>.                              *
\**************************************************************************/

package endercrypt.endercryptschallenge.modules.worlds.list.nether;


import endercrypt.endercryptschallenge.core.Core;
import endercrypt.endercryptschallenge.modules.worlds.AbstractWorldProfile;
import endercrypt.endercryptschallenge.modules.worlds.spawner.list.horde.ZombieHordeNetherSpawner;
import endercrypt.endercryptschallenge.modules.worlds.spawner.list.witherskeleton.NetherFortressWitherSkeletonSpawner;

import org.bukkit.World;


/**
 * @author EnderCrypt
 */
public class NetherWorld extends AbstractWorldProfile
{
	public static final int NETHER_CEILING_Y = 128;
	
	public static final String NAME = "world_nether";
	
	public NetherWorld(Core core, World world)
	{
		super(core, world);
		
		registerSpawner(ZombieHordeNetherSpawner::new);
		registerSpawner(NetherFortressWitherSkeletonSpawner::new);
	}
}
