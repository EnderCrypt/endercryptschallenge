/**************************************************************************\
* EnderCrypt's Challenge                                                   *
* Copyright (C) 2023 by EnderCrypt                                         *
*                                                                          *
* This program is free software: you can redistribute it and/or modify     *
* it under the terms of the GNU Affero General Public License as           *
* published by the Free Software Foundation, either version 3 of           *
* the License, or (at your option) any later version.                      *
*                                                                          *
* This program is distributed in the hope that it will be useful,          *
* but WITHOUT ANY WARRANTY; without even the implied warranty of           *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
* GNU Affero General Public License for more details.                      *
*                                                                          *
* You should have received a copy of the GNU Affero General Public License *
* along with this program in its root directory.                           *
* If not, see <http://www.gnu.org/licenses/>.                              *
\**************************************************************************/

package endercrypt.endercryptschallenge.modules.worlds;


import endercrypt.endercryptschallenge.core.Core;
import endercrypt.endercryptschallenge.core.CoreCollective;
import endercrypt.endercryptschallenge.modules.worlds.spawner.factory.factories.SpawnerFactory;

import java.util.List;
import java.util.Set;

import org.bukkit.World;
import org.bukkit.entity.Player;


/**
 * @author EnderCrypt
 */
public abstract class WorldProfile extends CoreCollective
{
	public interface Factory
	{
		public WorldProfile create(Core core, World world);
	}
	
	private final World world;
	
	protected WorldProfile(Core core, World world)
	{
		super(core);
		this.world = world;
	}
	
	public World getRoot()
	{
		return world;
	}
	
	public List<Player> getPlayers()
	{
		return getRoot().getPlayers();
	}
	
	public abstract Set<SpawnerFactory> getSpawners();
}
