/**************************************************************************\
* EnderCrypt's Challenge                                                   *
* Copyright (C) 2023 by EnderCrypt                                         *
*                                                                          *
* This program is free software: you can redistribute it and/or modify     *
* it under the terms of the GNU Affero General Public License as           *
* published by the Free Software Foundation, either version 3 of           *
* the License, or (at your option) any later version.                      *
*                                                                          *
* This program is distributed in the hope that it will be useful,          *
* but WITHOUT ANY WARRANTY; without even the implied warranty of           *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
* GNU Affero General Public License for more details.                      *
*                                                                          *
* You should have received a copy of the GNU Affero General Public License *
* along with this program in its root directory.                           *
* If not, see <http://www.gnu.org/licenses/>.                              *
\**************************************************************************/

package endercrypt.endercryptschallenge.modules.worlds;


import endercrypt.endercryptschallenge.core.Core;
import endercrypt.endercryptschallenge.modules.worlds.spawner.factory.factories.SpawnerFactory;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import org.bukkit.World;

import lombok.NonNull;


/**
 * @author EnderCrypt
 */
public abstract class AbstractWorldProfile extends WorldProfile
{
	private final Set<SpawnerFactory> spawners = new HashSet<>();
	
	protected AbstractWorldProfile(Core core, World world)
	{
		super(core, world);
	}
	
	public void registerSpawner(@NonNull SpawnerFactory.Factory spawnerFactoryFactory)
	{
		registerSpawner(spawnerFactoryFactory.build(getCore()));
	}
	
	public void registerSpawner(@NonNull SpawnerFactory spawnerFactory)
	{
		this.spawners.add(spawnerFactory);
	}
	
	@Override
	public Set<SpawnerFactory> getSpawners()
	{
		return Collections.unmodifiableSet(spawners);
	}
}
