/**************************************************************************\
* EnderCrypt's Challenge                                                   *
* Copyright (C) 2023 by EnderCrypt                                         *
*                                                                          *
* This program is free software: you can redistribute it and/or modify     *
* it under the terms of the GNU Affero General Public License as           *
* published by the Free Software Foundation, either version 3 of           *
* the License, or (at your option) any later version.                      *
*                                                                          *
* This program is distributed in the hope that it will be useful,          *
* but WITHOUT ANY WARRANTY; without even the implied warranty of           *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
* GNU Affero General Public License for more details.                      *
*                                                                          *
* You should have received a copy of the GNU Affero General Public License *
* along with this program in its root directory.                           *
* If not, see <http://www.gnu.org/licenses/>.                              *
\**************************************************************************/

package endercrypt.endercryptschallenge.modules.worlds;


import endercrypt.endercryptschallenge.core.Core;
import endercrypt.endercryptschallenge.core.modules.CoreModule;
import endercrypt.endercryptschallenge.modules.worlds.list.end.EndWorld;
import endercrypt.endercryptschallenge.modules.worlds.list.nether.NetherWorld;
import endercrypt.endercryptschallenge.modules.worlds.list.over.OverWorld;
import endercrypt.endercryptschallenge.modules.worlds.list.unknown.UnknownWorld;

import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.NoSuchElementException;

import org.bukkit.World;


/**
 * @author EnderCrypt
 */
public class WorldManager extends CoreModule implements Iterable<WorldProfile>
{
	private Map<World, WorldProfile> worlds = new HashMap<>();
	
	public WorldManager(Core core)
	{
		super(core);
		
		server.getWorlds().forEach(this::resolve);
	}
	
	private WorldProfile.Factory getWorldGenerator(String worldName)
	{
		switch (worldName)
		{
		case OverWorld.NAME:
			return OverWorld::new;
		case NetherWorld.NAME:
			return NetherWorld::new;
		case EndWorld.NAME:
			return EndWorld::new;
		default:
			return UnknownWorld::new;
		}
	}
	
	private WorldProfile generateWorld(World world)
	{
		WorldProfile.Factory worldFactory = getWorldGenerator(world.getName());
		return worldFactory.create(getCore(), world);
	}
	
	public synchronized WorldProfile resolve(World world)
	{
		return worlds.computeIfAbsent(world, this::generateWorld);
	}
	
	@SuppressWarnings("unchecked")
	public <T extends WorldProfile> T resolve(Class<T> worldClass)
	{
		for (WorldProfile profile : worlds.values())
		{
			if (worldClass.isInstance(profile))
			{
				return (T) profile;
			}
		}
		throw new NoSuchElementException("unknown world: " + worldClass);
	}
	
	public OverWorld getOverWorld()
	{
		return resolve(OverWorld.class);
	}
	
	public NetherWorld getNetherWorld()
	{
		return resolve(NetherWorld.class);
	}
	
	public EndWorld getEndWorld()
	{
		return resolve(EndWorld.class);
	}
	
	@Override
	public Iterator<WorldProfile> iterator()
	{
		return Collections.unmodifiableCollection(worlds.values()).iterator();
	}
}
