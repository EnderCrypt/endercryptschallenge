/**************************************************************************\
* EnderCrypt's Challenge                                                   *
* Copyright (C) 2023 by EnderCrypt                                         *
*                                                                          *
* This program is free software: you can redistribute it and/or modify     *
* it under the terms of the GNU Affero General Public License as           *
* published by the Free Software Foundation, either version 3 of           *
* the License, or (at your option) any later version.                      *
*                                                                          *
* This program is distributed in the hope that it will be useful,          *
* but WITHOUT ANY WARRANTY; without even the implied warranty of           *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
* GNU Affero General Public License for more details.                      *
*                                                                          *
* You should have received a copy of the GNU Affero General Public License *
* along with this program in its root directory.                           *
* If not, see <http://www.gnu.org/licenses/>.                              *
\**************************************************************************/

package endercrypt.endercryptschallenge.modules.worlds.spawner.factory.block;


import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Objects;
import java.util.function.Predicate;


/**
 * @author EnderCrypt
 */
public abstract class BlockConditions<T>
{
	private final List<T> conditions = new ArrayList<>();
	
	public void addAll(Collection<T> conditions)
	{
		conditions.forEach(this::add);
	}
	
	public void add(T condition)
	{
		conditions.add(Objects.requireNonNull(condition, "condition"));
	}
	
	public boolean test(Predicate<T> predicate)
	{
		for (T condition : conditions)
		{
			if (predicate.test(condition) == false)
			{
				return false;
			}
		}
		
		return true;
	}
}
