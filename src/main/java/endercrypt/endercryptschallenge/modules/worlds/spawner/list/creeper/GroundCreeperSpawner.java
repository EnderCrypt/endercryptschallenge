/**************************************************************************\
* EnderCrypt's Challenge                                                   *
* Copyright (C) 2023 by EnderCrypt                                         *
*                                                                          *
* This program is free software: you can redistribute it and/or modify     *
* it under the terms of the GNU Affero General Public License as           *
* published by the Free Software Foundation, either version 3 of           *
* the License, or (at your option) any later version.                      *
*                                                                          *
* This program is distributed in the hope that it will be useful,          *
* but WITHOUT ANY WARRANTY; without even the implied warranty of           *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
* GNU Affero General Public License for more details.                      *
*                                                                          *
* You should have received a copy of the GNU Affero General Public License *
* along with this program in its root directory.                           *
* If not, see <http://www.gnu.org/licenses/>.                              *
\**************************************************************************/

package endercrypt.endercryptschallenge.modules.worlds.spawner.list.creeper;


import endercrypt.endercryptschallenge.core.Core;
import endercrypt.endercryptschallenge.features.entities.all.growing.GrowingMonsters;
import endercrypt.endercryptschallenge.modules.features.MinecraftFeatureManager;
import endercrypt.endercryptschallenge.modules.worlds.spawner.factory.SpawnCondition;
import endercrypt.endercryptschallenge.modules.worlds.spawner.factory.aligner.SpawnAligner;
import endercrypt.endercryptschallenge.modules.worlds.spawner.factory.aligner.StandardSpawnAligner;
import endercrypt.endercryptschallenge.modules.worlds.spawner.factory.factories.AbstractSpawnerFactory;
import endercrypt.endercryptschallenge.utility.Utility;
import endercrypt.endercryptschallenge.utility.difficulty.Difficulties;
import endercrypt.endercryptschallenge.utility.difficulty.DifficultyRating;

import org.bukkit.Location;
import org.bukkit.block.Block;
import org.bukkit.entity.Creeper;
import org.bukkit.entity.Player;


/**
 * @author EnderCrypt
 */
public class GroundCreeperSpawner extends AbstractSpawnerFactory
{
	public GroundCreeperSpawner(Core core)
	{
		super(core);
	}
	
	@Override
	protected double getMimimumDistance(Player player)
	{
		return 40;
	}
	
	@Override
	protected double getMaximumDistance(Player player)
	{
		return 100;
	}
	
	@Override
	protected double getChance(Player player)
	{
		return 0.5;
	}
	
	@Override
	protected SpawnAligner getSpawnAligner()
	{
		return new CreeperSpawnAligner();
	}
	
	@Override
	public void spawn(Player player, Location location)
	{
		Block block = location.getBlock();
		
		if (GrowingMonsters.isValidGrowingBlock(block, false, 2))
		{
			GrowingMonsters growingMonsters = getCore().getModuleManager().fetchModule(MinecraftFeatureManager.class).getFeature(GrowingMonsters.class);
			growingMonsters.create(Creeper.class, block, 5);
			return;
		}
		
		location.getWorld().spawn(location, Creeper.class);
	}
	
	private static final class CreeperSpawnAligner extends StandardSpawnAligner
	{
		@Override
		public boolean align(SpawnCondition condition, Location location)
		{
			DifficultyRating difficulty = Difficulties.getFor(location);
			int prefferedDistance = difficulty.asIntValue(45, 30, 20);
			
			Creeper nearbyCreeper = Utility.getAnyEntityWhitinRange(location, Creeper.class, prefferedDistance);
			if (nearbyCreeper != null)
				return false;
			
			if (super.align(condition, location) == false)
				return false;
			
			return true;
		}
	}
}
