/**************************************************************************\
* EnderCrypt's Challenge                                                   *
* Copyright (C) 2023 by EnderCrypt                                         *
*                                                                          *
* This program is free software: you can redistribute it and/or modify     *
* it under the terms of the GNU Affero General Public License as           *
* published by the Free Software Foundation, either version 3 of           *
* the License, or (at your option) any later version.                      *
*                                                                          *
* This program is distributed in the hope that it will be useful,          *
* but WITHOUT ANY WARRANTY; without even the implied warranty of           *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
* GNU Affero General Public License for more details.                      *
*                                                                          *
* You should have received a copy of the GNU Affero General Public License *
* along with this program in its root directory.                           *
* If not, see <http://www.gnu.org/licenses/>.                              *
\**************************************************************************/

package endercrypt.endercryptschallenge.modules.worlds.spawner.list.witherskeleton;


import endercrypt.endercryptschallenge.core.Core;
import endercrypt.endercryptschallenge.materials.MaterialCollections;
import endercrypt.endercryptschallenge.modules.worlds.list.nether.NetherWorld;
import endercrypt.endercryptschallenge.modules.worlds.spawner.factory.SpawnCondition;
import endercrypt.endercryptschallenge.modules.worlds.spawner.factory.factories.AbstractSpawnerFactory;
import endercrypt.endercryptschallenge.utility.Utility;
import endercrypt.endercryptschallenge.utility.difficulty.Difficulties;
import endercrypt.endercryptschallenge.utility.difficulty.DifficultyRating;

import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.entity.WitherSkeleton;


/**
 * @author EnderCrypt
 */
public class NetherFortressWitherSkeletonSpawner extends AbstractSpawnerFactory
{
	public NetherFortressWitherSkeletonSpawner(Core core)
	{
		super(core);
	}
	
	@Override
	protected double getMimimumDistance(Player player)
	{
		return 15;
	}
	
	@Override
	protected double getMaximumDistance(Player player)
	{
		return 25;
	}
	
	@Override
	protected double getChance(Player player)
	{
		if (player.getWorld().getName().equals(NetherWorld.NAME) == false)
		{
			return 0.0;
		}
		
		int nearbyBricks = (int) Utility.getBlockQube(player.getLocation().getBlock(), 3)
			.stream()
			.filter(MaterialCollections.netherBricks::contains)
			.count();
		
		if (nearbyBricks < 5)
		{
			return 0.02;
		}
		
		DifficultyRating difficulty = Difficulties.getFor(player);
		return difficulty.asDoubleValue(0.075, 0.100, 0.200);
	}
	
	@Override
	protected int getMaximumLightLevel(Player player)
	{
		return SpawnCondition.MAX_LIGHT_LEVEL;
	}
	
	@Override
	public void spawn(Player player, Location location)
	{
		location.getWorld().spawn(location, WitherSkeleton.class);
	}
}
