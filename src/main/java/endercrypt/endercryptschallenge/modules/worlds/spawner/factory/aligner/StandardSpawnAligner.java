/**************************************************************************\
* EnderCrypt's Challenge                                                   *
* Copyright (C) 2023 by EnderCrypt                                         *
*                                                                          *
* This program is free software: you can redistribute it and/or modify     *
* it under the terms of the GNU Affero General Public License as           *
* published by the Free Software Foundation, either version 3 of           *
* the License, or (at your option) any later version.                      *
*                                                                          *
* This program is distributed in the hope that it will be useful,          *
* but WITHOUT ANY WARRANTY; without even the implied warranty of           *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
* GNU Affero General Public License for more details.                      *
*                                                                          *
* You should have received a copy of the GNU Affero General Public License *
* along with this program in its root directory.                           *
* If not, see <http://www.gnu.org/licenses/>.                              *
\**************************************************************************/

package endercrypt.endercryptschallenge.modules.worlds.spawner.factory.aligner;


import endercrypt.endercryptschallenge.modules.worlds.spawner.factory.SpawnCondition;
import endercrypt.endercryptschallenge.utility.Utility;

import org.bukkit.Location;


/**
 * @author EnderCrypt
 */
public class StandardSpawnAligner extends SpawnAligner
{
	private static final int DEFAULT_TOLERANCE = 16;
	private final int tolerance;
	
	public StandardSpawnAligner()
	{
		this(DEFAULT_TOLERANCE);
	}
	
	public StandardSpawnAligner(int tolerance)
	{
		this.tolerance = tolerance;
	}
	
	@Override
	public boolean align(SpawnCondition condition, Location location)
	{
		if (condition.isValidSpawnLocation(location))
		{
			return true;
		}
		Location up = location.clone();
		Location down = location.clone();
		for (int i = 0; i < tolerance; i++)
		{
			if (condition.isValidSpawnLocation(up))
			{
				Utility.setLocation(location, up);
				return true;
			}
			up.add(0, 1, 0);
			
			if (condition.isValidSpawnLocation(down))
			{
				Utility.setLocation(location, down);
				return true;
			}
			down.add(0, -1, 0);
		}
		return false;
	}
}
