/**************************************************************************\
* EnderCrypt's Challenge                                                   *
* Copyright (C) 2023 by EnderCrypt                                         *
*                                                                          *
* This program is free software: you can redistribute it and/or modify     *
* it under the terms of the GNU Affero General Public License as           *
* published by the Free Software Foundation, either version 3 of           *
* the License, or (at your option) any later version.                      *
*                                                                          *
* This program is distributed in the hope that it will be useful,          *
* but WITHOUT ANY WARRANTY; without even the implied warranty of           *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
* GNU Affero General Public License for more details.                      *
*                                                                          *
* You should have received a copy of the GNU Affero General Public License *
* along with this program in its root directory.                           *
* If not, see <http://www.gnu.org/licenses/>.                              *
\**************************************************************************/

package endercrypt.endercryptschallenge.modules.worlds.spawner.factory.factories;


import endercrypt.endercryptschallenge.core.Core;
import endercrypt.endercryptschallenge.core.CoreCollective;
import endercrypt.endercryptschallenge.modules.worlds.spawner.factory.SpawnAutomator;

import org.bukkit.Location;
import org.bukkit.entity.Player;


/**
 * @author EnderCrypt
 */
public abstract class SpawnerFactory extends CoreCollective
{
	public interface Factory
	{
		public SpawnerFactory build(Core core);
	}
	
	public SpawnerFactory(Core core)
	{
		super(core);
	}
	
	public abstract SpawnAutomator generate(Player player);
	
	public abstract void spawn(Player player, Location location);
	
	@Override
	public String toString()
	{
		return getClass().getSimpleName();
	}
}
