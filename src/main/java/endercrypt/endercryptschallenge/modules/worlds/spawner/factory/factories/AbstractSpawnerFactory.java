/**************************************************************************\
* EnderCrypt's Challenge                                                   *
* Copyright (C) 2023 by EnderCrypt                                         *
*                                                                          *
* This program is free software: you can redistribute it and/or modify     *
* it under the terms of the GNU Affero General Public License as           *
* published by the Free Software Foundation, either version 3 of           *
* the License, or (at your option) any later version.                      *
*                                                                          *
* This program is distributed in the hope that it will be useful,          *
* but WITHOUT ANY WARRANTY; without even the implied warranty of           *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
* GNU Affero General Public License for more details.                      *
*                                                                          *
* You should have received a copy of the GNU Affero General Public License *
* along with this program in its root directory.                           *
* If not, see <http://www.gnu.org/licenses/>.                              *
\**************************************************************************/

package endercrypt.endercryptschallenge.modules.worlds.spawner.factory.factories;


import endercrypt.endercryptschallenge.core.Core;
import endercrypt.endercryptschallenge.modules.worlds.spawner.factory.SpawnAutomator;
import endercrypt.endercryptschallenge.modules.worlds.spawner.factory.SpawnCondition;
import endercrypt.endercryptschallenge.modules.worlds.spawner.factory.aligner.SpawnAligner;
import endercrypt.endercryptschallenge.modules.worlds.spawner.factory.aligner.StandardSpawnAligner;

import org.bukkit.block.Block;
import org.bukkit.entity.Player;


/**
 * @author EnderCrypt
 */
public abstract class AbstractSpawnerFactory extends SpawnerFactory
{
	public AbstractSpawnerFactory(Core core)
	{
		super(core);
	}
	
	protected abstract double getMimimumDistance(Player player);
	
	protected abstract double getMaximumDistance(Player player);
	
	protected abstract double getChance(Player player);
	
	protected int getHeight()
	{
		return 2;
	}
	
	protected int getMaximumLightLevel(Player player)
	{
		return SpawnCondition.MAX_LIGHT_LEVEL;
	}
	
	protected SpawnAligner getSpawnAligner()
	{
		return new StandardSpawnAligner();
	}
	
	protected boolean testFloorBlock(Block block)
	{
		return true;
	}
	
	protected boolean testBodyBlock(Block block, int position)
	{
		return true;
	}
	
	@Override
	public final SpawnAutomator generate(Player player)
	{
		SpawnCondition spawnCondition = new SpawnCondition.Builder()
			.setMaximumLightLevel(getMaximumLightLevel(player))
			.setHeight(getHeight())
			.addFloorCondition(this::testFloorBlock)
			.addBodyCondition(this::testBodyBlock)
			.build();
		
		return new SpawnAutomator.Builder()
			.setCondition(spawnCondition)
			.setSpawnAligner(getSpawnAligner())
			.setRange(getMimimumDistance(player), getMaximumDistance(player))
			.setChance(getChance(player))
			.build();
	}
}
