/**************************************************************************\
* EnderCrypt's Challenge                                                   *
* Copyright (C) 2023 by EnderCrypt                                         *
*                                                                          *
* This program is free software: you can redistribute it and/or modify     *
* it under the terms of the GNU Affero General Public License as           *
* published by the Free Software Foundation, either version 3 of           *
* the License, or (at your option) any later version.                      *
*                                                                          *
* This program is distributed in the hope that it will be useful,          *
* but WITHOUT ANY WARRANTY; without even the implied warranty of           *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
* GNU Affero General Public License for more details.                      *
*                                                                          *
* You should have received a copy of the GNU Affero General Public License *
* along with this program in its root directory.                           *
* If not, see <http://www.gnu.org/licenses/>.                              *
\**************************************************************************/

package endercrypt.endercryptschallenge.modules.worlds.spawner.list.horde;


import endercrypt.endercryptschallenge.core.Core;
import endercrypt.endercryptschallenge.features.entities.mobs.zombie.hordes.HordeManager;
import endercrypt.endercryptschallenge.modules.worlds.spawner.factory.factories.AbstractSpawnerFactory;
import endercrypt.endercryptschallenge.utility.Utility;
import endercrypt.endercryptschallenge.utility.difficulty.Difficulties;
import endercrypt.endercryptschallenge.utility.difficulty.DifficultyRating;
import endercrypt.library.commons.misc.Ender;

import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.entity.Player;


/**
 * @author EnderCrypt
 */
public class ZombieHordeOverworldSpawner extends AbstractSpawnerFactory
{
	public static final int DEPTH = 50;
	
	public ZombieHordeOverworldSpawner(Core core)
	{
		super(core);
	}
	
	private double map(Player player, double min, double max)
	{
		double depth = Utility.getCanonicalDepth(player);
		if (depth < 0)
			depth = 0;
		if (depth > DEPTH)
			depth = DEPTH;
		return Ender.math.remap(depth, 0, DEPTH, min, max);
	}
	
	@Override
	protected double getMimimumDistance(Player player)
	{
		return map(player, 16 * 3, 16 * 1);
	}
	
	@Override
	protected double getMaximumDistance(Player player)
	{
		return getMimimumDistance(player) + 10;
	}
	
	@Override
	protected double getChance(Player player)
	{
		DifficultyRating difficulty = Difficulties.getFor(player);
		double min = difficulty.asDoubleValue(0.05, 0.15, 0.3);
		double max = difficulty.asDoubleValue(0.25, 0.35, 0.6);
		return map(player, min, max);
	}
	
	@Override
	protected int getMaximumLightLevel(Player player)
	{
		DifficultyRating difficulty = Difficulties.getFor(player);
		Location playerLocation = player.getLocation();
		int lightDrop = getLightLevelBoost(playerLocation, 3, 2);
		int baseLigthtLevel = difficulty.asIntValue(9, 12, 15);
		int lightLevel = baseLigthtLevel + lightDrop;
		// player.sendMessage("drop: " + lightDrop + " level: " + lightLevel + " at: " + playerLocation.getBlock().getLightLevel());
		return lightLevel;
	}
	
	private int getLightLevelBoost(Location location, int maxDrop, int blocksPerDrop)
	{
		World world = location.getWorld();
		int top = location.toHighestLocation().getBlockY();
		
		double lightDrop = 0;
		
		int x = location.getBlockX();
		int z = location.getBlockZ();
		for (int y = location.getBlockY(); (y <= top && lightDrop <= maxDrop); y++)
		{
			if (world.getBlockAt(x, y, z).isSolid())
			{
				lightDrop -= (1.0 / blocksPerDrop);
			}
		}
		
		if (lightDrop < -maxDrop)
		{
			lightDrop = -maxDrop;
		}
		
		return (int) Math.floor(lightDrop);
	}
	
	@Override
	public void spawn(Player player, Location location)
	{
		HordeManager hordeManager = getCore().getMinecraftFeatureManager().getFeature(HordeManager.class);
		hordeManager.spawnHorde(player, location);
	}
}
