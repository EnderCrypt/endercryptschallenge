/**************************************************************************\
* EnderCrypt's Challenge                                                   *
* Copyright (C) 2023 by EnderCrypt                                         *
*                                                                          *
* This program is free software: you can redistribute it and/or modify     *
* it under the terms of the GNU Affero General Public License as           *
* published by the Free Software Foundation, either version 3 of           *
* the License, or (at your option) any later version.                      *
*                                                                          *
* This program is distributed in the hope that it will be useful,          *
* but WITHOUT ANY WARRANTY; without even the implied warranty of           *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
* GNU Affero General Public License for more details.                      *
*                                                                          *
* You should have received a copy of the GNU Affero General Public License *
* along with this program in its root directory.                           *
* If not, see <http://www.gnu.org/licenses/>.                              *
\**************************************************************************/

package endercrypt.endercryptschallenge.modules.worlds.spawner.list.horde;


import endercrypt.endercryptschallenge.core.Core;
import endercrypt.endercryptschallenge.features.entities.mobs.zombie.hordes.HordeManager;
import endercrypt.endercryptschallenge.modules.worlds.spawner.factory.SpawnCondition;
import endercrypt.endercryptschallenge.modules.worlds.spawner.factory.factories.AbstractSpawnerFactory;
import endercrypt.endercryptschallenge.utility.difficulty.Difficulties;
import endercrypt.endercryptschallenge.utility.difficulty.DifficultyRating;

import org.bukkit.Location;
import org.bukkit.entity.Player;


/**
 * @author EnderCrypt
 */
public class ZombieHordeEndSpawner extends AbstractSpawnerFactory
{
	public ZombieHordeEndSpawner(Core core)
	{
		super(core);
	}
	
	@Override
	protected double getMimimumDistance(Player player)
	{
		return 10;
	}
	
	@Override
	protected double getMaximumDistance(Player player)
	{
		return 20;
	}
	
	@Override
	protected double getChance(Player player)
	{
		DifficultyRating difficulty = Difficulties.getFor(player);
		
		return difficulty.asDoubleValue(0.05, 0.15, 0.25);
	}
	
	@Override
	protected int getMaximumLightLevel(Player player)
	{
		return SpawnCondition.MAX_LIGHT_LEVEL;
	}
	
	@Override
	public void spawn(Player player, Location location)
	{
		HordeManager hordeManager = getCore().getMinecraftFeatureManager().getFeature(HordeManager.class);
		hordeManager.spawnHorde(player, location);
	}
}
