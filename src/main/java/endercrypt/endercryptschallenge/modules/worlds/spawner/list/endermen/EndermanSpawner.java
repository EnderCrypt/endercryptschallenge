/**************************************************************************\
* EnderCrypt's Challenge                                                   *
* Copyright (C) 2023 by EnderCrypt                                         *
*                                                                          *
* This program is free software: you can redistribute it and/or modify     *
* it under the terms of the GNU Affero General Public License as           *
* published by the Free Software Foundation, either version 3 of           *
* the License, or (at your option) any later version.                      *
*                                                                          *
* This program is distributed in the hope that it will be useful,          *
* but WITHOUT ANY WARRANTY; without even the implied warranty of           *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
* GNU Affero General Public License for more details.                      *
*                                                                          *
* You should have received a copy of the GNU Affero General Public License *
* along with this program in its root directory.                           *
* If not, see <http://www.gnu.org/licenses/>.                              *
\**************************************************************************/

package endercrypt.endercryptschallenge.modules.worlds.spawner.list.endermen;


import endercrypt.endercryptschallenge.core.Core;
import endercrypt.endercryptschallenge.features.entities.all.growing.GrowingMonsters;
import endercrypt.endercryptschallenge.modules.features.MinecraftFeatureManager;
import endercrypt.endercryptschallenge.modules.worlds.spawner.factory.factories.AbstractSpawnerFactory;

import org.bukkit.Location;
import org.bukkit.block.Block;
import org.bukkit.entity.Enderman;
import org.bukkit.entity.Player;


/**
 * @author EnderCrypt
 */
public class EndermanSpawner extends AbstractSpawnerFactory
{
	public EndermanSpawner(Core core)
	{
		super(core);
	}
	
	@Override
	protected double getMimimumDistance(Player player)
	{
		return 35;
	}
	
	@Override
	protected double getMaximumDistance(Player player)
	{
		return 100;
	}
	
	@Override
	protected double getChance(Player player)
	{
		return 0.02;
	}
	
	@Override
	public void spawn(Player player, Location location)
	{
		Block block = location.getBlock();
		
		if (GrowingMonsters.isValidGrowingBlock(block, false, 3))
		{
			getCore().getModuleManager()
				.fetchModule(MinecraftFeatureManager.class)
				.getFeature(GrowingMonsters.class)
				.create(Enderman.class, block, 5);
			
			return;
		}
		
		location.getWorld().spawn(location, Enderman.class);
	}
}
