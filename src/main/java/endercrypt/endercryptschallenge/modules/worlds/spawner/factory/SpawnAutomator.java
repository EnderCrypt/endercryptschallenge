/**************************************************************************\
* EnderCrypt's Challenge                                                   *
* Copyright (C) 2023 by EnderCrypt                                         *
*                                                                          *
* This program is free software: you can redistribute it and/or modify     *
* it under the terms of the GNU Affero General Public License as           *
* published by the Free Software Foundation, either version 3 of           *
* the License, or (at your option) any later version.                      *
*                                                                          *
* This program is distributed in the hope that it will be useful,          *
* but WITHOUT ANY WARRANTY; without even the implied warranty of           *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
* GNU Affero General Public License for more details.                      *
*                                                                          *
* You should have received a copy of the GNU Affero General Public License *
* along with this program in its root directory.                           *
* If not, see <http://www.gnu.org/licenses/>.                              *
\**************************************************************************/

package endercrypt.endercryptschallenge.modules.worlds.spawner.factory;


import endercrypt.endercryptschallenge.modules.worlds.spawner.factory.aligner.SpawnAligner;
import endercrypt.endercryptschallenge.modules.worlds.spawner.factory.aligner.StandardSpawnAligner;
import endercrypt.endercryptschallenge.utility.Utility;
import endercrypt.library.commons.misc.Ender;

import java.util.Objects;
import java.util.Optional;

import org.bukkit.Location;
import org.bukkit.entity.Player;


/**
 * @author EnderCrypt
 */
public class SpawnAutomator
{
	private static final int SPAWN_LOCATION_RETRIES = 5;
	
	private final SpawnCondition spawnCondition;
	private final SpawnAligner spawnAligner;
	private final double minimumDistance;
	private final double maximumDistance;
	private final double chance;
	
	private SpawnAutomator(Builder builder)
	{
		this.spawnCondition = Objects.requireNonNull(builder.spawnCondition, "spawnCondition");
		this.spawnAligner = Objects.requireNonNull(builder.spawnAligner, "spawnAligner");
		this.minimumDistance = Objects.requireNonNull(builder.minimumDistance, "minimumDistance");
		this.maximumDistance = Objects.requireNonNull(builder.maximumDistance, ".maximumDistance");
		this.chance = Objects.requireNonNull(builder.chance, "chance");
	}
	
	public double getHardMinimumDistance()
	{
		return getMinimumDistance() * 0.75;
	}
	
	public double getMinimumDistance()
	{
		return minimumDistance;
	}
	
	public double getMaximumDistance()
	{
		return maximumDistance;
	}
	
	public double generateDistance()
	{
		return Ender.math.randomRange(getMinimumDistance(), getMaximumDistance());
	}
	
	public double getChance()
	{
		return chance;
	}
	
	public Optional<Location> findSpawnSpot(Player player)
	{
		return findSpawnSpot(player.getLocation());
	}
	
	public Optional<Location> findSpawnSpot(Location location)
	{
		if (Math.random() > getChance())
		{
			return Optional.empty();
		}
		for (int i = 0; i < SPAWN_LOCATION_RETRIES; i++)
		{
			Location potential = tryFindSpawnSpot(location).orElse(null);
			if (potential != null)
			{
				return Optional.of(potential);
			}
		}
		return Optional.empty();
	}
	
	private Optional<Location> tryFindSpawnSpot(Location location)
	{
		double spawnDistance = generateDistance();
		Location potential = Utility.randomlyShiftOnPlane(location, spawnDistance);
		if (location.isChunkLoaded() == false)
		{
			return Optional.empty();
		}
		if (spawnAligner.align(spawnCondition, potential) == false)
		{
			return Optional.empty();
		}
		Player closestPlayer = Utility.getClosestPlayer(potential).get();
		double closestPlayerDistance = potential.distance(closestPlayer.getLocation());
		if (closestPlayerDistance < getHardMinimumDistance())
		{
			return Optional.empty();
		}
		return Optional.of(potential);
	}
	
	@Override
	public String toString()
	{
		return "SpawnAutomator [minimumDistance=" + Ender.math.round(minimumDistance, 1) + ", maximumDistance=" + Ender.math.round(maximumDistance, 1) + ", chance=" + Ender.math.round(chance * 100.0, 1) + "%]";
	}
	
	public static class Builder
	{
		private SpawnCondition spawnCondition;
		
		public Builder setCondition(SpawnCondition spawnCondition)
		{
			this.spawnCondition = spawnCondition;
			return this;
		}
		
		private SpawnAligner spawnAligner = new StandardSpawnAligner();
		
		public Builder setSpawnAligner(SpawnAligner spawnAligner)
		{
			this.spawnAligner = spawnAligner;
			return this;
		}
		
		private Double minimumDistance;
		
		public Builder setMinimumDistance(double minimumDistance)
		{
			this.minimumDistance = minimumDistance;
			return this;
		}
		
		private Double maximumDistance;
		
		public Builder setMaximumDistance(double maximumDistance)
		{
			this.maximumDistance = maximumDistance;
			return this;
		}
		
		public Builder setRange(double minimumDistance, double maximumDistance)
		{
			setMinimumDistance(minimumDistance);
			setMaximumDistance(maximumDistance);
			return this;
		}
		
		private Double chance;
		
		public Builder setChance(double chance)
		{
			this.chance = chance;
			return this;
		}
		
		public SpawnAutomator build()
		{
			return new SpawnAutomator(this);
		}
	}
}
