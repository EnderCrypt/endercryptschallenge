/**************************************************************************\
* EnderCrypt's Challenge                                                   *
* Copyright (C) 2023 by EnderCrypt                                         *
*                                                                          *
* This program is free software: you can redistribute it and/or modify     *
* it under the terms of the GNU Affero General Public License as           *
* published by the Free Software Foundation, either version 3 of           *
* the License, or (at your option) any later version.                      *
*                                                                          *
* This program is distributed in the hope that it will be useful,          *
* but WITHOUT ANY WARRANTY; without even the implied warranty of           *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
* GNU Affero General Public License for more details.                      *
*                                                                          *
* You should have received a copy of the GNU Affero General Public License *
* along with this program in its root directory.                           *
* If not, see <http://www.gnu.org/licenses/>.                              *
\**************************************************************************/

package endercrypt.endercryptschallenge.modules.worlds.spawner.factory;


import endercrypt.endercryptschallenge.materials.MaterialCollections;
import endercrypt.endercryptschallenge.modules.worlds.spawner.factory.block.body.BlockBodyCondition;
import endercrypt.endercryptschallenge.modules.worlds.spawner.factory.block.body.BlockBodyConditions;
import endercrypt.endercryptschallenge.modules.worlds.spawner.factory.block.floor.BlockFloorCondition;
import endercrypt.endercryptschallenge.modules.worlds.spawner.factory.block.floor.BlockFloorConditions;
import endercrypt.endercryptschallenge.utility.Utility;

import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

import org.bukkit.Location;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;


/**
 * @author EnderCrypt
 */
public class SpawnCondition
{
	public static final int MAX_LIGHT_LEVEL = 15;
	
	private final int maximumLightLevel;
	private final int height;
	private final boolean allowWater;
	private final boolean requireFloor;
	private final BlockFloorConditions floorConditions = new BlockFloorConditions();
	private final BlockBodyConditions bodyConditions = new BlockBodyConditions();
	
	private SpawnCondition(Builder builder)
	{
		this.maximumLightLevel = Objects.requireNonNull(builder.maximumLightLevel, "builder.maximumLightLevel");
		this.height = Objects.requireNonNull(builder.height, "builder.height");
		this.allowWater = Objects.requireNonNull(builder.allowWater, "builder.allowWater");
		this.requireFloor = Objects.requireNonNull(builder.requireFloor, "builder.requireFloor");
		this.floorConditions.addAll(builder.floorConditions);
		this.bodyConditions.addAll(builder.bodyConditions);
	}
	
	public int getHeight()
	{
		return height;
	}
	
	public int getMaximumLightLevel()
	{
		return maximumLightLevel;
	}
	
	public boolean isAllowWater()
	{
		return allowWater;
	}
	
	public boolean isRequireFloor()
	{
		return requireFloor;
	}
	
	public boolean isValidSpawnLocation(Location location)
	{
		if (location == null)
			return false;
		
		// floor
		Block floorBlock = location.getBlock().getRelative(BlockFace.DOWN);
		if (isValidFloorBlock(floorBlock) == false)
			return false;
		
		Block bodyBlock = floorBlock;
		for (int i = 0; i < getHeight(); i++)
		{
			bodyBlock = bodyBlock.getRelative(BlockFace.UP);
			if (isValidBodyBlock(bodyBlock, i) == false)
			{
				return false;
			}
		}
		
		return true;
	}
	
	private boolean isValidFloorBlock(Block block)
	{
		if (isRequireFloor() && block.isPassable())
			return false;
		
		if (MaterialCollections.forbiddenSpawnFloor.contains(block))
			return false;
		
		if (floorConditions.test(block) == false)
			return false;
		
		return true;
	}
	
	private boolean isValidBodyBlock(Block block, int position)
	{
		if (position == 0 && block.getLightLevel() > maximumLightLevel)
			return false;
		
		if (block.isPassable() == false)
			return false;
		
		if (MaterialCollections.forbiddenSpawnBody.contains(block))
			return false;
		
		if (isAllowWater() == false && Utility.hasWater(block))
			return false;
		
		if (bodyConditions.test(block, position) == false)
			return false;
		
		return true;
	}
	
	@Override
	public String toString()
	{
		return "SpawnCondition [maximumLightLevel=" + maximumLightLevel + ", height=" + height + "]";
	}
	
	public static class Builder
	{
		private Integer maximumLightLevel;
		
		public Builder setMaximumLightLevel(int maximumLightLevel)
		{
			if (maximumLightLevel < 0 || maximumLightLevel > MAX_LIGHT_LEVEL)
			{
				throw new IllegalArgumentException("light level cannot be " + maximumLightLevel);
			}
			this.maximumLightLevel = maximumLightLevel;
			return this;
		}
		
		public Builder setAnyLightLevel()
		{
			setMaximumLightLevel(MAX_LIGHT_LEVEL);
			return this;
		}
		
		private int height = 2;
		
		public Builder setHeight(int height)
		{
			if (height <= 0)
			{
				throw new IllegalArgumentException("height cannot be " + height);
			}
			this.height = height;
			return this;
		}
		
		private boolean allowWater = true;
		
		public Builder setAllowWater(boolean allowWater)
		{
			this.allowWater = allowWater;
			return this;
		}
		
		private boolean requireFloor = true;
		
		public Builder setRequireFloor(boolean requireFloor)
		{
			this.requireFloor = requireFloor;
			return this;
		}
		
		private Set<BlockFloorCondition> floorConditions = new HashSet<>();
		
		public Builder addFloorCondition(BlockFloorCondition condition)
		{
			floorConditions.add(condition);
			return this;
		}
		
		private Set<BlockBodyCondition> bodyConditions = new HashSet<>();
		
		public Builder addBodyCondition(BlockBodyCondition condition)
		{
			bodyConditions.add(condition);
			return this;
		}
		
		public SpawnCondition build()
		{
			return new SpawnCondition(this);
		}
	}
}
