/**************************************************************************\
* EnderCrypt's Challenge                                                   *
* Copyright (C) 2023 by EnderCrypt                                         *
*                                                                          *
* This program is free software: you can redistribute it and/or modify     *
* it under the terms of the GNU Affero General Public License as           *
* published by the Free Software Foundation, either version 3 of           *
* the License, or (at your option) any later version.                      *
*                                                                          *
* This program is distributed in the hope that it will be useful,          *
* but WITHOUT ANY WARRANTY; without even the implied warranty of           *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
* GNU Affero General Public License for more details.                      *
*                                                                          *
* You should have received a copy of the GNU Affero General Public License *
* along with this program in its root directory.                           *
* If not, see <http://www.gnu.org/licenses/>.                              *
\**************************************************************************/

package endercrypt.endercryptschallenge.bootstrap;


import java.io.InputStream;
import java.io.PrintStream;


/**
 * @author EnderCrypt
 */
public class InputStreamListener implements Runnable
{
	private final InputStream input;
	private final PrintStream printStream;
	private final StringBuilder buffer = new StringBuilder();
	
	public InputStreamListener(InputStream input, PrintStream printStream)
	{
		this.input = input;
		this.printStream = printStream;
	}
	
	@Override
	public void run()
	{
		try
		{
			while (true)
			{
				int i = input.read();
				char c = (char) i;
				if (i == -1)
				{
					return;
				}
				if (c == '\n')
				{
					printBuffer();
					continue;
				}
				buffer.append(c);
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			printBuffer();
		}
	}
	
	private void printBuffer()
	{
		if (buffer.length() > 0)
		{
			printStream.println(buffer);
			buffer.setLength(0);
		}
	}
}
