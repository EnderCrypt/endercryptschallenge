/**************************************************************************\
* EnderCrypt's Challenge                                                   *
* Copyright (C) 2023 by EnderCrypt                                         *
*                                                                          *
* This program is free software: you can redistribute it and/or modify     *
* it under the terms of the GNU Affero General Public License as           *
* published by the Free Software Foundation, either version 3 of           *
* the License, or (at your option) any later version.                      *
*                                                                          *
* This program is distributed in the hope that it will be useful,          *
* but WITHOUT ANY WARRANTY; without even the implied warranty of           *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
* GNU Affero General Public License for more details.                      *
*                                                                          *
* You should have received a copy of the GNU Affero General Public License *
* along with this program in its root directory.                           *
* If not, see <http://www.gnu.org/licenses/>.                              *
\**************************************************************************/

package endercrypt.endercryptschallenge.bootstrap;


import java.io.OutputStream;
import java.util.Scanner;


/**
 * @author EnderCrypt
 */
public class OutputStreamListener implements Runnable
{
	private static final Scanner scanner = new Scanner(System.in);
	
	private final OutputStream output;
	
	public OutputStreamListener(OutputStream output)
	{
		this.output = output;
	}
	
	@Override
	public void run()
	{
		try
		{
			while (true)
			{
				String input = scanner.nextLine();
				String command = input + "\n";
				output.write(command.getBytes());
				output.flush();
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}
}
