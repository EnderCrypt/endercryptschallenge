/**************************************************************************\
* EnderCrypt's Challenge                                                   *
* Copyright (C) 2023 by EnderCrypt                                         *
*                                                                          *
* This program is free software: you can redistribute it and/or modify     *
* it under the terms of the GNU Affero General Public License as           *
* published by the Free Software Foundation, either version 3 of           *
* the License, or (at your option) any later version.                      *
*                                                                          *
* This program is distributed in the hope that it will be useful,          *
* but WITHOUT ANY WARRANTY; without even the implied warranty of           *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
* GNU Affero General Public License for more details.                      *
*                                                                          *
* You should have received a copy of the GNU Affero General Public License *
* along with this program in its root directory.                           *
* If not, see <http://www.gnu.org/licenses/>.                              *
\**************************************************************************/

package endercrypt.endercryptschallenge.bootstrap;


import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


/**
 * @author EnderCrypt
 */
public class BinaryExecutor
{
	public static void run(String file, String[] args) throws IOException, InterruptedException
	{
		Path workingDirectory = Paths.get(System.getProperty("user.dir"));
		Path startScript = workingDirectory.resolve(file);
		List<String> processArguments = new ArrayList<>();
		processArguments.add(startScript.toString());
		processArguments.addAll(Arrays.asList(args));
		ProcessBuilder processBuilder = new ProcessBuilder(processArguments);
		processBuilder.directory(workingDirectory.toFile());
		final Process process = processBuilder.start();
		
		Thread threadIn = new Thread(new InputStreamListener(process.getInputStream(), System.out));
		threadIn.start();
		
		Thread threadErr = new Thread(new InputStreamListener(process.getErrorStream(), System.err));
		threadErr.start();
		
		Thread threadOut = new Thread(new OutputStreamListener(process.getOutputStream()));
		threadOut.start();
		
		System.exit(printExit(process));
	}
	
	private static int printExit(Process process) throws InterruptedException
	{
		int exitCode = process.waitFor();
		System.out.println("Exited with exit code: " + exitCode);
		return exitCode;
	}
}
