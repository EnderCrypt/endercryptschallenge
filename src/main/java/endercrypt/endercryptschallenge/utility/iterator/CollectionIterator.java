/**************************************************************************\
* EnderCrypt's Challenge                                                   *
* Copyright (C) 2023 by EnderCrypt                                         *
*                                                                          *
* This program is free software: you can redistribute it and/or modify     *
* it under the terms of the GNU Affero General Public License as           *
* published by the Free Software Foundation, either version 3 of           *
* the License, or (at your option) any later version.                      *
*                                                                          *
* This program is distributed in the hope that it will be useful,          *
* but WITHOUT ANY WARRANTY; without even the implied warranty of           *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
* GNU Affero General Public License for more details.                      *
*                                                                          *
* You should have received a copy of the GNU Affero General Public License *
* along with this program in its root directory.                           *
* If not, see <http://www.gnu.org/licenses/>.                              *
\**************************************************************************/

package endercrypt.endercryptschallenge.utility.iterator;


import java.util.Collection;
import java.util.Iterator;


/**
 * @author EnderCrypt
 */
public abstract class CollectionIterator<T> implements Iterator<T>
{
	private final Iterator<T> iterator;
	
	protected CollectionIterator(Collection<T> items)
	{
		this.iterator = items.iterator();
	}
	
	@Override
	public boolean hasNext()
	{
		return iterator.hasNext();
	}
	
	@Override
	public T next()
	{
		return iterator.next();
	}
}
