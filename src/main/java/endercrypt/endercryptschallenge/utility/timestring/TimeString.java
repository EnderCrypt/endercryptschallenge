/**************************************************************************\
* EnderCrypt's Challenge                                                   *
* Copyright (C) 2023 by EnderCrypt                                         *
*                                                                          *
* This program is free software: you can redistribute it and/or modify     *
* it under the terms of the GNU Affero General Public License as           *
* published by the Free Software Foundation, either version 3 of           *
* the License, or (at your option) any later version.                      *
*                                                                          *
* This program is distributed in the hope that it will be useful,          *
* but WITHOUT ANY WARRANTY; without even the implied warranty of           *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
* GNU Affero General Public License for more details.                      *
*                                                                          *
* You should have received a copy of the GNU Affero General Public License *
* along with this program in its root directory.                           *
* If not, see <http://www.gnu.org/licenses/>.                              *
\**************************************************************************/

package endercrypt.endercryptschallenge.utility.timestring;


import endercrypt.library.commons.misc.Ender;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;


/**
 * @author EnderCrypt
 */
public class TimeString
{
	public static String fullTimeString(long start, long end)
	{
		return fullTimeString(end - start);
	}
	
	public static String fullTimeString(long time)
	{
		TimeString timeString = new TimeString(time);
		return timeString.getTimeString();
	}
	
	public static String fullTimeStringWhitoutMs(long time)
	{
		TimeString timeString = new TimeString(time);
		return timeString.getTimeString(TimePiece.SECOND);
	}
	
	private boolean negative = false;
	private List<TimeLength> lengths = new ArrayList<>();
	
	public TimeString(long time)
	{
		if (time < 0)
		{
			this.negative = true;
			time = -time;
		}
		List<TimePiece> timePieces = TimePiece.getValues();
		for (int i = timePieces.size() - 1; i >= 0; i--)
		{
			TimePiece timePiece = timePieces.get(i);
			long timeLength = timePiece.getTime();
			long count = time / timeLength;
			time -= count * timeLength;
			TimeLength timeLengthObject = new TimeLength(timePiece, count);
			this.lengths.add(0, timeLengthObject);
		}
	}
	
	public boolean isNegative()
	{
		return this.negative;
	}
	
	public List<TimeLength> getHighest(int count)
	{
		List<TimeLength> results = new ArrayList<>();
		for (int i = this.lengths.size() - 1; i >= 0; i--)
		{
			TimeLength timeLength = this.lengths.get(i);
			if (timeLength.getCount() > 0)
			{
				results.add(timeLength);
				if (results.size() >= count)
				{
					break;
				}
			}
		}
		return results;
	}
	
	public List<TimeLength> getTimeLengths(TimePiece minimum)
	{
		List<TimeLength> results = new ArrayList<>();
		for (int i = this.lengths.size() - 1; i >= 0; i--)
		{
			TimeLength timeLength = this.lengths.get(i);
			if (timeLength.getTimePiece().getTime() >= minimum.getTime() && timeLength.getCount() > 0)
			{
				results.add(timeLength);
			}
		}
		return results;
	}
	
	public String getTimeString(TimePiece minimum)
	{
		return generateTimeString(getTimeLengths(minimum));
	}
	
	public String getTimeString()
	{
		return getHighestToString(getTimeLengthCount());
	}
	
	public String getHighestToString(int count)
	{
		return generateTimeString(getHighest(count));
	}
	
	private String generateTimeString(List<TimeLength> timeLengths)
	{
		Iterator<TimeLength> iterator = timeLengths.iterator();
		StringBuilder sb = new StringBuilder();
		if (isNegative())
		{
			sb.append("negative");
			if (iterator.hasNext())
			{
				sb.append(" ");
			}
		}
		if (iterator.hasNext() == false)
		{
			sb.append("no time");
		}
		List<String> timeStrings = new ArrayList<>();
		
		iterator.forEachRemaining((t) -> {
			timeStrings.add(t.getCount() + " " + t.getTimePiece().getName(t.getCount()));
		});
		sb.append(Ender.text.friendlyJoin(timeStrings));
		return sb.toString();
	}
	
	public Optional<TimeLength> getHighest()
	{
		return getHighest(1).stream().findFirst();
	}
	
	public TimeLength get(TimePiece timePiece)
	{
		return this.lengths.stream().filter(t -> t.getTimePiece() == timePiece).findFirst().get();
	}
	
	public TimeLength get(int index)
	{
		return this.lengths.get(index);
	}
	
	public List<TimeLength> getTimeLengths()
	{
		return Collections.unmodifiableList(this.lengths);
	}
	
	public int getTimeLengthCount()
	{
		return this.lengths.size();
	}
	
	@Override
	public String toString()
	{
		return getClass().getSimpleName() + "[" + this.lengths.toString() + "]";
	}
}
