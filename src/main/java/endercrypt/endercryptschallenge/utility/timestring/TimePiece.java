/**************************************************************************\
* EnderCrypt's Challenge                                                   *
* Copyright (C) 2023 by EnderCrypt                                         *
*                                                                          *
* This program is free software: you can redistribute it and/or modify     *
* it under the terms of the GNU Affero General Public License as           *
* published by the Free Software Foundation, either version 3 of           *
* the License, or (at your option) any later version.                      *
*                                                                          *
* This program is distributed in the hope that it will be useful,          *
* but WITHOUT ANY WARRANTY; without even the implied warranty of           *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
* GNU Affero General Public License for more details.                      *
*                                                                          *
* You should have received a copy of the GNU Affero General Public License *
* along with this program in its root directory.                           *
* If not, see <http://www.gnu.org/licenses/>.                              *
\**************************************************************************/

package endercrypt.endercryptschallenge.utility.timestring;


import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;


/**
 * @author EnderCrypt
 */
public class TimePiece
{
	private static List<TimePiece> pieces = new ArrayList<>();
	
	public static final TimePiece MILLISECOND = new TimePiece("ms", 1);
	public static final TimePiece SECOND = new TimePiece("second", MILLISECOND.getTime() * 1000);
	public static final TimePiece MINUTE = new TimePiece("minute", SECOND.getTime() * 60);
	public static final TimePiece HOUR = new TimePiece("hour", MINUTE.getTime() * 60);
	public static final TimePiece DAY = new TimePiece("day", HOUR.getTime() * 24);
	public static final TimePiece MONTH = new TimePiece("month", (long) (DAY.getTime() * 365.2422 / 12));
	public static final TimePiece YEAR = new TimePiece("year", (long) (DAY.getTime() * 365.2422));
	
	static
	{
		pieces.sort(new Comparator<TimePiece>()
		{
			@Override
			public int compare(TimePiece t1, TimePiece t2)
			{
				return Long.compare(t1.getTime(), t2.getTime());
			}
		});
	}
	
	public static List<TimePiece> getValues()
	{
		return Collections.unmodifiableList(pieces);
	}
	
	private String name;
	private long time;
	
	private TimePiece(String name, long time)
	{
		this.name = name;
		this.time = time;
		pieces.add(this);
	}
	
	public long getTime()
	{
		return this.time;
	}
	
	public String getName()
	{
		return this.name;
	}
	
	public String getName(long count)
	{
		if (getName().endsWith("s"))
		{
			return getName();
		}
		return count == 1 ? getName() : getName() + "s";
	}
}
