/**************************************************************************\
* EnderCrypt's Challenge                                                   *
* Copyright (C) 2023 by EnderCrypt                                         *
*                                                                          *
* This program is free software: you can redistribute it and/or modify     *
* it under the terms of the GNU Affero General Public License as           *
* published by the Free Software Foundation, either version 3 of           *
* the License, or (at your option) any later version.                      *
*                                                                          *
* This program is distributed in the hope that it will be useful,          *
* but WITHOUT ANY WARRANTY; without even the implied warranty of           *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
* GNU Affero General Public License for more details.                      *
*                                                                          *
* You should have received a copy of the GNU Affero General Public License *
* along with this program in its root directory.                           *
* If not, see <http://www.gnu.org/licenses/>.                              *
\**************************************************************************/

package endercrypt.endercryptschallenge.utility;


import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.stream.Collectors;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.EntityEquipment;
import org.bukkit.inventory.EquipmentSlot;
import org.bukkit.inventory.ItemStack;


/**
 * @author EnderCrypt
 */
public class ArmourUtility
{
	private static final Map<String, Double> materialBaseWeight = new HashMap<>();
	static
	{
		materialBaseWeight.put("leather", 3.0);
		materialBaseWeight.put("turtle", 5.0);
		materialBaseWeight.put("chainmail", 7.0);
		materialBaseWeight.put("iron", 15.0);
		materialBaseWeight.put("golden", 25.0);
		materialBaseWeight.put("diamond", 30.0);
		materialBaseWeight.put("netherite", 35.0);
	}
	private static final Map<String, Double> typeWeightModifier = new HashMap<>();
	static
	{
		typeWeightModifier.put("boots", 0.25);
		typeWeightModifier.put("leggings", 1.25);
		typeWeightModifier.put("chestplate", 2.0);
		typeWeightModifier.put("helmet", 0.5);
	}
	
	public static List<Material> getPlayerArmour(EntityEquipment equipment)
	{
		List<EquipmentSlot> equipmentSlots = Arrays.asList(EquipmentSlot.FEET, EquipmentSlot.LEGS, EquipmentSlot.CHEST, EquipmentSlot.HEAD);
		
		return equipmentSlots.stream()
			.map(equipment::getItem)
			.filter(Objects::nonNull)
			.map(ItemStack::getType)
			.collect(Collectors.toList());
	}
	
	private static double materialIdDetect(Map<String, Double> weights, Material material, MaterialComparator comparator)
	{
		for (Entry<String, Double> entry : weights.entrySet())
		{
			String name = entry.getKey();
			double weight = entry.getValue();
			if (comparator.check(material.name(), name.toUpperCase()))
			{
				return weight;
			}
		}
		return 0.0;
	}
	
	private interface MaterialComparator
	{
		public boolean check(String id, String value);
	}
	
	public static double getArmourWeight(Material material)
	{
		return materialIdDetect(materialBaseWeight, material, (id, value) -> id.startsWith(value + "_")) * materialIdDetect(typeWeightModifier, material, (id, value) -> id.endsWith("_" + value));
	}
	
	public static double getPlayerArmourWeight(Player player)
	{
		return getPlayerArmour(player.getEquipment())
			.stream()
			.mapToDouble(ArmourUtility::getArmourWeight)
			.sum();
	}
}
