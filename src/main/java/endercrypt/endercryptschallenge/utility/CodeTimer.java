/**************************************************************************\
* EnderCrypt's Challenge                                                   *
* Copyright (C) 2023 by EnderCrypt                                         *
*                                                                          *
* This program is free software: you can redistribute it and/or modify     *
* it under the terms of the GNU Affero General Public License as           *
* published by the Free Software Foundation, either version 3 of           *
* the License, or (at your option) any later version.                      *
*                                                                          *
* This program is distributed in the hope that it will be useful,          *
* but WITHOUT ANY WARRANTY; without even the implied warranty of           *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
* GNU Affero General Public License for more details.                      *
*                                                                          *
* You should have received a copy of the GNU Affero General Public License *
* along with this program in its root directory.                           *
* If not, see <http://www.gnu.org/licenses/>.                              *
\**************************************************************************/

package endercrypt.endercryptschallenge.utility;


import java.util.Objects;
import java.util.logging.Logger;


/**
 * @author EnderCrypt
 */
public class CodeTimer
{
	private final long start = System.currentTimeMillis();
	private final String name;
	private final LoggerFunction infoLogger;
	private final LoggerFunction warningLogger;
	
	public CodeTimer(String name)
	{
		this(name, LoggerFunction.silent, System.err::println);
	}
	
	public CodeTimer(String name, Logger logger)
	{
		this(name, logger::fine, logger::warning);
	}
	
	/**
	 * preffer using the builtin minecraft logger provided by
	 * bukkit/spigot/paper instead
	 */
	@Deprecated
	public CodeTimer(String name, org.apache.logging.log4j.Logger logger)
	{
		this(name, logger::info, logger::warn);
	}
	
	public CodeTimer(String name, LoggerFunction infoLogger, LoggerFunction warningLogger)
	{
		this.name = Objects.requireNonNull(name, "name");
		this.infoLogger = Objects.requireNonNull(infoLogger, "infoLogger");
		this.warningLogger = Objects.requireNonNull(warningLogger, "warningLogger");
	}
	
	public String getName()
	{
		return name;
	}
	
	public int check()
	{
		return check(0);
	}
	
	public int check(int warn)
	{
		int time = (int) (System.currentTimeMillis() - start);
		infoLogger.log(getName() + " took " + time + " ns");
		if (time > warn)
		{
			warningLogger.log(getName() + " took " + time + " ms");
		}
		return time;
	}
	
	public interface LoggerFunction
	{
		public static final LoggerFunction silent = (m) -> {
			// do nothing
		};
		
		public void log(String message);
	}
}
