/**************************************************************************\
* EnderCrypt's Challenge                                                   *
* Copyright (C) 2023 by EnderCrypt                                         *
*                                                                          *
* This program is free software: you can redistribute it and/or modify     *
* it under the terms of the GNU Affero General Public License as           *
* published by the Free Software Foundation, either version 3 of           *
* the License, or (at your option) any later version.                      *
*                                                                          *
* This program is distributed in the hope that it will be useful,          *
* but WITHOUT ANY WARRANTY; without even the implied warranty of           *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
* GNU Affero General Public License for more details.                      *
*                                                                          *
* You should have received a copy of the GNU Affero General Public License *
* along with this program in its root directory.                           *
* If not, see <http://www.gnu.org/licenses/>.                              *
\**************************************************************************/

package endercrypt.endercryptschallenge.utility;


import endercrypt.endercryptschallenge.materials.MaterialCollections;
import endercrypt.endercryptschallenge.materials.structure.MaterialCollection;

import java.util.EnumMap;
import java.util.Map;
import java.util.Objects;
import java.util.stream.StreamSupport;

import org.bukkit.Material;
import org.bukkit.inventory.Inventory;


/**
 * @author EnderCrypt
 */
public class ItemValues
{
	private static final double DEFAULT_VALUE = 1.0;
	private static final Map<Material, Double> values = new EnumMap<>(Material.class);
	
	public static void set(MaterialCollection materials, double value)
	{
		materials.forEach(material -> set(material, value));
	}
	
	public static void set(Material material, double value)
	{
		Objects.requireNonNull(material, "material");
		if (value < 0)
		{
			throw new IllegalArgumentException("value for " + material + " cannot be null");
		}
		values.put(material, value);
	}
	
	public static double get(Inventory inventory)
	{
		return StreamSupport.stream(inventory.spliterator(), false)
			.filter(Utility::isItem)
			.mapToDouble(item -> get(item.getType()) * item.getAmount())
			.sum();
	}
	
	public static double get(Material material)
	{
		return values.getOrDefault(material, DEFAULT_VALUE);
	}
	
	static
	{
		set(Material.DIRT, 0.25);
		set(Material.COBBLESTONE, 0.4);
		set(Material.DEEPSLATE, 0.7);
		set(MaterialCollections.digestibleFoods, 5);
		set(Material.BLAZE_POWDER, 10);
		set(Material.BLAZE_ROD, 15);
		set(Material.ENDER_PEARL, 30);
		set(Material.ENDER_EYE, 70);
		set(Material.ENCHANTED_BOOK, 100);
		set(MaterialCollections.smithingTemplates, 500);
		set(Material.TOTEM_OF_UNDYING, 1_000);
		set(Material.ELYTRA, 5_000);
		
		set(MaterialCollections.coal, 5);
		set(Material.COAL_ORE, 5);
		set(Material.COAL_BLOCK, 200);
		
		set(MaterialCollections.ironStuff, 100);
		set(Material.IRON_ORE, 50);
		set(Material.IRON_BLOCK, 300);
		
		set(Material.EMERALD, 100);
		set(Material.EMERALD_BLOCK, 600);
		
		set(MaterialCollections.diamondStuff, 500);
		set(Material.DIAMOND_ORE, 200);
		set(Material.DIAMOND_BLOCK, 1000);
		
		set(MaterialCollections.netheriteStuff, 1_000);
		set(Material.ANCIENT_DEBRIS, 1_500);
		set(Material.NETHERITE_INGOT, 2_000);
		set(Material.NETHERITE_BLOCK, 10_000);
		
		set(MaterialCollections.spawnEggs, 100_000);
		set(Material.BEDROCK, 100_000);
	}
}
