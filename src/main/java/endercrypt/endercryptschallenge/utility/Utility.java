/**************************************************************************\
* EnderCrypt's Challenge                                                   *
* Copyright (C) 2023 by EnderCrypt                                         *
*                                                                          *
* This program is free software: you can redistribute it and/or modify     *
* it under the terms of the GNU Affero General Public License as           *
* published by the Free Software Foundation, either version 3 of           *
* the License, or (at your option) any later version.                      *
*                                                                          *
* This program is distributed in the hope that it will be useful,          *
* but WITHOUT ANY WARRANTY; without even the implied warranty of           *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
* GNU Affero General Public License for more details.                      *
*                                                                          *
* You should have received a copy of the GNU Affero General Public License *
* along with this program in its root directory.                           *
* If not, see <http://www.gnu.org/licenses/>.                              *
\**************************************************************************/

package endercrypt.endercryptschallenge.utility;


import endercrypt.endercryptschallenge.materials.MaterialCollections;
import endercrypt.endercryptschallenge.modules.worlds.list.nether.NetherWorld;
import endercrypt.library.commons.RandomEntry;
import endercrypt.library.commons.misc.Ender;
import endercrypt.library.commons.position.Circle;
import endercrypt.library.commons.reject.ThrowRejectDueTo;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.Random;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Particle;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.block.data.Waterlogged;
import org.bukkit.craftbukkit.v1_21_R1.block.CraftChest;
import org.bukkit.entity.Boat;
import org.bukkit.entity.Enemy;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Minecart;
import org.bukkit.entity.Mob;
import org.bukkit.entity.Player;
import org.bukkit.entity.Projectile;
import org.bukkit.entity.Vehicle;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.player.PlayerBedLeaveEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.util.NumberConversions;
import org.bukkit.util.Vector;
import org.jetbrains.annotations.NotNull;
import org.joml.Math;

import com.destroystokyo.paper.entity.Pathfinder.PathResult;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import net.kyori.adventure.text.Component;
import net.kyori.adventure.text.serializer.plain.PlainTextComponentSerializer;


/**
 * @author EnderCrypt
 */
public class Utility
{
	public static final List<BlockFace> allblockFaces = Arrays.stream(BlockFace.values())
		.filter(facing -> Math.abs(facing.getModX() + facing.getModY() + facing.getModZ()) <= 1)
		.collect(Collectors.toUnmodifiableList());
	
	public static final List<BlockFace> cartesianBlockFaces = Arrays.stream(BlockFace.values())
		.filter(BlockFace::isCartesian)
		.collect(Collectors.toUnmodifiableList());
	
	public static final List<BlockFace> nonCartesianBlockFaces = Arrays.stream(BlockFace.values())
		.filter(b -> cartesianBlockFaces.contains(b) == false)
		.collect(Collectors.toUnmodifiableList());
	
	public static final List<BlockFace> cardinalBlockFaces = cartesianBlockFaces.stream()
		.filter(b -> b.getModY() == 0)
		.collect(Collectors.toUnmodifiableList());
	
	public static final Vector zeroVector = new Vector();
	
	private static final Random random = new Random();
	
	public static final Gson gson = new GsonBuilder()
		.create();
	
	private Utility()
	{
		ThrowRejectDueTo.forbiddenConstructionAttempt();
	}
	
	public static Location translate(Location location, double yaw, double pitch, double distance)
	{
		double x = Math.sin(yaw) * distance;
		double y = Math.cos(pitch) * distance;
		double z = Math.cos(yaw) * distance;
		return location.add(x, y, z);
	}
	
	public static Entity getDriver(Entity passanger)
	{
		if (passanger.isInsideVehicle() == false)
		{
			return null;
		}
		for (Entity entity : passanger.getWorld().getEntities())
		{
			if (entity.getPassengers().contains(passanger))
			{
				return entity;
			}
		}
		return null;
	}
	
	public static double pullNumber(double value, double target, double strength)
	{
		if (strength < 0 || strength > 1)
		{
			throw new IllegalArgumentException("strength cannot be " + strength);
		}
		double difference = target - value;
		double change = difference * strength;
		return value + change;
	}
	
	public static Optional<Player> getClosestPlayerByCondition(Location location, Predicate<Player> playerCondition)
	{
		double closestDistance = Double.MAX_VALUE;
		Player closestPlayer = null;
		
		for (Player player : location.getWorld().getPlayers())
		{
			if (playerCondition.test(player) == false)
			{
				continue;
			}
			double distance = location.distance(player.getLocation());
			if (distance < closestDistance)
			{
				closestDistance = distance;
				closestPlayer = player;
			}
		}
		return Optional.ofNullable(closestPlayer);
	}
	
	public static Optional<Player> getClosestPlayer(Location location)
	{
		return getClosestPlayerByCondition(location, p -> true);
	}
	
	private static final Predicate<Player> livingSurvivalPlayerCondition = player -> Utility.isInSurvival(player) && player.isDead() == false;
	
	public static Optional<Player> getClosestSurvivalPlayer(Location location)
	{
		return getClosestPlayerByCondition(location, livingSurvivalPlayerCondition);
	}
	
	public static void setLocation(Location location, Location target)
	{
		location.set(target.getX(), target.getY(), target.getZ());
	}
	
	public static Location randomlyShift(Location location, double distance)
	{
		double yaw = Math.random() * Circle.FULL;
		double pitch = Math.random() * Circle.FULL;
		return translate(location, yaw, pitch, distance);
	}
	
	public static Vector randomFlatVector(double size)
	{
		double yaw = Math.random() * Circle.FULL;
		return new Vector(Math.cos(yaw) * size, 0, Math.sin(yaw) * size);
	}
	
	public static Location randomlyShiftOnPlane(Location location, double distance)
	{
		return location.add(randomFlatVector(distance));
	}
	
	public static double getPlayerWeight(Player player)
	{
		return 50 + ArmourUtility.getPlayerArmourWeight(player);
	}
	
	public static boolean isInSurvival(Player player)
	{
		GameMode gameMode = player.getGameMode();
		return gameMode == GameMode.SURVIVAL || gameMode == GameMode.ADVENTURE;
	}
	
	public static double roughDistance(Block block1, Block block2)
	{
		return roughDistance(block1.getLocation(), block2.getLocation());
	}
	
	public static double roughDistance(Location location1, Location location2)
	{
		return roughDistance(location1.getX(), location1.getY(), location1.getZ(), location2.getX(), location2.getY(), location2.getZ());
	}
	
	public static double roughDistance(Location location)
	{
		return roughDistance(location.getX(), location.getY(), location.getZ());
	}
	
	public static double roughDistance(Vector vector1, Vector vector2)
	{
		return roughDistance(vector1.getX(), vector1.getY(), vector1.getZ(), vector2.getX(), vector2.getY(), vector2.getZ());
	}
	
	public static double roughDistance(Vector vector)
	{
		return roughDistance(vector.getX(), vector.getY(), vector.getZ());
	}
	
	public static double roughDistance(double x1, double y1, double z1, double x2, double y2, double z2)
	{
		return roughDistance(x1 - x2, y1 - y2, z1 - z2);
	}
	
	/**
	 * Error rate:</br>
	 * max: 1.207%</br>
	 * average: 1.207%</br>
	 */
	public static double roughDistance(double x, double y, double z)
	{
		return Ender.math.fastSqrt(NumberConversions.square(x) + NumberConversions.square(y) + NumberConversions.square(z));
	}
	
	public static double getFlatDistance(Vector vector)
	{
		return Math.atan2(vector.getZ(), vector.getX());
	}
	
	public static double getPitch(Vector vector)
	{
		return Math.atan2(getFlatDistance(vector), vector.getY());
	}
	
	public static void debugParticles(Location location)
	{
		debugParticles(location, 1);
	}
	
	public static void debugParticles(Location location, int strength)
	{
		World world = location.getWorld();
		world.spawnParticle(Particle.HEART, location, strength);
	}
	
	public static Vector superNormalize(@NotNull Vector vector)
	{
		double x = vector.getX();
		double y = vector.getY();
		double z = vector.getZ();
		if (Double.isNaN(x) || Double.isNaN(y) || Double.isNaN(z))
		{
			throw new IllegalArgumentException("cant normalize a vector with NaN: " + vector);
		}
		if (x == 0.0 && y == 0.0 && z == 0.0)
		{
			return vector;
		}
		boolean xInf = Double.isInfinite(x);
		boolean yInf = Double.isInfinite(y);
		boolean zInf = Double.isInfinite(z);
		if (xInf || yInf || zInf)
		{
			vector.setX(xInf ? Math.signum(x) : 0);
			vector.setY(yInf ? Math.signum(y) : 0);
			vector.setZ(zInf ? Math.signum(z) : 0);
		}
		else
		{
			vector.normalize();
		}
		return vector;
	}
	
	public static Vector getRandomVector()
	{
		double x = Ender.math.randomRange(-1.0, 1.0);
		double y = Ender.math.randomRange(-1.0, 1.0);
		double z = Ender.math.randomRange(-1.0, 1.0);
		return new Vector(x, y, z);
	}
	
	public static void destroyVehicle(Vehicle vehicle)
	{
		World world = vehicle.getWorld();
		vehicle.eject();
		ItemStack item = new ItemStack(getVehicleMaterial(vehicle));
		world.dropItemNaturally(vehicle.getLocation(), item);
		vehicle.remove();
	}
	
	public static Material getVehicleMaterial(Vehicle vehicle)
	{
		if (vehicle instanceof Boat boat)
		{
			return boat.getBoatMaterial();
		}
		if (vehicle instanceof Minecart)
		{
			return Material.MINECART;
		}
		throw new IllegalArgumentException("unknown vehicle type: " + vehicle.getClass());
	}
	
	public static double getFlatAngle(Vector vector)
	{
		return Math.atan2(vector.getZ(), vector.getX());
	}
	
	public static Optional<Block> getCeiling(Entity entity, int max)
	{
		Location location = entity.getLocation();
		location.add(0, entity.getHeight(), 0);
		return getCeiling(location, max);
	}
	
	public static Optional<Block> getAboveFloor(Location location, int max)
	{
		Block ceiling = Utility.getCeiling(location, max).orElse(null);
		if (ceiling == null)
			return null;
		
		Location ceilingLocation = Utility.getBlockFloorLocation(ceiling);
		max -= ceilingLocation.getY() - location.getY();
		return Utility.seekBlock(Utility.getBlockFloorLocation(ceiling), 0, 1, 0, (b) -> b.isPassable(), max);
	}
	
	public static Optional<Block> getCeiling(Location location, int max)
	{
		return seekBlock(location, 0, 1, 0, (b) -> b.isPassable() == false, max);
	}
	
	public static Optional<Block> seekBlock(Location location, int directionX, int directionY, int directionZ, Predicate<Block> condition, int max)
	{
		location = location.toBlockLocation();
		int tries = max;
		while (condition.test(location.getBlock()) == false)
		{
			location.add(directionX, directionY, directionZ);
			if (tries-- <= 0)
			{
				return Optional.empty();
			}
		}
		return Optional.of(location.getBlock());
	}
	
	public static boolean hasWaterUnderEntity(Entity entity, int maxSearch)
	{
		Block block = entity.getLocation().getBlock();
		for (int i = 0; i < maxSearch; i++)
		{
			if (block.isSolid())
				return false;
			if (block.getType() == Material.WATER)
				return true;
			block = block.getRelative(BlockFace.DOWN);
		}
		return false;
	}
	
	public static void destroyBlock(Block block)
	{
		World world = block.getWorld();
		Location location = block.getLocation().add(0.5, 0.5, 0.5);
		Material material = block.getType();
		
		if (material == Material.CHEST)
		{
			CraftChest chest = (CraftChest) block.getState();
			Inventory inventory = chest.getBlockInventory();
			for (ItemStack itemStack : inventory)
			{
				if (itemStack != null && itemStack.getType() != Material.AIR)
				{
					world.dropItemNaturally(location, itemStack);
				}
			}
		}
		
		block.setType(Material.AIR);
	}
	
	public static List<Block> getBlockQube(Block block, int radius)
	{
		if (radius < 0)
		{
			throw new IllegalArgumentException("block qube cannot be smaller than 0");
		}
		List<Block> blocks = new ArrayList<>();
		for (int x = -radius; x <= radius; x++)
		{
			for (int y = -radius; y <= radius; y++)
			{
				for (int z = -radius; z <= radius; z++)
				{
					blocks.add(block.getLocation().add(x, y, z).getBlock());
				}
			}
		}
		return blocks;
	}
	
	public static List<Block> getBlockSphere(Location location, double radius)
	{
		return getBlockQube(location.getBlock(), (int) (radius + 1))
			.stream()
			.filter(b -> Utility.getBlockFloorLocation(b).distance(location) <= radius)
			.collect(Collectors.toList());
	}
	
	public static boolean isConnectedFace(BlockFace face)
	{
		int x = Math.abs(face.getModX());
		int y = Math.abs(face.getModY());
		int z = Math.abs(face.getModZ());
		return x <= 1 && y <= 1 && z <= 1;
	}
	
	public static Location getBlockFloorLocation(Location location)
	{
		return getBlockFloorLocation(location.getBlock());
	}
	
	public static Location getBlockFloorLocation(Block block)
	{
		return block.getLocation().add(0.5, 0.0, 0.5);
	}
	
	public static Location getBlockCenterLocation(Location location)
	{
		return getBlockCenterLocation(location.getBlock());
	}
	
	public static Location getBlockCenterLocation(Block block)
	{
		return block.getLocation().add(0.5, 0.5, 0.5);
	}
	
	public static double getPathCompleteness(PathResult path, Location destination)
	{
		if (path == null)
			return 0.0;
		
		Location start = path.getPoints().get(0);
		Location end = path.getFinalPoint();
		double totalDistance = start.distance(destination);
		double missing = end.distance(destination);
		return 1.0 / totalDistance * (totalDistance - missing);
	}
	
	public static double getPathDistance(PathResult path)
	{
		List<Location> points = path.getPoints();
		double distance = 0;
		Location previous = null;
		for (Location point : points)
		{
			if (previous == null)
			{
				previous = point;
				continue;
			}
			distance += Utility.roughDistance(previous, point);
			previous = point;
		}
		return distance;
	}
	
	public static double getPathDistanceMissing(PathResult path, Location target)
	{
		return path.getFinalPoint().distance(target);
	}
	
	@Deprecated // unreliable
	public static Optional<Entity> getAttackerOf(Entity victim)
	{
		EntityDamageEvent entityDamageEvent = victim.getLastDamageCause();
		
		if (entityDamageEvent == null)
			return Optional.empty();
		
		if (entityDamageEvent.isCancelled())
			return Optional.empty();
		
		if (entityDamageEvent instanceof EntityDamageByEntityEvent == false)
			return Optional.empty();
		
		Entity damager = ((EntityDamageByEntityEvent) entityDamageEvent).getDamager();
		
		return getSourceAttacker(damager);
	}
	
	public static Optional<Entity> getSourceAttacker(Entity damager)
	{
		if (damager instanceof Projectile projectile && projectile.getShooter() instanceof Entity shoooter)
		{
			return Optional.of(shoooter);
		}
		return Optional.of(damager);
	}
	
	public static boolean isSpecialEnemy(Enemy enemy)
	{
		if (enemy.getCustomName() != null)
		{
			return true;
		}
		return false;
	}
	
	public static boolean isEligableForDeletion(Enemy enemy)
	{
		if (enemy.isValid() == false)
		{
			return false;
		}
		if (isSpecialEnemy(enemy))
		{
			return false;
		}
		if (enemy.getRemoveWhenFarAway() == false)
		{
			return false;
		}
		return true;
	}
	
	public static Vector createVector(double angle)
	{
		return new Vector(Math.cos(angle), 0, Math.sin(angle));
	}
	
	public static Vector createVector(double angle, double length)
	{
		return createVector(angle).multiply(length);
	}
	
	@SuppressWarnings("unchecked")
	public static <T extends Entity> T getAnyEntityWhitinRange(Location location, Class<T> target, double range)
	{
		World world = location.getWorld();
		for (Entity entity : world.getEntities())
		{
			if (target.isInstance(entity))
			{
				double distance = Utility.roughDistance(location, entity.getLocation());
				if (distance <= range)
				{
					return (T) entity;
				}
			}
		}
		return null;
	}
	
	public static boolean hasWater(Block block)
	{
		if (MaterialCollections.waterBlocks.contains(block))
			return true;
		
		if (block.getBlockData() instanceof Waterlogged water && water.isWaterlogged())
			return true;
		
		return false;
	}
	
	public static int countBlocks(Location location, int y)
	{
		int yStart = Math.min(location.getBlockY(), y);
		int yEnd = Math.max(location.getBlockY(), y);
		Block target = location.getBlock();
		int count = 0;
		for (int i = yStart; i <= yEnd; i++)
		{
			if (target.isSolid())
			{
				count++;
			}
			target = target.getRelative(BlockFace.UP);
		}
		return count;
	}
	
	public static boolean isOnNetherCeiling(Player player)
	{
		if (player.getWorld().getName().equals(NetherWorld.NAME) == false)
			return false;
		
		if (player.getLocation().getBlockY() < NetherWorld.NETHER_CEILING_Y)
			return false;
		
		return true;
	}
	
	public static boolean isOfClass(Class<?> target, Collection<Class<?>> classes)
	{
		for (Class<?> classTarget : classes)
		{
			if (classTarget.isAssignableFrom(target))
			{
				return true;
			}
		}
		return false;
	}
	
	/**
	 * use {@link #getCeiling(Location, int)}
	 */
	@Deprecated // untested
	public static Block findHighestBlockAbove(Block block)
	{
		Block best = null;
		while (block.getY() < 256)
		{
			if (block.getType() != Material.AIR)
			{
				best = block;
			}
			
			block = block.getRelative(BlockFace.UP);
		}
		return best;
	}
	
	/**
	 * use {@link #getCeiling(Location, int)}
	 */
	@Deprecated
	public static Block findHighestBlockForLocation(Location location)
	{
		return findHighestBlockForLocation(location.getWorld(), location.getBlockX(), location.getBlockZ());
	}
	
	/**
	 * use {@link #getCeiling(Location, int)}
	 */
	@Deprecated
	public static Block findHighestBlockForLocation(World world, int x, int z)
	{
		for (int y = 256; y > 0; y--)
		{
			Block block = world.getBlockAt(x, y, z);
			if (block.getType() != Material.AIR)
			{
				return block;
			}
		}
		return null;
	}
	
	public static boolean isItem(ItemStack itemStack)
	{
		if (itemStack == null)
			return false;
		
		if (itemStack.getType() == Material.AIR)
			return false;
		
		if (itemStack.getAmount() <= 0)
			return false;
		
		return true;
	}
	
	public static boolean isFullSleep(PlayerBedLeaveEvent event)
	{
		return event.getPlayer().getWorld().getTime() == 0;
	}
	
	public static Optional<Block> entityStuckBlock(Entity entity)
	{
		if (entity instanceof Mob mob)
		{
			Block inside = mob.getLocation().add(0, 0.1, 0).getBlock();
			if (inside.isPassable() == false)
			{
				return Optional.of(inside);
			}
			
			// ceiling
			Block ceiling = mob.getEyeLocation().getBlock();
			if (ceiling.isSolid())
			{
				Block floor = mob.getLocation().getBlock().getRelative(BlockFace.DOWN);
				return Optional.of(floor);
			}
		}
		
		return Optional.empty();
	}
	
	/**
	 * @return -1 to 1
	 */
	public static final int randomIntByOne()
	{
		return randomIntByRange(1);
	}
	
	public static final int randomIntByRange(int range)
	{
		return random.nextInt(range * 2) - range;
	}
	
	public static final double distance(Block block1, Block block2)
	{
		Location location1 = getBlockFloorLocation(block1);
		Location location2 = getBlockFloorLocation(block2);
		return location1.distance(location2);
	}
	
	public static <T> List<T> getNumberOfRandomElements(List<T> elements, int count)
	{
		List<T> result = new ArrayList<>(count);
		List<T> copy = new ArrayList<>(elements);
		for (int i = 0; i < count && copy.isEmpty() == false; i++)
		{
			T element = RandomEntry.from(copy);
			copy.remove(element);
			result.add(element);
		}
		return result;
	}
	
	public static void sendMessageToAll(String message)
	{
		for (Player player : Bukkit.getOnlinePlayers())
		{
			player.sendMessage(message);
		}
	}
	
	public static float fastInverseSqrt(float number)
	{
		float xhalf = 0.5f * number;
		int i = Float.floatToIntBits(number);
		i = 0x5f3759df - (i >> 1);
		number = Float.intBitsToFloat(i);
		number *= (1.5f - xhalf * number * number);
		return number;
	}
	
	public static String componentToString(Component component)
	{
		return PlainTextComponentSerializer.plainText().serialize(component);
	}
	
	public static String getItemName(@NotNull ItemStack itemStack)
	{
		return componentToString(itemStack.displayName());
	}
	
	public static Vector truncateVector(Vector vector, double maxLength)
	{
		double length = vector.length();
		if (length > maxLength)
		{
			double multiplication = length / maxLength;
			return vector.divide(new Vector(multiplication, multiplication, multiplication));
		}
		return vector;
	}
	
	public static double getCanonicalDepth(Player player)
	{
		Location location = player.getLocation();
		double trueDepth = location.toHighestLocation().getY() - location.getY();
		return Math.max(0, trueDepth);
	}
	
	public static boolean isPlayerProne(Player player)
	{
		Location location = player.getLocation();
		Block above = location.getBlock().getRelative(BlockFace.UP);
		return above.isSolid();
	}
}
