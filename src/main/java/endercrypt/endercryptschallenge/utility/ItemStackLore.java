/**************************************************************************\
* EnderCrypt's Challenge                                                   *
* Copyright (C) 2023 by EnderCrypt                                         *
*                                                                          *
* This program is free software: you can redistribute it and/or modify     *
* it under the terms of the GNU Affero General Public License as           *
* published by the Free Software Foundation, either version 3 of           *
* the License, or (at your option) any later version.                      *
*                                                                          *
* This program is distributed in the hope that it will be useful,          *
* but WITHOUT ANY WARRANTY; without even the implied warranty of           *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
* GNU Affero General Public License for more details.                      *
*                                                                          *
* You should have received a copy of the GNU Affero General Public License *
* along with this program in its root directory.                           *
* If not, see <http://www.gnu.org/licenses/>.                              *
\**************************************************************************/

package endercrypt.endercryptschallenge.utility;


import endercrypt.library.commons.pubimm.PublicallyImmutableSet;

import java.util.List;
import java.util.Optional;

import org.bukkit.inventory.ItemStack;

import lombok.NonNull;
import net.kyori.adventure.text.Component;


/**
 * @author EnderCrypt
 */
@SuppressWarnings("serial")
public class ItemStackLore extends PublicallyImmutableSet<Component>
{
	public static ItemStackLore of(ItemStack itemStack)
	{
		return new ItemStackLore(itemStack);
	}
	
	private final ItemStack itemStack;
	
	private ItemStackLore(@NonNull ItemStack itemStack)
	{
		this.itemStack = itemStack;
		Optional.ofNullable(itemStack.lore())
			.stream()
			.flatMap(List::stream)
			.forEach(elements::add);
	}
	
	public void add(@NonNull Component component)
	{
		elements.add(component);
		sync();
	}
	
	public boolean contains(Component component)
	{
		return stream().anyMatch(component::equals);
	}
	
	public boolean contains(String componentText)
	{
		return stream()
			.map(Utility::componentToString)
			.anyMatch(componentText::equals);
	}
	
	public void remove(Component component)
	{
		elements.remove(component);
		sync();
	}
	
	private void sync()
	{
		itemStack.lore(List.copyOf(elements));
	}
}
