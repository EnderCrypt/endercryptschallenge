/**************************************************************************\
* EnderCrypt's Challenge                                                   *
* Copyright (C) 2023 by EnderCrypt                                         *
*                                                                          *
* This program is free software: you can redistribute it and/or modify     *
* it under the terms of the GNU Affero General Public License as           *
* published by the Free Software Foundation, either version 3 of           *
* the License, or (at your option) any later version.                      *
*                                                                          *
* This program is distributed in the hope that it will be useful,          *
* but WITHOUT ANY WARRANTY; without even the implied warranty of           *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
* GNU Affero General Public License for more details.                      *
*                                                                          *
* You should have received a copy of the GNU Affero General Public License *
* along with this program in its root directory.                           *
* If not, see <http://www.gnu.org/licenses/>.                              *
\**************************************************************************/

package endercrypt.endercryptschallenge.utility.tracker;


import endercrypt.library.commons.iterator.PurifyingIterator;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.function.Consumer;

import lombok.NonNull;


/**
 * @author EnderCrypt
 */
public class ObjectTracker<T> implements Iterable<T>
{
	protected final Class<T> targetClass;
	protected final List<Condition<T>> conditions = new ArrayList<>();
	protected final Set<T> objects = new HashSet<>();
	protected final List<Consumer<T>> removeListener = new ArrayList<>();
	
	public ObjectTracker(@NonNull Class<T> targetClass)
	{
		this.targetClass = targetClass;
	}
	
	public ObjectTracker<T> addCondition(@NonNull Condition<T> condition)
	{
		conditions.add(condition);
		return this;
	}
	
	public ObjectTracker<T> addRemoveListener(@NonNull Consumer<T> callback)
	{
		removeListener.add(callback);
		return this;
	}
	
	public boolean contains(T object)
	{
		return objects.contains(object);
	}
	
	public int count()
	{
		return objects.size();
	}
	
	public boolean isMeetsConditions(T entity)
	{
		for (Condition<T> condition : conditions)
		{
			if (condition.check(entity) == false)
			{
				return false;
			}
		}
		return true;
	}
	
	@SuppressWarnings("unchecked")
	public boolean feed(Object object)
	{
		if (object == null)
			return false;
		
		if (targetClass.isAssignableFrom(object.getClass()) == false)
			return false;
		
		return add((T) object);
	}
	
	public boolean add(T object)
	{
		if (isMeetsConditions(object) == false)
		{
			return false;
		}
		return objects.add(object);
	}
	
	private void triggerRemoveListener(T entity)
	{
		removeListener.forEach(c -> c.accept(entity));
	}
	
	public boolean cleanup()
	{
		boolean cleaned = false;
		Iterator<T> iterator = objects.iterator();
		while (iterator.hasNext())
		{
			T entity = iterator.next();
			if (isMeetsConditions(entity) == false)
			{
				cleaned = true;
				iterator.remove();
				triggerRemoveListener(entity);
			}
		}
		return cleaned;
	}
	
	@Override
	public Iterator<T> iterator()
	{
		return new PurifyingIterator<>(objects.iterator(), this::isMeetsConditions)
			.addPurifyListener(this::triggerRemoveListener);
	}
}
