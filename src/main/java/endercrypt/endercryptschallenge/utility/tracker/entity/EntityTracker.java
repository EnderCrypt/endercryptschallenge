/**************************************************************************\
* EnderCrypt's Challenge                                                   *
* Copyright (C) 2023 by EnderCrypt                                         *
*                                                                          *
* This program is free software: you can redistribute it and/or modify     *
* it under the terms of the GNU Affero General Public License as           *
* published by the Free Software Foundation, either version 3 of           *
* the License, or (at your option) any later version.                      *
*                                                                          *
* This program is distributed in the hope that it will be useful,          *
* but WITHOUT ANY WARRANTY; without even the implied warranty of           *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
* GNU Affero General Public License for more details.                      *
*                                                                          *
* You should have received a copy of the GNU Affero General Public License *
* along with this program in its root directory.                           *
* If not, see <http://www.gnu.org/licenses/>.                              *
\**************************************************************************/

package endercrypt.endercryptschallenge.utility.tracker.entity;


import endercrypt.endercryptschallenge.utility.tracker.ObjectTracker;

import java.util.Optional;

import org.bukkit.Location;
import org.bukkit.entity.Entity;


/**
 * @author EnderCrypt
 */
public class EntityTracker<T extends Entity> extends ObjectTracker<T>
{
	public EntityTracker(Class<T> targetClass)
	{
		super(targetClass);
		addCondition(new ConditionAliveAndActive<>());
	}
	
	public Optional<T> getNearest(Location location)
	{
		T closest = null;
		double closestDistance = Double.MAX_VALUE;
		for (T entity : this)
		{
			double distance = entity.getLocation().distance(location);
			if (closest == null || distance < closestDistance)
			{
				closest = entity;
				closestDistance = distance;
			}
		}
		return Optional.ofNullable(closest);
	}
}
