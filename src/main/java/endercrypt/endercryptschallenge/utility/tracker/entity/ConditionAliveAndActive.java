/**************************************************************************\
* EnderCrypt's Challenge                                                   *
* Copyright (C) 2023 by EnderCrypt                                         *
*                                                                          *
* This program is free software: you can redistribute it and/or modify     *
* it under the terms of the GNU Affero General Public License as           *
* published by the Free Software Foundation, either version 3 of           *
* the License, or (at your option) any later version.                      *
*                                                                          *
* This program is distributed in the hope that it will be useful,          *
* but WITHOUT ANY WARRANTY; without even the implied warranty of           *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
* GNU Affero General Public License for more details.                      *
*                                                                          *
* You should have received a copy of the GNU Affero General Public License *
* along with this program in its root directory.                           *
* If not, see <http://www.gnu.org/licenses/>.                              *
\**************************************************************************/

package endercrypt.endercryptschallenge.utility.tracker.entity;


import endercrypt.endercryptschallenge.utility.tracker.Condition;

import org.bukkit.entity.Entity;


/**
 * @author EnderCrypt
 */
public class ConditionAliveAndActive<T extends Entity> implements Condition<T>
{
	public static boolean checkEntity(Entity entity)
	{
		return entity.isValid() && entity.isTicking();
	}
	
	@Override
	public boolean check(T entity)
	{
		return checkEntity(entity);
	}
}
