/**************************************************************************\
* EnderCrypt's Challenge                                                   *
* Copyright (C) 2023 by EnderCrypt                                         *
*                                                                          *
* This program is free software: you can redistribute it and/or modify     *
* it under the terms of the GNU Affero General Public License as           *
* published by the Free Software Foundation, either version 3 of           *
* the License, or (at your option) any later version.                      *
*                                                                          *
* This program is distributed in the hope that it will be useful,          *
* but WITHOUT ANY WARRANTY; without even the implied warranty of           *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
* GNU Affero General Public License for more details.                      *
*                                                                          *
* You should have received a copy of the GNU Affero General Public License *
* along with this program in its root directory.                           *
* If not, see <http://www.gnu.org/licenses/>.                              *
\**************************************************************************/

package endercrypt.endercryptschallenge.utility;


import java.util.Arrays;
import java.util.stream.Collectors;

import net.md_5.bungee.api.ChatColor;


/**
 * @author EnderCrypt
 */
public class NumberWaves
{
	private double[] layers;
	
	public NumberWaves(int layers)
	{
		this.layers = new double[layers];
		
		if (layers <= 1)
		{
			throw new IllegalArgumentException("cant have " + layers + " layers");
		}
	}
	
	public void push(double value)
	{
		layers[0] += value;
	}
	
	public void carry(double intensity)
	{
		for (int i = layers.length - 1; i >= 1; i--)
		{
			layers[i] += layers[i - 1] * intensity;
		}
	}
	
	public void decay(double intensity)
	{
		double friction = 1.0 - intensity;
		for (int i = 0; i < layers.length; i++)
		{
			layers[i] *= friction;
		}
	}
	
	public double get()
	{
		return layers[layers.length - 1];
	}
	
	private static ChatColor getIntensityColor(int amount)
	{
		if (amount < 1000)
			return ChatColor.WHITE;
		
		if (amount < 2000)
			return ChatColor.GREEN;
		
		if (amount < 5000)
			return ChatColor.YELLOW;
		
		if (amount < 10_000)
			return ChatColor.RED;
		
		if (amount < 20_000)
			return ChatColor.DARK_PURPLE;
		
		return ChatColor.BLACK;
	}
	
	public String toStringColored()
	{
		String numbers = Arrays.stream(layers)
			.mapToObj(n -> (int) Math.round(n))
			.map(n -> getIntensityColor(n).toString() + n)
			.collect(Collectors.joining(ChatColor.WHITE + " -> "));
		return "[" + numbers + ChatColor.WHITE + "]";
	}
	
	@Override
	public String toString()
	{
		String numbers = Arrays.stream(layers)
			.mapToObj(n -> String.valueOf(Math.round(n)))
			.collect(Collectors.joining(" -> "));
		return "[" + numbers + "]";
	}
}
