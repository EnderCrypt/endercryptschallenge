/**************************************************************************\
* EnderCrypt's Challenge                                                   *
* Copyright (C) 2023 by EnderCrypt                                         *
*                                                                          *
* This program is free software: you can redistribute it and/or modify     *
* it under the terms of the GNU Affero General Public License as           *
* published by the Free Software Foundation, either version 3 of           *
* the License, or (at your option) any later version.                      *
*                                                                          *
* This program is distributed in the hope that it will be useful,          *
* but WITHOUT ANY WARRANTY; without even the implied warranty of           *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
* GNU Affero General Public License for more details.                      *
*                                                                          *
* You should have received a copy of the GNU Affero General Public License *
* along with this program in its root directory.                           *
* If not, see <http://www.gnu.org/licenses/>.                              *
\**************************************************************************/

package endercrypt.endercryptschallenge.utility.difficulty;


import endercrypt.endercryptschallenge.modules.players.PlayerProfile;
import endercrypt.library.commons.reject.ThrowRejectDueTo;

import java.util.EnumMap;

import org.bukkit.Difficulty;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Monster;
import org.bukkit.event.block.BlockEvent;
import org.bukkit.event.entity.EntityEvent;
import org.bukkit.event.inventory.InventoryInteractEvent;
import org.bukkit.event.player.PlayerEvent;


/**
 * @author EnderCrypt
 */
public class Difficulties
{
	public static DifficultyRating getFor(PlayerProfile profile)
	{
		return getFor(profile.getPlayer());
	}
	
	public static DifficultyRating getFor(InventoryInteractEvent event)
	{
		return getFor(event.getWhoClicked());
	}
	
	public static DifficultyRating getFor(EntityEvent event)
	{
		return getFor(event.getEntity());
	}
	
	public static DifficultyRating getFor(PlayerEvent event)
	{
		return getFor(event.getPlayer());
	}
	
	public static DifficultyRating getFor(BlockEvent event)
	{
		return getFor(event.getBlock());
	}
	
	public static DifficultyRating getFor(Block block)
	{
		return getFor(block.getWorld());
	}
	
	public static DifficultyRating getFor(Monster monster)
	{
		return getFor(monster.getWorld());
	}
	
	public static DifficultyRating getFor(Entity entity)
	{
		return getFor(entity.getWorld());
	}
	
	public static DifficultyRating getFor(Location location)
	{
		return getFor(location.getWorld());
	}
	
	public static DifficultyRating getFor(World world)
	{
		return new DifficultyRating(world.getDifficulty());
	}
	
	public static DifficultyRating getFor(Difficulty difficulty)
	{
		return difficulties.get(difficulty);
	}
	
	private static final EnumMap<Difficulty, DifficultyRating> difficulties = new EnumMap<>(Difficulty.class);
	static
	{
		difficulties.replaceAll((difficulty, old) -> new DifficultyRating(difficulty));
	}
	
	private Difficulties()
	{
		ThrowRejectDueTo.forbiddenConstructionAttempt();
	}
}
