/**************************************************************************\
* EnderCrypt's Challenge                                                   *
* Copyright (C) 2023 by EnderCrypt                                         *
*                                                                          *
* This program is free software: you can redistribute it and/or modify     *
* it under the terms of the GNU Affero General Public License as           *
* published by the Free Software Foundation, either version 3 of           *
* the License, or (at your option) any later version.                      *
*                                                                          *
* This program is distributed in the hope that it will be useful,          *
* but WITHOUT ANY WARRANTY; without even the implied warranty of           *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
* GNU Affero General Public License for more details.                      *
*                                                                          *
* You should have received a copy of the GNU Affero General Public License *
* along with this program in its root directory.                           *
* If not, see <http://www.gnu.org/licenses/>.                              *
\**************************************************************************/

package endercrypt.endercryptschallenge.utility.difficulty;


import org.bukkit.Difficulty;


/**
 * @author EnderCrypt
 */
public class DifficultyRating
{
	private final Difficulty difficulty;
	
	protected DifficultyRating(Difficulty difficulty)
	{
		this.difficulty = difficulty;
	}
	
	// COMPARE //
	
	public boolean isHarderThan(Difficulty difficulty)
	{
		return (this.difficulty.ordinal() > difficulty.ordinal());
	}
	
	public boolean isEasierThan(Difficulty difficulty)
	{
		return (this.difficulty.ordinal() < difficulty.ordinal());
	}
	
	public boolean is(Difficulty difficulty)
	{
		return (this.difficulty == difficulty);
	}
	
	public boolean isNot(Difficulty difficulty)
	{
		return (this.difficulty != difficulty);
	}
	
	// DIFFICULTY //
	
	public Difficulty asDifficulty()
	{
		return difficulty;
	}
	
	// STANDARD MODIFIERS //
	
	public int asIntModifier()
	{
		return asIntValue(1, 2, 4);
	}
	
	public double asDoubleModifier()
	{
		return asDoubleValue(1.0, 2.0, 4.0);
	}
	
	// ARBITRARY INT VALUES //
	
	public int asIntValue(int easy, int normal, int hard)
	{
		return asIntValue(0, easy, normal, hard);
	}
	
	public int asIntValue(int peaceful, int easy, int normal, int hard)
	{
		return (int) Math.round(asDoubleValue(peaceful, easy, normal, hard));
	}
	
	// ARBITRARY DOUBLE VALUES //
	
	public double asDoubleValue(double easy, double normal, double hard)
	{
		return asDoubleValue(0.0, easy, normal, hard);
	}
	
	public double asDoubleValue(double peaceful, double easy, double normal, double hard)
	{
		switch (asDifficulty())
		{
		case PEACEFUL:
			return peaceful;
		case EASY:
			return easy;
		case NORMAL:
			return normal;
		case HARD:
			return hard;
		default:
			throw new IllegalArgumentException("unknown difficulty: " + asDifficulty());
		}
	}
}
