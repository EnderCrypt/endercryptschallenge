/**************************************************************************\
* EnderCrypt's Challenge                                                   *
* Copyright (C) 2023 by EnderCrypt                                         *
*                                                                          *
* This program is free software: you can redistribute it and/or modify     *
* it under the terms of the GNU Affero General Public License as           *
* published by the Free Software Foundation, either version 3 of           *
* the License, or (at your option) any later version.                      *
*                                                                          *
* This program is distributed in the hope that it will be useful,          *
* but WITHOUT ANY WARRANTY; without even the implied warranty of           *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
* GNU Affero General Public License for more details.                      *
*                                                                          *
* You should have received a copy of the GNU Affero General Public License *
* along with this program in its root directory.                           *
* If not, see <http://www.gnu.org/licenses/>.                              *
\**************************************************************************/

package endercrypt.endercryptschallenge.materials.structure;


import endercrypt.library.commons.RandomEntry;

import java.util.Collections;
import java.util.EnumSet;
import java.util.Iterator;
import java.util.stream.Stream;

import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Item;
import org.bukkit.inventory.ItemStack;

import lombok.Getter;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;


/**
 * @author EnderCrypt
 */
@RequiredArgsConstructor
public class MaterialCollection implements Iterable<Material>
{
	@NonNull
	@Getter
	private final String name;
	
	@NonNull
	private final EnumSet<Material> materials;
	
	public boolean contains(@NonNull Block block)
	{
		return contains(block.getType());
	}
	
	public boolean contains(@NonNull Item item)
	{
		return contains(item.getItemStack());
	}
	
	public boolean contains(@NonNull ItemStack itemStack)
	{
		return contains(itemStack.getType());
	}
	
	public boolean contains(@NonNull Material material)
	{
		return materials.contains(material);
	}
	
	public Material getRandom()
	{
		return RandomEntry.from(materials);
	}
	
	public Stream<Material> stream()
	{
		return materials.stream();
	}
	
	@Override
	public Iterator<Material> iterator()
	{
		return Collections.unmodifiableSet(materials).iterator();
	}
	
	@Override
	public String toString()
	{
		return materials.toString();
	}
}
