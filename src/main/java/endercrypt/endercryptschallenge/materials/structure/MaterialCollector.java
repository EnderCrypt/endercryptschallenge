/**************************************************************************\
* EnderCrypt's Challenge                                                   *
* Copyright (C) 2023 by EnderCrypt                                         *
*                                                                          *
* This program is free software: you can redistribute it and/or modify     *
* it under the terms of the GNU Affero General Public License as           *
* published by the Free Software Foundation, either version 3 of           *
* the License, or (at your option) any later version.                      *
*                                                                          *
* This program is distributed in the hope that it will be useful,          *
* but WITHOUT ANY WARRANTY; without even the implied warranty of           *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
* GNU Affero General Public License for more details.                      *
*                                                                          *
* You should have received a copy of the GNU Affero General Public License *
* along with this program in its root directory.                           *
* If not, see <http://www.gnu.org/licenses/>.                              *
\**************************************************************************/

package endercrypt.endercryptschallenge.materials.structure;


import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import org.bukkit.Material;


/**
 * @author EnderCrypt
 */
public interface MaterialCollector
{
	public static MaterialCollector copy(MaterialCollection collection)
	{
		return of(collection.stream().toArray(Material[]::new));
	}
	
	public static MaterialCollector of(Material material)
	{
		return of(new Material[] { material });
	}
	
	public static MaterialCollector of(Material[] materials)
	{
		List<Material> result = List.of(materials);
		return () -> result;
	}
	
	public static MaterialCollector byCondition(MaterialCondition condition)
	{
		return () -> Arrays.stream(Material.values())
			.filter(condition::check)
			.collect(Collectors.toList());
	}
	
	public List<Material> collect();
}
