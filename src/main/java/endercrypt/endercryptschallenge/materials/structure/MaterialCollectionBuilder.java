/**************************************************************************\
* EnderCrypt's Challenge                                                   *
* Copyright (C) 2023 by EnderCrypt                                         *
*                                                                          *
* This program is free software: you can redistribute it and/or modify     *
* it under the terms of the GNU Affero General Public License as           *
* published by the Free Software Foundation, either version 3 of           *
* the License, or (at your option) any later version.                      *
*                                                                          *
* This program is distributed in the hope that it will be useful,          *
* but WITHOUT ANY WARRANTY; without even the implied warranty of           *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
* GNU Affero General Public License for more details.                      *
*                                                                          *
* You should have received a copy of the GNU Affero General Public License *
* along with this program in its root directory.                           *
* If not, see <http://www.gnu.org/licenses/>.                              *
\**************************************************************************/

package endercrypt.endercryptschallenge.materials.structure;


import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.regex.Pattern;

import org.bukkit.Material;

import lombok.NonNull;
import lombok.RequiredArgsConstructor;


/**
 * @author EnderCrypt
 */
@RequiredArgsConstructor
public class MaterialCollectionBuilder
{
	private final List<MaterialCollector> collectors = new ArrayList<>();
	
	@NonNull
	private final String name;
	
	public MaterialCollectionBuilder add(@NonNull MaterialCollection collection)
	{
		addCollector(MaterialCollector.copy(collection));
		return this;
	}
	
	public MaterialCollectionBuilder add(@NonNull Material... materials)
	{
		addCollector(MaterialCollector.of(materials));
		return this;
	}
	
	public MaterialCollectionBuilder add(@NonNull Material material)
	{
		addCollector(MaterialCollector.of(material));
		return this;
	}
	
	public MaterialCollectionBuilder startsWith(@NonNull String text)
	{
		addCondition(MaterialCondition.byName(id -> id.startsWith(text)));
		return this;
	}
	
	public MaterialCollectionBuilder endsWith(@NonNull String text)
	{
		addCondition(MaterialCondition.byName(id -> id.endsWith(text)));
		return this;
	}
	
	public MaterialCollectionBuilder addRegex(@NonNull String text)
	{
		addCondition(new MaterialCondition()
		{
			private final Pattern pattern = Pattern.compile(text);
			
			@Override
			public boolean check(Material material)
			{
				return pattern
					.matcher(material.toString())
					.find();
			}
		});
		return this;
	}
	
	public MaterialCollectionBuilder contains(@NonNull String text)
	{
		addCondition(MaterialCondition.byName(id -> id.contains(text)));
		return this;
	}
	
	public MaterialCollectionBuilder addCondition(@NonNull MaterialCondition condition)
	{
		addCollector(MaterialCollector.byCondition(condition));
		return this;
	}
	
	public MaterialCollectionBuilder addCollector(@NonNull MaterialCollector collector)
	{
		collectors.add(collector);
		return this;
	}
	
	public MaterialCollection build()
	{
		Set<Material> result = new HashSet<>();
		
		for (MaterialCollector collector : collectors)
		{
			List<Material> conditionResult = collector.collect();
			if (conditionResult.isEmpty())
			{
				throw new MaterialCollectionFailed("failed to collect materials for " + collector);
			}
			result.addAll(conditionResult);
		}
		
		return new MaterialCollection(name, EnumSet.copyOf(result));
	}
}
