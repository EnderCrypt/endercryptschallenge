/**************************************************************************\
* EnderCrypt's Challenge                                                   *
* Copyright (C) 2023 by EnderCrypt                                         *
*                                                                          *
* This program is free software: you can redistribute it and/or modify     *
* it under the terms of the GNU Affero General Public License as           *
* published by the Free Software Foundation, either version 3 of           *
* the License, or (at your option) any later version.                      *
*                                                                          *
* This program is distributed in the hope that it will be useful,          *
* but WITHOUT ANY WARRANTY; without even the implied warranty of           *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
* GNU Affero General Public License for more details.                      *
*                                                                          *
* You should have received a copy of the GNU Affero General Public License *
* along with this program in its root directory.                           *
* If not, see <http://www.gnu.org/licenses/>.                              *
\**************************************************************************/

package endercrypt.endercryptschallenge.materials.structure;

/**
 * @author EnderCrypt
 */
public class MaterialCollectionFailed extends RuntimeException
{
	private static final long serialVersionUID = -6712687655811394547L;
	
	/**
	 * 
	 */
	
	public MaterialCollectionFailed(String message)
	{
		super(message);
	}
	
	public MaterialCollectionFailed(String message, Throwable cause)
	{
		super(message, cause);
	}
}
