/**************************************************************************\
* EnderCrypt's Challenge                                                   *
* Copyright (C) 2023 by EnderCrypt                                         *
*                                                                          *
* This program is free software: you can redistribute it and/or modify     *
* it under the terms of the GNU Affero General Public License as           *
* published by the Free Software Foundation, either version 3 of           *
* the License, or (at your option) any later version.                      *
*                                                                          *
* This program is distributed in the hope that it will be useful,          *
* but WITHOUT ANY WARRANTY; without even the implied warranty of           *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
* GNU Affero General Public License for more details.                      *
*                                                                          *
* You should have received a copy of the GNU Affero General Public License *
* along with this program in its root directory.                           *
* If not, see <http://www.gnu.org/licenses/>.                              *
\**************************************************************************/

package endercrypt.endercryptschallenge.materials;


import endercrypt.endercryptschallenge.materials.structure.MaterialCollection;
import endercrypt.endercryptschallenge.materials.structure.MaterialCollectionBuilder;
import endercrypt.library.commons.reject.ThrowRejectDueTo;

import org.bukkit.Material;


/**
 * @author EnderCrypt
 */
public class MaterialCollections
{
	private MaterialCollections()
	{
		ThrowRejectDueTo.forbiddenConstructionAttempt();
	}
	
	public static final MaterialCollection indestructable = new MaterialCollectionBuilder("Indestructable")
		.add(Material.BEDROCK)
		.add(Material.END_PORTAL_FRAME)
		.build();
	
	public static final MaterialCollection armour = new MaterialCollectionBuilder("Armour")
		.endsWith("_HELMET")
		.endsWith("_CHESTPLATE")
		.endsWith("_LEGGINGS")
		.endsWith("_BOOTS")
		.build();
	
	public static final MaterialCollection beds = new MaterialCollectionBuilder("Beds")
		.endsWith("_BED")
		.build();
	
	public static final MaterialCollection coal = new MaterialCollectionBuilder("Coal")
		.add(Material.COAL)
		.add(Material.CHARCOAL)
		.build();
	
	public static final MaterialCollection leaves = new MaterialCollectionBuilder("Leaves")
		.endsWith("_LEAVES")
		.build();
	
	public static final MaterialCollection woodBlocks = new MaterialCollectionBuilder("Wood Blocks")
		.endsWith("_LOG")
		.endsWith("_WOOD")
		.endsWith("_PLANKS")
		.add(leaves)
		.add(beds)
		.build();
	
	public static final MaterialCollection signs = new MaterialCollectionBuilder("Signs")
		.endsWith("_SIGN")
		.build();
	
	public static final MaterialCollection flammables = new MaterialCollectionBuilder("Flammables")
		.add(beds)
		.add(coal)
		.add(woodBlocks)
		.add(signs)
		.endsWith("_CARPET")
		.build();
	
	public static final MaterialCollection swords = new MaterialCollectionBuilder("Swords")
		.endsWith("_SWORD")
		.build();
	
	public static final MaterialCollection tools = new MaterialCollectionBuilder("Tools")
		.endsWith("_PICKAXE")
		.endsWith("_SHOVEL")
		.endsWith("_AXE")
		.endsWith("_HOE")
		.build();
	
	public static final MaterialCollection equipment = new MaterialCollectionBuilder("Equipment")
		.add(armour)
		.add(swords)
		.add(tools)
		.add(Material.SHIELD)
		.build();
	
	public static final MaterialCollection fireSpreaders = new MaterialCollectionBuilder("Fire Spreaders")
		.add(Material.TORCH, Material.CAMPFIRE, Material.LAVA, Material.FIRE, Material.MAGMA_BLOCK, Material.FURNACE)
		.build();
	
	public static final MaterialCollection burning = new MaterialCollectionBuilder("Burning")
		.add(Material.LAVA_BUCKET)
		.add(Material.BLAZE_POWDER)
		.add(Material.BLAZE_ROD)
		.add(Material.MAGMA_BLOCK)
		.build();
	
	public static final MaterialCollection waterSensitive = new MaterialCollectionBuilder("Water sensitive")
		.add(beds)
		.add(signs)
		.add(Material.LADDER)
		.endsWith("_DOOR")
		.endsWith("_TRAPDOOR")
		.endsWith("_BUTTON")
		.endsWith("_CARPET")
		.endsWith("_BANNER")
		.build();
	
	public static final MaterialCollection halfBlocks = new MaterialCollectionBuilder("Half Blocks")
		.endsWith("_FENCE")
		.endsWith("_FENCE_GATE")
		.endsWith("_WALL")
		.endsWith("_SLAB")
		.endsWith("_STAIRS")
		.endsWith("_TRAPDOOR")
		.add(Material.LADDER)
		.add(Material.CAMPFIRE)
		.build();
	
	public static final MaterialCollection explosionSensitive = new MaterialCollectionBuilder("Explosion Sensitive")
		.add(halfBlocks)
		.build();
	
	public static final MaterialCollection seeds = new MaterialCollectionBuilder("Seeds")
		.endsWith("_SEEDS")
		.build();
	
	public static final MaterialCollection mushrooms = new MaterialCollectionBuilder("Mushrooms")
		.add(Material.BROWN_MUSHROOM)
		.add(Material.RED_MUSHROOM)
		.build();
	
	public static final MaterialCollection ingridients = new MaterialCollectionBuilder("Ingridients")
		.add(Material.WHEAT)
		.add(Material.COCOA)
		.build();
	
	public static final MaterialCollection uncookedMeats = new MaterialCollectionBuilder("Uncooked meats")
		.add(Material.PORKCHOP)
		.add(Material.MUTTON)
		.add(Material.BEEF)
		.add(Material.CHICKEN)
		.add(Material.RABBIT)
		.add(Material.COD)
		.add(Material.SALMON)
		.add(Material.TROPICAL_FISH)
		.build();
	
	public static final MaterialCollection cookedMeats = new MaterialCollectionBuilder("Cooked meats")
		.add(Material.COOKED_PORKCHOP)
		.add(Material.COOKED_MUTTON)
		.add(Material.COOKED_BEEF)
		.add(Material.COOKED_CHICKEN)
		.add(Material.COOKED_RABBIT)
		.add(Material.COOKED_COD)
		.add(Material.COOKED_SALMON)
		.add(Material.COOKED_COD)
		.build();
	
	public static final MaterialCollection digestibleFoods = new MaterialCollectionBuilder("Digestible food")
		.add(uncookedMeats)
		.add(cookedMeats)
		.add(Material.COOKIE)
		.add(Material.SWEET_BERRIES)
		.add(Material.GLOW_BERRIES)
		.add(Material.GOLDEN_APPLE)
		.add(Material.ENCHANTED_GOLDEN_APPLE)
		.add(Material.CARROT)
		.add(Material.GOLDEN_CARROT)
		.add(Material.POTATO)
		.add(Material.APPLE)
		.add(Material.BREAD)
		.add(Material.COOKED_SALMON)
		.add(Material.MUSHROOM_STEW)
		.add(Material.RABBIT_STEW)
		.add(Material.SUSPICIOUS_STEW)
		.add(Material.TROPICAL_FISH)
		.add(Material.MELON)
		.add(Material.MELON_SLICE)
		.add(Material.PUMPKIN)
		.add(Material.PUMPKIN_PIE)
		.add(Material.CHORUS_FRUIT)
		.add(Material.CAKE)
		.build();
	
	public static final MaterialCollection indigestibleFoods = new MaterialCollectionBuilder("Indigestible foods")
		.add(Material.POISONOUS_POTATO)
		.add(Material.ROTTEN_FLESH)
		.build();
	
	public static final MaterialCollection myceliumFoods = new MaterialCollectionBuilder("Mycelium Foods")
		.add(seeds)
		.add(ingridients)
		.add(digestibleFoods)
		.add(indigestibleFoods)
		.build();
	
	public static final MaterialCollection foods = new MaterialCollectionBuilder("Foods")
		.add(seeds)
		.add(ingridients)
		.add(mushrooms)
		.add(digestibleFoods)
		.add(indigestibleFoods)
		.build();
	
	public static final MaterialCollection oreBlocks = new MaterialCollectionBuilder("Ore blocks")
		.endsWith("_ORE")
		.build();
	
	public static final MaterialCollection glassBlocks = new MaterialCollectionBuilder("Glass blocks")
		.add(Material.GLASS)
		.add(Material.GLASS_PANE)
		.endsWith("_GLASS_PANE")
		.endsWith("_GLASS")
		.build();
	
	public static final MaterialCollection wools = new MaterialCollectionBuilder("Wools")
		.endsWith("_WOOL")
		.build();
	
	public static final MaterialCollection crops = new MaterialCollectionBuilder("Plants")
		.add(Material.WHEAT)
		.add(Material.MELON_STEM)
		.add(Material.PUMPKIN_STEM)
		.add(Material.POTATOES)
		.add(Material.CARROTS)
		.add(Material.BEETROOTS)
		.build();
	
	public static final MaterialCollection spawnEggs = new MaterialCollectionBuilder("Spawn Eggs")
		.endsWith("_SPAWN_EGG")
		.build();
	
	public static final MaterialCollection boats = new MaterialCollectionBuilder("Boats")
		.endsWith("_BOAT")
		.build();
	
	public static final MaterialCollection waterBlocks = new MaterialCollectionBuilder("Water Blocks")
		.add(Material.WATER)
		.add(Material.KELP_PLANT)
		.add(Material.SEAGRASS)
		.add(Material.TALL_SEAGRASS)
		.build();
	
	public static final MaterialCollection liquids = new MaterialCollectionBuilder("Liquids")
		.add(Material.WATER)
		.add(Material.LAVA)
		.build();
	
	public static final MaterialCollection dyingTrees = new MaterialCollectionBuilder("Dying tree's")
		.addRegex("^((?!POTTED).).*_SAPLING")
		.build();
	
	public static final MaterialCollection ironStuff = new MaterialCollectionBuilder("Iron Stuff")
		.startsWith("IRON_")
		.build();
	
	public static final MaterialCollection diamondStuff = new MaterialCollectionBuilder("Diamond Stuff")
		.startsWith("DIAMOND_")
		.build();
	
	public static final MaterialCollection netheriteStuff = new MaterialCollectionBuilder("Netherite Stuff")
		.startsWith("NETHERITE_")
		.build();
	
	public static final MaterialCollection forbiddenSpawnBody = new MaterialCollectionBuilder("Spawning (forbidden body)")
		.add(Material.LAVA)
		.build();
	
	public static final MaterialCollection forbiddenSpawnFloor = new MaterialCollectionBuilder("Spawning (forbidden floor)")
		.add(forbiddenSpawnBody)
		.add(Material.CACTUS)
		.build();
	
	public static final MaterialCollection alwaysMineable = new MaterialCollectionBuilder("Always Mineable Blocks")
		.add(glassBlocks)
		.add(leaves)
		.add(explosionSensitive)
		.endsWith("_GRASS")
		.endsWith("_SLAB")
		.endsWith("_ICE")
		.contains("CORAL")
		.add(Material.HANGING_ROOTS)
		.endsWith("_TRAPDOOR")
		.build();
	
	public static final MaterialCollection netherBricks = new MaterialCollectionBuilder("Nether Bricks")
		.contains("NETHER_BRICK")
		.build();
	
	public static final MaterialCollection storage = new MaterialCollectionBuilder("Storage")
		.add(Material.CHEST)
		.add(Material.TRAPPED_CHEST)
		.add(Material.BARREL)
		.add(Material.FURNACE)
		.add(Material.HOPPER)
		.add(Material.SMOKER)
		.add(Material.BLAST_FURNACE)
		.add(Material.DROPPER)
		.add(Material.DISPENSER)
		.add(Material.ENDER_CHEST)
		.build();
	
	public static final MaterialCollection mooshroomableTerrain = new MaterialCollectionBuilder("mooshromable terrain")
		.add(Material.GRASS_BLOCK)
		.add(Material.PODZOL)
		.add(Material.ROOTED_DIRT)
		.add(Material.FARMLAND)
		.add(Material.DIRT_PATH)
		.build();
	
	public static final MaterialCollection naturalBlocks = new MaterialCollectionBuilder("natural blocks")
		.add(Material.DIRT)
		.add(Material.GRASS_BLOCK)
		.add(Material.GRAVEL)
		.add(Material.STONE)
		.add(Material.GRANITE)
		.add(Material.DIORITE)
		.add(Material.ANDESITE)
		.add(Material.TUFF)
		.add(Material.DEEPSLATE)
		.add(Material.NETHERRACK)
		.endsWith("_ORE")
		.build();
	
	public static final MaterialCollection smithingTemplates = new MaterialCollectionBuilder("smithing templates")
		.endsWith("_TRIM_SMITHING_TEMPLATE")
		.build();
}
