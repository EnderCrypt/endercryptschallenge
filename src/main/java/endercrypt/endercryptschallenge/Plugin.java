/**************************************************************************\
* EnderCrypt's Challenge                                                   *
* Copyright (C) 2023 by EnderCrypt                                         *
*                                                                          *
* This program is free software: you can redistribute it and/or modify     *
* it under the terms of the GNU Affero General Public License as           *
* published by the Free Software Foundation, either version 3 of           *
* the License, or (at your option) any later version.                      *
*                                                                          *
* This program is distributed in the hope that it will be useful,          *
* but WITHOUT ANY WARRANTY; without even the implied warranty of           *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
* GNU Affero General Public License for more details.                      *
*                                                                          *
* You should have received a copy of the GNU Affero General Public License *
* along with this program.  If not, see <http://www.gnu.org/licenses/>.    *
\**************************************************************************/

package endercrypt.endercryptschallenge;


import endercrypt.endercryptschallenge.core.Core;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.logging.Level;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.plugin.java.JavaPlugin;


/**
 * Congratulations to
 * 
 * BigSox (https://www.twitch.tv/lionbigsox)
 * 
 * and
 * 
 * Benwyartt (https://www.twitch.tv/benwyartt)
 * 
 * for working together and and being the first to beat the Ender dragon!
 * 
 * @author EnderCrypt
 */
public class Plugin extends JavaPlugin
{
	public static final Path pluginDirectory = Paths.get("./plugins/EndercryptsChallenge");
	
	private Core core = null;
	
	@Override
	public void onEnable()
	{
		if (core != null)
		{
			getLogger().severe("Failed to enable, Plugin already running!");
			return;
		}
		getLogger().info("Starting up plugin core ...");
		try
		{
			core = new Core(this);
		}
		catch (Exception e)
		{
			getLogger().log(Level.SEVERE, "Completely failed to initialize EnderCryptChallenge Core", e);
		}
	}
	
	@Override
	public void onDisable()
	{
		if (core == null)
		{
			getLogger().log(Level.SEVERE, "Failed to disable, Plugin not running!");
			return;
		}
		String leaveMessage = core.getDataManager().getMessages().getLeave().getRandom();
		try
		{
			getLogger().info("Shutting down plugin core ...");
			core.shutdown();
		}
		catch (Exception e)
		{
			getLogger().log(Level.SEVERE, "failed to properly stop EnderCryptChallenge Core", e);
		}
		getLogger().info("\n" + leaveMessage);
		core = null;
	}
	
	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args)
	{
		return core.getCommandManager().onCommand(sender, command, label, args);
	}
}
