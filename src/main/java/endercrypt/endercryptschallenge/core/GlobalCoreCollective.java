/**************************************************************************\
* EnderCrypt's Challenge                                                   *
* Copyright (C) 2023 by EnderCrypt                                         *
*                                                                          *
* This program is free software: you can redistribute it and/or modify     *
* it under the terms of the GNU Affero General Public License as           *
* published by the Free Software Foundation, either version 3 of           *
* the License, or (at your option) any later version.                      *
*                                                                          *
* This program is distributed in the hope that it will be useful,          *
* but WITHOUT ANY WARRANTY; without even the implied warranty of           *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
* GNU Affero General Public License for more details.                      *
*                                                                          *
* You should have received a copy of the GNU Affero General Public License *
* along with this program in its root directory.                           *
* If not, see <http://www.gnu.org/licenses/>.                              *
\**************************************************************************/

package endercrypt.endercryptschallenge.core;


import endercrypt.endercryptschallenge.modules.players.PlayerProfile;
import endercrypt.endercryptschallenge.modules.worlds.WorldProfile;

import org.bukkit.World;
import org.bukkit.entity.Player;


/**
 * @author EnderCrypt
 */
public abstract class GlobalCoreCollective extends CoreCollective
{
	protected GlobalCoreCollective(Core core)
	{
		super(core);
	}
	
	protected WorldProfile resolve(World world)
	{
		return getCore().getWorldManager().resolve(world);
	}
	
	protected PlayerProfile resolve(Player player)
	{
		return getCore().getPlayerManager().resolve(player);
	}
}
