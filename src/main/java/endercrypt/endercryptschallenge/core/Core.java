/**************************************************************************\
* EnderCrypt's Challenge                                                   *
* Copyright (C) 2023 by EnderCrypt                                         *
*                                                                          *
* This program is free software: you can redistribute it and/or modify     *
* it under the terms of the GNU Affero General Public License as           *
* published by the Free Software Foundation, either version 3 of           *
* the License, or (at your option) any later version.                      *
*                                                                          *
* This program is distributed in the hope that it will be useful,          *
* but WITHOUT ANY WARRANTY; without even the implied warranty of           *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
* GNU Affero General Public License for more details.                      *
*                                                                          *
* You should have received a copy of the GNU Affero General Public License *
* along with this program in its root directory.                           *
* If not, see <http://www.gnu.org/licenses/>.                              *
\**************************************************************************/

package endercrypt.endercryptschallenge.core;


import endercrypt.endercryptschallenge.Plugin;
import endercrypt.endercryptschallenge.core.modules.CoreModuleManager;
import endercrypt.endercryptschallenge.core.modules.Module;
import endercrypt.endercryptschallenge.modules.command.CommandManager;
import endercrypt.endercryptschallenge.modules.coreprotect.CoreProtectManager;
import endercrypt.endercryptschallenge.modules.cycler.CyclerManager;
import endercrypt.endercryptschallenge.modules.data.DataManager;
import endercrypt.endercryptschallenge.modules.events.EventManager;
import endercrypt.endercryptschallenge.modules.features.MinecraftFeatureManager;
import endercrypt.endercryptschallenge.modules.information.InformationManager;
import endercrypt.endercryptschallenge.modules.pathfinder.PathfinderManager;
import endercrypt.endercryptschallenge.modules.players.PlayerManager;
import endercrypt.endercryptschallenge.modules.settings.SettingsManager;
import endercrypt.endercryptschallenge.modules.timer.TimerManager;
import endercrypt.endercryptschallenge.modules.traveller.TravellerManager;
import endercrypt.endercryptschallenge.modules.worlds.WorldManager;

import java.util.Optional;
import java.util.logging.Logger;

import org.bukkit.Server;

import lombok.Getter;


/**
 * @author EnderCrypt
 */
@Getter
public class Core
{
	private final Plugin plugin;
	
	public Core(Plugin plugin)
	{
		this.plugin = plugin;
		
		// core modules //
		moduleManager = new CoreModuleManager(this);
		moduleManager.start();
	}
	
	public Server getServer()
	{
		return getPlugin().getServer();
	}
	
	public Logger getLogger()
	{
		return plugin.getLogger();
	}
	
	// EXTRA //
	
	public Optional<org.bukkit.plugin.Plugin> getExternalPlugin(String name)
	{
		org.bukkit.plugin.Plugin externalPlugin = plugin.getServer().getPluginManager().getPlugin(name);
		if (externalPlugin == null)
		{
			return Optional.empty();
		}
		return Optional.of(externalPlugin);
	}
	
	// MODULE MANAGER //
	
	private final CoreModuleManager moduleManager;
	
	// FINAL //
	
	public void shutdown()
	{
		moduleManager.shutdown();
	}
	
	// MODULES //
	
	@Module
	private CyclerManager cyclerManager;
	
	@Module
	private EventManager eventManager;
	
	@Module
	private CommandManager commandManager;
	
	public CommandManager getCommandManager()
	{
		return commandManager;
	}
	
	@Module
	private MinecraftFeatureManager minecraftFeatureManager;
	
	@Module
	private TimerManager timerManager;
	
	@Module
	private CoreProtectManager coreProtectManager;
	
	@Module
	private DataManager dataManager;
	
	@Module
	private WorldManager worldManager;
	
	@Module
	private PlayerManager playerManager;
	
	@Module
	private InformationManager informationManager;
	
	@Module
	private PathfinderManager pathfinderManager;
	
	@Module
	private TravellerManager travellerManager;
	
	@Module
	private SettingsManager settingsManager;
}
