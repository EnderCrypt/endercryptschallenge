/**************************************************************************\
* EnderCrypt's Challenge                                                   *
* Copyright (C) 2023 by EnderCrypt                                         *
*                                                                          *
* This program is free software: you can redistribute it and/or modify     *
* it under the terms of the GNU Affero General Public License as           *
* published by the Free Software Foundation, either version 3 of           *
* the License, or (at your option) any later version.                      *
*                                                                          *
* This program is distributed in the hope that it will be useful,          *
* but WITHOUT ANY WARRANTY; without even the implied warranty of           *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
* GNU Affero General Public License for more details.                      *
*                                                                          *
* You should have received a copy of the GNU Affero General Public License *
* along with this program in its root directory.                           *
* If not, see <http://www.gnu.org/licenses/>.                              *
\**************************************************************************/

package endercrypt.endercryptschallenge.core;


import endercrypt.endercryptschallenge.Plugin;

import java.util.Objects;
import java.util.logging.Logger;

import org.bukkit.Server;


/**
 * @author EnderCrypt
 */
public abstract class CoreCollective
{
	protected final Core core;
	protected final Plugin plugin;
	protected final Server server;
	protected final Logger logger;
	
	protected CoreCollective(Core core)
	{
		this.core = Objects.requireNonNull(core, "core");
		this.plugin = core.getPlugin();
		this.server = plugin.getServer();
		this.logger = server.getLogger();
	}
	
	public Core getCore()
	{
		return core;
	}
	
	public Plugin getPlugin()
	{
		return plugin;
	}
	
	public Server getServer()
	{
		return server;
	}
	
	public Logger getLogger()
	{
		return logger;
	}
}
