/**************************************************************************\
* EnderCrypt's Challenge                                                   *
* Copyright (C) 2023 by EnderCrypt                                         *
*                                                                          *
* This program is free software: you can redistribute it and/or modify     *
* it under the terms of the GNU Affero General Public License as           *
* published by the Free Software Foundation, either version 3 of           *
* the License, or (at your option) any later version.                      *
*                                                                          *
* This program is distributed in the hope that it will be useful,          *
* but WITHOUT ANY WARRANTY; without even the implied warranty of           *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
* GNU Affero General Public License for more details.                      *
*                                                                          *
* You should have received a copy of the GNU Affero General Public License *
* along with this program in its root directory.                           *
* If not, see <http://www.gnu.org/licenses/>.                              *
\**************************************************************************/

package endercrypt.endercryptschallenge.core.modules;


import endercrypt.endercryptschallenge.core.Core;
import endercrypt.endercryptschallenge.core.CoreCollective;
import endercrypt.library.commons.misc.Ender;


/**
 * @author EnderCrypt
 */
public abstract class CoreModule extends CoreCollective
{
	public CoreModule(Core core)
	{
		super(core);
	}
	
	public String getModuleName()
	{
		return Ender.clazz.getHumanReadableClassName(getClass());
	}
	
	// start // 
	
	protected boolean start() throws Exception
	{
		try
		{
			onStart();
			return true;
		}
		catch (NoStart e)
		{
			return false;
		}
		catch (Exception e)
		{
			throw e;
		}
	}
	
	protected void onStart() throws Exception
	{
		throw new NoStart();
	}
	
	@SuppressWarnings("serial")
	static class NoStart extends RuntimeException
	{
		private NoStart()
		{
			
		}
	}
	
	// shutdown //
	
	protected boolean shutdown() throws Exception
	{
		try
		{
			onShutdown();
			return true;
		}
		catch (NoShutdown e)
		{
			return false;
		}
		catch (Exception e)
		{
			throw e;
		}
	}
	
	protected void onShutdown() throws Exception
	{
		throw new NoShutdown();
	}
	
	@SuppressWarnings("serial")
	static class NoShutdown extends RuntimeException
	{
		private NoShutdown()
		{
			
		}
	}
}
