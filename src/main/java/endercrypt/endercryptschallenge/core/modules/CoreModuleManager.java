/**************************************************************************\
* EnderCrypt's Challenge                                                   *
* Copyright (C) 2023 by EnderCrypt                                         *
*                                                                          *
* This program is free software: you can redistribute it and/or modify     *
* it under the terms of the GNU Affero General Public License as           *
* published by the Free Software Foundation, either version 3 of           *
* the License, or (at your option) any later version.                      *
*                                                                          *
* This program is distributed in the hope that it will be useful,          *
* but WITHOUT ANY WARRANTY; without even the implied warranty of           *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
* GNU Affero General Public License for more details.                      *
*                                                                          *
* You should have received a copy of the GNU Affero General Public License *
* along with this program in its root directory.                           *
* If not, see <http://www.gnu.org/licenses/>.                              *
\**************************************************************************/

package endercrypt.endercryptschallenge.core.modules;


import endercrypt.endercryptschallenge.core.Core;
import endercrypt.endercryptschallenge.core.exception.CoreActionException;
import endercrypt.endercryptschallenge.core.exception.CoreInitializationException;
import endercrypt.endercryptschallenge.core.exception.CoreModuleUnavailable;
import endercrypt.endercryptschallenge.core.modules.actor.ModuleActor;
import endercrypt.endercryptschallenge.core.modules.actor.impl.ShutdownActor;
import endercrypt.endercryptschallenge.core.modules.actor.impl.StartActor;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.apache.commons.lang.WordUtils;


/**
 * @author EnderCrypt
 */
public class CoreModuleManager
{
	final Core core;
	private final List<CoreModuleHolder> modules = new ArrayList<>();
	
	public CoreModuleManager(Core core)
	{
		this(core, core);
	}
	
	public Core getCore()
	{
		return core;
	}
	
	public CoreModuleManager(Core core, Object source)
	{
		this.core = core;
		
		// collect modules
		for (Field field : source.getClass().getDeclaredFields())
		{
			try
			{
				if (CoreModuleHolder.isModuleField(field))
				{
					CoreModuleHolder kurisuModuleHolder = CoreModuleHolder.resolve(core, source, field);
					modules.add(kurisuModuleHolder);
				}
			}
			catch (InvocationTargetException e)
			{
				throw new CoreInitializationException("exception occured while trying to initialize " + CoreModuleHolder.getModuleFieldName(field), e.getCause());
			}
			catch (IllegalArgumentException | IllegalAccessException | NoSuchMethodException | SecurityException | InstantiationException e)
			{
				throw new CoreInitializationException("Failed to interact with " + CoreModuleHolder.getModuleFieldName(field), e);
			}
		}
		
		// sort modules (by priority)
		Collections.sort(modules);
	}
	
	public List<CoreModuleHolder> getModules()
	{
		return Collections.unmodifiableList(modules);
	}
	
	private void performAct(String action, CoreModuleState targetState, ModuleActor actor)
	{
		action = WordUtils.capitalize(action);
		
		synchronized (modules)
		{
			CoreModuleHolder[] suitableModules = modules.stream().filter(m -> m.getState() == targetState).toArray(CoreModuleHolder[]::new);
			for (CoreModuleHolder moduleHolder : suitableModules)
			{
				try
				{
					long moduleTime = System.currentTimeMillis();
					if (actor.act(moduleHolder))
					{
						moduleTime = (System.currentTimeMillis() - moduleTime);
						actor.onSuccess(moduleHolder, moduleTime);
					}
				}
				catch (CoreModuleUnavailable e)
				{
					throw new CoreActionException("Failed to " + action + " " + moduleHolder.getName() + " due to missing module dependancy", e);
				}
				catch (Exception e)
				{
					actor.onFail(moduleHolder, e);
				}
			}
		}
	}
	
	private final StartActor startActor = new StartActor(this);
	
	public void start()
	{
		performAct("Start", CoreModuleState.OFFLINE, startActor);
	}
	
	private final ShutdownActor shutdownActor = new ShutdownActor(this);
	
	public void shutdown()
	{
		synchronized (modules)
		{
			// performing shutdown in reverse order as to not break functionality during shutdown
			Collections.sort(modules, Comparator.reverseOrder());
			
			performAct("Shutdown", CoreModuleState.ONLINE, shutdownActor);
			
			Collections.sort(modules);
		}
	}
	
	@SuppressWarnings("unchecked")
	public <T extends CoreModule> T fetchModule(Class<T> moduleClass)
	{
		return modules.stream()
			.filter(mh -> mh.getState() == CoreModuleState.ONLINE)
			.map(CoreModuleHolder::getModule)
			.filter(m -> moduleClass.isAssignableFrom(m.getClass()))
			.map(m -> (T) m)
			.findAny()
			.orElseThrow(() -> new CoreModuleUnavailable(moduleClass.getSimpleName() + " is not available or offline"));
	}
}
