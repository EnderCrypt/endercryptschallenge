/**************************************************************************\
* EnderCrypt's Challenge                                                   *
* Copyright (C) 2023 by EnderCrypt                                         *
*                                                                          *
* This program is free software: you can redistribute it and/or modify     *
* it under the terms of the GNU Affero General Public License as           *
* published by the Free Software Foundation, either version 3 of           *
* the License, or (at your option) any later version.                      *
*                                                                          *
* This program is distributed in the hope that it will be useful,          *
* but WITHOUT ANY WARRANTY; without even the implied warranty of           *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
* GNU Affero General Public License for more details.                      *
*                                                                          *
* You should have received a copy of the GNU Affero General Public License *
* along with this program in its root directory.                           *
* If not, see <http://www.gnu.org/licenses/>.                              *
\**************************************************************************/

package endercrypt.endercryptschallenge.core.modules.actor;


import endercrypt.endercryptschallenge.core.modules.CoreModuleHolder;
import endercrypt.endercryptschallenge.core.modules.CoreModuleManager;

import org.apache.commons.lang.exception.ExceptionUtils;


/**
 * @author EnderCrypt
 */
public abstract class StrictModuleActor extends StandardModuleActor
{
	protected StrictModuleActor(CoreModuleManager manager, String success, String failed)
	{
		super(manager, success, failed);
	}
	
	@Override
	public void onFail(CoreModuleHolder module, Exception e)
	{
		getManager().getCore().getLogger().warning("+++ Exception occured while " + failed.toLowerCase() + " " + module.getName() + "\n" + ExceptionUtils.getStackTrace(e));
	}
}
