/**************************************************************************\
* EnderCrypt's Challenge                                                   *
* Copyright (C) 2023 by EnderCrypt                                         *
*                                                                          *
* This program is free software: you can redistribute it and/or modify     *
* it under the terms of the GNU Affero General Public License as           *
* published by the Free Software Foundation, either version 3 of           *
* the License, or (at your option) any later version.                      *
*                                                                          *
* This program is distributed in the hope that it will be useful,          *
* but WITHOUT ANY WARRANTY; without even the implied warranty of           *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
* GNU Affero General Public License for more details.                      *
*                                                                          *
* You should have received a copy of the GNU Affero General Public License *
* along with this program in its root directory.                           *
* If not, see <http://www.gnu.org/licenses/>.                              *
\**************************************************************************/

package endercrypt.endercryptschallenge.core.modules;


import endercrypt.endercryptschallenge.core.Core;
import endercrypt.library.commons.misc.Ender;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.util.Objects;


/**
 * @author EnderCrypt
 */
public class CoreModuleHolder implements Comparable<CoreModuleHolder>
{
	public static String getModuleFieldName(Field field)
	{
		if (isModuleField(field) == false)
		{
			throw new IllegalArgumentException("not a valid module field");
		}
		Module module = field.getAnnotation(Module.class);
		return module.name().equals("") ? Ender.clazz.getHumanReadableClassName(field.getType()) : module.name();
	}
	
	public static boolean isModuleField(Field field)
	{
		if (field.isAnnotationPresent(Module.class) == false)
		{
			return false;
		}
		field.setAccessible(true);
		if (CoreModule.class.isAssignableFrom(field.getType()) == false)
		{
			return false;
		}
		return true;
	}
	
	public static CoreModuleHolder resolve(Core core, Object source, Field field) throws IllegalArgumentException, IllegalAccessException, NoSuchMethodException, SecurityException, InstantiationException, InvocationTargetException
	{
		if (isModuleField(field) == false)
		{
			throw new IllegalArgumentException("not a valid module field");
		}
		field.setAccessible(true);
		Module module = field.getAnnotation(Module.class);
		CoreModule coreModule = (CoreModule) field.get(source);
		if (coreModule == null)
		{
			Constructor<?> constructor = field.getType().getConstructor(Core.class);
			coreModule = (CoreModule) constructor.newInstance(core);
			field.set(source, coreModule);
		}
		return resolve(module, coreModule);
	}
	
	public static CoreModuleHolder resolve(Module module, CoreModule coreModule)
	{
		String name = module.name().equals("") ? Ender.clazz.getHumanReadableClassName(coreModule.getClass()) : module.name();
		int priority = module.priority();
		return new CoreModuleHolder(name, priority, coreModule);
	}
	
	private final String name;
	private final int priority;
	private final CoreModule module;
	private CoreModuleState state = CoreModuleState.OFFLINE;
	
	public CoreModuleHolder(String name, int priority, CoreModule module)
	{
		this.name = Objects.requireNonNull(name, "NAME");
		this.priority = priority;
		this.module = Objects.requireNonNull(module, "module");
	}
	
	public String getName()
	{
		return name;
	}
	
	public int getPriority()
	{
		return priority;
	}
	
	public CoreModule getModule()
	{
		return module;
	}
	
	public CoreModuleState getState()
	{
		return state;
	}
	
	private void requireState(String action, CoreModuleState target)
	{
		if (state != target)
		{
			throw new IllegalStateException("cant " + action + " a module thats not in " + target + " state");
		}
	}
	
	public boolean start() throws Exception
	{
		requireState("start", CoreModuleState.OFFLINE);
		state = CoreModuleState.ONLINE;
		return module.start();
	}
	
	public boolean shutdown() throws Exception
	{
		requireState("shutdown", CoreModuleState.ONLINE);
		state = CoreModuleState.DEAD;
		return module.shutdown();
	}
	
	@Override
	public int compareTo(CoreModuleHolder other)
	{
		return Integer.compare(other.priority, this.priority);
	}
}
