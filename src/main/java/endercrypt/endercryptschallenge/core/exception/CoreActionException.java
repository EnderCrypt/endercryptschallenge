/**************************************************************************\
* EnderCrypt's Challenge                                                   *
* Copyright (C) 2023 by EnderCrypt                                         *
*                                                                          *
* This program is free software: you can redistribute it and/or modify     *
* it under the terms of the GNU Affero General Public License as           *
* published by the Free Software Foundation, either version 3 of           *
* the License, or (at your option) any later version.                      *
*                                                                          *
* This program is distributed in the hope that it will be useful,          *
* but WITHOUT ANY WARRANTY; without even the implied warranty of           *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
* GNU Affero General Public License for more details.                      *
*                                                                          *
* You should have received a copy of the GNU Affero General Public License *
* along with this program in its root directory.                           *
* If not, see <http://www.gnu.org/licenses/>.                              *
\**************************************************************************/

package endercrypt.endercryptschallenge.core.exception;

/**
 * @author EnderCrypt
 */
public class CoreActionException extends CoreException
{
	private static final long serialVersionUID = 2495291306855958163L;
	
	/**
	 * 
	 */
	
	public CoreActionException(String message)
	{
		super(message);
	}
	
	public CoreActionException(Throwable cause)
	{
		super(cause);
	}
	
	public CoreActionException(String message, Throwable cause)
	{
		super(message, cause);
	}
}
