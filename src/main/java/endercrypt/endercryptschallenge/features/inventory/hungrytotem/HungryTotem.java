/**************************************************************************\
* EnderCrypt's Challenge                                                   *
* Copyright (C) 2023 by EnderCrypt                                         *
*                                                                          *
* This program is free software: you can redistribute it and/or modify     *
* it under the terms of the GNU Affero General Public License as           *
* published by the Free Software Foundation, either version 3 of           *
* the License, or (at your option) any later version.                      *
*                                                                          *
* This program is distributed in the hope that it will be useful,          *
* but WITHOUT ANY WARRANTY; without even the implied warranty of           *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
* GNU Affero General Public License for more details.                      *
*                                                                          *
* You should have received a copy of the GNU Affero General Public License *
* along with this program in its root directory.                           *
* If not, see <http://www.gnu.org/licenses/>.                              *
\**************************************************************************/

package endercrypt.endercryptschallenge.features.inventory.hungrytotem;


import endercrypt.endercryptschallenge.core.Core;
import endercrypt.endercryptschallenge.features.popup.deathmessage.DeathMessage;
import endercrypt.endercryptschallenge.materials.MaterialCollections;
import endercrypt.endercryptschallenge.modules.features.MinecraftFeature;
import endercrypt.endercryptschallenge.utility.Utility;
import endercrypt.endercryptschallenge.utility.difficulty.Difficulties;
import endercrypt.endercryptschallenge.utility.difficulty.DifficultyRating;
import endercrypt.library.commons.RandomEntry;
import endercrypt.library.commons.misc.Ender;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;


/**
 * @author EnderCrypt
 */
public class HungryTotem extends MinecraftFeature
{
	private static final String totemName = ChatColor.YELLOW + "[Totem of Undying]" + ChatColor.RESET;
	
	public HungryTotem(Core core)
	{
		super(core);
	}
	
	@Override
	public void onSecond()
	{
		for (World world : server.getWorlds())
		{
			world.getPlayers().forEach(this::processPlayer);
		}
	}
	
	private void processPlayer(Player player)
	{
		int totems = countPlayerTotems(player);
		for (int i = 0; i < totems; i++)
		{
			processTotem(player);
		}
	}
	
	private static int countPlayerTotems(Player player)
	{
		PlayerInventory inventory = player.getInventory();
		
		int count = inventory.all(Material.TOTEM_OF_UNDYING)
			.keySet()
			.stream()
			.mapToInt(i -> inventory.getItem(i).getAmount())
			.sum();
		
		ItemStack offHand = inventory.getItemInOffHand();
		if (offHand.getType() == Material.TOTEM_OF_UNDYING)
		{
			count++;
		}
		
		return count;
	}
	
	private void processTotem(Player player)
	{
		DifficultyRating difficulty = Difficulties.getFor(player);
		double chance = 1.0 / difficulty.asIntValue(30, 20, 10);
		if (Math.random() > chance)
		{
			return;
		}
		List<ItemStack> foods = new ArrayList<>();
		for (ItemStack itemStack : player.getInventory())
		{
			if (itemStack != null && itemStack.getAmount() > 0 && MaterialCollections.foods.contains(itemStack.getType()))
			{
				foods.add(itemStack);
			}
		}
		if (foods.isEmpty())
		{
			player.sendMessage("The " + totemName + " took a " + ChatColor.RED + "bite out of you!");
			getFeature(DeathMessage.class).setKilledBy(player, totemName);
			double damage = Ender.math.randomRange(0.5, 2.0);
			player.damage(damage);
		}
		else
		{
			ItemStack food = RandomEntry.from(foods);
			String qualifier = food.getAmount() > 1 ? "some" : "all";
			player.sendMessage("The " + totemName + " ate " + qualifier + " of your " + Utility.getItemName(food));
			food.subtract();
		}
	}
}
