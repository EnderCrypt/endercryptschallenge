/**************************************************************************\
* EnderCrypt's Challenge                                                   *
* Copyright (C) 2023 by EnderCrypt                                         *
*                                                                          *
* This program is free software: you can redistribute it and/or modify     *
* it under the terms of the GNU Affero General Public License as           *
* published by the Free Software Foundation, either version 3 of           *
* the License, or (at your option) any later version.                      *
*                                                                          *
* This program is distributed in the hope that it will be useful,          *
* but WITHOUT ANY WARRANTY; without even the implied warranty of           *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
* GNU Affero General Public License for more details.                      *
*                                                                          *
* You should have received a copy of the GNU Affero General Public License *
* along with this program in its root directory.                           *
* If not, see <http://www.gnu.org/licenses/>.                              *
\**************************************************************************/

package endercrypt.endercryptschallenge.features.inventory.eatinggold;


import endercrypt.endercryptschallenge.core.Core;
import endercrypt.endercryptschallenge.modules.features.MinecraftFeature;

import java.util.HashMap;
import java.util.Map;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.player.PlayerItemConsumeEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffectType;


/**
 * @author EnderCrypt
 */
public class EatingGold extends MinecraftFeature
{
	private final Map<Material, Integer> foods = new HashMap<>();
	
	public EatingGold(Core core)
	{
		super(core);
		foods.put(Material.GOLDEN_CARROT, 30);
		foods.put(Material.GLISTERING_MELON_SLICE, 30);
		foods.put(Material.GOLDEN_APPLE, 60);
		foods.put(Material.ENCHANTED_GOLDEN_APPLE, 120);
	}
	
	@EventHandler
	public void onPlayerEat(PlayerItemConsumeEvent event)
	{
		Player player = event.getPlayer();
		ItemStack itemStack = event.getItem();
		Material itemType = itemStack.getType();
		
		int time = foods.getOrDefault(itemType, 0);
		if (time == 0)
		{
			return;
		}
		
		PlayerGoldHarmer harmer = new PlayerGoldHarmer(player, time);
		harmer.influence(PotionEffectType.NAUSEA, 1.0, 1);
		harmer.influence(PotionEffectType.POISON, 0.5, 1);
		harmer.influence(PotionEffectType.BLINDNESS, 0.05, 1);
		
		player.sendMessage("Eating unpurified gold caused your stomach to become upset!");
	}
}
