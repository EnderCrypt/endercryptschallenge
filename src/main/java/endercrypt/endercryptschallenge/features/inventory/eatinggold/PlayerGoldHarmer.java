/**************************************************************************\
* EnderCrypt's Challenge                                                   *
* Copyright (C) 2023 by EnderCrypt                                         *
*                                                                          *
* This program is free software: you can redistribute it and/or modify     *
* it under the terms of the GNU Affero General Public License as           *
* published by the Free Software Foundation, either version 3 of           *
* the License, or (at your option) any later version.                      *
*                                                                          *
* This program is distributed in the hope that it will be useful,          *
* but WITHOUT ANY WARRANTY; without even the implied warranty of           *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
* GNU Affero General Public License for more details.                      *
*                                                                          *
* You should have received a copy of the GNU Affero General Public License *
* along with this program in its root directory.                           *
* If not, see <http://www.gnu.org/licenses/>.                              *
\**************************************************************************/

package endercrypt.endercryptschallenge.features.inventory.eatinggold;


import java.util.Objects;

import org.bukkit.entity.Player;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;


/**
 * @author EnderCrypt
 */
public class PlayerGoldHarmer
{
	private final Player player;
	private final int durationTicks;
	
	public PlayerGoldHarmer(Player player, int durationSeconds)
	{
		this.player = Objects.requireNonNull(player, "player");
		this.durationTicks = durationSeconds * 20;
	}
	
	public void influence(PotionEffectType effect, double durationMultiplier, int intensity)
	{
		int effectDuration = (int) Math.ceil(durationTicks * durationMultiplier);
		player.addPotionEffect(new PotionEffect(effect, effectDuration, intensity));
	}
}
