/**************************************************************************\
* EnderCrypt's Challenge                                                   *
* Copyright (C) 2023 by EnderCrypt                                         *
*                                                                          *
* This program is free software: you can redistribute it and/or modify     *
* it under the terms of the GNU Affero General Public License as           *
* published by the Free Software Foundation, either version 3 of           *
* the License, or (at your option) any later version.                      *
*                                                                          *
* This program is distributed in the hope that it will be useful,          *
* but WITHOUT ANY WARRANTY; without even the implied warranty of           *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
* GNU Affero General Public License for more details.                      *
*                                                                          *
* You should have received a copy of the GNU Affero General Public License *
* along with this program in its root directory.                           *
* If not, see <http://www.gnu.org/licenses/>.                              *
\**************************************************************************/

package endercrypt.endercryptschallenge.features.inventory.nethermelt;


import endercrypt.endercryptschallenge.core.Core;
import endercrypt.endercryptschallenge.features.inventory.nethermelt.processors.MeltConvert;
import endercrypt.endercryptschallenge.features.inventory.nethermelt.processors.MeltDestroy;
import endercrypt.endercryptschallenge.modules.features.MinecraftFeature;
import endercrypt.endercryptschallenge.utility.difficulty.Difficulties;
import endercrypt.endercryptschallenge.utility.difficulty.DifficultyRating;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;


/**
 * @author EnderCrypt
 */
public class NetherEvaporation extends MinecraftFeature
{
	private List<MeltProcessor> processors = new ArrayList<>();
	
	public NetherEvaporation(Core core)
	{
		super(core);
		processors.add(new MeltDestroy(Material.SNOWBALL));
		processors.add(new MeltDestroy(Material.SNOW_BLOCK));
		processors.add(new MeltDestroy(Material.ICE));
		processors.add(new MeltDestroy(Material.BLUE_ICE));
		processors.add(new MeltDestroy(Material.FROSTED_ICE));
		processors.add(new MeltConvert(Material.MILK_BUCKET, Material.BUCKET));
		processors.add(new MeltConvert(Material.WATER_BUCKET, Material.BUCKET));
		processors.add(new MeltConvert(Material.POWDER_SNOW_BUCKET, Material.BUCKET));
	}
	
	@Override
	public void onCycleInventoryItem(Player player, int index, ItemStack item)
	{
		DifficultyRating difficulty = Difficulties.getFor(player);
		if (Math.random() > difficulty.asDoubleValue(0.1, 0.2, 0.3))
		{
			return;
		}
		
		if ("world_nether".equals(player.getWorld().getName()) == false)
		{
			return;
		}
		
		for (MeltProcessor processor : processors)
		{
			MeltResult result = processor.process(item);
			result.perform(player, index, item);
			if (result.isEnd())
			{
				break;
			}
		}
	}
}
