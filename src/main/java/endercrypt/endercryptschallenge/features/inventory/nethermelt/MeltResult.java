/**************************************************************************\
* EnderCrypt's Challenge                                                   *
* Copyright (C) 2023 by EnderCrypt                                         *
*                                                                          *
* This program is free software: you can redistribute it and/or modify     *
* it under the terms of the GNU Affero General Public License as           *
* published by the Free Software Foundation, either version 3 of           *
* the License, or (at your option) any later version.                      *
*                                                                          *
* This program is distributed in the hope that it will be useful,          *
* but WITHOUT ANY WARRANTY; without even the implied warranty of           *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
* GNU Affero General Public License for more details.                      *
*                                                                          *
* You should have received a copy of the GNU Affero General Public License *
* along with this program in its root directory.                           *
* If not, see <http://www.gnu.org/licenses/>.                              *
\**************************************************************************/

package endercrypt.endercryptschallenge.features.inventory.nethermelt;


import endercrypt.endercryptschallenge.utility.Utility;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;


/**
 * @author EnderCrypt
 */
public abstract class MeltResult
{
	public static MeltResult doNothing()
	{
		return new MeltResult(false)
		{
			@Override
			public void perform(Player player, int index, ItemStack item)
			{
				// do nothing
			}
		};
	}
	
	public static MeltResult destroyItem()
	{
		return new MeltResult(true)
		{
			@Override
			public void perform(Player player, int index, ItemStack item)
			{
				MeltResult.sendMeltMessage(player, item);
				item.subtract();
			}
		};
	}
	
	public static MeltResult convertItem(Material target)
	{
		return new MeltResult(true)
		{
			@Override
			public void perform(Player player, int index, ItemStack item)
			{
				MeltResult.sendMeltMessage(player, item);
				item.subtract();
				player.getInventory().addItem(new ItemStack(target, 1));
			}
		};
	}
	
	private static void sendMeltMessage(Player player, ItemStack item)
	{
		player.sendMessage("Your " + Utility.getItemName(item) + " evaporated due to the heat");
	}
	
	private final boolean end;
	
	private MeltResult(boolean end)
	{
		this.end = end;
	}
	
	public abstract void perform(Player player, int index, ItemStack item);
	
	public boolean isEnd()
	{
		return end;
	}
}
