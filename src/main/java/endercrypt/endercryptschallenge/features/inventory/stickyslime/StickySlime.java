/**************************************************************************\
* EnderCrypt's Challenge                                                   *
* Copyright (C) 2023 by EnderCrypt                                         *
*                                                                          *
* This program is free software: you can redistribute it and/or modify     *
* it under the terms of the GNU Affero General Public License as           *
* published by the Free Software Foundation, either version 3 of           *
* the License, or (at your option) any later version.                      *
*                                                                          *
* This program is distributed in the hope that it will be useful,          *
* but WITHOUT ANY WARRANTY; without even the implied warranty of           *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
* GNU Affero General Public License for more details.                      *
*                                                                          *
* You should have received a copy of the GNU Affero General Public License *
* along with this program in its root directory.                           *
* If not, see <http://www.gnu.org/licenses/>.                              *
\**************************************************************************/

package endercrypt.endercryptschallenge.features.inventory.stickyslime;


import endercrypt.endercryptschallenge.core.Core;
import endercrypt.endercryptschallenge.modules.features.MinecraftFeature;
import endercrypt.endercryptschallenge.utility.difficulty.Difficulties;
import endercrypt.endercryptschallenge.utility.difficulty.DifficultyRating;

import org.bukkit.Material;
import org.bukkit.entity.HumanEntity;
import org.bukkit.event.EventHandler;
import org.bukkit.event.inventory.InventoryAction;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.ItemStack;


/**
 * @author EnderCrypt
 */
public class StickySlime extends MinecraftFeature
{
	public StickySlime(Core core)
	{
		super(core);
	}
	
	@EventHandler
	public void onPickupSlime(InventoryClickEvent event)
	{
		// chance
		DifficultyRating difficulty = Difficulties.getFor(event);
		if (Math.random() > difficulty.asDoubleValue(0.5, 0.75, 0.9))
		{
			return;
		}
		// slimeball
		ItemStack item = event.getCurrentItem();
		if (item == null)
		{
			return; // wtf?
		}
		if (item.getType() != Material.SLIME_BALL)
		{
			return;
		}
		// click
		InventoryAction action = event.getAction();
		if (action.name().startsWith("PICKUP_") == false && action != InventoryAction.MOVE_TO_OTHER_INVENTORY)
		{
			return;
		}
		// cancel
		event.setCancelled(true);
		HumanEntity human = event.getWhoClicked();
		human.sendMessage("The slimeball is stuck!");
	}
}
