/**************************************************************************\
* EnderCrypt's Challenge                                                   *
* Copyright (C) 2023 by EnderCrypt                                         *
*                                                                          *
* This program is free software: you can redistribute it and/or modify     *
* it under the terms of the GNU Affero General Public License as           *
* published by the Free Software Foundation, either version 3 of           *
* the License, or (at your option) any later version.                      *
*                                                                          *
* This program is distributed in the hope that it will be useful,          *
* but WITHOUT ANY WARRANTY; without even the implied warranty of           *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
* GNU Affero General Public License for more details.                      *
*                                                                          *
* You should have received a copy of the GNU Affero General Public License *
* along with this program in its root directory.                           *
* If not, see <http://www.gnu.org/licenses/>.                              *
\**************************************************************************/

package endercrypt.endercryptschallenge.features.inventory.burning;


import endercrypt.endercryptschallenge.core.Core;
import endercrypt.endercryptschallenge.features.popup.deathmessage.DeathMessage;
import endercrypt.endercryptschallenge.materials.MaterialCollections;
import endercrypt.endercryptschallenge.modules.features.MinecraftFeature;
import endercrypt.endercryptschallenge.utility.Utility;
import endercrypt.endercryptschallenge.utility.difficulty.Difficulties;
import endercrypt.endercryptschallenge.utility.difficulty.DifficultyRating;
import endercrypt.library.commons.misc.Ender;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffectType;


/**
 * @author EnderCrypt
 */
public class BurningItems extends MinecraftFeature
{
	public BurningItems(Core core)
	{
		super(core);
	}
	
	@Override
	public void onCycleInventoryItem(Player player, int index, ItemStack item)
	{
		DifficultyRating difficulty = Difficulties.getFor(player);
		if (Math.random() > difficulty.asDoubleValue(0.333, 0.666, 1.0))
			return;
		
		if (Utility.isInSurvival(player) == false)
			return;
		
		if (player.hasPotionEffect(PotionEffectType.FIRE_RESISTANCE))
			return;
		
		if (MaterialCollections.burning.contains(item) == false)
			return;
		
		double damage = Ender.math.randomRange(0.75, 1.25) * difficulty.asDoubleValue(0, 1, 2, 3);
		
		if (damage > 0)
		{
			DeathMessage message = core.getMinecraftFeatureManager().getFeature(DeathMessage.class);
			message.setKilledBy(player, "burns from " + Utility.getItemName(item));
			player.damage(damage);
			player.sendMessage("The " + Utility.getItemName(item) + " " + ChatColor.RED + "burns" + ChatColor.RESET + " you!");
		}
	}
}
