/**************************************************************************\
* EnderCrypt's Challenge                                                   *
* Copyright (C) 2023 by EnderCrypt                                         *
*                                                                          *
* This program is free software: you can redistribute it and/or modify     *
* it under the terms of the GNU Affero General Public License as           *
* published by the Free Software Foundation, either version 3 of           *
* the License, or (at your option) any later version.                      *
*                                                                          *
* This program is distributed in the hope that it will be useful,          *
* but WITHOUT ANY WARRANTY; without even the implied warranty of           *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
* GNU Affero General Public License for more details.                      *
*                                                                          *
* You should have received a copy of the GNU Affero General Public License *
* along with this program in its root directory.                           *
* If not, see <http://www.gnu.org/licenses/>.                              *
\**************************************************************************/

package endercrypt.endercryptschallenge.features.inventory.hatchegg;


import endercrypt.endercryptschallenge.core.Core;
import endercrypt.endercryptschallenge.modules.features.MinecraftFeature;

import java.util.Set;

import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;


/**
 * @author EnderCrypt
 */
public class HatchEgg extends MinecraftFeature
{
	private static final Set<EggHatcher> eggHatchers = Set.of(new NormalEggHatcher(), new SpiderSpawnEggHatcher());
	
	public HatchEgg(Core core)
	{
		super(core);
	}
	
	@Override
	public void onCycleInventoryItem(Player player, int index, ItemStack item)
	{
		hatchEggs(player, item);
	}
	
	private void hatchEggs(Player player, ItemStack item)
	{
		int count = item.getAmount();
		for (int i = 0; i < count; i++)
		{
			hatchSingleEgg(player, item);
		}
	}
	
	private void hatchSingleEgg(Player player, ItemStack item)
	{
		for (EggHatcher hatcher : eggHatchers)
		{
			if (Math.random() > hatcher.getChance())
			{
				continue;
			}
			if (hatcher.isType(item.getType()) == false)
			{
				continue;
			}
			
			hatcher.hatch(player, item);
			item.subtract();
			player.sendMessage("The egg in your inventory hatches");
		}
	}
}
