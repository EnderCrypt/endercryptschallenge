/**************************************************************************\
* EnderCrypt's Challenge                                                   *
* Copyright (C) 2023 by EnderCrypt                                         *
*                                                                          *
* This program is free software: you can redistribute it and/or modify     *
* it under the terms of the GNU Affero General Public License as           *
* published by the Free Software Foundation, either version 3 of           *
* the License, or (at your option) any later version.                      *
*                                                                          *
* This program is distributed in the hope that it will be useful,          *
* but WITHOUT ANY WARRANTY; without even the implied warranty of           *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
* GNU Affero General Public License for more details.                      *
*                                                                          *
* You should have received a copy of the GNU Affero General Public License *
* along with this program in its root directory.                           *
* If not, see <http://www.gnu.org/licenses/>.                              *
\**************************************************************************/

package endercrypt.endercryptschallenge.features.inventory.hatchegg;


import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.entity.Player;
import org.bukkit.entity.Spider;
import org.bukkit.inventory.ItemStack;


/**
 * spawn eggs can be picked up by players via hoppers<br>
 * (bug found by AtomicWarlock)
 * 
 * @author EnderCrypt
 */
public class SpiderSpawnEggHatcher implements EggHatcher
{
	@Override
	public boolean isType(Material material)
	{
		return material == Material.SPIDER_SPAWN_EGG;
	}
	
	@Override
	public double getChance()
	{
		return 1.0 / 50;
	}
	
	@Override
	public void hatch(Player player, ItemStack itemStack)
	{
		Location location = player.getLocation();
		World world = location.getWorld();
		Spider spider = world.spawn(location, Spider.class);
		player.addPassenger(spider);
		spider.setTarget(player);
		
		// TODO: make all spawn eggs hatch like this
	}
}
