/**************************************************************************\
* EnderCrypt's Challenge                                                   *
* Copyright (C) 2023 by EnderCrypt                                         *
*                                                                          *
* This program is free software: you can redistribute it and/or modify     *
* it under the terms of the GNU Affero General Public License as           *
* published by the Free Software Foundation, either version 3 of           *
* the License, or (at your option) any later version.                      *
*                                                                          *
* This program is distributed in the hope that it will be useful,          *
* but WITHOUT ANY WARRANTY; without even the implied warranty of           *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
* GNU Affero General Public License for more details.                      *
*                                                                          *
* You should have received a copy of the GNU Affero General Public License *
* along with this program in its root directory.                           *
* If not, see <http://www.gnu.org/licenses/>.                              *
\**************************************************************************/

package endercrypt.endercryptschallenge.features.inventory.myceliumfood;


import endercrypt.endercryptschallenge.core.Core;
import endercrypt.endercryptschallenge.materials.MaterialCollections;
import endercrypt.endercryptschallenge.modules.features.MinecraftFeature;
import endercrypt.endercryptschallenge.utility.Utility;
import endercrypt.library.commons.RandomEntry;
import endercrypt.library.commons.misc.Ender;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;


/**
 * @author EnderCrypt
 */
public class MyceliumFood extends MinecraftFeature
{
	public MyceliumFood(Core core)
	{
		super(core);
	}
	
	@Override
	public void onCycleEntity(Entity entity)
	{
		if (entity instanceof Player player)
		{
			processPlayer(player);
		}
	}
	
	private void processPlayer(Player player)
	{
		Block floor = player.getLocation().getBlock().getRelative(BlockFace.DOWN);
		if (floor.getType() != Material.MYCELIUM)
			return;
		
		List<ItemStack> consumptions = consumeMushrooms(player);
		
		if (consumptions.isEmpty() == false)
		{
			// create mushrooms
			int mushrooms = consumptions.stream().mapToInt(ItemStack::getAmount).sum();
			while (mushrooms > 0)
			{
				int stackSize = Math.min(64, mushrooms);
				mushrooms -= stackSize;
				
				Material mushroomMaterial = MaterialCollections.mushrooms.getRandom();
				ItemStack mushroomStack = new ItemStack(mushroomMaterial, stackSize);
				player.getInventory().addItem(mushroomStack);
			}
			
			// info
			String itemNames = consumptions.stream()
				.map(s -> s.getAmount() + "x " + Utility.getItemName(s))
				.collect(Collectors.joining(", "));
			
			player.sendMessage("Your " + itemNames + " gets engulfed by a quickly growing mushroom");
		}
	}
	
	private List<ItemStack> consumeMushrooms(Player player)
	{
		Map<Material, Integer> consumptions = new HashMap<>();
		
		// all viable item stacks
		PlayerInventory inventory = player.getInventory();
		List<ItemStack> foods = Arrays.stream(inventory.getContents())
			.filter(Objects::nonNull)
			.filter(stack -> stack.getType() != Material.AIR)
			.filter(MaterialCollections.myceliumFoods::contains)
			.collect(Collectors.toUnmodifiableList());
		
		// consume
		int foodAmount = foods.stream().mapToInt(ItemStack::getAmount).sum();
		int mushroomConsumptions = (int) Math.ceil(foodAmount * Ender.math.randomRange(0.05, 0.3));
		for (int i = 0; i < mushroomConsumptions; i++)
		{
			ItemStack item = RandomEntry.from(foods);
			if (item.getAmount() > 0)
			{
				Material material = item.getType();
				int count = consumptions.getOrDefault(material, 0);
				count++;
				consumptions.put(material, count);
				item.subtract();
			}
		}
		
		// amounts to stacks
		return consumptions.entrySet()
			.stream()
			.map(e -> new ItemStack(e.getKey(), e.getValue()))
			.collect(Collectors.toUnmodifiableList());
	}
}
