/**************************************************************************\
* EnderCrypt's Challenge                                                   *
* Copyright (C) 2023 by EnderCrypt                                         *
*                                                                          *
* This program is free software: you can redistribute it and/or modify     *
* it under the terms of the GNU Affero General Public License as           *
* published by the Free Software Foundation, either version 3 of           *
* the License, or (at your option) any later version.                      *
*                                                                          *
* This program is distributed in the hope that it will be useful,          *
* but WITHOUT ANY WARRANTY; without even the implied warranty of           *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
* GNU Affero General Public License for more details.                      *
*                                                                          *
* You should have received a copy of the GNU Affero General Public License *
* along with this program in its root directory.                           *
* If not, see <http://www.gnu.org/licenses/>.                              *
\**************************************************************************/

package endercrypt.endercryptschallenge.features.easteregg.plantedmonsters;


import endercrypt.endercryptschallenge.core.Core;
import endercrypt.endercryptschallenge.features.entities.all.growing.GrowingMonsters;
import endercrypt.endercryptschallenge.modules.features.MinecraftFeature;
import endercrypt.endercryptschallenge.utility.Utility;
import endercrypt.library.commons.misc.Ender;

import java.util.EnumMap;
import java.util.Map;

import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.entity.Creeper;
import org.bukkit.entity.Item;
import org.bukkit.entity.Mob;
import org.bukkit.entity.Skeleton;
import org.bukkit.entity.Zombie;
import org.bukkit.event.EventHandler;
import org.bukkit.event.entity.ItemDespawnEvent;


/**
 * @author EnderCrypt
 */
public class PlantedMonsters extends MinecraftFeature
{
	private Map<Material, Class<? extends Mob>> seeds = new EnumMap<>(Material.class);
	
	public PlantedMonsters(Core core)
	{
		super(core);
		seeds.put(Material.ROTTEN_FLESH, Zombie.class);
		seeds.put(Material.BONE, Skeleton.class);
		seeds.put(Material.BONE_MEAL, Skeleton.class);
		seeds.put(Material.GUNPOWDER, Creeper.class);
	}
	
	@EventHandler
	public void onItemDespawn(ItemDespawnEvent event)
	{
		Item item = event.getEntity();
		Class<? extends Mob> mob = seeds.get(item.getItemStack().getType());
		if (mob == null)
			return;
		
		Block block = item.getLocation().getBlock();
		if (isSeedBlockStructure(block) == false)
			return;
		
		Block target = block.getRelative(BlockFace.UP, 2);
		if (GrowingMonsters.isValidGrowingBlock(target, false, 2) == false)
			return;
		
		int amount = item.getItemStack().getAmount();
		for (int i = 0; i < amount; i++)
		{
			getFeature(GrowingMonsters.class).create(mob, target, Ender.math.randomRange(30, 45));
		}
	}
	
	private boolean isSeedBlockStructure(Block block)
	{
		if (block.getType() != Material.AIR)
			return false;
		
		for (BlockFace face : Utility.cartesianBlockFaces)
		{
			if (block.getRelative(face).getType().isSolid() == false)
			{
				return false;
			}
		}
		
		return true;
	}
}
