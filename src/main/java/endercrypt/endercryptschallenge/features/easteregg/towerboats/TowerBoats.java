/**************************************************************************\
* EnderCrypt's Challenge                                                   *
* Copyright (C) 2023 by EnderCrypt                                         *
*                                                                          *
* This program is free software: you can redistribute it and/or modify     *
* it under the terms of the GNU Affero General Public License as           *
* published by the Free Software Foundation, either version 3 of           *
* the License, or (at your option) any later version.                      *
*                                                                          *
* This program is distributed in the hope that it will be useful,          *
* but WITHOUT ANY WARRANTY; without even the implied warranty of           *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
* GNU Affero General Public License for more details.                      *
*                                                                          *
* You should have received a copy of the GNU Affero General Public License *
* along with this program in its root directory.                           *
* If not, see <http://www.gnu.org/licenses/>.                              *
\**************************************************************************/

package endercrypt.endercryptschallenge.features.easteregg.towerboats;


import endercrypt.endercryptschallenge.core.Core;
import endercrypt.endercryptschallenge.materials.MaterialCollections;
import endercrypt.endercryptschallenge.modules.features.MinecraftFeature;

import org.bukkit.Material;
import org.bukkit.TreeSpecies;
import org.bukkit.entity.Boat;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.player.PlayerInteractEntityEvent;
import org.bukkit.inventory.EntityEquipment;
import org.bukkit.inventory.ItemStack;


/**
 * @author EnderCrypt
 */
public class TowerBoats extends MinecraftFeature
{
	public TowerBoats(Core core)
	{
		super(core);
	}
	
	@Override
	public void onCycleEntity(Entity entity)
	{
		if (entity instanceof Boat boat)
		{
			updateBoat(boat);
		}
	}
	
	private void updateBoat(Boat boat)
	{
		Entity carrier = boat.getVehicle();
		if (carrier == null)
		{
			return;
		}
		if (carrier instanceof Player == false)
		{
			return;
		}
		Player playerCarrier = (Player) carrier;
		if (isCapableOfCarryingBoat(playerCarrier))
		{
			return;
		}
		
		playerCarrier.removePassenger(boat);
	}
	
	@EventHandler
	public void onPlayerClick(PlayerInteractEntityEvent event)
	{
		Player player = event.getPlayer();
		ItemStack item = player.getEquipment().getItem(event.getHand());
		Material itemType = item.getType();
		if (MaterialCollections.boats.contains(itemType) == false)
		{
			return;
		}
		Entity target = event.getRightClicked();
		if (isCapableOfCarryingBoat(target) == false)
		{
			return;
		}
		
		// perform
		perform(player, target, item.getType());
		item.subtract();
	}
	
	private static void perform(Player source, Entity target, Material boatType)
	{
		// create boat
		Boat boat = target.getWorld().spawn(target.getLocation(), Boat.class);
		TreeSpecies treeSpecies = getBoatTreeSpecies(boatType);
		boat.setWoodType(treeSpecies);
		
		// mount boat on target
		target.addPassenger(boat);
		
		// mount source on boat
		boat.addPassenger(source);
	}
	
	private static boolean isCapableOfCarryingBoat(Entity target)
	{
		if (target instanceof Player == false)
		{
			return false;
		}
		Player player = (Player) target;
		EntityEquipment equipment = player.getEquipment();
		int items = equipment.getItemInMainHand().getAmount() + equipment.getItemInOffHand().getAmount();
		if (items > 0)
		{
			return false;
		}
		
		return true;
	}
	
	private static TreeSpecies getBoatTreeSpecies(Material material)
	{
		switch (material)
		{
		case OAK_BOAT:
			return TreeSpecies.GENERIC;
		case ACACIA_BOAT:
			return TreeSpecies.ACACIA;
		case BIRCH_BOAT:
			return TreeSpecies.BIRCH;
		case DARK_OAK_BOAT:
			return TreeSpecies.DARK_OAK;
		case JUNGLE_BOAT:
			return TreeSpecies.JUNGLE;
		default:
			throw new IllegalArgumentException("unknown boat type: " + material);
		}
	}
}
