/**************************************************************************\
* EnderCrypt's Challenge                                                   *
* Copyright (C) 2023 by EnderCrypt                                         *
*                                                                          *
* This program is free software: you can redistribute it and/or modify     *
* it under the terms of the GNU Affero General Public License as           *
* published by the Free Software Foundation, either version 3 of           *
* the License, or (at your option) any later version.                      *
*                                                                          *
* This program is distributed in the hope that it will be useful,          *
* but WITHOUT ANY WARRANTY; without even the implied warranty of           *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
* GNU Affero General Public License for more details.                      *
*                                                                          *
* You should have received a copy of the GNU Affero General Public License *
* along with this program in its root directory.                           *
* If not, see <http://www.gnu.org/licenses/>.                              *
\**************************************************************************/

package endercrypt.endercryptschallenge.features.easteregg.twitter;


import endercrypt.endercryptschallenge.core.Core;
import endercrypt.endercryptschallenge.modules.features.MinecraftFeature;
import endercrypt.endercryptschallenge.utility.Utility;
import endercrypt.library.commons.position.Circle;

import java.util.List;
import java.util.stream.Stream;

import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.entity.Witch;
import org.bukkit.event.EventHandler;

import io.papermc.paper.event.player.AsyncChatEvent;
import net.kyori.adventure.text.Component;
import net.kyori.adventure.text.format.TextColor;


/**
 * Idea by FatherLorenzo
 * 
 * @author EnderCrypt
 */
public class Twitter extends MinecraftFeature
{
	private static final double DISTANCE = 15;
	private static final int AMOUNT = 20;
	private static final int CHANCE = 50; // one in x chance
	private static final List<String> words = List.of("white", "black", "man", "woman", "elon", "gender", "trigger", "twitter", "kill", "fat", "history", "4chan");
	
	public Twitter(Core core)
	{
		super(core);
	}
	
	private boolean isOffensive(String message)
	{
		message = message.toLowerCase();
		for (String word : words)
		{
			if (message.contains(word))
			{
				return true;
			}
		}
		return false;
	}
	
	@EventHandler
	public void chat(AsyncChatEvent event)
	{
		if (Math.random() > 1.0 / CHANCE)
			return;
		
		String message = Utility.componentToString(event.message());
		if (isOffensive(message) == false)
			return;
		
		runOnBukkit(() -> attack(event.getPlayer()));
	}
	
	private void attack(Player player)
	{
		List<Location> locations = Stream.generate(() -> getRandomLocation(player, DISTANCE))
			.limit(AMOUNT * 2)
			.filter(l -> l.distance(player.getLocation()) < DISTANCE * 1.25)
			.limit(AMOUNT)
			.toList();
		
		if (locations.size() < AMOUNT)
			return;
		
		player.sendMessage("What did you just say?");
		locations.forEach(this::spawnTwitterWitch);
	}
	
	private Location getRandomLocation(Player player, double distance)
	{
		double angle = Circle.random();
		double x = Math.sin(angle) * distance;
		double z = Math.cos(angle) * distance;
		Location location = player.getLocation().add(x, 0, z);
		return player.getWorld().getHighestBlockAt(location).getLocation().add(0, 1, 0);
	}
	
	private void spawnTwitterWitch(Location location)
	{
		Witch witch = location.getWorld().spawn(location, Witch.class);
		witch.setCustomNameVisible(true);
		witch.customName(Component.text("Twitter user").color(TextColor.color(29, 161, 242)));
	}
}
