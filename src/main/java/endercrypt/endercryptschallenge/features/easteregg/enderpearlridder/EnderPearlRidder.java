/**************************************************************************\
* EnderCrypt's Challenge                                                   *
* Copyright (C) 2023 by EnderCrypt                                         *
*                                                                          *
* This program is free software: you can redistribute it and/or modify     *
* it under the terms of the GNU Affero General Public License as           *
* published by the Free Software Foundation, either version 3 of           *
* the License, or (at your option) any later version.                      *
*                                                                          *
* This program is distributed in the hope that it will be useful,          *
* but WITHOUT ANY WARRANTY; without even the implied warranty of           *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
* GNU Affero General Public License for more details.                      *
*                                                                          *
* You should have received a copy of the GNU Affero General Public License *
* along with this program in its root directory.                           *
* If not, see <http://www.gnu.org/licenses/>.                              *
\**************************************************************************/

package endercrypt.endercryptschallenge.features.easteregg.enderpearlridder;


import endercrypt.endercryptschallenge.core.Core;
import endercrypt.endercryptschallenge.modules.features.MinecraftFeature;

import java.util.List;
import java.util.Objects;

import org.bukkit.Material;
import org.bukkit.entity.EnderPearl;
import org.bukkit.entity.Player;
import org.bukkit.entity.Projectile;
import org.bukkit.event.EventHandler;
import org.bukkit.event.entity.ProjectileLaunchEvent;
import org.bukkit.inventory.EntityEquipment;
import org.bukkit.inventory.ItemStack;
import org.bukkit.projectiles.ProjectileSource;


/**
 * @author EnderCrypt
 */
public class EnderPearlRidder extends MinecraftFeature
{
	private static Material requiredItemMaterial = Material.LEAD;
	private static String requiredItemLauchMessage = "You attach the lead to the Ender pearl and launch it";
	private static boolean consumeRequiredItem = false;
	
	public EnderPearlRidder(Core core)
	{
		super(core);
	}
	
	@EventHandler
	public void onProjectileLaunch(ProjectileLaunchEvent event)
	{
		Projectile projectile = event.getEntity();
		ProjectileSource projectileSource = projectile.getShooter();
		if (projectile instanceof EnderPearl == false || projectileSource instanceof Player == false)
		{
			return;
		}
		
		EnderPearl enderPearl = (EnderPearl) projectile;
		Player player = (Player) projectileSource;
		
		if (requiredItemMaterial != null && isPlayerHoldsItem(player, requiredItemMaterial) == false)
		{
			return;
		}
		
		// message
		if (requiredItemMaterial != null)
		{
			player.sendMessage(requiredItemLauchMessage);
		}
		
		// consume item
		if (requiredItemMaterial != null && consumeRequiredItem)
		{
			player.getInventory().remove(new ItemStack(requiredItemMaterial, 1));
		}
		
		// delete other active ender pearls by this player
		projectile.getWorld().getEntitiesByClass(EnderPearl.class)
			.stream()
			.filter(e -> e.getShooter() instanceof Player)
			.filter(e -> player.equals(e.getShooter()))
			.filter(e -> e != enderPearl)
			.forEach(EnderPearl::remove);
		
		// passanger
		enderPearl.addPassenger(player);
	}
	
	private boolean isPlayerHoldsItem(Player player, Material material)
	{
		EntityEquipment equipment = player.getEquipment();
		return List.of(equipment.getItemInMainHand(), equipment.getItemInOffHand())
			.stream()
			.map(ItemStack::getType)
			.anyMatch(m -> Objects.equals(m, material));
	}
}
