/**************************************************************************\
* EnderCrypt's Challenge                                                   *
* Copyright (C) 2023 by EnderCrypt                                         *
*                                                                          *
* This program is free software: you can redistribute it and/or modify     *
* it under the terms of the GNU Affero General Public License as           *
* published by the Free Software Foundation, either version 3 of           *
* the License, or (at your option) any later version.                      *
*                                                                          *
* This program is distributed in the hope that it will be useful,          *
* but WITHOUT ANY WARRANTY; without even the implied warranty of           *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
* GNU Affero General Public License for more details.                      *
*                                                                          *
* You should have received a copy of the GNU Affero General Public License *
* along with this program in its root directory.                           *
* If not, see <http://www.gnu.org/licenses/>.                              *
\**************************************************************************/

package endercrypt.endercryptschallenge.features.nature.acg;


import endercrypt.endercryptschallenge.core.Core;
import endercrypt.endercryptschallenge.modules.features.MinecraftFeature;

import java.util.HashMap;
import java.util.Map;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerMoveEvent;


/**
 * prevent disconnect to avoid ACG
 * 
 * @author EnderCrypt
 */
public class AntiCheesingGodReconnect extends MinecraftFeature
{
	private final Map<Player, Integer> immidiete = new HashMap<>();
	
	public AntiCheesingGodReconnect(Core core)
	{
		super(core);
	}
	
	@EventHandler
	public void onPlayerJoin(PlayerJoinEvent event)
	{
		Player player = event.getPlayer();
		if (resolve(player).acg().isImmidieteStrike())
		{
			immidiete.put(player, 20);
		}
	}
	
	@EventHandler
	public void onPlayerMove(PlayerMoveEvent event)
	{
		Player player = event.getPlayer();
		Integer count = immidiete.get(player);
		if (count != null)
		{
			AntiCheesingGod acg = getFeature(AntiCheesingGod.class);
			if (acg.isEligableForBeingStruck(player))
			{
				count--;
				immidiete.put(player, count);
				if (count <= 0)
				{
					immidiete.remove(player);
					if (acg.isPlayerInDirectSight(player))
					{
						acg.talk(player, "Thou disconnect button wont save you today mortal!");
					}
					acg.strikePlayerDown(player);
				}
			}
		}
	}
}
