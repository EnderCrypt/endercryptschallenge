/**************************************************************************\
* EnderCrypt's Challenge                                                   *
* Copyright (C) 2023 by EnderCrypt                                         *
*                                                                          *
* This program is free software: you can redistribute it and/or modify     *
* it under the terms of the GNU Affero General Public License as           *
* published by the Free Software Foundation, either version 3 of           *
* the License, or (at your option) any later version.                      *
*                                                                          *
* This program is distributed in the hope that it will be useful,          *
* but WITHOUT ANY WARRANTY; without even the implied warranty of           *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
* GNU Affero General Public License for more details.                      *
*                                                                          *
* You should have received a copy of the GNU Affero General Public License *
* along with this program in its root directory.                           *
* If not, see <http://www.gnu.org/licenses/>.                              *
\**************************************************************************/

package endercrypt.endercryptschallenge.features.nature.acg;


import endercrypt.endercryptschallenge.core.Core;
import endercrypt.endercryptschallenge.modules.features.MinecraftFeature;
import endercrypt.endercryptschallenge.modules.worlds.WorldProfile;
import endercrypt.endercryptschallenge.modules.worlds.list.nether.NetherWorld;
import endercrypt.endercryptschallenge.utility.Utility;
import endercrypt.endercryptschallenge.utility.difficulty.Difficulties;
import endercrypt.endercryptschallenge.utility.difficulty.DifficultyRating;
import endercrypt.library.commons.misc.Ender;
import endercrypt.library.commons.randomizer.Randomizer;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Particle;
import org.bukkit.World;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.player.PlayerRespawnEvent;


/**
 * All hail the great Anti Cheesing God, for he will strike you down
 * 
 * @author EnderCrypt
 */
public class AntiCheesingGod extends MinecraftFeature
{
	private static final Particle particleEffect = Particle.SOUL;
	private static final int strikeDelay = 15;
	
	private final Map<Player, Double> countdowns = new HashMap<>();
	
	public AntiCheesingGod(Core core)
	{
		super(core);
		getDebug().enableDebugSupport();
	}
	
	@Override
	public void onSecond()
	{
		server.getWorlds().forEach(this::processWorld);
	}
	
	private void processWorld(World world)
	{
		DifficultyRating difficulty = Difficulties.getFor(world);
		double missChance = difficulty.asDoubleValue(1000, 30, 25, 20) * 100_000_000;
		Randomizer<Player> randomizer = new Randomizer<>();
		randomizer.enter(missChance, null);
		for (Player player : world.getPlayers())
		{
			if (countdowns.containsKey(player))
			{
				continue;
			}
			if (isEligableForBeingStruck(player) == false)
			{
				continue;
			}
			double dangerRating = getDangerRating(player);
			if (dangerRating < 0)
			{
				continue;
			}
			double chance = Math.pow(dangerRating * 0.1, 6);
			if (isDebug())
			{
				double hitChance = 1.0 / (missChance + chance) * chance;
				int hitFrequency = (int) Math.round(1.0 / hitChance);
				player.sendMessage("danger: " + (int) Math.round(dangerRating) + " chance: " + Ender.math.round(100.0 * hitChance, 3) + "% (1 hit every " + hitFrequency + " seconds)");
			}
			if (chance > 0)
			{
				randomizer.enter(chance, player);
			}
		}
		
		Player player = randomizer.pull();
		if (player == null)
		{
			return;
		}
		
		resolve(player).acg().setImmidieteStrike(true);
		player.sendMessage(ChatColor.BOLD.toString() + ChatColor.RED + core.getDataManager().getMessages().getAcgWarning().getRandom());
		countdowns.put(player, (double) strikeDelay);
	}
	
	@Override
	public void onTick()
	{
		for (Map.Entry<Player, Double> entry : countdowns.entrySet())
		{
			updatePlayer(entry);
		}
	}
	
	@EventHandler
	public void onRespawn(PlayerRespawnEvent event)
	{
		Player player = event.getPlayer();
		if (event.isBedSpawn() && countdowns.containsKey(player))
		{
			countdowns.put(player, 0.25);
		}
	}
	
	private void updatePlayer(Entry<Player, Double> entry)
	{
		Player player = entry.getKey();
		Double countdown = entry.getValue();
		
		if (player.isOnline() == false)
		{
			countdowns.remove(player);
			return;
		}
		
		if (player.isDead())
			return;
		
		if (isEligableForBeingStruck(player) == false)
			return;
		
		countdown -= (1.0 / 20);
		entry.setValue(countdown);
		
		double particleStrength = Math.max(1, 5 - countdown);
		int particleAmount = (int) Math.pow(particleStrength, 2.0);
		double particleHeight = Math.random();
		particleHeight = (particleHeight * particleHeight * 10);
		Location particleLocation = getTargetLocation(player).add(0, particleHeight / 2.0, 0);
		player.getWorld().spawnParticle(particleEffect, particleLocation, particleAmount, 1.5, particleHeight, 1.5, 0.1);
		
		if (countdown <= 0)
		{
			countdowns.remove(player);
			strikePlayerDown(player);
		}
	}
	
	private double getDangerRating(Player player)
	{
		Location playerLocation = player.getLocation();
		if (Utility.countBlocks(playerLocation, playerLocation.toHighestLocation().getBlockY()) > 5)
		{
			return 0;
		}
		
		double dangerRating = 0;
		
		// add height
		dangerRating += player.getLocation().getY();
		
		// add water
		int water = countWaterBlocks(player);
		double waterDanger = water * 1.25;
		dangerRating += Math.min(waterDanger, 50);
		
		// minimum 50
		if (dangerRating < 50)
		{
			dangerRating = 50;
		}
		
		// nether ceiling
		if (Utility.isOnNetherCeiling(player))
		{
			dangerRating *= 10;
		}
		
		return dangerRating;
	}
	
	private int countWaterBlocks(Player player)
	{
		Location location = player.getLocation().toBlockLocation();
		int water = 0;
		while (location.getBlockY() > 0)
		{
			location.subtract(0, 1, 0);
			if (location.getBlock().getType() == Material.WATER)
			{
				water++;
			}
		}
		return water;
	}
	
	public void strikePlayerDown(Player player)
	{
		resolve(player).acg().setImmidieteStrike(false);
		
		World world = player.getWorld();
		DifficultyRating difficulty = Difficulties.getFor(player);
		Location target = getTargetLocation(player);
		
		world.strikeLightning(target);
		double damage = difficulty.asDoubleValue(1.5f, 1.75f, 2.0f);
		world.createExplosion(target, (float) damage, true, true);
		if (target.distance(player.getLocation()) < 5)
		{
			talk(player, core.getDataManager().getMessages().getAcg().getRandom());
		}
	}
	
	public void talk(Player player, String message)
	{
		player.sendMessage("You sense a voice: " + ChatColor.UNDERLINE + ChatColor.ITALIC + message);
	}
	
	public boolean isPlayerInDirectSight(Player player)
	{
		Location playerLocation = player.getLocation();
		Location highestBlock = playerLocation.toHighestLocation();
		return (playerLocation.getY() >= highestBlock.getY());
	}
	
	public Location getTargetLocation(Player player)
	{
		if (isPlayerInDirectSight(player))
		{
			return player.getLocation();
		}
		else
		{
			return player.getLocation().toHighestLocation();
		}
	}
	
	public boolean isEligableForBeingStruck(Player player)
	{
		WorldProfile world = resolve(player.getWorld());
		
		if (world instanceof NetherWorld && Utility.isOnNetherCeiling(player) == false)
		{
			return false;
		}
		
		return true;
	}
}
