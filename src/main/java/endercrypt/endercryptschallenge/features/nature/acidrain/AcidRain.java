/**************************************************************************\
* EnderCrypt's Challenge                                                   *
* Copyright (C) 2023 by EnderCrypt                                         *
*                                                                          *
* This program is free software: you can redistribute it and/or modify     *
* it under the terms of the GNU Affero General Public License as           *
* published by the Free Software Foundation, either version 3 of           *
* the License, or (at your option) any later version.                      *
*                                                                          *
* This program is distributed in the hope that it will be useful,          *
* but WITHOUT ANY WARRANTY; without even the implied warranty of           *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
* GNU Affero General Public License for more details.                      *
*                                                                          *
* You should have received a copy of the GNU Affero General Public License *
* along with this program in its root directory.                           *
* If not, see <http://www.gnu.org/licenses/>.                              *
\**************************************************************************/

package endercrypt.endercryptschallenge.features.nature.acidrain;


import endercrypt.endercryptschallenge.core.Core;
import endercrypt.endercryptschallenge.features.popup.deathmessage.DeathMessage;
import endercrypt.endercryptschallenge.modules.features.MinecraftFeature;
import endercrypt.endercryptschallenge.utility.Utility;
import endercrypt.endercryptschallenge.utility.difficulty.Difficulties;
import endercrypt.library.commons.randomizer.Randomizer;

import java.util.function.BiConsumer;
import java.util.function.Consumer;
import java.util.function.Supplier;

import org.bukkit.ChatColor;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.Damageable;
import org.bukkit.inventory.meta.ItemMeta;


/**
 * @author EnderCrypt
 */
public class AcidRain extends MinecraftFeature
{
	public AcidRain(Core core)
	{
		super(core);
	}
	
	@Override
	public void onCycleEntity(Entity entity)
	{
		if (entity instanceof Player player)
		{
			playerHurtByRain(player);
		}
	}
	
	public void playerHurtByRain(Player player)
	{
		if (isEligableForPain(player))
		{
			double playerDamage = Difficulties.getFor(player).asDoubleValue(0.5, 1.0, 2.5);
			int armourDamage = Difficulties.getFor(player).asIntValue(1, 3, 10);
			
			if (Math.random() > getProtectionChance(player))
			{
				ArmourSlot armourSlot = getRandomArmour(player);
				damageArmourPiece(player, armourDamage, armourSlot);
			}
			else
			{
				player.sendMessage("The acid rain " + ChatColor.RED + "hurts" + ChatColor.RESET + " you!");
				getFeature(DeathMessage.class).setKilledBy(player, "Acid Rain");
				player.damage(playerDamage);
			}
		}
	}
	
	private boolean isEligableForPain(Player player)
	{
		return Utility.isInSurvival(player) && player.isInRain();
	}
	
	private void damageArmourPiece(Player player, int armourDamage, ArmourSlot armourSlot)
	{
		ItemStack armour = armourSlot.getItemStack();
		if (armour == null) // [QUICK FIX] TODO: investigate why this method gets called and sometimes armour is null
		{
			return;
		}
		ItemMeta armourItemMeta = armour.getItemMeta();
		if (armourItemMeta instanceof Damageable armourDamageable)
		{
			// damage
			armourDamageable.setDamage(armourDamageable.getDamage() + armourDamage);
			int damage = armourDamageable.getDamage();
			armour.setItemMeta(armourItemMeta);
			
			// message
			int armourDurability = armour.getType().getMaxDurability();
			int armourRemainingHealth = (armourDurability - damage);
			double armourHealth = 1.0 / armourDurability * armourRemainingHealth;
			if (armourRemainingHealth <= 0)
			{
				armourSlot.destroyItem();
				player.sendMessage(ChatColor.RED + "The acid rain DESTROYS your " + Utility.getItemName(armour) + "!");
			}
			else
			{
				String armourWarning = (armourHealth < 0.25) ? ChatColor.RED + "[DAMAGED] " + ChatColor.WHITE : "";
				player.sendMessage("The acid rain damages your " + armourWarning + "" + Utility.getItemName(armour) + "!");
			}
		}
		else
		{
			player.sendMessage("The acid rain has no affect on your " + Utility.getItemName(armour));
		}
	}
	
	public double getProtectionChance(Player player)
	{
		double protection = 1.0;
		
		if (player.getInventory().getHelmet() != null)
		{
			protection += 2.0;
		}
		if (player.getInventory().getChestplate() != null)
		{
			protection += 1.2;
		}
		if (player.getInventory().getLeggings() != null)
		{
			protection += 0.75;
		}
		if (player.getInventory().getBoots() != null)
		{
			protection += 0.4;
		}
		
		return 1.0 / protection;
	}
	
	public ArmourSlot getRandomArmour(Player player)
	{
		Randomizer<ArmourSlot> randomizer = new Randomizer<>();
		
		BiConsumer<Double, ArmourSlot> enterer = (tickets, itemStack) -> {
			if (itemStack != null)
			{
				randomizer.enter(tickets, itemStack);
			}
		};
		
		enterer.accept(4.0, new ArmourSlot(player.getInventory()::setHelmet, player.getInventory()::getHelmet));
		enterer.accept(1.0, new ArmourSlot(player.getInventory()::setChestplate, player.getInventory()::getChestplate));
		enterer.accept(0.5, new ArmourSlot(player.getInventory()::setLeggings, player.getInventory()::getLeggings));
		enterer.accept(1.0, new ArmourSlot(player.getInventory()::setBoots, player.getInventory()::getBoots));
		
		return randomizer.pull();
	}
	
	protected static class ArmourSlot
	{
		private final Consumer<ItemStack> setter;
		private final Supplier<ItemStack> getter;
		
		public ArmourSlot(Consumer<ItemStack> setter, Supplier<ItemStack> getter)
		{
			this.setter = setter;
			this.getter = getter;
		}
		
		public ItemStack getItemStack()
		{
			return getter.get();
		}
		
		public void destroyItem()
		{
			setter.accept(null);
		}
	}
}
