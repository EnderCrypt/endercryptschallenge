/**************************************************************************\
* EnderCrypt's Challenge                                                   *
* Copyright (C) 2023 by EnderCrypt                                         *
*                                                                          *
* This program is free software: you can redistribute it and/or modify     *
* it under the terms of the GNU Affero General Public License as           *
* published by the Free Software Foundation, either version 3 of           *
* the License, or (at your option) any later version.                      *
*                                                                          *
* This program is distributed in the hope that it will be useful,          *
* but WITHOUT ANY WARRANTY; without even the implied warranty of           *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
* GNU Affero General Public License for more details.                      *
*                                                                          *
* You should have received a copy of the GNU Affero General Public License *
* along with this program in its root directory.                           *
* If not, see <http://www.gnu.org/licenses/>.                              *
\**************************************************************************/

package endercrypt.endercryptschallenge.features.nature.rottingfood;


import endercrypt.endercryptschallenge.core.Core;
import endercrypt.endercryptschallenge.materials.MaterialCollections;
import endercrypt.endercryptschallenge.modules.features.MinecraftFeature;

import org.bukkit.Location;
import org.bukkit.Sound;
import org.bukkit.World;
import org.bukkit.entity.Item;
import org.bukkit.entity.Silverfish;
import org.bukkit.event.EventHandler;
import org.bukkit.event.entity.ItemDespawnEvent;
import org.bukkit.inventory.ItemStack;


/**
 * @author EnderCrypt
 */
public class RottingFood extends MinecraftFeature
{
	public RottingFood(Core core)
	{
		super(core);
	}
	
	@EventHandler
	public void onItemDespawn(ItemDespawnEvent event)
	{
		Item item = event.getEntity();
		ItemStack itemStack = item.getItemStack();
		
		if (MaterialCollections.digestibleFoods.contains(itemStack) == false)
			return;
		
		Location location = item.getLocation();
		World world = location.getWorld();
		world.playSound(location, Sound.ENTITY_SILVERFISH_AMBIENT, 1.0f, 1.0f);
		
		for (int i = 0; i < itemStack.getAmount(); i++)
		{
			world.spawn(location, Silverfish.class);
		}
	}
}
