/**************************************************************************\
* EnderCrypt's Challenge                                                   *
* Copyright (C) 2023 by EnderCrypt                                         *
*                                                                          *
* This program is free software: you can redistribute it and/or modify     *
* it under the terms of the GNU Affero General Public License as           *
* published by the Free Software Foundation, either version 3 of           *
* the License, or (at your option) any later version.                      *
*                                                                          *
* This program is distributed in the hope that it will be useful,          *
* but WITHOUT ANY WARRANTY; without even the implied warranty of           *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
* GNU Affero General Public License for more details.                      *
*                                                                          *
* You should have received a copy of the GNU Affero General Public License *
* along with this program in its root directory.                           *
* If not, see <http://www.gnu.org/licenses/>.                              *
\**************************************************************************/

package endercrypt.endercryptschallenge.features.blocks.explosion.bedpoison;


import endercrypt.endercryptschallenge.core.Core;
import endercrypt.endercryptschallenge.materials.MaterialCollections;
import endercrypt.endercryptschallenge.modules.features.MinecraftFeature;
import endercrypt.endercryptschallenge.modules.worlds.WorldProfile;
import endercrypt.endercryptschallenge.modules.worlds.list.over.OverWorld;

import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import net.kyori.adventure.text.Component;
import net.kyori.adventure.text.TextComponent;
import net.kyori.adventure.text.format.TextColor;


/**
 * @author EnderCrypt
 */
public class ExplosionBedPoisoning extends MinecraftFeature
{
	public ExplosionBedPoisoning(Core core)
	{
		super(core);
	}
	
	@EventHandler
	public void onInteract(PlayerInteractEvent event)
	{
		if (event.getAction() != Action.RIGHT_CLICK_BLOCK)
			return;
		
		Block block = event.getClickedBlock();
		if (MaterialCollections.beds.contains(block) == false)
			return;
		
		WorldProfile world = core.getWorldManager().resolve(block.getWorld());
		if (world instanceof OverWorld)
			return;
		
		Player player = event.getPlayer();
		event.setCancelled(true);
		player.addPotionEffect(new PotionEffect(PotionEffectType.NAUSEA, 10 * 20, 1));
		player.addPotionEffect(new PotionEffect(PotionEffectType.POISON, 60 * 20, 1));
		
		TextComponent message = Component.text("Dont worry, the poisoning from the bed is ")
			.append(Component.text("[Intentional Game Design]").color(TextColor.color(100, 255, 100)));
		
		player.sendMessage(message);
	}
}
