/**************************************************************************\
* EnderCrypt's Challenge                                                   *
* Copyright (C) 2023 by EnderCrypt                                         *
*                                                                          *
* This program is free software: you can redistribute it and/or modify     *
* it under the terms of the GNU Affero General Public License as           *
* published by the Free Software Foundation, either version 3 of           *
* the License, or (at your option) any later version.                      *
*                                                                          *
* This program is distributed in the hope that it will be useful,          *
* but WITHOUT ANY WARRANTY; without even the implied warranty of           *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
* GNU Affero General Public License for more details.                      *
*                                                                          *
* You should have received a copy of the GNU Affero General Public License *
* along with this program in its root directory.                           *
* If not, see <http://www.gnu.org/licenses/>.                              *
\**************************************************************************/

package endercrypt.endercryptschallenge.features.blocks.explosion.liquids;


import endercrypt.endercryptschallenge.core.Core;
import endercrypt.endercryptschallenge.modules.features.MinecraftFeature;
import endercrypt.endercryptschallenge.utility.Utility;

import java.util.List;

import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.craftbukkit.v1_21_R1.block.impl.CraftFluids;
import org.bukkit.event.EventHandler;
import org.bukkit.event.entity.EntityExplodeEvent;


/**
 * @author EnderCrypt
 */
public class ExplosionLiquids extends MinecraftFeature
{
	public ExplosionLiquids(Core core)
	{
		super(core);
	}
	
	@EventHandler
	public void onExplosions(EntityExplodeEvent event)
	{
		List<Block> blocks = Utility.getBlockQube(event.getLocation().getBlock(), 2);
		for (Block block : blocks)
		{
			if (block.getBlockData() instanceof CraftFluids)
			{
				block.setType(Material.AIR);
			}
		}
	}
}
