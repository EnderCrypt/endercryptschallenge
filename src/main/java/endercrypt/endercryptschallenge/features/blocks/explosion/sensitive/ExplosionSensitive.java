/**************************************************************************\
* EnderCrypt's Challenge                                                   *
* Copyright (C) 2023 by EnderCrypt                                         *
*                                                                          *
* This program is free software: you can redistribute it and/or modify     *
* it under the terms of the GNU Affero General Public License as           *
* published by the Free Software Foundation, either version 3 of           *
* the License, or (at your option) any later version.                      *
*                                                                          *
* This program is distributed in the hope that it will be useful,          *
* but WITHOUT ANY WARRANTY; without even the implied warranty of           *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
* GNU Affero General Public License for more details.                      *
*                                                                          *
* You should have received a copy of the GNU Affero General Public License *
* along with this program in its root directory.                           *
* If not, see <http://www.gnu.org/licenses/>.                              *
\**************************************************************************/

package endercrypt.endercryptschallenge.features.blocks.explosion.sensitive;


import endercrypt.endercryptschallenge.core.Core;
import endercrypt.endercryptschallenge.materials.MaterialCollections;
import endercrypt.endercryptschallenge.modules.features.MinecraftFeature;
import endercrypt.endercryptschallenge.utility.Utility;

import org.bukkit.Location;
import org.bukkit.block.Block;
import org.bukkit.event.EventHandler;
import org.bukkit.event.block.BlockExplodeEvent;
import org.bukkit.event.entity.EntityExplodeEvent;


/**
 * @author EnderCrypt
 */
public class ExplosionSensitive extends MinecraftFeature
{
	public ExplosionSensitive(Core core)
	{
		super(core);
		getDebug().enableDebugSupport();
	}
	
	@EventHandler
	public void onBlockExplosion(BlockExplodeEvent event)
	{
		onExplosion(Utility.getBlockFloorLocation(event.getBlock()), event.getYield());
	}
	
	@EventHandler
	public void onEntityExplosion(EntityExplodeEvent event)
	{
		onExplosion(event.getLocation(), event.getYield());
	}
	
	private void onExplosion(Location location, double yield)
	{
		debug(location.getWorld(), "explosion on " + location + " with yield " + yield);
		
		double range = 3.5;
		for (Block block : Utility.getBlockSphere(location, range))
		{
			if (MaterialCollections.explosionSensitive.contains(block))
			{
				block.breakNaturally(true);
			}
		}
	}
}
