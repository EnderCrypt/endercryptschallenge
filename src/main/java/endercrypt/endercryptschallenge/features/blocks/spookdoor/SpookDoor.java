/**************************************************************************\
* EnderCrypt's Challenge                                                   *
* Copyright (C) 2023 by EnderCrypt                                         *
*                                                                          *
* This program is free software: you can redistribute it and/or modify     *
* it under the terms of the GNU Affero General Public License as           *
* published by the Free Software Foundation, either version 3 of           *
* the License, or (at your option) any later version.                      *
*                                                                          *
* This program is distributed in the hope that it will be useful,          *
* but WITHOUT ANY WARRANTY; without even the implied warranty of           *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
* GNU Affero General Public License for more details.                      *
*                                                                          *
* You should have received a copy of the GNU Affero General Public License *
* along with this program in its root directory.                           *
* If not, see <http://www.gnu.org/licenses/>.                              *
\**************************************************************************/

package endercrypt.endercryptschallenge.features.blocks.spookdoor;


import endercrypt.endercryptschallenge.core.Core;
import endercrypt.endercryptschallenge.modules.features.MinecraftFeature;
import endercrypt.endercryptschallenge.utility.Utility;
import endercrypt.library.commons.misc.Ender;
import endercrypt.library.commons.position.Circle;

import org.bukkit.Location;
import org.bukkit.block.Block;
import org.bukkit.block.data.BlockData;
import org.bukkit.block.data.Openable;
import org.bukkit.entity.Player;


/**
 * @author EnderCrypt
 */
public class SpookDoor extends MinecraftFeature
{
	private static final double CHANCE = 0.15;
	private static final double MINIMUM_DISTANCE = 5;
	private static final double CHECK_DISTANCE = 20;
	
	public SpookDoor(Core core)
	{
		super(core);
	}
	
	@Override
	public void onCycleBlock(Block block)
	{
		if (Math.random() > CHANCE)
		{
			return;
		}
		
		BlockData blockData = block.getBlockData();
		if (blockData instanceof Openable == false)
		{
			return;
		}
		Openable openable = (Openable) blockData;
		
		Location location = block.getLocation().add(0.5, 0.5, 0.5);
		if (location.getNearbyPlayers(MINIMUM_DISTANCE).isEmpty() == false)
		{
			return;
		}
		for (Player player : location.getNearbyPlayers(CHECK_DISTANCE))
		{
			Location playerHead = player.getEyeLocation();
			double playerLookDirection = Math.toRadians(playerHead.getYaw());
			
			Location offset = location.clone().subtract(playerHead);
			double offsetDirection = Utility.getFlatAngle(offset.toVector());
			
			double angleDifference = Math.abs(Ender.math.angleDifference(playerLookDirection, offsetDirection) - Circle.QUARTER);
			
			if (angleDifference <= 1.0)
			{
				return;
			}
		}
		openable.setOpen(openable.isOpen() == false);
		block.setBlockData(blockData);
	}
}
