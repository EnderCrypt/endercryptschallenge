/**************************************************************************\
* EnderCrypt's Challenge                                                   *
* Copyright (C) 2023 by EnderCrypt                                         *
*                                                                          *
* This program is free software: you can redistribute it and/or modify     *
* it under the terms of the GNU Affero General Public License as           *
* published by the Free Software Foundation, either version 3 of           *
* the License, or (at your option) any later version.                      *
*                                                                          *
* This program is distributed in the hope that it will be useful,          *
* but WITHOUT ANY WARRANTY; without even the implied warranty of           *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
* GNU Affero General Public License for more details.                      *
*                                                                          *
* You should have received a copy of the GNU Affero General Public License *
* along with this program in its root directory.                           *
* If not, see <http://www.gnu.org/licenses/>.                              *
\**************************************************************************/

package endercrypt.endercryptschallenge.features.blocks.drippinglava;


import endercrypt.endercryptschallenge.core.Core;
import endercrypt.endercryptschallenge.features.popup.deathmessage.DeathMessage;
import endercrypt.endercryptschallenge.modules.features.MinecraftFeature;
import endercrypt.endercryptschallenge.utility.Utility;

import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;


/**
 * idea by Qwuuk (https://www.twitch.tv/qwuuk)
 * 
 * @author EnderCrypt
 */
public class DrippingLava extends MinecraftFeature
{
	public DrippingLava(Core core)
	{
		super(core);
	}
	
	@Override
	public void onCycleEntity(Entity entity)
	{
		if (entity instanceof Player player)
		{
			processPlayer(player);
		}
	}
	
	private void processPlayer(Player player)
	{
		Block block = Utility.getCeiling(player, 16).orElse(null);
		if (block == null)
			return;
		
		Block aboveBlock = block.getLocation().add(0, 1, 0).getBlock();
		if (aboveBlock.getType() != Material.LAVA)
			return;
		
		DeathMessage deathMessage = core.getMinecraftFeatureManager().getFeature(DeathMessage.class);
		deathMessage.setKilledBy(player, "dripping lava");
		player.damage(0.75);
		player.sendMessage("The dripping lava burns your skin");
	}
}
