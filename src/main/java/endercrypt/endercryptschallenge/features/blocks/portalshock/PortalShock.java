/**************************************************************************\
* EnderCrypt's Challenge                                                   *
* Copyright (C) 2023 by EnderCrypt                                         *
*                                                                          *
* This program is free software: you can redistribute it and/or modify     *
* it under the terms of the GNU Affero General Public License as           *
* published by the Free Software Foundation, either version 3 of           *
* the License, or (at your option) any later version.                      *
*                                                                          *
* This program is distributed in the hope that it will be useful,          *
* but WITHOUT ANY WARRANTY; without even the implied warranty of           *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
* GNU Affero General Public License for more details.                      *
*                                                                          *
* You should have received a copy of the GNU Affero General Public License *
* along with this program in its root directory.                           *
* If not, see <http://www.gnu.org/licenses/>.                              *
\**************************************************************************/

package endercrypt.endercryptschallenge.features.blocks.portalshock;


import endercrypt.endercryptschallenge.core.Core;
import endercrypt.endercryptschallenge.modules.features.MinecraftFeature;
import endercrypt.endercryptschallenge.utility.difficulty.Difficulties;
import endercrypt.endercryptschallenge.utility.difficulty.DifficultyRating;
import endercrypt.library.commons.RandomEntry;
import endercrypt.library.commons.misc.Ender;
import endercrypt.library.commons.position.Circle;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.block.BlockState;
import org.bukkit.event.EventHandler;
import org.bukkit.event.world.PortalCreateEvent;
import org.bukkit.event.world.PortalCreateEvent.CreateReason;


/**
 * @author EnderCrypt
 */
public class PortalShock extends MinecraftFeature
{
	public PortalShock(Core core)
	{
		super(core);
	}
	
	@EventHandler
	public void onOpenPortal(PortalCreateEvent event)
	{
		if (event.getReason() != CreateReason.FIRE)
			return;
		
		List<Block> blocks = getBottomBlocks(event.getBlocks());
		
		Location location = blocks.iterator().next().getLocation();
		event.getWorld().playSound(location, Sound.ENTITY_LIGHTNING_BOLT_THUNDER, 1.0f, 1.0f);
		
		DifficultyRating difficulty = Difficulties.getFor(location);
		int trails = difficulty.asIntValue(10, 20, 40);
		for (int i = 0; i < trails; i++)
		{
			try
			{
				createFireTrail(RandomEntry.from(blocks), i + 15);
			}
			catch (TrailInterruptedException e)
			{
				// trail interrupted early, ignore
			}
		}
	}
	
	private List<Block> getBottomBlocks(List<BlockState> blocks)
	{
		List<Block> lowest = new ArrayList<>();
		int lowestY = Integer.MAX_VALUE;
		for (BlockState blockState : blocks)
		{
			Block block = blockState.getBlock();
			int blockY = block.getLocation().getBlockY();
			if (blockY <= lowestY)
			{
				lowestY = blockY;
				lowest.clear();
			}
			if (lowest.isEmpty())
			{
				lowest.add(block);
			}
		}
		return lowest;
	}
	
	private void createFireTrail(Block block, int length) throws TrailInterruptedException
	{
		double direction = Math.random() * Circle.FULL * 2;
		
		for (int i = 0; i < length; i++)
		{
			int x = (int) Math.round(Math.cos(direction));
			int z = (int) Math.round(Math.sin(direction));
			block = block.getRelative(x, 0, z);
			
			double dispersion = Circle.QUARTER * 0.2;
			direction += Ender.math.randomRange(-dispersion, dispersion);
			
			block = alignBlock(block);
			block.setType(Material.FIRE);
		}
	}
	
	private Block alignBlock(Block block) throws TrailInterruptedException
	{
		block = alignBlock(block, BlockFace.DOWN, m -> m == Material.AIR, 3);
		block = alignBlock(block, BlockFace.UP, m -> m != Material.AIR, 3);
		return block;
	}
	
	private Block alignBlock(Block block, BlockFace direction, Predicate<Material> condition, int max) throws TrailInterruptedException
	{
		for (int i = 0; i < max; i++)
		{
			if (condition.test(block.getType()) == false)
			{
				return block;
			}
			block = block.getRelative(direction);
		}
		throw new TrailInterruptedException();
	}
}
