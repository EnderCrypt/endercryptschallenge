/**************************************************************************\
* EnderCrypt's Challenge                                                   *
* Copyright (C) 2023 by EnderCrypt                                         *
*                                                                          *
* This program is free software: you can redistribute it and/or modify     *
* it under the terms of the GNU Affero General Public License as           *
* published by the Free Software Foundation, either version 3 of           *
* the License, or (at your option) any later version.                      *
*                                                                          *
* This program is distributed in the hope that it will be useful,          *
* but WITHOUT ANY WARRANTY; without even the implied warranty of           *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
* GNU Affero General Public License for more details.                      *
*                                                                          *
* You should have received a copy of the GNU Affero General Public License *
* along with this program in its root directory.                           *
* If not, see <http://www.gnu.org/licenses/>.                              *
\**************************************************************************/

package endercrypt.endercryptschallenge.features.blocks.pricklyberries;


import endercrypt.endercryptschallenge.core.Core;
import endercrypt.endercryptschallenge.features.popup.deathmessage.DeathMessage;
import endercrypt.endercryptschallenge.modules.features.MinecraftFeature;
import endercrypt.endercryptschallenge.utility.difficulty.Difficulties;
import endercrypt.endercryptschallenge.utility.difficulty.DifficultyRating;
import endercrypt.library.commons.misc.Ender;

import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerItemConsumeEvent;


public class PricklyBerries extends MinecraftFeature
{
	public PricklyBerries(Core core)
	{
		super(core);
	}
	
	@EventHandler
	public void onGatherBerry(PlayerInteractEvent event)
	{
		Block block = event.getClickedBlock();
		if (block != null && block.getType() == Material.SWEET_BERRY_BUSH)
		{
			hurtPlayer(event.getPlayer());
		}
	}
	
	@EventHandler
	public void onEatBerry(PlayerItemConsumeEvent event)
	{
		if (event.getItem().getType() == Material.SWEET_BERRIES)
		{
			hurtPlayer(event.getPlayer());
		}
	}
	
	private void hurtPlayer(Player player)
	{
		core.getMinecraftFeatureManager().getFeature(DeathMessage.class).setKilledBy(player, "some painful berries");
		DifficultyRating difficulty = Difficulties.getFor(player);
		double damage = Ender.math.randomRange(1.0, 2.0) * difficulty.asDoubleValue(1.0, 1.5, 2.5);
		player.damage(damage);
	}
}
