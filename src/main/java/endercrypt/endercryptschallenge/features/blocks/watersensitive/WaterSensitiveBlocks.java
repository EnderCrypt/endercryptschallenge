/**************************************************************************\
* EnderCrypt's Challenge                                                   *
* Copyright (C) 2023 by EnderCrypt                                         *
*                                                                          *
* This program is free software: you can redistribute it and/or modify     *
* it under the terms of the GNU Affero General Public License as           *
* published by the Free Software Foundation, either version 3 of           *
* the License, or (at your option) any later version.                      *
*                                                                          *
* This program is distributed in the hope that it will be useful,          *
* but WITHOUT ANY WARRANTY; without even the implied warranty of           *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
* GNU Affero General Public License for more details.                      *
*                                                                          *
* You should have received a copy of the GNU Affero General Public License *
* along with this program in its root directory.                           *
* If not, see <http://www.gnu.org/licenses/>.                              *
\**************************************************************************/

package endercrypt.endercryptschallenge.features.blocks.watersensitive;


import endercrypt.endercryptschallenge.core.Core;
import endercrypt.endercryptschallenge.materials.MaterialCollections;
import endercrypt.endercryptschallenge.modules.features.MinecraftFeature;
import endercrypt.endercryptschallenge.utility.Utility;

import java.util.List;
import java.util.stream.Collectors;

import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.craftbukkit.v1_21_R1.block.impl.CraftFluids;
import org.bukkit.event.EventHandler;
import org.bukkit.event.block.BlockPhysicsEvent;
import org.bukkit.event.block.BlockPlaceEvent;


/**
 * @author EnderCrypt
 */
public class WaterSensitiveBlocks extends MinecraftFeature
{
	private static final List<BlockFace> faces = Utility.cartesianBlockFaces.stream()
		.filter(b -> b != BlockFace.DOWN)
		.collect(Collectors.toUnmodifiableList());
	
	public WaterSensitiveBlocks(Core core)
	{
		super(core);
	}
	
	@EventHandler
	public void onblockPlaceEvent(BlockPlaceEvent event)
	{
		if (isWaterSensitiveBlock(event.getBlock()))
		{
			event.setCancelled(true);
		}
	}
	
	@EventHandler
	public void onBlockPhysics(BlockPhysicsEvent event)
	{
		triggerBlock(event.getBlock());
	}
	
	private void triggerBlock(Block block)
	{
		if (isWaterSensitiveBlock(block))
		{
			destroyBlock(block);
		}
	}
	
	private boolean isWaterSensitiveBlock(Block block)
	{
		Material material = block.getType();
		if (MaterialCollections.waterSensitive.contains(material) == false)
			return false;
		
		if (isConnectedToWater(block) == false)
			return false;
		
		return true;
	}
	
	private void destroyBlock(Block block)
	{
		if (MaterialCollections.signs.contains(block))
		{
			schedule(1, block::breakNaturally);
			return;
		}
		
		block.breakNaturally();
	}
	
	public static boolean isConnectedToWater(Block block)
	{
		for (BlockFace face : faces)
		{
			if (isConnectedToWater(block, face))
			{
				return true;
			}
		}
		return false;
	}
	
	private static boolean isConnectedToWater(Block block, BlockFace face)
	{
		Block relativeBlock = block.getRelative(face);
		if (relativeBlock.getType() != Material.WATER)
		{
			return false;
		}
		
		CraftFluids fluids = (CraftFluids) relativeBlock.getBlockData();
		if (fluids.getLevel() >= 7)
		{
			return false;
		}
		
		return true;
	}
}
