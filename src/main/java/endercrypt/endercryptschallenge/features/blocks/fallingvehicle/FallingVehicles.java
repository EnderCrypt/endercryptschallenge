/**************************************************************************\
* EnderCrypt's Challenge                                                   *
* Copyright (C) 2023 by EnderCrypt                                         *
*                                                                          *
* This program is free software: you can redistribute it and/or modify     *
* it under the terms of the GNU Affero General Public License as           *
* published by the Free Software Foundation, either version 3 of           *
* the License, or (at your option) any later version.                      *
*                                                                          *
* This program is distributed in the hope that it will be useful,          *
* but WITHOUT ANY WARRANTY; without even the implied warranty of           *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
* GNU Affero General Public License for more details.                      *
*                                                                          *
* You should have received a copy of the GNU Affero General Public License *
* along with this program in its root directory.                           *
* If not, see <http://www.gnu.org/licenses/>.                              *
\**************************************************************************/

package endercrypt.endercryptschallenge.features.blocks.fallingvehicle;


import endercrypt.endercryptschallenge.core.Core;
import endercrypt.endercryptschallenge.features.popup.deathmessage.DeathMessage;
import endercrypt.endercryptschallenge.modules.features.MinecraftFeature;

import java.util.HashMap;
import java.util.Map;

import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.entity.Vehicle;
import org.bukkit.event.EventHandler;
import org.bukkit.event.vehicle.VehicleMoveEvent;


/**
 * @author EnderCrypt
 */
public class FallingVehicles extends MinecraftFeature
{
	private final Map<Vehicle, Double> vehicles = new HashMap<>();
	
	public FallingVehicles(Core core)
	{
		super(core);
	}
	
	@EventHandler
	public void onVehicleMove(VehicleMoveEvent event)
	{
		Vehicle vehicle = event.getVehicle();
		boolean onGround = vehicle.isOnGround();
		
		if (onGround && vehicles.containsKey(vehicle))
		{
			double peak = vehicles.remove(vehicle);
			double distance = peak - vehicle.getLocation().getY();
			double damage = (distance - 3) * 0.5;
			causeFallDamage(vehicle, damage);
			
		}
		if (onGround == false)
		{
			double y = vehicle.getLocation().getY();
			Double peak = vehicles.get(vehicle);
			if (peak == null)
			{
				peak = y;
			}
			peak = Math.max(peak, y);
			vehicles.put(vehicle, peak);
		}
	}
	
	private void causeFallDamage(Entity vehicle, double damage)
	{
		for (Entity passenger : vehicle.getPassengers())
		{
			if (passenger instanceof Player player)
			{
				getFeature(DeathMessage.class).setKilledBy(player, "falling in a vehicle");
			}
			if (passenger instanceof LivingEntity living)
			{
				living.damage(damage);
			}
			causeFallDamage(passenger, damage * 0.75);
		}
	}
}
