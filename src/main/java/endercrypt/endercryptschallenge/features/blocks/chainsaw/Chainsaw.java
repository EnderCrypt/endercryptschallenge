/**************************************************************************\
* EnderCrypt's Challenge                                                   *
* Copyright (C) 2023 by EnderCrypt                                         *
*                                                                          *
* This program is free software: you can redistribute it and/or modify     *
* it under the terms of the GNU Affero General Public License as           *
* published by the Free Software Foundation, either version 3 of           *
* the License, or (at your option) any later version.                      *
*                                                                          *
* This program is distributed in the hope that it will be useful,          *
* but WITHOUT ANY WARRANTY; without even the implied warranty of           *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
* GNU Affero General Public License for more details.                      *
*                                                                          *
* You should have received a copy of the GNU Affero General Public License *
* along with this program in its root directory.                           *
* If not, see <http://www.gnu.org/licenses/>.                              *
\**************************************************************************/

package endercrypt.endercryptschallenge.features.blocks.chainsaw;


import endercrypt.endercryptschallenge.core.Core;
import endercrypt.endercryptschallenge.features.popup.deathmessage.DeathMessage;
import endercrypt.endercryptschallenge.modules.features.MinecraftFeature;

import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;


/**
 * @author EnderCrypt
 */
public class Chainsaw extends MinecraftFeature
{
	public Chainsaw(Core core)
	{
		super(core);
	}
	
	@Override
	public void onTick()
	{
		for (World world : server.getWorlds())
		{
			for (Player player : world.getPlayers())
			{
				processPlayer(player);
			}
		}
	}
	
	private void processPlayer(Player player)
	{
		Block block = player.getLocation().add(0, -0.5, 0).getBlock();
		if (block.getType() != Material.STONECUTTER)
			return;
		
		core.getMinecraftFeatureManager().getFeature(DeathMessage.class).setKilledBy(player, "stonecutter saw");
		player.damage(1.0);
	}
}
