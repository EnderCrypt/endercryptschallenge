/**************************************************************************\
* EnderCrypt's Challenge                                                   *
* Copyright (C) 2023 by EnderCrypt                                         *
*                                                                          *
* This program is free software: you can redistribute it and/or modify     *
* it under the terms of the GNU Affero General Public License as           *
* published by the Free Software Foundation, either version 3 of           *
* the License, or (at your option) any later version.                      *
*                                                                          *
* This program is distributed in the hope that it will be useful,          *
* but WITHOUT ANY WARRANTY; without even the implied warranty of           *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
* GNU Affero General Public License for more details.                      *
*                                                                          *
* You should have received a copy of the GNU Affero General Public License *
* along with this program in its root directory.                           *
* If not, see <http://www.gnu.org/licenses/>.                              *
\**************************************************************************/

package endercrypt.endercryptschallenge.features.blocks.ligthtningcrystals;


import endercrypt.endercryptschallenge.core.Core;
import endercrypt.endercryptschallenge.modules.features.MinecraftFeature;

import org.bukkit.World;
import org.bukkit.entity.EnderCrystal;
import org.bukkit.event.EventHandler;
import org.bukkit.event.entity.EntityExplodeEvent;


/**
 * @author EnderCrypt
 */
public class LightingEndCrystals extends MinecraftFeature
{
	public LightingEndCrystals(Core core)
	{
		super(core);
	}
	
	@EventHandler
	public void onEndCrystalDestruction(EntityExplodeEvent event)
	{
		if (event.getEntity() instanceof EnderCrystal == false)
		{
			return;
		}
		EnderCrystal enderCrystal = (EnderCrystal) event.getEntity();
		World world = enderCrystal.getWorld();
		world.strikeLightning(enderCrystal.getLocation());
	}
}
