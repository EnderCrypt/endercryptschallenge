/**************************************************************************\
* EnderCrypt's Challenge                                                   *
* Copyright (C) 2023 by EnderCrypt                                         *
*                                                                          *
* This program is free software: you can redistribute it and/or modify     *
* it under the terms of the GNU Affero General Public License as           *
* published by the Free Software Foundation, either version 3 of           *
* the License, or (at your option) any later version.                      *
*                                                                          *
* This program is distributed in the hope that it will be useful,          *
* but WITHOUT ANY WARRANTY; without even the implied warranty of           *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
* GNU Affero General Public License for more details.                      *
*                                                                          *
* You should have received a copy of the GNU Affero General Public License *
* along with this program in its root directory.                           *
* If not, see <http://www.gnu.org/licenses/>.                              *
\**************************************************************************/

package endercrypt.endercryptschallenge.features.blocks.torchfire;


import endercrypt.endercryptschallenge.core.Core;
import endercrypt.endercryptschallenge.materials.MaterialCollections;
import endercrypt.endercryptschallenge.modules.features.MinecraftFeature;
import endercrypt.endercryptschallenge.utility.Utility;
import endercrypt.endercryptschallenge.utility.difficulty.Difficulties;
import endercrypt.endercryptschallenge.utility.difficulty.DifficultyRating;

import java.util.Collections;
import java.util.List;

import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;


/**
 * @author EnderCrypt
 */
public class TorchFire extends MinecraftFeature
{
	public TorchFire(Core core)
	{
		super(core);
	}
	
	@Override
	public void onCycleBlock(Block block)
	{
		DifficultyRating difficulty = Difficulties.getFor(block);
		if (Math.random() > difficulty.asDoubleValue(0.2, 0.4, 0.6))
		{
			return;
		}
		Material blockType = block.getType();
		if (MaterialCollections.fireSpreaders.contains(blockType) == false)
		{
			return;
		}
		Block targetBlock = getViableBlock(block);
		if (targetBlock == null)
		{
			return;
		}
		if (targetBlock.getType() != Material.AIR)
		{
			return;
		}
		targetBlock.setType(Material.FIRE);
	}
	
	private static Block getViableBlock(Block sourceBlock)
	{
		List<Block> blocks = Utility.getBlockQube(sourceBlock, 1);
		blocks.remove(sourceBlock);
		Collections.shuffle(blocks);
		
		for (Block block : blocks)
		{
			Block floor = block.getRelative(BlockFace.DOWN);
			if (floor.isSolid() && block.getType() == Material.AIR)
			{
				return block;
			}
		}
		return null;
	}
}
