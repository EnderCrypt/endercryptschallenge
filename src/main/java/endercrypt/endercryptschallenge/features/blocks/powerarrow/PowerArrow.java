/**************************************************************************\
* EnderCrypt's Challenge                                                   *
* Copyright (C) 2023 by EnderCrypt                                         *
*                                                                          *
* This program is free software: you can redistribute it and/or modify     *
* it under the terms of the GNU Affero General Public License as           *
* published by the Free Software Foundation, either version 3 of           *
* the License, or (at your option) any later version.                      *
*                                                                          *
* This program is distributed in the hope that it will be useful,          *
* but WITHOUT ANY WARRANTY; without even the implied warranty of           *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
* GNU Affero General Public License for more details.                      *
*                                                                          *
* You should have received a copy of the GNU Affero General Public License *
* along with this program in its root directory.                           *
* If not, see <http://www.gnu.org/licenses/>.                              *
\**************************************************************************/

package endercrypt.endercryptschallenge.features.blocks.powerarrow;


import endercrypt.endercryptschallenge.core.Core;
import endercrypt.endercryptschallenge.materials.MaterialCollections;
import endercrypt.endercryptschallenge.modules.features.DisabledFeature;
import endercrypt.endercryptschallenge.modules.features.MinecraftFeature;
import endercrypt.endercryptschallenge.utility.Utility;

import org.bukkit.block.Block;
import org.bukkit.entity.Arrow;
import org.bukkit.event.EventHandler;
import org.bukkit.event.entity.ProjectileHitEvent;
import org.bukkit.util.Vector;


/**
 * idea by Person Person#0378
 * 
 * disabled for now due to arrow glitchines
 * 
 * @author EnderCrypt
 */
@DisabledFeature
public class PowerArrow extends MinecraftFeature
{
	public PowerArrow(Core core)
	{
		super(core);
	}
	
	@EventHandler
	public void onArrowCollise(ProjectileHitEvent event)
	{
		Block hitBlock = event.getHitBlock();
		
		if (hitBlock == null)
			return;
		
		if (event.getEntity() instanceof Arrow == false)
			return;
		Arrow arrow = (Arrow) event.getEntity();
		
		if (MaterialCollections.glassBlocks.contains(hitBlock) == false)
			return;
		
		hitBlock.breakNaturally(true);
		Vector randomVelocity = Utility.getRandomVector().multiply(0.5);
		arrow.setVelocity(arrow.getVelocity().add(randomVelocity));
	}
}
