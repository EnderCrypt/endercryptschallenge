/**************************************************************************\
* EnderCrypt's Challenge                                                   *
* Copyright (C) 2023 by EnderCrypt                                         *
*                                                                          *
* This program is free software: you can redistribute it and/or modify     *
* it under the terms of the GNU Affero General Public License as           *
* published by the Free Software Foundation, either version 3 of           *
* the License, or (at your option) any later version.                      *
*                                                                          *
* This program is distributed in the hope that it will be useful,          *
* but WITHOUT ANY WARRANTY; without even the implied warranty of           *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
* GNU Affero General Public License for more details.                      *
*                                                                          *
* You should have received a copy of the GNU Affero General Public License *
* along with this program in its root directory.                           *
* If not, see <http://www.gnu.org/licenses/>.                              *
\**************************************************************************/

package endercrypt.endercryptschallenge.features.blocks.torchextinguish;


import endercrypt.endercryptschallenge.core.Core;
import endercrypt.endercryptschallenge.modules.features.MinecraftFeature;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.inventory.ItemStack;


/**
 * idea by fehleno
 * 
 * @author EnderCrypt
 */
public class TorchExtinguish extends MinecraftFeature
{
	public TorchExtinguish(Core core)
	{
		super(core);
	}
	
	@Override
	public void onCycleBlock(Block block)
	{
		if (block.getWorld().hasStorm() == false)
			return;
		
		if (block.getType() != Material.TORCH)
			return;
		
		Location blockLocation = block.getLocation();
		World world = blockLocation.getWorld();
		Location highestLocation = blockLocation.toHighestLocation();
		if (blockLocation.getBlockY() < highestLocation.getBlockY())
			return;
		
		ItemStack item = new ItemStack(Material.STICK, 1);
		world.dropItemNaturally(highestLocation, item);
		block.setType(Material.AIR);
		
		world.playSound(blockLocation, Sound.BLOCK_REDSTONE_TORCH_BURNOUT, 1.0f, 1.0f);
	}
}
