/**************************************************************************\
* EnderCrypt's Challenge                                                   *
* Copyright (C) 2023 by EnderCrypt                                         *
*                                                                          *
* This program is free software: you can redistribute it and/or modify     *
* it under the terms of the GNU Affero General Public License as           *
* published by the Free Software Foundation, either version 3 of           *
* the License, or (at your option) any later version.                      *
*                                                                          *
* This program is distributed in the hope that it will be useful,          *
* but WITHOUT ANY WARRANTY; without even the implied warranty of           *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
* GNU Affero General Public License for more details.                      *
*                                                                          *
* You should have received a copy of the GNU Affero General Public License *
* along with this program in its root directory.                           *
* If not, see <http://www.gnu.org/licenses/>.                              *
\**************************************************************************/

package endercrypt.endercryptschallenge.features.blocks.anvildamage;


import endercrypt.endercryptschallenge.core.Core;
import endercrypt.endercryptschallenge.modules.features.MinecraftFeature;

import java.util.List;

import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.event.EventHandler;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.inventory.ItemStack;


/**
 * Idea by FatherLorenzo
 * 
 * @author EnderCrypt
 */
public class AnvilDamage extends MinecraftFeature
{
	private final List<Material> anvilLevels = List.of(Material.DAMAGED_ANVIL, Material.CHIPPED_ANVIL, Material.ANVIL);
	
	public AnvilDamage(Core core)
	{
		super(core);
	}
	
	@EventHandler
	public void onDestroyBlock(BlockBreakEvent event)
	{
		// old anvil
		Block block = event.getBlock();
		Material oldType = block.getType();
		int oldLevel = anvilLevels.indexOf(oldType);
		if (oldLevel == -1)
			return;
		
		// silk touch
		ItemStack item = event.getPlayer().getInventory().getItemInMainHand();
		if (item != null && item.containsEnchantment(Enchantment.SILK_TOUCH))
			return;
		
		// new anvil
		int newLevel = oldLevel - 1;
		if (newLevel == -1)
		{
			event.setDropItems(false);
			return;
		}
		Material newType = anvilLevels.get(newLevel);
		block.setType(newType);
	}
}
