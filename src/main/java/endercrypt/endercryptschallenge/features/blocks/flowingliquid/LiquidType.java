/**************************************************************************\
* EnderCrypt's Challenge                                                   *
* Copyright (C) 2023 by EnderCrypt                                         *
*                                                                          *
* This program is free software: you can redistribute it and/or modify     *
* it under the terms of the GNU Affero General Public License as           *
* published by the Free Software Foundation, either version 3 of           *
* the License, or (at your option) any later version.                      *
*                                                                          *
* This program is distributed in the hope that it will be useful,          *
* but WITHOUT ANY WARRANTY; without even the implied warranty of           *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
* GNU Affero General Public License for more details.                      *
*                                                                          *
* You should have received a copy of the GNU Affero General Public License *
* along with this program in its root directory.                           *
* If not, see <http://www.gnu.org/licenses/>.                              *
\**************************************************************************/

package endercrypt.endercryptschallenge.features.blocks.flowingliquid;


import java.util.Optional;

import org.bukkit.Material;


/**
 * @author EnderCrypt
 */
public enum LiquidType
{
	WATER(Material.WATER, 10),
	LAVA(Material.LAVA, 30);
	
	private final Material material;
	private final int moveDelay;
	
	private LiquidType(Material material, int moveDelay)
	{
		this.material = material;
		this.moveDelay = moveDelay;
	}
	
	public Material getMaterial()
	{
		return material;
	}
	
	public int getMoveDelay()
	{
		return moveDelay;
	}
	
	public static Optional<LiquidType> getType(Material material)
	{
		for (LiquidType type : values())
		{
			if (type.material == material)
			{
				return Optional.of(type);
			}
		}
		return Optional.empty();
	}
}
