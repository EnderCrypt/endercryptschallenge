/**************************************************************************\
* EnderCrypt's Challenge                                                   *
* Copyright (C) 2023 by EnderCrypt                                         *
*                                                                          *
* This program is free software: you can redistribute it and/or modify     *
* it under the terms of the GNU Affero General Public License as           *
* published by the Free Software Foundation, either version 3 of           *
* the License, or (at your option) any later version.                      *
*                                                                          *
* This program is distributed in the hope that it will be useful,          *
* but WITHOUT ANY WARRANTY; without even the implied warranty of           *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
* GNU Affero General Public License for more details.                      *
*                                                                          *
* You should have received a copy of the GNU Affero General Public License *
* along with this program in its root directory.                           *
* If not, see <http://www.gnu.org/licenses/>.                              *
\**************************************************************************/

package endercrypt.endercryptschallenge.features.blocks.flowingliquid;


import endercrypt.endercryptschallenge.core.Core;
import endercrypt.endercryptschallenge.modules.features.MinecraftFeature;
import endercrypt.library.commons.iterator.PurifyingIterator;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.bukkit.block.Block;
import org.bukkit.event.EventHandler;
import org.bukkit.event.player.PlayerBucketEmptyEvent;


/**
 * @author EnderCrypt
 */
public class FlowingLiquid extends MinecraftFeature
{
	private List<Liquid> liquids = new ArrayList<>();
	
	public FlowingLiquid(Core core)
	{
		super(core);
	}
	
	private Iterator<Liquid> iterator()
	{
		return new PurifyingIterator<>(liquids, Liquid::isAlive);
	}
	
	@Override
	public void onTick()
	{
		Iterator<Liquid> iterator = iterator();
		while (iterator.hasNext())
		{
			Liquid liquid = iterator.next();
			liquid.update();
		}
	}
	
	@EventHandler
	public void onPlaceLiquid(PlayerBucketEmptyEvent event)
	{
		Block block = event.getBlock();
		schedule(1, () -> processLiquid(block));
	}
	
	private void processLiquid(Block block)
	{
		if (LiquidType.getType(block.getType()).isPresent())
		{
			liquids.add(new Liquid(block));
		}
	}
}
