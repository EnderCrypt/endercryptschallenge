/**************************************************************************\
* EnderCrypt's Challenge                                                   *
* Copyright (C) 2023 by EnderCrypt                                         *
*                                                                          *
* This program is free software: you can redistribute it and/or modify     *
* it under the terms of the GNU Affero General Public License as           *
* published by the Free Software Foundation, either version 3 of           *
* the License, or (at your option) any later version.                      *
*                                                                          *
* This program is distributed in the hope that it will be useful,          *
* but WITHOUT ANY WARRANTY; without even the implied warranty of           *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
* GNU Affero General Public License for more details.                      *
*                                                                          *
* You should have received a copy of the GNU Affero General Public License *
* along with this program in its root directory.                           *
* If not, see <http://www.gnu.org/licenses/>.                              *
\**************************************************************************/

package endercrypt.endercryptschallenge.features.blocks.flowingliquid;


import endercrypt.endercryptschallenge.utility.Utility;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.block.data.BlockData;
import org.bukkit.block.data.Levelled;


/**
 * @author EnderCrypt
 */
public class Liquid
{
	private static final List<BlockFace> directions = Utility.cartesianBlockFaces
		.stream()
		.filter(b -> b.getModY() <= 0)
		.collect(Collectors.toList());
	
	private static final int lifetime = 100;
	private static final double maxDecay = 250.0;
	private final LiquidType type;
	
	private Block block;
	private double decay = 0;
	private int timer = 0;
	private int idleTimer = 0;
	private boolean merged = false;
	
	public Liquid(Block block)
	{
		this.type = LiquidType.getType(block.getType()).orElseThrow(() -> new IllegalArgumentException("unknown liquid type: " + block.getType()));
		this.block = block;
	}
	
	public LiquidType getType()
	{
		return type;
	}
	
	public boolean isAlive()
	{
		return idleTimer < lifetime && block.getType() == type.getMaterial() && merged == false && decay < maxDecay;
	}
	
	public void update()
	{
		idleTimer++;
		timer++;
		
		move();
	}
	
	private void move()
	{
		decay += 0.05;
		
		// timer
		int newTimer = (int) (timer - (type.getMoveDelay() + decay));
		if (newTimer < 0)
			return;
		timer = newTimer;
		
		Collections.shuffle(directions);
		boolean moved = false;
		int sames = 0;
		for (BlockFace direction : directions)
		{
			Block potential = block.getRelative(direction);
			switch (checkBlockMovability(potential))
			{
			case MOVABLE:
				if (moved == false)
				{
					moveTo(potential);
					moved = true;
				}
			break;
			case COMPATIBLE:
				sames++;
			break;
			default:
			break;
			}
		}
		if (sames >= 3)
		{
			merged = true;
		}
		return;
	}
	
	private Movable checkBlockMovability(Block block)
	{
		Material material = block.getType();
		if (material == Material.AIR)
			return Movable.MOVABLE;
		
		if (material != type.getMaterial())
			return Movable.IMMOVABLE;
		
		return isSourceBlock(block) ? Movable.COMPATIBLE : Movable.MOVABLE;
	}
	
	private enum Movable
	{
		MOVABLE,
		COMPATIBLE,
		IMMOVABLE;
	}
	
	private void moveTo(Block target)
	{
		if (target.getBlockData() instanceof Levelled levelled)
		{
			levelled.setLevel(1);
			block.setBlockData(levelled);
		}
		else
		{
			block.setType(Material.AIR);
		}
		
		if (target.getY() < block.getY())
		{
			decay = 0;
		}
		
		block = target;
		block.setType(type.getMaterial());
		idleTimer = 0;
	}
	
	private boolean isSourceBlock(Block block)
	{
		BlockData data = block.getBlockData();
		if (data instanceof Levelled levelled)
		{
			int level = levelled.getLevel();
			return (level == 0);
		}
		
		throw new IllegalStateException("failed to check water level on " + block);
	}
	
	@Override
	public String toString()
	{
		return Liquid.class.getSimpleName() + " [material=" + type.getMaterial() + "]";
	}
}
