/**************************************************************************\
* EnderCrypt's Challenge                                                   *
* Copyright (C) 2023 by EnderCrypt                                         *
*                                                                          *
* This program is free software: you can redistribute it and/or modify     *
* it under the terms of the GNU Affero General Public License as           *
* published by the Free Software Foundation, either version 3 of           *
* the License, or (at your option) any later version.                      *
*                                                                          *
* This program is distributed in the hope that it will be useful,          *
* but WITHOUT ANY WARRANTY; without even the implied warranty of           *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
* GNU Affero General Public License for more details.                      *
*                                                                          *
* You should have received a copy of the GNU Affero General Public License *
* along with this program in its root directory.                           *
* If not, see <http://www.gnu.org/licenses/>.                              *
\**************************************************************************/

package endercrypt.endercryptschallenge.features.blocks.spawnerburst;


import endercrypt.endercryptschallenge.core.Core;
import endercrypt.endercryptschallenge.modules.features.MinecraftFeature;
import endercrypt.endercryptschallenge.utility.Utility;
import endercrypt.library.commons.misc.Ender;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.craftbukkit.v1_21_R1.block.CraftCreatureSpawner;
import org.bukkit.entity.Entity;
import org.bukkit.event.EventHandler;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.entity.EntityExplodeEvent;
import org.bukkit.util.Vector;


/**
 * @author EnderCrypt
 */
public class SpawnerBurst extends MinecraftFeature
{
	public SpawnerBurst(Core core)
	{
		super(core);
	}
	
	@EventHandler
	public void onPlayerDestroyBlock(BlockBreakEvent event)
	{
		processBlock(event.getBlock());
	}
	
	@EventHandler
	public void test(EntityExplodeEvent event)
	{
		event.blockList().forEach(this::processBlock);
	}
	
	private void processBlock(Block block)
	{
		if (block.getType() != Material.SPAWNER)
		{
			return;
		}
		World world = block.getWorld();
		Location blockLocation = block.getLocation().add(0.5, 0.5, 0.5);
		if (block.getState() instanceof CraftCreatureSpawner spawner)
		{
			Class<? extends Entity> entityClass = spawner.getSpawnedType().getEntityClass();
			int amount = Ender.math.randomRange(15, 20);
			for (int i = 0; i < amount; i++)
			{
				Entity entity = world.spawn(blockLocation, entityClass);
				
				Vector velocity = Utility.getRandomVector().multiply(2.5);
				entity.setVelocity(velocity);
			}
		}
	}
}
