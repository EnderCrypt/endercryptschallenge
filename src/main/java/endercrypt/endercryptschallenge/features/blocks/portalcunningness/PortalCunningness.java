/**************************************************************************\
* EnderCrypt's Challenge                                                   *
* Copyright (C) 2023 by EnderCrypt                                         *
*                                                                          *
* This program is free software: you can redistribute it and/or modify     *
* it under the terms of the GNU Affero General Public License as           *
* published by the Free Software Foundation, either version 3 of           *
* the License, or (at your option) any later version.                      *
*                                                                          *
* This program is distributed in the hope that it will be useful,          *
* but WITHOUT ANY WARRANTY; without even the implied warranty of           *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
* GNU Affero General Public License for more details.                      *
*                                                                          *
* You should have received a copy of the GNU Affero General Public License *
* along with this program in its root directory.                           *
* If not, see <http://www.gnu.org/licenses/>.                              *
\**************************************************************************/

package endercrypt.endercryptschallenge.features.blocks.portalcunningness;


import endercrypt.endercryptschallenge.core.Core;
import endercrypt.endercryptschallenge.features.entities.mobs.creeper.cunning.core.CunningCreeperCore;
import endercrypt.endercryptschallenge.features.entities.mobs.creeper.cunning.core.triggers.TriggerSpotManager;
import endercrypt.endercryptschallenge.modules.features.MinecraftFeature;
import endercrypt.endercryptschallenge.utility.Utility;
import endercrypt.endercryptschallenge.utility.tracker.ObjectTracker;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.event.EventHandler;
import org.bukkit.event.player.PlayerPortalEvent;


/**
 * @author EnderCrypt
 */
public class PortalCunningness extends MinecraftFeature
{
	private ObjectTracker<Block> portals = new ObjectTracker<>(Block.class)
		.addCondition(this::isPortal);
	
	public PortalCunningness(Core core)
	{
		super(core);
	}
	
	private boolean isPortal(Block block)
	{
		Material material = block.getType();
		return material == Material.END_PORTAL || material == Material.NETHER_PORTAL;
	}
	
	@Override
	public void onCycleBlock(Block block)
	{
		portals.add(block);
	}
	
	@Override
	public void onSecond()
	{
		TriggerSpotManager triggers = getFeature(CunningCreeperCore.class).getTriggers();
		for (Block block : portals)
		{
			Location location = Utility.getBlockFloorLocation(block);
			triggers.punch(location, 1.0 / 40);
		}
	}
	
	@EventHandler
	public void onPlayerEnterPortal(PlayerPortalEvent event)
	{
		TriggerSpotManager triggers = getFeature(CunningCreeperCore.class).getTriggers();
		
		Location location = event.getPlayer().getLocation();
		triggers.punch(location, 1.0);
	}
}
