/**************************************************************************\
* EnderCrypt's Challenge                                                   *
* Copyright (C) 2023 by EnderCrypt                                         *
*                                                                          *
* This program is free software: you can redistribute it and/or modify     *
* it under the terms of the GNU Affero General Public License as           *
* published by the Free Software Foundation, either version 3 of           *
* the License, or (at your option) any later version.                      *
*                                                                          *
* This program is distributed in the hope that it will be useful,          *
* but WITHOUT ANY WARRANTY; without even the implied warranty of           *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
* GNU Affero General Public License for more details.                      *
*                                                                          *
* You should have received a copy of the GNU Affero General Public License *
* along with this program in its root directory.                           *
* If not, see <http://www.gnu.org/licenses/>.                              *
\**************************************************************************/

package endercrypt.endercryptschallenge.features.blocks.exhaustedsoil;


import endercrypt.endercryptschallenge.core.Core;
import endercrypt.endercryptschallenge.modules.features.MinecraftFeature;
import endercrypt.endercryptschallenge.utility.difficulty.Difficulties;
import endercrypt.endercryptschallenge.utility.difficulty.DifficultyRating;

import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.event.EventHandler;
import org.bukkit.event.block.BlockBreakEvent;


/**
 * @author EnderCrypt
 */
public class ExhaustedSoil extends MinecraftFeature
{
	public ExhaustedSoil(Core core)
	{
		super(core);
	}
	
	@EventHandler
	public void onWheatDestroy(BlockBreakEvent event)
	{
		Block block = event.getBlock();
		if (block.getType() == Material.WHEAT)
		{
			DifficultyRating difficulty = Difficulties.getFor(event);
			double chance = difficulty.asDoubleValue(0.01, 0.05, 0.1);
			if (Math.random() < chance)
			{
				Block soil = block.getRelative(BlockFace.DOWN);
				soil.setType(Material.GRAVEL);
			}
		}
	}
}
