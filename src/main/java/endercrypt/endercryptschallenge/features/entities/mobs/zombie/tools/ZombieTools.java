/**************************************************************************\
* EnderCrypt's Challenge                                                   *
* Copyright (C) 2023 by EnderCrypt                                         *
*                                                                          *
* This program is free software: you can redistribute it and/or modify     *
* it under the terms of the GNU Affero General Public License as           *
* published by the Free Software Foundation, either version 3 of           *
* the License, or (at your option) any later version.                      *
*                                                                          *
* This program is distributed in the hope that it will be useful,          *
* but WITHOUT ANY WARRANTY; without even the implied warranty of           *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
* GNU Affero General Public License for more details.                      *
*                                                                          *
* You should have received a copy of the GNU Affero General Public License *
* along with this program in its root directory.                           *
* If not, see <http://www.gnu.org/licenses/>.                              *
\**************************************************************************/

package endercrypt.endercryptschallenge.features.entities.mobs.zombie.tools;


import endercrypt.endercryptschallenge.core.Core;
import endercrypt.endercryptschallenge.modules.features.MinecraftFeature;
import endercrypt.endercryptschallenge.utility.Utility;
import endercrypt.endercryptschallenge.utility.difficulty.Difficulties;
import endercrypt.endercryptschallenge.utility.difficulty.DifficultyRating;
import endercrypt.library.commons.misc.Ender;
import endercrypt.library.commons.randomizer.Randomizer;
import endercrypt.library.commons.randomizer.Ticket;

import org.bukkit.Material;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Zombie;
import org.bukkit.event.EventHandler;
import org.bukkit.event.entity.EntitySpawnEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.Damageable;
import org.bukkit.inventory.meta.ItemMeta;


/**
 * @author EnderCrypt
 */
public class ZombieTools extends MinecraftFeature
{
	private static final Randomizer<Material> zombieToolRandomizer = new Randomizer<>();
	
	static
	{
		zombieToolRandomizer.enter(2.0 / 1, Material.WOODEN_PICKAXE);
		zombieToolRandomizer.enter(2.0 / 2, Material.WOODEN_SHOVEL);
		zombieToolRandomizer.enter(2.0 / 3, Material.WOODEN_AXE);
		zombieToolRandomizer.enter(2.0 / 4, Material.WOODEN_SWORD);
		
		zombieToolRandomizer.enter(1.0 / 1, Material.STONE_SHOVEL);
		zombieToolRandomizer.enter(1.0 / 2, Material.STONE_PICKAXE);
		zombieToolRandomizer.enter(1.0 / 3, Material.STONE_AXE);
		zombieToolRandomizer.enter(1.0 / 4, Material.STONE_SWORD);
		
		zombieToolRandomizer.enter(0.5 / 1, Material.IRON_SHOVEL);
		zombieToolRandomizer.enter(0.5 / 2, Material.IRON_PICKAXE);
		zombieToolRandomizer.enter(0.5 / 3, Material.IRON_AXE);
		zombieToolRandomizer.enter(0.5 / 4, Material.IRON_SWORD);
		
		zombieToolRandomizer.enter(0.25 / 1, Material.DIAMOND_SHOVEL);
		zombieToolRandomizer.enter(0.25 / 2, Material.DIAMOND_PICKAXE);
		zombieToolRandomizer.enter(0.25 / 3, Material.DIAMOND_AXE);
		zombieToolRandomizer.enter(0.25 / 4, Material.DIAMOND_SWORD);
	}
	
	public static boolean isValidMiningTool(Material material)
	{
		for (Ticket<Material> ticket : zombieToolRandomizer)
		{
			Material ticketMaterial = ticket.getItem();
			if (ticketMaterial == material)
			{
				return true;
			}
		}
		return false;
	}
	
	public ZombieTools(Core core)
	{
		super(core);
	}
	
	public double getToolChance(Entity entity)
	{
		if (entity instanceof Zombie zombie && zombie.isAdult() == false)
		{
			return 0;
		}
		
		if (Utility.entityStuckBlock(entity).isPresent())
		{
			return 0.75;
		}
		
		DifficultyRating difficulty = Difficulties.getFor(entity);
		return 1.0 / difficulty.asIntValue(20, 10, 5);
	}
	
	@EventHandler
	public void onEntitySpawn(EntitySpawnEvent event)
	{
		if (event.getEntity() instanceof Zombie zombie && Math.random() < getToolChance(zombie))
		{
			Material toolMaterial = zombieToolRandomizer.pull();
			ItemStack tool = new ItemStack(toolMaterial);
			
			// damage
			ItemMeta meta = tool.getItemMeta();
			if (meta instanceof Damageable metaDamageable)
			{
				int maxHealth = tool.getType().getMaxDurability();
				int damage = (int) (maxHealth * Ender.math.randomRange(0.5, 0.9));
				metaDamageable.setDamage(damage);
				tool.setItemMeta(meta);
			}
			
			zombie.getEquipment().setItemInMainHand(tool);
			zombie.getEquipment().setItemInMainHandDropChance(0.05f);
		}
	}
}
