/**************************************************************************\
* EnderCrypt's Challenge                                                   *
* Copyright (C) 2023 by EnderCrypt                                         *
*                                                                          *
* This program is free software: you can redistribute it and/or modify     *
* it under the terms of the GNU Affero General Public License as           *
* published by the Free Software Foundation, either version 3 of           *
* the License, or (at your option) any later version.                      *
*                                                                          *
* This program is distributed in the hope that it will be useful,          *
* but WITHOUT ANY WARRANTY; without even the implied warranty of           *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
* GNU Affero General Public License for more details.                      *
*                                                                          *
* You should have received a copy of the GNU Affero General Public License *
* along with this program in its root directory.                           *
* If not, see <http://www.gnu.org/licenses/>.                              *
\**************************************************************************/

package endercrypt.endercryptschallenge.features.entities.mobs.creeper.cunning.angry;


import endercrypt.endercryptschallenge.core.Core;
import endercrypt.endercryptschallenge.features.entities.mobs.creeper.cunning.core.CunningCreeperCore;
import endercrypt.endercryptschallenge.features.entities.mobs.creeper.cunning.core.triggers.TriggerSpot;
import endercrypt.endercryptschallenge.modules.features.MinecraftFeature;
import endercrypt.endercryptschallenge.modules.worlds.spawner.factory.SpawnCondition;
import endercrypt.endercryptschallenge.utility.Utility;
import endercrypt.endercryptschallenge.utility.difficulty.Difficulties;
import endercrypt.endercryptschallenge.utility.difficulty.DifficultyRating;
import endercrypt.library.commons.interfaces.Code;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.entity.Creeper;

import com.destroystokyo.paper.entity.Pathfinder;
import com.destroystokyo.paper.entity.Pathfinder.PathResult;


/**
 * @author EnderCrypt
 */
public class CunningCreeperAngry extends MinecraftFeature
{
	private static final SpawnCondition spawnCondition = new SpawnCondition.Builder()
		.setAnyLightLevel()
		.setHeight(2)
		.build();
	
	private Set<Creeper> temporaryCreepers = new HashSet<>();
	
	public CunningCreeperAngry(Core core)
	{
		super(core);
	}
	
	@Override
	public void onCycleBlock(Block block)
	{
		DifficultyRating difficulty = Difficulties.getFor(block);
		if (Math.random() > difficulty.asDoubleValue(0.1, 0.25, 1.0))
			return;
		
		Location location = block.getLocation();
		if (spawnCondition.isValidSpawnLocation(location) == false)
			return;
		
		if (block.getRelative(BlockFace.DOWN).getType() == Material.LAVA)
			return;
		
		TriggerSpot spot = getFeature(CunningCreeperCore.class).getTriggers().getAggroSpot(location).orElse(null);
		if (spot == null)
			return;
		
		if (spot.isSuperAngry() == false)
			return;
		
		if (location.getNearbyPlayers(8).isEmpty() == false)
			return;
		
		if (Utility.getAnyEntityWhitinRange(location, Creeper.class, 10) != null)
			return;
		
		World world = location.getWorld();
		Creeper creeper = world.spawn(location, Creeper.class, this::onAddCreeper);
		
		schedule(5, () -> temporaryCreepers.remove(creeper));
		
		schedule(1, new Code()
		{
			@Override
			public void run() throws Exception
			{
				Location target = spot.getLocation();
				Pathfinder pathfinder = creeper.getPathfinder();
				PathResult path = pathfinder.findPath(target);
				if (path == null || path.getFinalPoint().distance(target) > spot.getTriggerRange())
				{
					creeper.remove();
					return;
				}
				pathfinder.moveTo(path);
				creeper.setInvisible(false);
			}
		});
	}
	
	private void onAddCreeper(Creeper creeper)
	{
		temporaryCreepers.add(creeper);
		creeper.setInvisible(true);
	}
	
	public Set<Creeper> getTemporaryCreepers()
	{
		return Collections.unmodifiableSet(temporaryCreepers);
	}
}
