/**************************************************************************\
* EnderCrypt's Challenge                                                   *
* Copyright (C) 2023 by EnderCrypt                                         *
*                                                                          *
* This program is free software: you can redistribute it and/or modify     *
* it under the terms of the GNU Affero General Public License as           *
* published by the Free Software Foundation, either version 3 of           *
* the License, or (at your option) any later version.                      *
*                                                                          *
* This program is distributed in the hope that it will be useful,          *
* but WITHOUT ANY WARRANTY; without even the implied warranty of           *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
* GNU Affero General Public License for more details.                      *
*                                                                          *
* You should have received a copy of the GNU Affero General Public License *
* along with this program in its root directory.                           *
* If not, see <http://www.gnu.org/licenses/>.                              *
\**************************************************************************/

package endercrypt.endercryptschallenge.features.entities.mobs.creeper.cunning.core.triggers;


import endercrypt.endercryptschallenge.features.entities.mobs.creeper.cunning.core.EnemyValues;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.Consumer;
import java.util.function.Predicate;

import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.entity.Monster;


/**
 * @author EnderCrypt
 */
public class TriggerSpotManager
{
	private static final EnemyValues values = new EnemyValues();
	
	private final Map<World, List<TriggerSpot>> worlds = new HashMap<>();
	
	public void punch(Monster monster)
	{
		Location location = monster.getLocation();
		double strength = values.get(monster);
		punch(location, strength);
	}
	
	public void punch(Location location, double strength)
	{
		TriggerSpot spot = new SimpleTriggerSpot(location);
		spot.punch(strength);
		
		getSpots(location.getWorld()).add(spot);
		
		performSmartMerge(spot);
	}
	
	private void performSmartMerge(TriggerSpot spot)
	{
		List<TriggerSpot> triggerSpots = getSpots(spot.getLocation().getWorld());
		Iterator<TriggerSpot> iterator = triggerSpots.iterator();
		while (iterator.hasNext())
		{
			TriggerSpot other = iterator.next();
			TriggerSpot merged = spot.attemptMerge(other).orElse(null);
			if (merged != null)
			{
				other.delete();
				iterator.remove();
				triggerSpots.remove(spot);
				triggerSpots.add(merged);
				performSmartMerge(merged);
				return;
			}
		}
	}
	
	public List<TriggerSpot> getSpots(World world)
	{
		return worlds.computeIfAbsent(world, (w) -> new ArrayList<>());
	}
	
	public Optional<TriggerSpot> getAggroSpot(Location location)
	{
		return getSpot(location, (spot) -> spot.isWhitinAggroRange(location));
	}
	
	public Optional<TriggerSpot> getTriggerSpot(Location location)
	{
		return getSpot(location, (spot) -> spot.isWhitinTriggerRange(location));
	}
	
	private Optional<TriggerSpot> getSpot(Location location, Predicate<TriggerSpot> condition)
	{
		TriggerSpot target = null;
		for (TriggerSpot spot : getSpots(location.getWorld()))
		{
			if (condition.test(spot) && (target == null || spot.getStrength() > target.getStrength()))
			{
				target = spot;
			}
		}
		return Optional.ofNullable(target);
	}
	
	public void updateDebug(boolean debug)
	{
		forEach(s -> s.updateDebug(debug));
	}
	
	public void update()
	{
		forEach(TriggerSpot::update);
	}
	
	public void forEach(Consumer<TriggerSpot> consumer)
	{
		for (List<TriggerSpot> triggerSpots : worlds.values())
		{
			Iterator<TriggerSpot> iterator = triggerSpots.iterator();
			while (iterator.hasNext())
			{
				TriggerSpot spot = iterator.next();
				if (spot.getLocation().isChunkLoaded())
				{
					consumer.accept(spot);
					if (spot.isDecayed())
					{
						spot.delete();
						iterator.remove();
					}
				}
			}
		}
	}
}
