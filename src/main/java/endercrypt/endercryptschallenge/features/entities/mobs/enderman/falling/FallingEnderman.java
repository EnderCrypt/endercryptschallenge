/**************************************************************************\
* EnderCrypt's Challenge                                                   *
* Copyright (C) 2023 by EnderCrypt                                         *
*                                                                          *
* This program is free software: you can redistribute it and/or modify     *
* it under the terms of the GNU Affero General Public License as           *
* published by the Free Software Foundation, either version 3 of           *
* the License, or (at your option) any later version.                      *
*                                                                          *
* This program is distributed in the hope that it will be useful,          *
* but WITHOUT ANY WARRANTY; without even the implied warranty of           *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
* GNU Affero General Public License for more details.                      *
*                                                                          *
* You should have received a copy of the GNU Affero General Public License *
* along with this program in its root directory.                           *
* If not, see <http://www.gnu.org/licenses/>.                              *
\**************************************************************************/

package endercrypt.endercryptschallenge.features.entities.mobs.enderman.falling;


import endercrypt.endercryptschallenge.core.Core;
import endercrypt.endercryptschallenge.modules.features.MinecraftFeature;
import endercrypt.endercryptschallenge.utility.Utility;
import endercrypt.endercryptschallenge.utility.tracker.entity.ConditionAliveAndActive;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.function.Predicate;

import org.bukkit.Location;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.entity.Enderman;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.event.EventHandler;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;


/**
 * @author EnderCrypt
 */
public class FallingEnderman extends MinecraftFeature
{
	private Map<Enderman, Block> safeLocations = new HashMap<>();
	
	public FallingEnderman(Core core)
	{
		super(core);
	}
	
	@Override
	public void onSecond()
	{
		safeLocations.keySet()
			.removeIf(Predicate.not(ConditionAliveAndActive::checkEntity));
	}
	
	@Override
	public void onCycleEntity(Entity entity)
	{
		if (entity instanceof Enderman enderman)
		{
			// falling
			if (enderman.getVelocity().getY() < -0.4)
			{
				escape(enderman);
			}
			
			// safe?
			Block block = enderman.getLocation().getBlock();
			if (isSafeLandingPoint(block))
			{
				safeLocations.put(enderman, block);
			}
		}
	}
	
	public boolean isSafeLandingPoint(Block block)
	{
		Block floor = block.getRelative(BlockFace.DOWN);
		if (floor.isSolid() == false)
			return false;
		
		Block body = block;
		for (int i = 0; i < 2; i++)
		{
			if (body.isSolid())
				return false;
			body = body.getRelative(BlockFace.UP);
		}
		
		return true;
	}
	
	@EventHandler
	public void onLand(EntityDamageEvent event)
	{
		if (event.getEntity() instanceof Enderman == false)
			return;
		Enderman enderman = (Enderman) event.getEntity();
		
		if (event.getCause() != DamageCause.FALL)
			return;
		
		if (escape(enderman))
		{
			event.setCancelled(true);
		}
	}
	
	public Optional<Location> getEmergencyLandingPoint(Enderman enderman)
	{
		Block block = safeLocations.get(enderman);
		if (block == null)
			return Optional.empty();
		
		if (isSafeLandingPoint(block) == false)
		{
			block = block.getWorld().getHighestBlockAt(Utility.getBlockFloorLocation(block))
				.getRelative(BlockFace.UP);
		}
		
		return Optional.of(Utility.getBlockFloorLocation(block));
	}
	
	public boolean escape(Enderman enderman)
	{
		enderman.setScreaming(true);
		
		if (enderman.teleport())
			return true;
		
		LivingEntity target = enderman.getTarget();
		if (target != null && enderman.teleportTowards(target))
			return true;
		
		Location emergencyLanding = getEmergencyLandingPoint(enderman).orElse(null);
		if (emergencyLanding != null)
		{
			if (enderman.teleport(emergencyLanding))
			{
				return true;
			}
		}
		
		return false;
	}
}
