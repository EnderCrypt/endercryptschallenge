/**************************************************************************\
* EnderCrypt's Challenge                                                   *
* Copyright (C) 2023 by EnderCrypt                                         *
*                                                                          *
* This program is free software: you can redistribute it and/or modify     *
* it under the terms of the GNU Affero General Public License as           *
* published by the Free Software Foundation, either version 3 of           *
* the License, or (at your option) any later version.                      *
*                                                                          *
* This program is distributed in the hope that it will be useful,          *
* but WITHOUT ANY WARRANTY; without even the implied warranty of           *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
* GNU Affero General Public License for more details.                      *
*                                                                          *
* You should have received a copy of the GNU Affero General Public License *
* along with this program in its root directory.                           *
* If not, see <http://www.gnu.org/licenses/>.                              *
\**************************************************************************/

package endercrypt.endercryptschallenge.features.entities.mobs.guardian.behavior;


import endercrypt.endercryptschallenge.features.entities.mobs.guardian.behavior.behaviors.AscendBehavior;
import endercrypt.endercryptschallenge.features.entities.mobs.guardian.behavior.behaviors.FollowBehavior;
import endercrypt.endercryptschallenge.features.entities.mobs.guardian.behavior.behaviors.LeapBehavior;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.entity.Guardian;
import org.bukkit.entity.LivingEntity;


/**
 * @author EnderCrypt
 */
public class GuardianBehaviorSystem
{
	private final List<GuardianBehavior> behaviorQueue = new ArrayList<>();
	
	public GuardianBehaviorSystem()
	{
		behaviorQueue.add(new LeapBehavior());
		behaviorQueue.add(new AscendBehavior());
		behaviorQueue.add(new FollowBehavior());
	}
	
	public boolean execute(Guardian guardian, LivingEntity target)
	{
		for (GuardianBehavior guardianBehavior : behaviorQueue)
		{
			if (guardianBehavior.trigger(guardian, target))
			{
				return true;
			}
		}
		return false;
	}
}
