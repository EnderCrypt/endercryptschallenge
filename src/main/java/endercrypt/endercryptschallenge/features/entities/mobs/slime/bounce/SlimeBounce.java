/**************************************************************************\
* EnderCrypt's Challenge                                                   *
* Copyright (C) 2023 by EnderCrypt                                         *
*                                                                          *
* This program is free software: you can redistribute it and/or modify     *
* it under the terms of the GNU Affero General Public License as           *
* published by the Free Software Foundation, either version 3 of           *
* the License, or (at your option) any later version.                      *
*                                                                          *
* This program is distributed in the hope that it will be useful,          *
* but WITHOUT ANY WARRANTY; without even the implied warranty of           *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
* GNU Affero General Public License for more details.                      *
*                                                                          *
* You should have received a copy of the GNU Affero General Public License *
* along with this program in its root directory.                           *
* If not, see <http://www.gnu.org/licenses/>.                              *
\**************************************************************************/

package endercrypt.endercryptschallenge.features.entities.mobs.slime.bounce;


import endercrypt.endercryptschallenge.core.Core;
import endercrypt.endercryptschallenge.modules.features.MinecraftFeature;
import endercrypt.endercryptschallenge.utility.Utility;
import endercrypt.endercryptschallenge.utility.tracker.entity.EntityTracker;

import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Slime;
import org.bukkit.event.EventHandler;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;
import org.bukkit.util.Vector;


/**
 * @author EnderCrypt
 */
public class SlimeBounce extends MinecraftFeature
{
	private static final double MOTION_SIDEWAYS = 0.5;
	private static final double MOTION_UP = 1.25;
	private static final double MOTION_TOWARDS_ENEMY = 0.3;
	
	private EntityTracker<Slime> slimes = new EntityTracker<>(Slime.class);
	
	public SlimeBounce(Core core)
	{
		super(core);
	}
	
	@Override
	public void onCycleEntity(Entity entity)
	{
		slimes.feed(entity);
	}
	
	@Override
	public void onTick()
	{
		slimes.forEach(this::processSlime);
	}
	
	@EventHandler
	public void onFallDamage(EntityDamageEvent event)
	{
		if (event.getEntity() instanceof Slime && event.getCause() == DamageCause.FALL)
		{
			event.setCancelled(true);
		}
	}
	
	private void processSlime(Slime slime)
	{
		if (slime.isOnGround())
		{
			double randomSideways = MOTION_SIDEWAYS * slime.getSize();
			double randomUp = MOTION_UP * slime.getSize();
			
			Vector motion = Utility.getRandomVector()
				.setY(0)
				.multiply(randomSideways)
				.setY(Math.random() * randomUp);
			
			LivingEntity target = slime.getTarget();
			if (target != null)
			{
				Vector enemyVector = target.getLocation().toVector().subtract(slime.getLocation().toVector());
				enemyVector.setY(0);
				enemyVector.multiply(MOTION_TOWARDS_ENEMY * slime.getSize());
				motion.add(enemyVector);
			}
			
			slime.setVelocity(slime.getVelocity().add(motion));
		}
	}
}
