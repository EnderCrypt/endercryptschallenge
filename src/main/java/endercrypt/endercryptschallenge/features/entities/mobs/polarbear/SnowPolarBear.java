/**************************************************************************\
* EnderCrypt's Challenge                                                   *
* Copyright (C) 2023 by EnderCrypt                                         *
*                                                                          *
* This program is free software: you can redistribute it and/or modify     *
* it under the terms of the GNU Affero General Public License as           *
* published by the Free Software Foundation, either version 3 of           *
* the License, or (at your option) any later version.                      *
*                                                                          *
* This program is distributed in the hope that it will be useful,          *
* but WITHOUT ANY WARRANTY; without even the implied warranty of           *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
* GNU Affero General Public License for more details.                      *
*                                                                          *
* You should have received a copy of the GNU Affero General Public License *
* along with this program in its root directory.                           *
* If not, see <http://www.gnu.org/licenses/>.                              *
\**************************************************************************/

package endercrypt.endercryptschallenge.features.entities.mobs.polarbear;


import endercrypt.endercryptschallenge.core.Core;
import endercrypt.endercryptschallenge.modules.features.MinecraftFeature;
import endercrypt.endercryptschallenge.utility.difficulty.Difficulties;
import endercrypt.endercryptschallenge.utility.difficulty.DifficultyRating;

import org.bukkit.Sound;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.PolarBear;
import org.bukkit.entity.Snowball;
import org.bukkit.event.EventHandler;
import org.bukkit.event.entity.ProjectileHitEvent;
import org.bukkit.util.Vector;


/**
 * @author EnderCrypt
 */
public class SnowPolarBear extends MinecraftFeature
{
	public SnowPolarBear(Core core)
	{
		super(core);
	}
	
	@EventHandler
	public void onPolarBearSnowball(ProjectileHitEvent event)
	{
		if (event.getEntity() instanceof Snowball snowBall && snowBall.getShooter() instanceof PolarBear source && event.getHitEntity() instanceof LivingEntity victim)
		{
			victim.damage(1.0, source);
		}
	}
	
	@Override
	public void onCycleEntity(Entity entity)
	{
		if (entity instanceof PolarBear polarBear)
		{
			processPolarBear(polarBear);
		}
	}
	
	private void processPolarBear(PolarBear polarBear)
	{
		DifficultyRating difficulty = Difficulties.getFor(polarBear);
		if (Math.random() > difficulty.asDoubleValue(0.1, 0.2, 0.4))
			return;
		
		LivingEntity target = polarBear.getTarget();
		if (target == null)
			return;
		
		shoot(polarBear, target);
	}
	
	private void shoot(PolarBear polarBear, LivingEntity target)
	{
		Vector offset = target.getEyeLocation().toVector().subtract(polarBear.getEyeLocation().toVector());
		offset.normalize();
		offset.multiply(2.5);
		
		Snowball snowBall = polarBear.getWorld().spawn(polarBear.getEyeLocation(), Snowball.class);
		snowBall.setShooter(polarBear);
		snowBall.setVelocity(offset);
		
		polarBear.getWorld().playSound(polarBear.getEyeLocation(), Sound.ENTITY_SNOWBALL_THROW, 1.0f, 1.0f);
	}
}
