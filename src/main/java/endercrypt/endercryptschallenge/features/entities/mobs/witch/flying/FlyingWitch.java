/**************************************************************************\
* EnderCrypt's Challenge                                                   *
* Copyright (C) 2023 by EnderCrypt                                         *
*                                                                          *
* This program is free software: you can redistribute it and/or modify     *
* it under the terms of the GNU Affero General Public License as           *
* published by the Free Software Foundation, either version 3 of           *
* the License, or (at your option) any later version.                      *
*                                                                          *
* This program is distributed in the hope that it will be useful,          *
* but WITHOUT ANY WARRANTY; without even the implied warranty of           *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
* GNU Affero General Public License for more details.                      *
*                                                                          *
* You should have received a copy of the GNU Affero General Public License *
* along with this program in its root directory.                           *
* If not, see <http://www.gnu.org/licenses/>.                              *
\**************************************************************************/

package endercrypt.endercryptschallenge.features.entities.mobs.witch.flying;


import endercrypt.endercryptschallenge.core.Core;
import endercrypt.endercryptschallenge.modules.features.MinecraftFeature;
import endercrypt.endercryptschallenge.utility.Utility;
import endercrypt.endercryptschallenge.utility.difficulty.Difficulties;
import endercrypt.endercryptschallenge.utility.difficulty.DifficultyRating;
import endercrypt.endercryptschallenge.utility.tracker.entity.EntityTracker;
import endercrypt.library.commons.position.Circle;

import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Witch;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.util.Vector;


/**
 * @author EnderCrypt
 */
public class FlyingWitch extends MinecraftFeature
{
	private final EntityTracker<Witch> witches = new EntityTracker<>(Witch.class);
	
	public FlyingWitch(Core core)
	{
		super(core);
	}
	
	@Override
	public void onCycleEntity(Entity entity)
	{
		if (entity instanceof Witch witch)
		{
			witches.add(witch);
		}
	}
	
	@Override
	public void onTick()
	{
		witches.forEach(this::processWitch);
	}
	
	private void processWitch(Witch witch)
	{
		if (witch.isOnGround() == false)
		{
			witch.addPotionEffect(new PotionEffect(PotionEffectType.SLOW_FALLING, 5, 3));
		}
		
		if (witch.getTarget() == null)
		{
			return;
		}
		
		if (Utility.getCeiling(witch, 3).isPresent())
			return;
		
		LivingEntity target = witch.getTarget();
		
		double targetDistance = target.getLocation().distance(witch.getLocation());
		if (targetDistance > 16)
		{
			return;
		}
		
		double targetHeight = target.getEyeLocation().getY() + 2.5;
		if (witch.getLocation().getY() < targetHeight)
		{
			witch.addPotionEffect(new PotionEffect(PotionEffectType.LEVITATION, 5, 3));
		}
		
		DifficultyRating difficulty = Difficulties.getFor(witch);
		
		int direction = server.getCurrentTick() % 1000 < 500 ? 1 : -1;
		
		Vector witchVector = witch.getVelocity();
		
		Vector vector = target.getLocation().subtract(witch.getLocation()).toVector();
		vector.setY(0.0);
		vector.rotateAroundY((Circle.QUARTER * 0.7) * direction);
		vector.normalize();
		double invisibilityBoost = witch.hasPotionEffect(PotionEffectType.INVISIBILITY) ? 2.0 : 1.0;
		double speed = difficulty.asDoubleValue(0.035, 0.045, 0.065) * invisibilityBoost;
		vector.multiply(speed);
		
		witchVector.add(vector);
		
		witch.setVelocity(witchVector);
	}
}
