/**************************************************************************\
* EnderCrypt's Challenge                                                   *
* Copyright (C) 2023 by EnderCrypt                                         *
*                                                                          *
* This program is free software: you can redistribute it and/or modify     *
* it under the terms of the GNU Affero General Public License as           *
* published by the Free Software Foundation, either version 3 of           *
* the License, or (at your option) any later version.                      *
*                                                                          *
* This program is distributed in the hope that it will be useful,          *
* but WITHOUT ANY WARRANTY; without even the implied warranty of           *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
* GNU Affero General Public License for more details.                      *
*                                                                          *
* You should have received a copy of the GNU Affero General Public License *
* along with this program in its root directory.                           *
* If not, see <http://www.gnu.org/licenses/>.                              *
\**************************************************************************/

package endercrypt.endercryptschallenge.features.entities.mobs.slime.ride;


import endercrypt.endercryptschallenge.core.Core;
import endercrypt.endercryptschallenge.modules.features.MinecraftFeature;
import endercrypt.endercryptschallenge.utility.difficulty.Difficulties;
import endercrypt.endercryptschallenge.utility.difficulty.DifficultyRating;

import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Slime;
import org.bukkit.event.EventHandler;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;


/**
 * slowness was an idea by Qwuuk (https://www.twitch.tv/qwuuk)
 * 
 * @author EnderCrypt
 */
public class SlimeRide extends MinecraftFeature
{
	public SlimeRide(Core core)
	{
		super(core);
	}
	
	@EventHandler
	public void onSlimeHit(EntityDamageByEntityEvent event)
	{
		if (event.getDamager() instanceof Slime slime && event.getEntity() instanceof LivingEntity victim)
		{
			affectCreature(slime, victim);
		}
	}
	
	private void affectCreature(Slime slime, LivingEntity victim)
	{
		// ride
		if (slime.getSize() >= 2)
		{
			slime.addPassenger(victim);
		}
		
		// slowness
		DifficultyRating difficulty = Difficulties.getFor(slime);
		int duration = difficulty.asIntValue(20, 40, 80);
		int size = slime.getSize();
		victim.addPotionEffect(new PotionEffect(PotionEffectType.SLOWNESS, duration, size));
	}
}
