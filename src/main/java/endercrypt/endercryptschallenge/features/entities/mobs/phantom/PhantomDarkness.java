/**************************************************************************\
* EnderCrypt's Challenge                                                   *
* Copyright (C) 2023 by EnderCrypt                                         *
*                                                                          *
* This program is free software: you can redistribute it and/or modify     *
* it under the terms of the GNU Affero General Public License as           *
* published by the Free Software Foundation, either version 3 of           *
* the License, or (at your option) any later version.                      *
*                                                                          *
* This program is distributed in the hope that it will be useful,          *
* but WITHOUT ANY WARRANTY; without even the implied warranty of           *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
* GNU Affero General Public License for more details.                      *
*                                                                          *
* You should have received a copy of the GNU Affero General Public License *
* along with this program in its root directory.                           *
* If not, see <http://www.gnu.org/licenses/>.                              *
\**************************************************************************/

package endercrypt.endercryptschallenge.features.entities.mobs.phantom;


import endercrypt.endercryptschallenge.core.Core;
import endercrypt.endercryptschallenge.modules.features.MinecraftFeature;
import endercrypt.library.commons.misc.Ender;

import org.bukkit.entity.Phantom;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;


/**
 * idea by Qwuuk (https://www.twitch.tv/qwuuk)
 * 
 * @author EnderCrypt
 */
public class PhantomDarkness extends MinecraftFeature
{
	public PhantomDarkness(Core core)
	{
		super(core);
	}
	
	@EventHandler
	public void onPlayerHurtByPhantom(EntityDamageByEntityEvent event)
	{
		if (event.getDamager()instanceof Phantom phantom && event.getEntity()instanceof Player player)
		{
			player.addPotionEffect(new PotionEffect(PotionEffectType.BLINDNESS, 20 * Ender.math.randomRange(3, 5), 1));
		}
	}
}
