/**************************************************************************\
* EnderCrypt's Challenge                                                   *
* Copyright (C) 2023 by EnderCrypt                                         *
*                                                                          *
* This program is free software: you can redistribute it and/or modify     *
* it under the terms of the GNU Affero General Public License as           *
* published by the Free Software Foundation, either version 3 of           *
* the License, or (at your option) any later version.                      *
*                                                                          *
* This program is distributed in the hope that it will be useful,          *
* but WITHOUT ANY WARRANTY; without even the implied warranty of           *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
* GNU Affero General Public License for more details.                      *
*                                                                          *
* You should have received a copy of the GNU Affero General Public License *
* along with this program in its root directory.                           *
* If not, see <http://www.gnu.org/licenses/>.                              *
\**************************************************************************/

package endercrypt.endercryptschallenge.features.entities.mobs.guardian.behavior;


import endercrypt.endercryptschallenge.utility.Utility;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Guardian;
import org.bukkit.entity.LivingEntity;
import org.bukkit.util.Vector;


/**
 * @author EnderCrypt
 */
public abstract class StandardGuardianBehavior implements GuardianBehavior
{
	private static final double MAX_SPEED = 2.0;
	
	protected abstract boolean isApplicable(Guardian guardian, LivingEntity target, double distance, int depth);
	
	private static int getDepth(Entity entity)
	{
		int depth = 0;
		Block block = entity.getLocation().getBlock();
		while (block.getType() == Material.WATER)
		{
			block = block.getRelative(BlockFace.UP);
			depth++;
		}
		return depth;
	}
	
	@Override
	public final boolean trigger(Guardian guardian, LivingEntity target)
	{
		int depth = getDepth(guardian);
		
		if (depth == 0)
			return false;
		
		double distance = Utility.roughDistance(guardian.getLocation(), target.getLocation());
		if (isApplicable(guardian, target, distance, depth) == false)
			return false;
		
		double speed = getSpeed(guardian, target, distance, depth);
		Vector acceleration = getAim(guardian, target, distance, depth)
			.subtract(guardian.getLocation())
			.toVector()
			.normalize()
			.multiply(speed);
		
		Vector velocity = guardian.getVelocity();
		velocity.add(acceleration);
		velocity = Utility.truncateVector(velocity, MAX_SPEED);
		guardian.setVelocity(velocity);
		
		postAction(guardian, target, distance, depth);
		return true;
	}
	
	protected abstract double getSpeed(Guardian guardian, LivingEntity target, double distance, int depth);
	
	protected abstract Location getAim(Guardian guardian, LivingEntity target, double distance, int depth);
	
	@SuppressWarnings("unused")
	protected void postAction(Guardian guardian, LivingEntity target, double distance, int depth)
	{
		// do nothing
	}
}
