/**************************************************************************\
* EnderCrypt's Challenge                                                   *
* Copyright (C) 2023 by EnderCrypt                                         *
*                                                                          *
* This program is free software: you can redistribute it and/or modify     *
* it under the terms of the GNU Affero General Public License as           *
* published by the Free Software Foundation, either version 3 of           *
* the License, or (at your option) any later version.                      *
*                                                                          *
* This program is distributed in the hope that it will be useful,          *
* but WITHOUT ANY WARRANTY; without even the implied warranty of           *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
* GNU Affero General Public License for more details.                      *
*                                                                          *
* You should have received a copy of the GNU Affero General Public License *
* along with this program in its root directory.                           *
* If not, see <http://www.gnu.org/licenses/>.                              *
\**************************************************************************/

package endercrypt.endercryptschallenge.features.entities.mobs.blaze.fire;


import endercrypt.endercryptschallenge.core.Core;
import endercrypt.endercryptschallenge.modules.features.MinecraftFeature;
import endercrypt.endercryptschallenge.utility.Utility;
import endercrypt.endercryptschallenge.utility.tracker.entity.EntityTracker;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.entity.Blaze;
import org.bukkit.entity.Entity;


public class BlazeFire extends MinecraftFeature
{
	private final EntityTracker<Blaze> blazes = new EntityTracker<>(Blaze.class);
	
	public BlazeFire(Core core)
	{
		super(core);
	}
	
	@Override
	public void onCycleEntity(Entity entity)
	{
		blazes.feed(entity);
	}
	
	@Override
	public void onTick()
	{
		blazes.forEach(this::process);
	}
	
	private void process(Blaze blaze)
	{
		Block target = pickFireLocation(blaze).getBlock();
		
		if (target == null)
			return;
		
		if (target.getType() != Material.AIR)
			return;
		
		Block floor = target.getRelative(BlockFace.DOWN);
		
		if (floor == null)
			return;
		
		if (floor.isSolid() == false)
			return;
		
		target.setType(Material.FIRE);
	}
	
	private Location pickFireLocation(Blaze blaze)
	{
		final double SPREAD = 30;
		final double RANGE = 10;
		Location location = blaze.getLocation().clone();
		double distance = Utility.fastInverseSqrt((float) (Math.random() * SPREAD)) * RANGE;
		Utility.randomlyShift(location, distance);
		return location;
	}
}
