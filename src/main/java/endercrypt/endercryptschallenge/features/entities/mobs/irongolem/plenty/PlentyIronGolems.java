/**************************************************************************\
* EnderCrypt's Challenge                                                   *
* Copyright (C) 2023 by EnderCrypt                                         *
*                                                                          *
* This program is free software: you can redistribute it and/or modify     *
* it under the terms of the GNU Affero General Public License as           *
* published by the Free Software Foundation, either version 3 of           *
* the License, or (at your option) any later version.                      *
*                                                                          *
* This program is distributed in the hope that it will be useful,          *
* but WITHOUT ANY WARRANTY; without even the implied warranty of           *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
* GNU Affero General Public License for more details.                      *
*                                                                          *
* You should have received a copy of the GNU Affero General Public License *
* along with this program in its root directory.                           *
* If not, see <http://www.gnu.org/licenses/>.                              *
\**************************************************************************/

package endercrypt.endercryptschallenge.features.entities.mobs.irongolem.plenty;


import endercrypt.endercryptschallenge.core.Core;
import endercrypt.endercryptschallenge.modules.features.MinecraftFeature;
import endercrypt.library.commons.misc.Ender;

import java.util.Optional;

import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.attribute.Attribute;
import org.bukkit.block.Block;
import org.bukkit.entity.Entity;
import org.bukkit.entity.IronGolem;
import org.bukkit.entity.Villager;
import org.bukkit.event.EventHandler;
import org.bukkit.event.entity.EntityDamageByBlockEvent;
import org.bukkit.scheduler.BukkitRunnable;


/**
 * @author EnderCrypt
 */
public class PlentyIronGolems extends MinecraftFeature
{
	public PlentyIronGolems(Core core)
	{
		super(core);
	}
	
	@EventHandler
	public void onIronGolemHurtByBlock(EntityDamageByBlockEvent event)
	{
		if (event.getEntity() instanceof IronGolem ironGolem)
		{
			Block block = event.getDamager();
			schedule(10, () -> {
				if (ironGolem.isDead() == false)
				{
					ironGolem.swingMainHand();
					block.breakNaturally();
				}
			});
		}
	}
	
	@Override
	public void onCycleEntity(Entity entity)
	{
		if (entity instanceof Villager villager)
		{
			processVillager(villager);
		}
	}
	
	private void processVillager(Villager villager)
	{
		// chance
		if (Math.random() > 0.1)
		{
			return;
		}
		
		// in water
		if (villager.isInWater())
		{
			return;
		}
		
		// iron golem count short cap
		int ironGolemShort = villager.getLocation().getNearbyEntitiesByType(IronGolem.class, 16).size();
		if (ironGolemShort > 0)
		{
			return;
		}
		
		// iron golem count far cap
		int ironGolemFar = villager.getLocation().getNearbyEntitiesByType(IronGolem.class, 64).size();
		if (ironGolemFar > 10)
		{
			return;
		}
		
		spawnGolem(villager);
	}
	
	private Optional<IronGolem> spawnGolem(Villager villager)
	{
		Location location = villager.getLocation();
		World world = location.getWorld();
		Location highest = location.toHighestLocation();
		if (location.getY() > highest.getY())
		{
			final int spread = 5;
			int rx = Ender.math.randomRange(-spread, spread);
			int rz = Ender.math.randomRange(-spread, spread);
			Location spawnLocation = location.clone().add(rx, 100, rz);
			logger.info("Dropping a golem onto " + spawnLocation);
			IronGolem spawnedGolem = world.spawn(spawnLocation, IronGolem.class);
			configuredSpawnedGolem(spawnedGolem);
			return Optional.of(spawnedGolem);
		}
		if (location.getNearbyPlayers(16).isEmpty() == false)
		{
			return Optional.empty();
		}
		logger.info("Spawning a golem onto " + location);
		IronGolem spawnedGolem = world.spawn(location, IronGolem.class);
		spawnedGolem.setInvisible(true);
		new BukkitRunnable()
		{
			@Override
			public void run()
			{
				double maxHealth = spawnedGolem.getAttribute(Attribute.GENERIC_MAX_HEALTH).getBaseValue();
				if (spawnedGolem.getHealth() < maxHealth)
				{
					spawnedGolem.remove();
					return;
				}
				// succeess
				configuredSpawnedGolem(spawnedGolem);
				logger.info("spawned " + spawnedGolem + " to guard for villager");
			}
		}.runTaskLater(plugin, 20);
		return Optional.of(spawnedGolem);
	}
	
	private void configuredSpawnedGolem(IronGolem ironGolem)
	{
		ironGolem.setInvisible(false);
		ironGolem.clearLootTable();
	}
}
