/**************************************************************************\
* EnderCrypt's Challenge                                                   *
* Copyright (C) 2023 by EnderCrypt                                         *
*                                                                          *
* This program is free software: you can redistribute it and/or modify     *
* it under the terms of the GNU Affero General Public License as           *
* published by the Free Software Foundation, either version 3 of           *
* the License, or (at your option) any later version.                      *
*                                                                          *
* This program is distributed in the hope that it will be useful,          *
* but WITHOUT ANY WARRANTY; without even the implied warranty of           *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
* GNU Affero General Public License for more details.                      *
*                                                                          *
* You should have received a copy of the GNU Affero General Public License *
* along with this program in its root directory.                           *
* If not, see <http://www.gnu.org/licenses/>.                              *
\**************************************************************************/

package endercrypt.endercryptschallenge.features.entities.mobs.zombie.hordes;


import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.entity.Monster;


/**
 * @author EnderCrypt
 */
public class HordeLocator
{
	private Location target;
	private Location realLocation;
	
	public HordeLocator(Location location)
	{
		this.target = location;
		this.realLocation = location;
	}
	
	public World getWorld()
	{
		return target().getWorld();
	}
	
	public Location target()
	{
		return target;
	}
	
	public Location getLocation()
	{
		return realLocation.clone();
	}
	
	protected void updateRealLocation(Horde horde)
	{
		realLocation = getRealLocation(horde);
	}
	
	private Location getRealLocation(Horde horde)
	{
		if (horde.isDead())
		{
			return target();
		}
		else
		{
			synchronized (horde.monsters)
			{
				Location result = new Location(target.getWorld(), 0, 0, 0);
				for (Monster monster : horde.monsters)
				{
					result.add(monster.getLocation());
				}
				result.multiply(1.0 / horde.monsters.size());
				return result;
			}
		}
	}
	
	public double getDistanceOffset()
	{
		return target.distance(realLocation);
	}
}
