/**************************************************************************\
* EnderCrypt's Challenge                                                   *
* Copyright (C) 2023 by EnderCrypt                                         *
*                                                                          *
* This program is free software: you can redistribute it and/or modify     *
* it under the terms of the GNU Affero General Public License as           *
* published by the Free Software Foundation, either version 3 of           *
* the License, or (at your option) any later version.                      *
*                                                                          *
* This program is distributed in the hope that it will be useful,          *
* but WITHOUT ANY WARRANTY; without even the implied warranty of           *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
* GNU Affero General Public License for more details.                      *
*                                                                          *
* You should have received a copy of the GNU Affero General Public License *
* along with this program in its root directory.                           *
* If not, see <http://www.gnu.org/licenses/>.                              *
\**************************************************************************/

package endercrypt.endercryptschallenge.features.entities.mobs.bee.poison;


import endercrypt.endercryptschallenge.core.Core;
import endercrypt.endercryptschallenge.modules.features.MinecraftFeature;
import endercrypt.endercryptschallenge.utility.difficulty.Difficulties;
import endercrypt.endercryptschallenge.utility.difficulty.DifficultyRating;

import org.bukkit.entity.Bee;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.event.EventHandler;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;


/**
 * idea by Qwuuk (https://www.twitch.tv/qwuuk)
 * 
 * @author EnderCrypt
 */
public class BeePoison extends MinecraftFeature
{
	public BeePoison(Core core)
	{
		super(core);
	}
	
	@Override
	public void onCycleEntity(Entity entity)
	{
		if (entity instanceof Bee bee && bee.getTarget() == null)
		{
			bee.getWorld()
				.getPlayers()
				.stream()
				.filter(p -> p.getLocation().distance(bee.getLocation()) < 5)
				.findAny()
				.ifPresent(bee::setTarget);
		}
	}
	
	@EventHandler
	public void onBeeSting(EntityDamageByEntityEvent event)
	{
		if (event.getDamager() instanceof Bee bee && event.getEntity() instanceof LivingEntity victim)
		{
			applyBeeSting(bee, victim);
		}
	}
	
	private void applyBeeSting(Bee bee, LivingEntity victim)
	{
		DifficultyRating difficulty = Difficulties.getFor(bee);
		int duration = 20 * difficulty.asIntValue(10, 20, 30);
		int amplifier = difficulty.asIntModifier();
		victim.addPotionEffect(new PotionEffect(PotionEffectType.POISON, duration, amplifier));
	}
}
