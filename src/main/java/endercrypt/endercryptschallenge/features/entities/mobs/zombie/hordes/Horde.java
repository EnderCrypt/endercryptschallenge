/**************************************************************************\
* EnderCrypt's Challenge                                                   *
* Copyright (C) 2023 by EnderCrypt                                         *
*                                                                          *
* This program is free software: you can redistribute it and/or modify     *
* it under the terms of the GNU Affero General Public License as           *
* published by the Free Software Foundation, either version 3 of           *
* the License, or (at your option) any later version.                      *
*                                                                          *
* This program is distributed in the hope that it will be useful,          *
* but WITHOUT ANY WARRANTY; without even the implied warranty of           *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
* GNU Affero General Public License for more details.                      *
*                                                                          *
* You should have received a copy of the GNU Affero General Public License *
* along with this program in its root directory.                           *
* If not, see <http://www.gnu.org/licenses/>.                              *
\**************************************************************************/

package endercrypt.endercryptschallenge.features.entities.mobs.zombie.hordes;


import endercrypt.endercryptschallenge.utility.Utility;
import endercrypt.endercryptschallenge.utility.difficulty.Difficulties;
import endercrypt.endercryptschallenge.utility.difficulty.DifficultyRating;
import endercrypt.library.commons.RandomEntry;

import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import org.bukkit.Location;
import org.bukkit.attribute.Attribute;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Monster;
import org.bukkit.entity.PigZombie;
import org.bukkit.entity.Player;

import com.destroystokyo.paper.entity.Pathfinder;


/**
 * @author EnderCrypt
 */
public class Horde
{
	/**
	 * monsters in a horde will attempt to select a new target location every
	 * tick if the monster does not already have a movement target, this will be
	 * selected for where they should travel if they do already have a movement
	 * target, the distance from it to the new potential target is compared to
	 * this number only if the distance is more than what this constant defines,
	 * will the new target be set and the pathfinder will be engaged (draining
	 * precious cpu resources)<br/>
	 * <br/>
	 * a low number: <br/>
	 * will mean accurate, almost robotic movement and potentially cause
	 * performance issues <br/>
	 * <br/>
	 * a high number:<br/>
	 * will make the monsters spread out, the movements will be rough, but
	 * little performance penalty will be incurred
	 */
	private static final int TARGET_MINIMUM_REALIGNMENT_DISTANCE = 4;
	
	/**
	 * if a player (or other target) is whitin this distance, the pathfinder
	 * will simply stop attempting to target anything and let minecraft do its
	 * own thing
	 */
	private static final double PLAYER_PRIORITY_GOAL = 5;
	
	private final HordeManager hordes;
	
	private final HordeConfiguration configuration = new HordeConfiguration();
	
	private HordeLocator location;
	
	protected Set<Monster> monsters = new HashSet<>();
	private int recordSize = 0;
	
	private int travelTime = 0;
	
	public Horde(HordeManager hordes, Location location)
	{
		this.hordes = hordes;
		this.location = new HordeLocator(location);
	}
	
	public HordeLocator getLocator()
	{
		return location;
	}
	
	public HordeConfiguration configuration()
	{
		return configuration;
	}
	
	public int getSize()
	{
		return monsters.size();
	}
	
	public int getRecordSize()
	{
		return recordSize;
	}
	
	public boolean isDead()
	{
		return monsters.isEmpty();
	}
	
	public Set<Monster> getMonsters()
	{
		return Collections.unmodifiableSet(monsters);
	}
	
	private void cleanMonsters()
	{
		monsters.removeIf(this::isShouldRemoveMonster);
	}
	
	private boolean isShouldRemoveMonster(Monster monster)
	{
		if (monster.isDead())
		{
			return true;
		}
		if (monster.getWorld().equals(location.getWorld()) == false)
		{
			return true;
		}
		return false;
	}
	
	protected void addMonster(Monster monster)
	{
		monsters.add(monster);
	}
	
	protected void removeMonster(Monster monster)
	{
		monsters.remove(monster);
	}
	
	public void recruit()
	{
		if (configuration.isJoinable() && configuration.isMergeable())
		{
			for (Class<? extends Monster> joinableSpeciesClass : configuration.joinableSpecies())
			{
				location.getLocation().getNearbyEntitiesByType(joinableSpeciesClass, 8).forEach(this::tryRecruit);
			}
		}
	}
	
	private void tryRecruit(Monster monster)
	{
		if (monsters.contains(monster))
			return; // already in horde
		
		if (Utility.isSpecialEnemy(monster))
			return; // special monster
		
		if (monster instanceof PigZombie && configuration.joinableSpecies().contains(PigZombie.class) == false)
			return; // horde tries to join zombies, but not pig zombies so ignore them
		
		Horde oldHorde = hordes.getHorde(monster).orElse(null);
		
		if (oldHorde != null)
		{
			if (oldHorde.configuration.isMergeable() == false)
			{
				return;
			}
			
			if (oldHorde.configuration.getPriority() > configuration.getPriority())
			{
				return;
			}
			
			// remove monster from previous horde
			oldHorde.removeMonster(monster);
		}
		
		// configure follow range
		monster.getAttribute(Attribute.GENERIC_FOLLOW_RANGE).setBaseValue(32);
		
		// add monster
		addMonster(monster);
	}
	
	public void setTarget(Player player)
	{
		for (Monster monster : monsters)
		{
			monster.setTarget(player);
		}
	}
	
	public Optional<Player> getPrimaryTarget()
	{
		Map<Player, Integer> counts = new HashMap<>();
		
		for (Monster monster : monsters)
		{
			LivingEntity target = monster.getTarget();
			if (target instanceof Player player)
			{
				int count = counts.getOrDefault(player, 0);
				count++;
				counts.put(player, count);
			}
		}
		return counts.entrySet().stream() // TODO: verify that this works correctly
			.sorted((e1, e2) -> Integer.compare(e2.getValue(), e1.getValue()))
			.map(e -> e.getKey())
			.findFirst();
	}
	
	//	public static long t1 = 0;
	//	public static long t2 = 0;
	//	public static long t3 = 0;
	
	public void update()
	{
		long time = 0L;
		
		// clean out the dead
		// time = System.currentTimeMillis();
		cleanMonsters();
		// t1 += (System.currentTimeMillis() - time);
		
		// calculate real location
		location.updateRealLocation(this);
		
		// join entities
		// time = System.currentTimeMillis();
		recruit();
		// t2 += (System.currentTimeMillis() - time);
		
		// track record size
		recordSize = Math.max(recordSize, getSize());
		
		// time up
		travelTime++;
		
		// move
		// time = System.currentTimeMillis();
		autonamousActivity();
		// t3 += (System.currentTimeMillis() - time);
	}
	
	private void autonamousActivity()
	{
		// calculate playerlocation
		Location hordeLocation = location.getLocation();
		Player targetPlayer = Utility.getClosestSurvivalPlayer(hordeLocation).orElse(null);
		
		// no player available, do nothing
		if (targetPlayer == null)
		{
			return;
		}
		
		// distance
		double playerDistance = targetPlayer.getLocation().distance(hordeLocation);
		
		// target player
		DifficultyRating difficulty = Difficulties.getFor(location.getLocation());
		if (playerDistance < difficulty.asIntValue(20, 40, 80))
		{
			travelTime = 0;
			Utility.setLocation(location.target(), targetPlayer.getLocation());
		}
		
		// autonamous re-target
		if (travelTime > 60 || location.getDistanceOffset() < 10)
		{
			travelTime = 0;
			Utility.setLocation(location.target(), Utility.randomlyShift(location.getLocation(), 32).toHighestLocation());
		}
		
		// set targets
		monsters.forEach(this::autonamousActivity);
		
		// despawn
		double despawnChance = Math.max(0.05, getSize() / 100);
		if (isDead() == false && playerDistance > configuration.getDespawnRange() && Math.random() < despawnChance)
		{
			Monster monster = RandomEntry.from(monsters);
			monster.remove();
			monsters.remove(monster);
		}
	}
	
	private void autonamousActivity(Monster monster)
	{
		PathfinderTarget prefferedTarget = autonamousTargetSelection(monster).orElse(null);
		if (prefferedTarget != null && isReasonableFreshTarget(monster, prefferedTarget))
		{
			prefferedTarget.setAsTargetFor(monster);
		}
	}
	
	private boolean isReasonableFreshTarget(Monster monster, PathfinderTarget target)
	{
		LivingEntity killTarget = monster.getTarget();
		if (killTarget != null)
		{
			double distanceToKillTarget = killTarget.getLocation().distance(monster.getLocation());
			if (distanceToKillTarget <= PLAYER_PRIORITY_GOAL)
			{
				return false;
			}
		}
		
		Pathfinder pathfinder = monster.getPathfinder();
		if (pathfinder.hasPath() == false)
			return true;
		
		double newTargetOffset = pathfinder.getCurrentPath().getFinalPoint().distance(target.getLocation());
		if (newTargetOffset >= TARGET_MINIMUM_REALIGNMENT_DISTANCE)
			return true;
		
		return false;
	}
	
	private Optional<PathfinderTarget> autonamousTargetSelection(Monster monster)
	{
		// target: horde center
		Location monsterLocation = monster.getLocation();
		double hordeDistance = monsterLocation.distance(location.getLocation());
		if (hordeDistance > 16)
		{ // target horde center (due to being too far away)
			return Optional.of(PathfinderTarget.of(location.target()));
		}
		
		// target: player
		Player monsterTargetPlayer = Utility.getClosestSurvivalPlayer(monsterLocation).orElse(null);
		if (monsterTargetPlayer != null)
		{
			double monsterTargetPlayerDistance = monsterTargetPlayer.getLocation().distance(monsterLocation);
			if (monsterTargetPlayerDistance < 16)
			{ // target player (due to no nearby players)
				return Optional.of(PathfinderTarget.of(monsterTargetPlayer));
			}
		}
		
		return Optional.empty();
	}
}
