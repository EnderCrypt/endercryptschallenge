/**************************************************************************\
* EnderCrypt's Challenge                                                   *
* Copyright (C) 2023 by EnderCrypt                                         *
*                                                                          *
* This program is free software: you can redistribute it and/or modify     *
* it under the terms of the GNU Affero General Public License as           *
* published by the Free Software Foundation, either version 3 of           *
* the License, or (at your option) any later version.                      *
*                                                                          *
* This program is distributed in the hope that it will be useful,          *
* but WITHOUT ANY WARRANTY; without even the implied warranty of           *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
* GNU Affero General Public License for more details.                      *
*                                                                          *
* You should have received a copy of the GNU Affero General Public License *
* along with this program in its root directory.                           *
* If not, see <http://www.gnu.org/licenses/>.                              *
\**************************************************************************/

package endercrypt.endercryptschallenge.features.entities.mobs.ghast.spawning;


import endercrypt.endercryptschallenge.core.Core;
import endercrypt.endercryptschallenge.modules.features.MinecraftFeature;
import endercrypt.endercryptschallenge.modules.worlds.list.nether.NetherWorld;
import endercrypt.endercryptschallenge.utility.Utility;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.entity.Ghast;
import org.bukkit.entity.Player;
import org.bukkit.util.Vector;


/**
 * @author EnderCrypt
 */
public class GhastSpawning extends MinecraftFeature
{
	private static final int SPAWN_RANGE_THRESHOLD = 16 * 3;
	private static final int SPAWN_RANGE = 16 * 2;
	
	public GhastSpawning(Core core)
	{
		super(core);
		getDebug().enableDebugSupport();
	}
	
	@Override
	public void onSecond()
	{
		for (World world : server.getWorlds())
		{
			for (Player player : world.getPlayers())
			{
				processPlayer(player);
			}
		}
	}
	
	private void processPlayer(Player player)
	{
		World world = player.getWorld();
		
		if (resolve(world) instanceof NetherWorld == false)
			return;
		
		Location playerLocation = player.getLocation();
		
		int ghastLimit = Utility.isOnNetherCeiling(player) ? 10 : 3;
		int nearbyGhasts = playerLocation.getNearbyEntitiesByType(Ghast.class, SPAWN_RANGE_THRESHOLD).size();
		
		if (nearbyGhasts >= ghastLimit)
			return;
		
		Vector vector = Utility.getRandomVector().multiply(SPAWN_RANGE);
		Location spawnTarget = playerLocation.clone().add(vector);
		boolean isBlocked = Utility.getBlockQube(spawnTarget.getBlock(), 3)
			.stream()
			.anyMatch(b -> b.getType() != Material.AIR);
		
		if (isBlocked)
			return;
		
		world.spawn(spawnTarget, Ghast.class);
	}
}
