/**************************************************************************\
* EnderCrypt's Challenge                                                   *
* Copyright (C) 2023 by EnderCrypt                                         *
*                                                                          *
* This program is free software: you can redistribute it and/or modify     *
* it under the terms of the GNU Affero General Public License as           *
* published by the Free Software Foundation, either version 3 of           *
* the License, or (at your option) any later version.                      *
*                                                                          *
* This program is distributed in the hope that it will be useful,          *
* but WITHOUT ANY WARRANTY; without even the implied warranty of           *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
* GNU Affero General Public License for more details.                      *
*                                                                          *
* You should have received a copy of the GNU Affero General Public License *
* along with this program in its root directory.                           *
* If not, see <http://www.gnu.org/licenses/>.                              *
\**************************************************************************/

package endercrypt.endercryptschallenge.features.entities.mobs.zombie.miner.techniques.types;


import endercrypt.endercryptschallenge.features.entities.mobs.zombie.miner.techniques.MiningTechnique;
import endercrypt.library.commons.misc.Ender;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;


/**
 * @author EnderCrypt
 */
public class RandomMiningTechnique implements MiningTechnique
{
	@Override
	public Block choose(Location location, Location target, double range)
	{
		Location targetLocation = location.clone().add(Ender.math.randomRange(-2, 2), Ender.math.randomRange(-2, 3), Ender.math.randomRange(-2, 2));
		Block targetBlock = location.getWorld().getBlockAt(targetLocation);
		if (targetBlock.getType() == Material.AIR)
		{
			return null;
		}
		return targetBlock;
	}
}
