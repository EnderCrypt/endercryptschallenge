/**************************************************************************\
* EnderCrypt's Challenge                                                   *
* Copyright (C) 2023 by EnderCrypt                                         *
*                                                                          *
* This program is free software: you can redistribute it and/or modify     *
* it under the terms of the GNU Affero General Public License as           *
* published by the Free Software Foundation, either version 3 of           *
* the License, or (at your option) any later version.                      *
*                                                                          *
* This program is distributed in the hope that it will be useful,          *
* but WITHOUT ANY WARRANTY; without even the implied warranty of           *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
* GNU Affero General Public License for more details.                      *
*                                                                          *
* You should have received a copy of the GNU Affero General Public License *
* along with this program in its root directory.                           *
* If not, see <http://www.gnu.org/licenses/>.                              *
\**************************************************************************/

package endercrypt.endercryptschallenge.features.entities.mobs.zombie.miner;


import endercrypt.endercryptschallenge.core.Core;
import endercrypt.endercryptschallenge.modules.features.MinecraftFeature;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Optional;

import org.bukkit.entity.Entity;
import org.bukkit.entity.Monster;


/**
 * @author EnderCrypt
 */
public class ZombieMiners extends MinecraftFeature
{
	private Map<Entity, Miner> miners = new HashMap<>();
	
	public ZombieMiners(Core core)
	{
		super(core);
	}
	
	@Override
	public void onCycleEntity(Entity entity)
	{
		if (entity instanceof Monster monster)
		{
			miners.put(monster, new Miner(core, monster));
		}
	}
	
	@Override
	public void onTick()
	{
		Iterator<Miner> iterator = miners.values().iterator();
		while (iterator.hasNext())
		{
			Miner miner = iterator.next();
			miner.update();
			if (miner.isAlive() == false)
			{
				iterator.remove();
			}
		}
	}
	
	public Optional<Miner> getMiner(Entity entity)
	{
		return Optional.ofNullable(miners.get(entity));
	}
}
