/**************************************************************************\
* EnderCrypt's Challenge                                                   *
* Copyright (C) 2023 by EnderCrypt                                         *
*                                                                          *
* This program is free software: you can redistribute it and/or modify     *
* it under the terms of the GNU Affero General Public License as           *
* published by the Free Software Foundation, either version 3 of           *
* the License, or (at your option) any later version.                      *
*                                                                          *
* This program is distributed in the hope that it will be useful,          *
* but WITHOUT ANY WARRANTY; without even the implied warranty of           *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
* GNU Affero General Public License for more details.                      *
*                                                                          *
* You should have received a copy of the GNU Affero General Public License *
* along with this program in its root directory.                           *
* If not, see <http://www.gnu.org/licenses/>.                              *
\**************************************************************************/

package endercrypt.endercryptschallenge.features.entities.mobs.bee.hive;


import endercrypt.endercryptschallenge.core.Core;
import endercrypt.endercryptschallenge.modules.features.MinecraftFeature;
import endercrypt.endercryptschallenge.utility.Utility;
import endercrypt.endercryptschallenge.utility.difficulty.Difficulties;
import endercrypt.endercryptschallenge.utility.difficulty.DifficultyRating;
import endercrypt.library.commons.misc.Ender;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.entity.Bee;
import org.bukkit.entity.FallingBlock;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPhysicsEvent;
import org.bukkit.event.entity.EntityChangeBlockEvent;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;


/**
 * @author EnderCrypt
 */
public class BeeHive extends MinecraftFeature
{
	private static final Material HIVE = Material.BEE_NEST;
	
	public BeeHive(Core core)
	{
		super(core);
	}
	
	@EventHandler
	public void onBeeHiveUpdate(BlockPhysicsEvent event)
	{
		Block block = event.getBlock();
		
		if (block.getType() != HIVE)
			return;
		
		boolean connected = Utility.cartesianBlockFaces
			.stream()
			.map(block::getRelative)
			.map(Block::getType)
			.anyMatch(m -> m != Material.AIR);
		
		if (connected)
			return;
		
		World world = block.getWorld();
		world.spawnFallingBlock(Utility.getBlockFloorLocation(block), block.getBlockData());
		block.setType(Material.AIR);
	}
	
	@EventHandler
	public void onBeeHiveFallen(EntityChangeBlockEvent event)
	{
		if (event.getEntity() instanceof FallingBlock fallingBlock && fallingBlock.getBlockData().getMaterial() == HIVE)
		{
			schedule(1, () -> explodeBeeHive(event.getBlock()));
		}
	}
	
	@EventHandler
	public void onDestroyBeeHive(BlockBreakEvent event)
	{
		explodeBeeHive(event.getBlock());
	}
	
	private void explodeBeeHive(Block block)
	{
		if (block.getType() != HIVE)
			return;
		
		spawnAngryBees(block);
	}
	
	public static void spawnAngryBees(Block block)
	{
		block.breakNaturally(true);
		
		Location location = Utility.getBlockFloorLocation(block);
		World world = location.getWorld();
		
		DifficultyRating difficulty = Difficulties.getFor(block);
		int minBees = difficulty.asIntValue(3, 6, 12);
		int maxBees = difficulty.asIntValue(6, 12, 24);
		int bees = Ender.math.randomRange(minBees, maxBees);
		for (int i = 0; i < bees; i++)
		{
			Bee bee = world.spawn(location, Bee.class);
			Player target = Utility.getClosestSurvivalPlayer(location).orElse(null);
			if (target != null)
			{
				bee.setTarget(target);
				bee.addPotionEffect(new PotionEffect(PotionEffectType.SPEED, 20 * 60, 5, false, false));
			}
		}
	}
}
