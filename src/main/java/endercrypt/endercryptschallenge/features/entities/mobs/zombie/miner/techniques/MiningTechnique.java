/**************************************************************************\
* EnderCrypt's Challenge                                                   *
* Copyright (C) 2023 by EnderCrypt                                         *
*                                                                          *
* This program is free software: you can redistribute it and/or modify     *
* it under the terms of the GNU Affero General Public License as           *
* published by the Free Software Foundation, either version 3 of           *
* the License, or (at your option) any later version.                      *
*                                                                          *
* This program is distributed in the hope that it will be useful,          *
* but WITHOUT ANY WARRANTY; without even the implied warranty of           *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
* GNU Affero General Public License for more details.                      *
*                                                                          *
* You should have received a copy of the GNU Affero General Public License *
* along with this program in its root directory.                           *
* If not, see <http://www.gnu.org/licenses/>.                              *
\**************************************************************************/

package endercrypt.endercryptschallenge.features.entities.mobs.zombie.miner.techniques;


import endercrypt.endercryptschallenge.features.entities.mobs.zombie.miner.techniques.types.DirectionalMiningTechnique;
import endercrypt.endercryptschallenge.features.entities.mobs.zombie.miner.techniques.types.RandomMiningTechnique;
import endercrypt.endercryptschallenge.utility.difficulty.Difficulties;

import org.bukkit.Location;
import org.bukkit.block.Block;
import org.bukkit.entity.LivingEntity;


/**
 * @author EnderCrypt
 */
public interface MiningTechnique
{
	public static MiningTechnique directional = new DirectionalMiningTechnique();
	public static MiningTechnique random = new RandomMiningTechnique();
	
	public static MiningTechnique getMiningTechnique(LivingEntity entity)
	{
		if (Math.random() < Difficulties.getFor(entity).asDoubleValue(0.05, 0.1, 0.5))
		{
			return MiningTechnique.random;
		}
		else
		{
			return MiningTechnique.directional;
		}
	}
	
	public Block choose(Location location, Location target, double range);
}
