/**************************************************************************\
* EnderCrypt's Challenge                                                   *
* Copyright (C) 2023 by EnderCrypt                                         *
*                                                                          *
* This program is free software: you can redistribute it and/or modify     *
* it under the terms of the GNU Affero General Public License as           *
* published by the Free Software Foundation, either version 3 of           *
* the License, or (at your option) any later version.                      *
*                                                                          *
* This program is distributed in the hope that it will be useful,          *
* but WITHOUT ANY WARRANTY; without even the implied warranty of           *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
* GNU Affero General Public License for more details.                      *
*                                                                          *
* You should have received a copy of the GNU Affero General Public License *
* along with this program in its root directory.                           *
* If not, see <http://www.gnu.org/licenses/>.                              *
\**************************************************************************/

package endercrypt.endercryptschallenge.features.entities.mobs.witherskeleton;


import endercrypt.endercryptschallenge.core.Core;
import endercrypt.endercryptschallenge.materials.MaterialCollections;
import endercrypt.endercryptschallenge.materials.structure.MaterialCollection;
import endercrypt.endercryptschallenge.modules.features.MinecraftFeature;
import endercrypt.endercryptschallenge.utility.Utility;
import endercrypt.endercryptschallenge.utility.difficulty.Difficulties;
import endercrypt.endercryptschallenge.utility.difficulty.DifficultyRating;
import endercrypt.library.commons.RandomEntry;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.entity.WitherSkeleton;
import org.bukkit.event.EventHandler;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;


/**
 * idea by Just_a_bookworm_ (https://www.twitch.tv/just_a_bookworm_)
 * 
 * @author EnderCrypt
 */
public class WitherSkeletonCoalConvertion extends MinecraftFeature
{
	private static final List<MaterialCollection> priorities = List.of(
		MaterialCollections.equipment,
		MaterialCollections.flammables);
	
	public WitherSkeletonCoalConvertion(Core core)
	{
		super(core);
	}
	
	@EventHandler
	public void onWitherSkeletonHit(EntityDamageByEntityEvent event)
	{
		if (event.getEntity() instanceof Player player && event.getDamager() instanceof WitherSkeleton)
		{
			processWitherSkeletonHit(player);
		}
	}
	
	private void processWitherSkeletonHit(Player player)
	{
		DifficultyRating difficulty = Difficulties.getFor(player);
		if (Math.random() > difficulty.asDoubleValue(0.5, 0.25, 0.0))
			return;
		
		PlayerInventory inventory = player.getInventory();
		ItemStack item = getTargetItem(inventory);
		
		if (item == null)
			return;
		
		ItemStack product = new ItemStack(MaterialCollections.coal.getRandom());
		player.sendMessage("As the Wither skeleton touches you, your\n" + ChatColor.UNDERLINE + ChatColor.ITALIC + Utility.getItemName(item) + ChatColor.RESET + " turns into " + Utility.getItemName(product) + "!");
		item.subtract();
		inventory.addItem(product);
	}
	
	private ItemStack getTargetItem(PlayerInventory inventory)
	{
		List<ItemStack> contents = Arrays.stream(inventory.getContents())
			.filter(Objects::nonNull)
			.filter(i -> MaterialCollections.coal.contains(i) == false)
			.collect(Collectors.toList());
		
		for (MaterialCollection priority : priorities)
		{
			List<ItemStack> items = contents.stream()
				.filter(priority::contains)
				.collect(Collectors.toList());
			
			ItemStack target = RandomEntry.from(items);
			if (target != null)
				return target;
		}
		
		return RandomEntry.from(contents);
	}
}
