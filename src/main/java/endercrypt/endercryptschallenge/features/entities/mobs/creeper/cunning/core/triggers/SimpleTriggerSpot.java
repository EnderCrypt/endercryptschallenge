/**************************************************************************\
* EnderCrypt's Challenge                                                   *
* Copyright (C) 2023 by EnderCrypt                                         *
*                                                                          *
* This program is free software: you can redistribute it and/or modify     *
* it under the terms of the GNU Affero General Public License as           *
* published by the Free Software Foundation, either version 3 of           *
* the License, or (at your option) any later version.                      *
*                                                                          *
* This program is distributed in the hope that it will be useful,          *
* but WITHOUT ANY WARRANTY; without even the implied warranty of           *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
* GNU Affero General Public License for more details.                      *
*                                                                          *
* You should have received a copy of the GNU Affero General Public License *
* along with this program in its root directory.                           *
* If not, see <http://www.gnu.org/licenses/>.                              *
\**************************************************************************/

package endercrypt.endercryptschallenge.features.entities.mobs.creeper.cunning.core.triggers;


import endercrypt.endercryptschallenge.utility.difficulty.Difficulties;
import endercrypt.endercryptschallenge.utility.difficulty.DifficultyRating;
import endercrypt.library.commons.misc.Ender;

import org.bukkit.Location;
import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.Entity;


/**
 * @author EnderCrypt
 */
public class SimpleTriggerSpot extends TriggerSpot
{
	public static final double minimumStrength = 0.03;
	public static final double decaySpeed = 0.00125;
	
	private double strength = 0;
	private Entity debugEntity;
	
	public SimpleTriggerSpot(Location location)
	{
		super(location);
	}
	
	@Override
	protected TriggerSpot createMergedTriggerSpot(Location location)
	{
		return new SimpleTriggerSpot(location);
	}
	
	@Override
	public void punch(double strength)
	{
		this.strength += strength;
	}
	
	@Override
	public double getStrength()
	{
		return strength;
	}
	
	@Override
	public boolean isDecayed()
	{
		return getStrength() <= minimumStrength;
	}
	
	@Override
	public void updateDebug(boolean enabled)
	{
		if (enabled)
		{
			if (debugEntity == null || debugEntity.isValid() == false)
			{
				debugEntity = getLocation().getWorld().spawn(getLocation(), ArmorStand.class);
			}
			debugEntity.teleport(getLocation());
			debugEntity.setCustomName(String.valueOf(Ender.math.round(getStrength(), 3)));
			debugEntity.setCustomNameVisible(true);
		}
		else
		{
			if (debugEntity != null)
			{
				debugEntity.remove();
				debugEntity = null;
			}
		}
	}
	
	@Override
	public void update()
	{
		strength -= (strength * decaySpeed);
	}
	
	@Override
	public boolean isSuperAngry()
	{
		DifficultyRating difficulty = Difficulties.getFor(getLocation());
		double superAngryRange = difficulty.asDoubleValue(12, 6, 3);
		return getStrength() >= superAngryRange;
	}
	
	@Override
	public boolean isAngry()
	{
		return getStrength() >= 3;
	}
	
	@Override
	public double getTriggerRange()
	{
		double volume = getStrength() * 100;
		return Ender.math.nthRoot((3 * volume) / (4 * Math.PI), 3);
	}
	
	@Override
	public double getAggroRange()
	{
		return 15 + (getTriggerRange() * 2.0);
	}
	
	@Override
	public void delete()
	{
		if (debugEntity != null)
		{
			debugEntity.remove();
			debugEntity = null;
		}
	}
}
