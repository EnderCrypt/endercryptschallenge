/**************************************************************************\
* EnderCrypt's Challenge                                                   *
* Copyright (C) 2023 by EnderCrypt                                         *
*                                                                          *
* This program is free software: you can redistribute it and/or modify     *
* it under the terms of the GNU Affero General Public License as           *
* published by the Free Software Foundation, either version 3 of           *
* the License, or (at your option) any later version.                      *
*                                                                          *
* This program is distributed in the hope that it will be useful,          *
* but WITHOUT ANY WARRANTY; without even the implied warranty of           *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
* GNU Affero General Public License for more details.                      *
*                                                                          *
* You should have received a copy of the GNU Affero General Public License *
* along with this program in its root directory.                           *
* If not, see <http://www.gnu.org/licenses/>.                              *
\**************************************************************************/

package endercrypt.endercryptschallenge.features.entities.mobs.zombie.babythrow;


import endercrypt.endercryptschallenge.core.Core;
import endercrypt.endercryptschallenge.modules.features.MinecraftFeature;
import endercrypt.endercryptschallenge.utility.Utility;
import endercrypt.endercryptschallenge.utility.difficulty.Difficulties;
import endercrypt.endercryptschallenge.utility.difficulty.DifficultyRating;
import endercrypt.library.commons.misc.Ender;

import java.util.List;
import java.util.stream.Collectors;

import org.bukkit.Location;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.entity.Zombie;
import org.bukkit.util.Vector;


/**
 * @author EnderCrypt
 */
public class ZombieBabyThrow extends MinecraftFeature
{
	public ZombieBabyThrow(Core core)
	{
		super(core);
	}
	
	@Override
	public void onCycleEntity(Entity entity)
	{
		if (entity instanceof Zombie zombie)
		{
			zombieGrabChildren(zombie);
			zombieDropChildren(zombie);
			zombieThrowChildren(zombie);
		}
	}
	
	private void zombieDropChildren(Zombie zombie)
	{
		if (zombie.isAdult() == false)
			return;
		
		if (zombie.getPassengers().isEmpty())
			return;
		
		if (isBlockedOverHead(zombie) == false)
			return;
		
		zombie.eject();
	}
	
	public void zombieGrabChildren(Zombie zombie)
	{
		if (zombie.isAdult() == false)
			return;
		
		if (zombie.getPassengers().isEmpty() == false)
			return;
		
		if (isBlockedOverHead(zombie))
			return;
		
		zombie.getWorld().getNearbyEntitiesByType(Zombie.class, zombie.getLocation(), 2)
			.stream()
			.filter(z -> z.isAdult() == false)
			.filter(z -> z.isInsideVehicle() == false)
			.findFirst()
			.ifPresent(zombie::addPassenger);
	}
	
	private boolean isBlockedOverHead(Zombie zombie)
	{
		return zombie.getLocation().add(0, zombie.getHeight() + 0.75, 0).getBlock().isSolid();
	}
	
	public void zombieThrowChildren(Zombie zombie)
	{
		if (zombie.isAdult())
		{
			Zombie throwableZombie = zombie.getPassengers()
				.stream()
				.filter(Zombie.class::isInstance)
				.map(Zombie.class::cast)
				.filter(z -> z.isAdult() == false)
				.findFirst()
				.orElse(null);
			
			if (throwableZombie != null)
			{
				DifficultyRating difficulty = Difficulties.getFor(zombie);
				int radius = difficulty.asIntValue(10, 15, 30);
				List<Player> nearbyPlayers = zombie.getWorld().getNearbyEntitiesByType(Player.class, zombie.getLocation(), radius)
					.stream()
					.filter(Utility::isInSurvival)
					.collect(Collectors.toList());
				if (nearbyPlayers.isEmpty() == false)
				{
					Player target = nearbyPlayers.get((int) (Math.random() * nearbyPlayers.size()));
					zombie.removePassenger(throwableZombie);
					final double throwSpeedMultiplier = Ender.math.randomRange(0.15, 0.2);
					Location offsetLocation = target.getLocation().clone().subtract(zombie.getLocation());
					Vector velocity = new Vector(
						offsetLocation.getX() * throwSpeedMultiplier,
						(offsetLocation.getY() * throwSpeedMultiplier) + 0.2,
						offsetLocation.getZ() * throwSpeedMultiplier);
					
					throwableZombie.getPathfinder().stopPathfinding();
					throwableZombie.setVelocity(velocity);
					throwableZombie.setTarget(target);
				}
			}
		}
	}
}
