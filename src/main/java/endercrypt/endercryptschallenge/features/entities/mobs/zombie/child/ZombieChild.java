/**************************************************************************\
* EnderCrypt's Challenge                                                   *
* Copyright (C) 2023 by EnderCrypt                                         *
*                                                                          *
* This program is free software: you can redistribute it and/or modify     *
* it under the terms of the GNU Affero General Public License as           *
* published by the Free Software Foundation, either version 3 of           *
* the License, or (at your option) any later version.                      *
*                                                                          *
* This program is distributed in the hope that it will be useful,          *
* but WITHOUT ANY WARRANTY; without even the implied warranty of           *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
* GNU Affero General Public License for more details.                      *
*                                                                          *
* You should have received a copy of the GNU Affero General Public License *
* along with this program in its root directory.                           *
* If not, see <http://www.gnu.org/licenses/>.                              *
\**************************************************************************/

package endercrypt.endercryptschallenge.features.entities.mobs.zombie.child;


import endercrypt.endercryptschallenge.core.Core;
import endercrypt.endercryptschallenge.modules.features.MinecraftFeature;
import endercrypt.endercryptschallenge.utility.difficulty.Difficulties;
import endercrypt.endercryptschallenge.utility.difficulty.DifficultyRating;

import java.util.Objects;

import org.bukkit.Difficulty;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.entity.Zombie;
import org.bukkit.event.EventHandler;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.util.Vector;


/**
 * @author EnderCrypt
 */
public class ZombieChild extends MinecraftFeature
{
	private static final int ZOMBIE_CHILD_ATTRACTION_RADIUS = 20;
	
	public ZombieChild(Core core)
	{
		super(core);
	}
	
	@EventHandler
	public void onEntityDamageByEntity(EntityDamageByEntityEvent event)
	{
		Entity target = event.getEntity();
		Entity source = event.getDamager();
		double damage = event.getFinalDamage();
		
		if (target instanceof Player player)
		{
			if (source instanceof Zombie zombie && zombie.isAdult() == false && player.getPassengers().isEmpty())
			{ // zombie riding children
				player.addPassenger(zombie);
			}
		}
		if (source instanceof Player player)
		{
			if (target instanceof Zombie zombie)
			{
				if (zombie.isAdult() == false && zombie.isInsideVehicle() && Objects.equals(zombie.getVehicle(), player))
				{
					player.removePassenger(zombie);
					
					double force = damage / 2;
					double pitch = Math.toRadians(-player.getLocation().getPitch() - 90);
					double yaw = -Math.toRadians(player.getLocation().getYaw());
					
					Vector velocity = new Vector(
						Math.sin(yaw) * force,
						Math.cos(pitch) * force,
						Math.cos(yaw) * force);
					
					zombie.setVelocity(velocity);
				}
			}
		}
	}
	
	@Override
	public void onCycleEntity(Entity entity)
	{
		if (entity instanceof Player player)
		{
			playerRiddingZombieCheck(player);
		}
	}
	
	public void playerRiddingZombieCheck(Player player)
	{
		// being ridden by zombie child makes you a target of neaby zombies
		Zombie ridder = player.getPassengers()
			.stream()
			.filter(Zombie.class::isInstance)
			.map(Zombie.class::cast)
			.filter(z -> z.isAdult() == false)
			.findFirst()
			.orElse(null);
		
		if (ridder != null)
		{
			// debuffs
			DifficultyRating difficulty = Difficulties.getFor(player);
			int duration = difficulty.asIntValue(10, 20, 50);
			player.addPotionEffect(new PotionEffect(PotionEffectType.SLOWNESS, duration, 1));
			player.addPotionEffect(new PotionEffect(PotionEffectType.MINING_FATIGUE, duration, 1));
			player.addPotionEffect(new PotionEffect(PotionEffectType.NAUSEA, duration, 1));
			if (difficulty.isHarderThan(Difficulty.NORMAL))
			{
				player.addPotionEffect(new PotionEffect(PotionEffectType.POISON, duration, 1));
			}
			
			// damage
			player.damage(0.5, ridder);
			
			// target
			player.getWorld().getNearbyEntitiesByType(Zombie.class, player.getLocation(), ZOMBIE_CHILD_ATTRACTION_RADIUS)
				.stream()
				.filter(z -> z.getTarget() == null)
				.forEach(z -> z.setTarget(player));
			
			// message
			player.sendMessage("WARNING: a zombie child is riding you!");
		}
	}
}
