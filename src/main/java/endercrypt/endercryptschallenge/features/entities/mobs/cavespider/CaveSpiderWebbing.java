/**************************************************************************\
* EnderCrypt's Challenge                                                   *
* Copyright (C) 2023 by EnderCrypt                                         *
*                                                                          *
* This program is free software: you can redistribute it and/or modify     *
* it under the terms of the GNU Affero General Public License as           *
* published by the Free Software Foundation, either version 3 of           *
* the License, or (at your option) any later version.                      *
*                                                                          *
* This program is distributed in the hope that it will be useful,          *
* but WITHOUT ANY WARRANTY; without even the implied warranty of           *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
* GNU Affero General Public License for more details.                      *
*                                                                          *
* You should have received a copy of the GNU Affero General Public License *
* along with this program in its root directory.                           *
* If not, see <http://www.gnu.org/licenses/>.                              *
\**************************************************************************/

package endercrypt.endercryptschallenge.features.entities.mobs.cavespider;


import endercrypt.endercryptschallenge.core.Core;
import endercrypt.endercryptschallenge.modules.features.MinecraftFeature;
import endercrypt.endercryptschallenge.utility.Utility;
import endercrypt.endercryptschallenge.utility.tracker.ObjectTracker;
import endercrypt.endercryptschallenge.utility.tracker.entity.ConditionAliveAndActive;
import endercrypt.library.commons.structs.sign.Signs;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.entity.CaveSpider;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Item;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.util.Vector;

import lombok.EqualsAndHashCode;
import lombok.RequiredArgsConstructor;


/**
 * Idea by Kyu
 * 
 * @author EnderCrypt
 */
public class CaveSpiderWebbing extends MinecraftFeature
{
	private final ObjectTracker<CaveSpiderEntity> spiders = new ObjectTracker<>(CaveSpiderEntity.class)
		.addCondition(CaveSpiderEntity::isAlive);
	
	private final ObjectTracker<ProjectileEntity> projectiles = new ObjectTracker<>(ProjectileEntity.class)
		.addCondition(ProjectileEntity::isAlive)
		.addRemoveListener(ProjectileEntity::onDestroy);
	
	public CaveSpiderWebbing(Core core)
	{
		super(core);
	}
	
	@Override
	public void onCycleEntity(Entity entity)
	{
		if (entity instanceof CaveSpider caveSpider)
		{
			spiders.add(new CaveSpiderEntity(caveSpider));
		}
	}
	
	@Override
	public void onTick()
	{
		spiders.forEach(CaveSpiderEntity::onTick);
		projectiles.forEach(ProjectileEntity::onTick);
	}
	
	@RequiredArgsConstructor
	@EqualsAndHashCode(onlyExplicitlyIncluded = true)
	private class CaveSpiderEntity
	{
		private static final double MAX_RANDOM_DIRECTION = 16; // degrees
		private static final int RELOAD_TIME = 20 * 2;
		
		@EqualsAndHashCode.Include
		private final CaveSpider caveSpider;
		private int reload = 0;
		
		public void onTick()
		{
			LivingEntity target = caveSpider.getTarget();
			if (reload <= 0 && target != null)
			{
				shootAt(target);
				reload = RELOAD_TIME;
			}
			reload--;
		}
		
		public void shootAt(LivingEntity target)
		{
			double distance = target.getLocation().distance(caveSpider.getLocation());
			double overAim = distance / 15;
			
			Location sourceLocation = caveSpider.getEyeLocation();
			Location targetLocation = target.getEyeLocation().add(0, overAim, 0);
			
			Vector offset = targetLocation.subtract(sourceLocation).toVector();
			Vector velocity = offset.normalize().multiply(1.25);
			double randomDirection = Signs.random().asInt() * Math.random() * Math.toRadians(MAX_RANDOM_DIRECTION);
			velocity.rotateAroundY(randomDirection);
			
			Item item = caveSpider.getWorld().spawn(sourceLocation, Item.class);
			item.setVelocity(velocity);
			
			item.setCanMobPickup(false);
			item.setCanPlayerPickup(false);
			item.setItemStack(new ItemStack(Material.COBWEB, 1));
			
			ProjectileEntity projectileEntity = new ProjectileEntity(item);
			projectiles.add(projectileEntity);
		}
		
		public boolean isAlive()
		{
			return ConditionAliveAndActive.checkEntity(caveSpider);
		}
	}
	
	@RequiredArgsConstructor
	private class ProjectileEntity
	{
		private final Item item;
		
		public void onTick()
		{
			Player player = Utility.getClosestPlayer(item.getLocation()).orElse(null);
			
			Block current = item.getLocation().getBlock();
			Block bottom = player.getLocation().getBlock();
			Block top = bottom.getRelative(BlockFace.UP);
			if (current.equals(bottom) || current.equals(top))
			{
				item.remove();
			}
		}
		
		public boolean isAlive()
		{
			if (ConditionAliveAndActive.checkEntity(item) == false)
				return false;
			
			double speed = item.getVelocity().length();
			if (speed < 0.05)
				return false;
			
			return true;
		}
		
		public void onDestroy()
		{
			Block block = item.getLocation().getBlock();
			block.breakNaturally();
			block.setType(Material.COBWEB);
			
			item.remove();
		}
	}
}
