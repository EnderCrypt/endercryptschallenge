/**************************************************************************\
* EnderCrypt's Challenge                                                   *
* Copyright (C) 2023 by EnderCrypt                                         *
*                                                                          *
* This program is free software: you can redistribute it and/or modify     *
* it under the terms of the GNU Affero General Public License as           *
* published by the Free Software Foundation, either version 3 of           *
* the License, or (at your option) any later version.                      *
*                                                                          *
* This program is distributed in the hope that it will be useful,          *
* but WITHOUT ANY WARRANTY; without even the implied warranty of           *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
* GNU Affero General Public License for more details.                      *
*                                                                          *
* You should have received a copy of the GNU Affero General Public License *
* along with this program in its root directory.                           *
* If not, see <http://www.gnu.org/licenses/>.                              *
\**************************************************************************/

package endercrypt.endercryptschallenge.features.entities.mobs.drowned.deconvertd;


import endercrypt.endercryptschallenge.core.Core;
import endercrypt.endercryptschallenge.modules.features.MinecraftFeature;

import org.bukkit.Location;
import org.bukkit.Sound;
import org.bukkit.World;
import org.bukkit.entity.Drowned;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Zombie;
import org.bukkit.inventory.EquipmentSlot;
import org.bukkit.inventory.ItemStack;


/**
 * @author EnderCrypt
 */
public class DeconvertDrowned extends MinecraftFeature
{
	public DeconvertDrowned(Core core)
	{
		super(core);
	}
	
	@Override
	public void onCycleEntity(Entity entity)
	{
		if (entity instanceof Drowned drowned)
		{
			processDrowned(drowned);
		}
	}
	
	private void processDrowned(Drowned drowned)
	{
		if (Math.random() > 0.1)
		{
			return;
		}
		if (drowned.isInWater())
		{
			return;
		}
		deconvertDrowned(drowned);
	}
	
	private void deconvertDrowned(Drowned drowned)
	{
		// spawn
		Location location = drowned.getLocation();
		World world = location.getWorld();
		Zombie zombie = world.spawn(location, Zombie.class);
		
		// transfer stats
		transferZombieStats(drowned, zombie);
		
		// sound
		world.playSound(location, Sound.ENTITY_ZOMBIE_CONVERTED_TO_DROWNED, 1.0f, 1.0f);
		
		// remove
		drowned.remove();
	}
	
	private void transferZombieStats(Drowned drowned, Zombie zombie)
	{
		// NAME
		zombie.setCustomName(drowned.getCustomName());
		
		// health
		zombie.setHealth(drowned.getHealth());
		
		// fire
		zombie.setFireTicks(drowned.getFireTicks());
		
		// adult
		if (drowned.isAdult())
		{
			zombie.setAdult();
		}
		else
		{
			zombie.setBaby();
		}
		
		// target
		zombie.setTarget(drowned.getTarget());
		
		// equipment
		for (EquipmentSlot slot : EquipmentSlot.values())
		{
			ItemStack item = drowned.getEquipment().getItem(slot);
			float chance = drowned.getEquipment().getDropChance(slot);
			
			zombie.getEquipment().setItem(slot, item);
			zombie.getEquipment().setDropChance(slot, chance);
		}
	}
}
