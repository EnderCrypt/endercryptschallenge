/**************************************************************************\
* EnderCrypt's Challenge                                                   *
* Copyright (C) 2023 by EnderCrypt                                         *
*                                                                          *
* This program is free software: you can redistribute it and/or modify     *
* it under the terms of the GNU Affero General Public License as           *
* published by the Free Software Foundation, either version 3 of           *
* the License, or (at your option) any later version.                      *
*                                                                          *
* This program is distributed in the hope that it will be useful,          *
* but WITHOUT ANY WARRANTY; without even the implied warranty of           *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
* GNU Affero General Public License for more details.                      *
*                                                                          *
* You should have received a copy of the GNU Affero General Public License *
* along with this program in its root directory.                           *
* If not, see <http://www.gnu.org/licenses/>.                              *
\**************************************************************************/

package endercrypt.endercryptschallenge.features.entities.mobs.bee.worm.creature;


import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import org.bukkit.EntityEffect;
import org.bukkit.Location;


/**
 * @author EnderCrypt
 */
public class Worm implements Iterable<Segment>
{
	protected final List<Segment> segments = new ArrayList<>();
	protected double health;
	
	public Worm(Location location, int segments)
	{
		if (segments <= 1)
			throw new IllegalArgumentException(Worm.class.getSimpleName() + " cannot have " + segments + "segments");
		
		for (int id = 0; id < segments; id++)
		{
			this.segments.add(new Segment(this, id, location));
		}
		
		this.health = 50 + (segments * 10);
	}
	
	public void damage(double damage)
	{
		health -= damage;
		for (Segment segment : segments)
		{
			segment.getEntity().playEffect(EntityEffect.HURT);
		}
	}
	
	public void update()
	{
		for (Segment segment : segments)
		{
			segment.update();
			
			if (segment.getEntity().isValid() == false)
			{
				health = 0;
				delete();
			}
		}
	}
	
	public boolean isDead()
	{
		return health < 0;
	}
	
	public double getGroundTouch()
	{
		return 1.0 / segments.size() * segments.stream()
			.filter(Segment::isOnGround)
			.count();
	}
	
	@Override
	public Iterator<Segment> iterator()
	{
		return Collections.unmodifiableList(segments).iterator();
	}
	
	public void delete()
	{
		for (Segment segment : segments)
		{
			segment.getEntity().remove();
		}
	}
}
