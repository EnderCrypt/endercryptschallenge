/**************************************************************************\
* EnderCrypt's Challenge                                                   *
* Copyright (C) 2023 by EnderCrypt                                         *
*                                                                          *
* This program is free software: you can redistribute it and/or modify     *
* it under the terms of the GNU Affero General Public License as           *
* published by the Free Software Foundation, either version 3 of           *
* the License, or (at your option) any later version.                      *
*                                                                          *
* This program is distributed in the hope that it will be useful,          *
* but WITHOUT ANY WARRANTY; without even the implied warranty of           *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
* GNU Affero General Public License for more details.                      *
*                                                                          *
* You should have received a copy of the GNU Affero General Public License *
* along with this program in its root directory.                           *
* If not, see <http://www.gnu.org/licenses/>.                              *
\**************************************************************************/

package endercrypt.endercryptschallenge.features.entities.mobs.pigzombie.curious;


import endercrypt.endercryptschallenge.utility.Utility;

import java.util.Objects;

import org.bukkit.Location;
import org.bukkit.block.Block;
import org.bukkit.entity.PigZombie;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import com.destroystokyo.paper.entity.Pathfinder;
import com.destroystokyo.paper.entity.Pathfinder.PathResult;


/**
 * @author EnderCrypt
 */
public class TravellingPigman
{
	public static final int MAX_RETRIES = 5;
	
	private final PigZombie entity;
	private final Block target;
	private int retries = MAX_RETRIES;
	
	public TravellingPigman(PigZombie zombie, Block target)
	{
		this.entity = Objects.requireNonNull(zombie, "zombie");
		this.target = Objects.requireNonNull(target, "target");
	}
	
	public PigZombie getZombie()
	{
		return entity;
	}
	
	public Block getTarget()
	{
		return target;
	}
	
	public int getRetries()
	{
		return retries;
	}
	
	public boolean isValid()
	{
		if (retries < 0)
			return false;
		
		if (entity.isValid() == false)
			return false;
		
		if (entity.getTarget() != null)
			return false;
		
		if (isArrived())
			return false;
		
		return true;
	}
	
	public boolean isArrived()
	{
		return Utility.roughDistance(entity.getLocation(), target.getLocation()) <= CuriousPigmen.AREA_ARRIVED;
	}
	
	public void update()
	{
		PathResult currentPath = entity.getPathfinder().getCurrentPath();
		if (currentPath == null || currentPath.getFinalPoint().getBlock().equals(target) == false)
		{
			path();
		}
	}
	
	public boolean path()
	{
		Location location = target.getLocation();
		Pathfinder pathFinder = entity.getPathfinder();
		PathResult path = pathFinder.findPath(location);
		if (path == null || path.getFinalPoint().getBlock().equals(target) == false)
		{
			retries--;
			return false;
		}
		
		double distance = Utility.getPathDistance(path);
		
		pathFinder.moveTo(path);
		retries = MAX_RETRIES;
		
		double seconds = distance * 0.5;
		entity.addPotionEffect(new PotionEffect(PotionEffectType.SPEED, (int) (20 * seconds), 3, true, false));
		
		return true;
	}
}
