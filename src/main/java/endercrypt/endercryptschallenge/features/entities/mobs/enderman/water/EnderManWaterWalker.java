/**************************************************************************\
* EnderCrypt's Challenge                                                   *
* Copyright (C) 2023 by EnderCrypt                                         *
*                                                                          *
* This program is free software: you can redistribute it and/or modify     *
* it under the terms of the GNU Affero General Public License as           *
* published by the Free Software Foundation, either version 3 of           *
* the License, or (at your option) any later version.                      *
*                                                                          *
* This program is distributed in the hope that it will be useful,          *
* but WITHOUT ANY WARRANTY; without even the implied warranty of           *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
* GNU Affero General Public License for more details.                      *
*                                                                          *
* You should have received a copy of the GNU Affero General Public License *
* along with this program in its root directory.                           *
* If not, see <http://www.gnu.org/licenses/>.                              *
\**************************************************************************/

package endercrypt.endercryptschallenge.features.entities.mobs.enderman.water;


import endercrypt.endercryptschallenge.core.Core;
import endercrypt.endercryptschallenge.modules.features.MinecraftFeature;
import endercrypt.endercryptschallenge.utility.difficulty.Difficulties;
import endercrypt.endercryptschallenge.utility.difficulty.DifficultyRating;

import org.bukkit.Material;
import org.bukkit.attribute.Attribute;
import org.bukkit.attribute.AttributeInstance;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Enderman;
import org.bukkit.event.EventHandler;
import org.bukkit.event.entity.EntitySpawnEvent;
import org.bukkit.inventory.EntityEquipment;
import org.bukkit.inventory.ItemStack;


/**
 * @author EnderCrypt
 */
public class EnderManWaterWalker extends MinecraftFeature
{
	public EnderManWaterWalker(Core core)
	{
		super(core);
	}
	
	@EventHandler
	public void onEnderManSpawn(EntitySpawnEvent event)
	{
		DifficultyRating difficulty = Difficulties.getFor(event);
		if (event.getEntity()instanceof Enderman enderman)
		{
			AttributeInstance maxHealth = enderman.getAttribute(Attribute.GENERIC_MAX_HEALTH);
			maxHealth.setBaseValue(difficulty.asIntValue(30, 40, 50));
			
			EntityEquipment equipment = enderman.getEquipment();
			
			ItemStack boots = new ItemStack(Material.LEATHER_BOOTS, 1);
			boots.addEnchantment(Enchantment.FROST_WALKER, 1);
			equipment.setBoots(boots, false);
			equipment.setBootsDropChance(0.01f);
		}
	}
}
