/**************************************************************************\
* EnderCrypt's Challenge                                                   *
* Copyright (C) 2023 by EnderCrypt                                         *
*                                                                          *
* This program is free software: you can redistribute it and/or modify     *
* it under the terms of the GNU Affero General Public License as           *
* published by the Free Software Foundation, either version 3 of           *
* the License, or (at your option) any later version.                      *
*                                                                          *
* This program is distributed in the hope that it will be useful,          *
* but WITHOUT ANY WARRANTY; without even the implied warranty of           *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
* GNU Affero General Public License for more details.                      *
*                                                                          *
* You should have received a copy of the GNU Affero General Public License *
* along with this program in its root directory.                           *
* If not, see <http://www.gnu.org/licenses/>.                              *
\**************************************************************************/

package endercrypt.endercryptschallenge.features.entities.mobs.wither.upgrade;


import endercrypt.endercryptschallenge.core.Core;
import endercrypt.endercryptschallenge.materials.MaterialCollections;
import endercrypt.endercryptschallenge.modules.features.MinecraftFeature;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Wither;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.util.Vector;


/**
 * @author EnderCrypt
 */
public class UpgradedWither extends MinecraftFeature
{
	private Map<Wither, Vector> witherMotions = new HashMap<>();
	
	public UpgradedWither(Core core)
	{
		super(core);
	}
	
	@Override
	public void onTick()
	{
		Iterator<Map.Entry<Wither, Vector>> iterator = witherMotions.entrySet().iterator();
		while (iterator.hasNext())
		{
			Map.Entry<Wither, Vector> entry = iterator.next();
			Wither wither = entry.getKey();
			Vector vector = entry.getValue();
			if (wither.isDead())
			{
				iterator.remove();
				continue;
			}
			if (vector.length() < 0.1)
			{
				continue;
			}
			wither.setVelocity(wither.getVelocity().add(vector));
		}
	}
	
	@Override
	public void onCycleEntity(Entity entity)
	{
		if (entity instanceof Wither wither)
		{
			processWither(wither);
		}
	}
	
	private void processWither(Wither wither)
	{
		Location location = wither.getLocation().add(0, wither.getEyeHeight() * 0.5, 0);
		WitherScan witherScan = WitherScan.perform(location, 4);
		
		// blocks
		witherScan.getBlocks().forEach(this::witherDestroyBlock);
		
		// vector
		Vector vector = witherScan.getVector();
		witherMotions.put(wither, vector);
		
		// speed
		wither.addPotionEffect(new PotionEffect(PotionEffectType.SPEED, 25, 1));
	}
	
	private void witherDestroyBlock(Block block)
	{
		core.getCoreProtectManager().logRemoval("#Wither", block);
		
		if (isAnnihilateBlock(block))
		{
			block.setType(Material.AIR);
		}
		else
		{
			block.breakNaturally();
		}
	}
	
	private boolean isAnnihilateBlock(Block block)
	{
		Material material = block.getType();
		if (MaterialCollections.oreBlocks.contains(material))
		{
			return true;
		}
		
		if (Math.random() < 0.5)
		{
			return true;
		}
		
		return false;
	}
}
