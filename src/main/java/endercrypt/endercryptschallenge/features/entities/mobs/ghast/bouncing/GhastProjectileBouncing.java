/**************************************************************************\
* EnderCrypt's Challenge                                                   *
* Copyright (C) 2023 by EnderCrypt                                         *
*                                                                          *
* This program is free software: you can redistribute it and/or modify     *
* it under the terms of the GNU Affero General Public License as           *
* published by the Free Software Foundation, either version 3 of           *
* the License, or (at your option) any later version.                      *
*                                                                          *
* This program is distributed in the hope that it will be useful,          *
* but WITHOUT ANY WARRANTY; without even the implied warranty of           *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
* GNU Affero General Public License for more details.                      *
*                                                                          *
* You should have received a copy of the GNU Affero General Public License *
* along with this program in its root directory.                           *
* If not, see <http://www.gnu.org/licenses/>.                              *
\**************************************************************************/

package endercrypt.endercryptschallenge.features.entities.mobs.ghast.bouncing;


import endercrypt.endercryptschallenge.core.Core;
import endercrypt.endercryptschallenge.materials.MaterialCollections;
import endercrypt.endercryptschallenge.modules.features.MinecraftFeature;
import endercrypt.endercryptschallenge.utility.difficulty.Difficulties;
import endercrypt.endercryptschallenge.utility.difficulty.DifficultyRating;

import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.entity.Fireball;
import org.bukkit.entity.Ghast;
import org.bukkit.event.EventHandler;
import org.bukkit.event.entity.ProjectileHitEvent;
import org.bukkit.util.Vector;


/**
 * @author EnderCrypt
 */
public class GhastProjectileBouncing extends MinecraftFeature
{
	public GhastProjectileBouncing(Core core)
	{
		super(core);
	}
	
	@EventHandler
	public void onGhastProjectile(ProjectileHitEvent event)
	{
		if (event.getEntity() instanceof Fireball fireball)
		{
			Block block = event.getHitBlock();
			BlockFace face = event.getHitBlockFace();
			processFireball(block, face, fireball);
		}
	}
	
	private void processFireball(Block block, BlockFace face, Fireball fireball)
	{
		// no block was hit
		if (block == null)
			return;
		
		boolean shotByGhast = fireball.getShooter() instanceof Ghast;
		
		// destroy impacted wall
		if (MaterialCollections.indestructable.contains(block) == false && shotByGhast)
		{
			block.breakNaturally();
		}
		
		// fail to bounce
		DifficultyRating difficulty = Difficulties.getFor(block);
		double chance = difficulty.asDoubleValue(0.35, 0.65, 0.95);
		
		if (shotByGhast == false)
		{
			chance = chance + ((1.0 - chance) / 2); // double chance of bouncing
		}
		
		if (Math.random() > chance)
			return;
		
		// create mirrored
		World world = fireball.getWorld();
		Location mirrorFireballLocation = getPreviousLocation(fireball, 5);
		Fireball mirroredFireball = world.spawn(mirrorFireballLocation, Fireball.class);
		mirroredFireball.setShooter(fireball.getShooter());
		
		// direction
		Vector direction = fireball.getDirection();
		deflectVector(face, direction);
		mirroredFireball.setDirection(direction);
		
		// vector
		Vector vector = fireball.getVelocity();
		deflectVector(face, vector);
		mirroredFireball.setVelocity(vector);
	}
	
	private Location getPreviousLocation(Fireball projectile, double multiplier)
	{
		Vector back = projectile.getDirection().multiply(multiplier);
		return projectile.getLocation().subtract(back);
	}
	
	private void deflectVector(BlockFace face, Vector vector)
	{
		if (face.getModX() != 0)
			vector.setX(-vector.getX());
		if (face.getModY() != 0)
			vector.setY(-vector.getY());
		if (face.getModZ() != 0)
			vector.setZ(-vector.getZ());
	}
}
