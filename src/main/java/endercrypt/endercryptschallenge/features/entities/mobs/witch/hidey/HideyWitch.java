/**************************************************************************\
* EnderCrypt's Challenge                                                   *
* Copyright (C) 2023 by EnderCrypt                                         *
*                                                                          *
* This program is free software: you can redistribute it and/or modify     *
* it under the terms of the GNU Affero General Public License as           *
* published by the Free Software Foundation, either version 3 of           *
* the License, or (at your option) any later version.                      *
*                                                                          *
* This program is distributed in the hope that it will be useful,          *
* but WITHOUT ANY WARRANTY; without even the implied warranty of           *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
* GNU Affero General Public License for more details.                      *
*                                                                          *
* You should have received a copy of the GNU Affero General Public License *
* along with this program in its root directory.                           *
* If not, see <http://www.gnu.org/licenses/>.                              *
\**************************************************************************/

package endercrypt.endercryptschallenge.features.entities.mobs.witch.hidey;


import endercrypt.endercryptschallenge.core.Core;
import endercrypt.endercryptschallenge.modules.features.MinecraftFeature;
import endercrypt.endercryptschallenge.utility.Utility;
import endercrypt.endercryptschallenge.utility.difficulty.Difficulties;
import endercrypt.endercryptschallenge.utility.difficulty.DifficultyRating;
import endercrypt.library.commons.misc.Ender;

import org.bukkit.Material;
import org.bukkit.Particle;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Witch;
import org.bukkit.event.EventHandler;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;


/**
 * @author EnderCrypt
 */
public class HideyWitch extends MinecraftFeature
{
	public HideyWitch(Core core)
	{
		super(core);
	}
	
	@EventHandler
	public void onWitchHurt(EntityDamageByEntityEvent event)
	{
		Entity entity = event.getEntity();
		if (entity instanceof Witch == false)
			return;
		Witch witch = (Witch) entity;
		
		DifficultyRating difficulty = Difficulties.getFor(event);
		double duration = difficulty.asDoubleValue(2.0, 5.0, 10.0) * Ender.math.randomRange(0.75, 1.0);
		
		// target
		if (witch.getLocation().add(0, -0.5, 0).getBlock().getType() != Material.AIR)
		{
			Entity attacker = Utility.getSourceAttacker(event.getDamager()).orElse(null);
			if (attacker != null)
			{
				witch.addPotionEffect(new PotionEffect(PotionEffectType.SPEED, (int) (20 * (duration + 10)), 5));
				
				if (attacker instanceof LivingEntity livingAttacker)
				{
					witch.getPathfinder().findPath(livingAttacker);
				}
				else
				{
					witch.getPathfinder().findPath(attacker.getLocation());
				}
			}
		}
		
		double invisibilityChance = difficulty.asDoubleValue(0.5, 0.75, 1.0);
		if (Math.random() > invisibilityChance)
			return;
		
		// invisibility
		witch.getWorld().spawnParticle(Particle.EXPLOSION, witch.getEyeLocation(), 30, 1, 2, 1);
		witch.addPotionEffect(new PotionEffect(PotionEffectType.INVISIBILITY, (int) (20 * duration), 1));
	}
}
