/**************************************************************************\
* EnderCrypt's Challenge                                                   *
* Copyright (C) 2023 by EnderCrypt                                         *
*                                                                          *
* This program is free software: you can redistribute it and/or modify     *
* it under the terms of the GNU Affero General Public License as           *
* published by the Free Software Foundation, either version 3 of           *
* the License, or (at your option) any later version.                      *
*                                                                          *
* This program is distributed in the hope that it will be useful,          *
* but WITHOUT ANY WARRANTY; without even the implied warranty of           *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
* GNU Affero General Public License for more details.                      *
*                                                                          *
* You should have received a copy of the GNU Affero General Public License *
* along with this program in its root directory.                           *
* If not, see <http://www.gnu.org/licenses/>.                              *
\**************************************************************************/

package endercrypt.endercryptschallenge.features.entities.mobs.drowned.speed;


import endercrypt.endercryptschallenge.core.Core;
import endercrypt.endercryptschallenge.modules.features.MinecraftFeature;
import endercrypt.endercryptschallenge.utility.difficulty.Difficulties;
import endercrypt.endercryptschallenge.utility.difficulty.DifficultyRating;
import endercrypt.endercryptschallenge.utility.tracker.ObjectTracker;

import org.bukkit.attribute.Attribute;
import org.bukkit.entity.Drowned;
import org.bukkit.entity.Entity;


/**
 * @author EnderCrypt
 */
public class DrownedSpeed extends MinecraftFeature
{
	public static boolean isValidSwimmer(Drowned drowned)
	{
		if (drowned.isValid() == false)
			return false;
		
		if (drowned.isInWater() == false)
			return false;
		
		return true;
	}
	
	private final ObjectTracker<Drowned> swimmers = new ObjectTracker<>(Drowned.class)
		.addCondition(DrownedSpeed::isValidSwimmer);
	
	public DrownedSpeed(Core core)
	{
		super(core);
		swimmers.addRemoveListener(this::onDrownedRemoved);
	}
	
	@Override
	public void onTick()
	{
		swimmers.cleanup();
	}
	
	@Override
	public void onCycleEntity(Entity entity)
	{
		if (entity instanceof Drowned drowned && swimmers.add(drowned))
		{
			setSpeed(drowned, true);
		}
	}
	
	private void onDrownedRemoved(Drowned drowned)
	{
		setSpeed(drowned, false);
	}
	
	private void setSpeed(Drowned drowned, boolean enabled)
	{
		double targetSpeed = getTargetSpeed(drowned, enabled);
		drowned.getAttribute(Attribute.GENERIC_MOVEMENT_SPEED).setBaseValue(targetSpeed);
	}
	
	public double getTargetSpeed(Drowned drowned, boolean enabled)
	{
		if (enabled == false)
		{
			return 0.25;
		}
		DifficultyRating difficulty = Difficulties.getFor(drowned);
		return difficulty.asDoubleValue(0.75, 1.25, 2.0);
	}
}
