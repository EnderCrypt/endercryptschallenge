/**************************************************************************\
* EnderCrypt's Challenge                                                   *
* Copyright (C) 2023 by EnderCrypt                                         *
*                                                                          *
* This program is free software: you can redistribute it and/or modify     *
* it under the terms of the GNU Affero General Public License as           *
* published by the Free Software Foundation, either version 3 of           *
* the License, or (at your option) any later version.                      *
*                                                                          *
* This program is distributed in the hope that it will be useful,          *
* but WITHOUT ANY WARRANTY; without even the implied warranty of           *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
* GNU Affero General Public License for more details.                      *
*                                                                          *
* You should have received a copy of the GNU Affero General Public License *
* along with this program in its root directory.                           *
* If not, see <http://www.gnu.org/licenses/>.                              *
\**************************************************************************/

package endercrypt.endercryptschallenge.features.entities.mobs.creeper.targetswitch;


import endercrypt.endercryptschallenge.core.Core;
import endercrypt.endercryptschallenge.modules.features.MinecraftFeature;
import endercrypt.endercryptschallenge.utility.Utility;
import endercrypt.endercryptschallenge.utility.tracker.ObjectTracker;
import endercrypt.endercryptschallenge.utility.tracker.entity.ConditionAliveAndActive;

import org.bukkit.Location;
import org.bukkit.entity.Creeper;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;

import com.google.common.base.Objects;


/**
 * @author EnderCrypt
 */
public class TargetSwitchCreeper extends MinecraftFeature
{
	private final ObjectTracker<Creeper> creepers = new ObjectTracker<>(Creeper.class)
		.addCondition(this::condition);
	
	public TargetSwitchCreeper(Core core)
	{
		super(core);
	}
	
	private boolean condition(Creeper creeper)
	{
		if (ConditionAliveAndActive.checkEntity(creeper) == false)
			return false;
		
		if (creeper.getTarget() == null)
			return false;
		
		return true;
	}
	
	@Override
	public void onCycleEntity(Entity entity)
	{
		creepers.feed(entity);
	}
	
	@Override
	public void onTick()
	{
		creepers.forEach(this::processCreeper);
	}
	
	private void processCreeper(Creeper creeper)
	{
		Location location = creeper.getLocation();
		Player nearbyPlayer = Utility.getClosestSurvivalPlayer(location).orElse(null);
		
		if (nearbyPlayer == null)
			return;
		
		if (Objects.equal(creeper.getTarget(), nearbyPlayer))
			return;
		
		if (location.distance(nearbyPlayer.getLocation()) > 2)
			return;
		
		creeper.setTarget(nearbyPlayer);
	}
}
