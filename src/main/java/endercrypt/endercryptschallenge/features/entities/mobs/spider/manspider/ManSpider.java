/**************************************************************************\
* EnderCrypt's Challenge                                                   *
* Copyright (C) 2023 by EnderCrypt                                         *
*                                                                          *
* This program is free software: you can redistribute it and/or modify     *
* it under the terms of the GNU Affero General Public License as           *
* published by the Free Software Foundation, either version 3 of           *
* the License, or (at your option) any later version.                      *
*                                                                          *
* This program is distributed in the hope that it will be useful,          *
* but WITHOUT ANY WARRANTY; without even the implied warranty of           *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
* GNU Affero General Public License for more details.                      *
*                                                                          *
* You should have received a copy of the GNU Affero General Public License *
* along with this program in its root directory.                           *
* If not, see <http://www.gnu.org/licenses/>.                              *
\**************************************************************************/

package endercrypt.endercryptschallenge.features.entities.mobs.spider.manspider;


import endercrypt.endercryptschallenge.core.Core;
import endercrypt.endercryptschallenge.modules.features.MinecraftFeature;
import endercrypt.endercryptschallenge.utility.Utility;
import endercrypt.endercryptschallenge.utility.difficulty.Difficulties;
import endercrypt.endercryptschallenge.utility.difficulty.DifficultyRating;
import endercrypt.library.commons.RandomEntry;
import endercrypt.library.commons.misc.Ender;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;

import org.bukkit.Difficulty;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.attribute.Attribute;
import org.bukkit.entity.AbstractArrow.PickupStatus;
import org.bukkit.entity.Arrow;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Item;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.entity.Spider;
import org.bukkit.event.EventHandler;
import org.bukkit.event.entity.EntityDeathEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.util.Vector;


/**
 * @author EnderCrypt
 */
public class ManSpider extends MinecraftFeature
{
	private static String NAME = "ManSpider";
	
	private Map<World, Spider> manSpiders = new HashMap<>();
	
	public ManSpider(Core core)
	{
		super(core);
		getDebug().enableDebugSupport();
	}
	
	public Optional<Spider> getSpiderMan(World world)
	{
		Spider manSpider = manSpiders.get(world);
		
		// clear
		if (manSpider != null && (manSpider.isDead() || manSpider.isTicking() == false))
		{
			debug("Removed irrelevant/dead SpiderMan: " + manSpider + " in " + world);
			destroyManSpiderLater(manSpider);
			manSpider = null;
		}
		
		// peaceful
		if (world.getDifficulty() == Difficulty.PEACEFUL)
		{
			return Optional.empty();
		}
		
		// create
		if (manSpider == null)
		{
			manSpider = getRandomSpider(world).orElse(null);
			
			if (manSpider != null)
			{
				createSpiderMan(manSpider);
				debug("Created new SpiderMan: " + manSpider);
			}
		}
		
		// not found
		if (manSpider == null)
		{
			core.getLogger().fine("no spiders found in " + world + "!");
		}
		
		return Optional.ofNullable(manSpider);
	}
	
	private void destroyManSpiderLater(final Spider manSpider)
	{
		new BukkitRunnable()
		{
			@Override
			public void run()
			{
				manSpider.remove();
			}
		}.runTaskLater(plugin, 50);
	}
	
	public void createSpiderMan(Spider spider)
	{
		Objects.requireNonNull(spider, "spider");
		DifficultyRating difficulty = Difficulties.getFor(spider);
		logger.fine("created: " + spider + " in " + spider.getWorld());
		
		// NAME
		spider.setCustomName(NAME);
		spider.setCustomNameVisible(true);
		
		// health
		int hp = difficulty.asIntValue(25, 40, 100);
		spider.getAttribute(Attribute.GENERIC_MAX_HEALTH).setBaseValue(hp);
		spider.setHealth(hp);
		
		// effects
		spider.addPotionEffect(new PotionEffect(PotionEffectType.JUMP_BOOST, 20 * 60 * 60 * 24, 10));
		spider.addPotionEffect(new PotionEffect(PotionEffectType.SLOW_FALLING, 20 * 60 * 60 * 24, 2));
		spider.addPotionEffect(new PotionEffect(PotionEffectType.RESISTANCE, 20 * 60 * 60 * 24, 1));
		spider.addPotionEffect(new PotionEffect(PotionEffectType.WATER_BREATHING, 20 * 60 * 60 * 24, 1));
		
		// remove invalid spiders
		World world = spider.getLocation().getWorld();
		for (Spider otherSpider : world.getEntitiesByClass(Spider.class))
		{
			if (spider != otherSpider && NAME.equals(otherSpider.getCustomName()))
			{
				otherSpider.remove();
			}
		}
		
		// save
		manSpiders.put(world, spider);
	}
	
	private static Optional<Spider> getRandomSpider(World world)
	{
		List<Spider> spiders = List.copyOf(world.getEntitiesByClass(Spider.class));
		if (spiders.isEmpty())
		{
			return Optional.empty();
		}
		final int RETRIES = 10;
		for (int i = 0; i < RETRIES; i++)
		{
			Spider spider = RandomEntry.from(spiders);
			Player closestPlayer = Utility.getClosestPlayer(spider.getLocation()).orElse(null);
			if (closestPlayer == null || spider.getLocation().distance(closestPlayer.getLocation()) > 16 * 3)
			{
				return Optional.of(spider);
			}
		}
		return Optional.empty();
	}
	
	@Override
	public void onSecond()
	{
		for (World world : server.getWorlds())
		{
			if (world.getPlayerCount() == 0)
			{
				continue;
			}
			Spider spiderman = getSpiderMan(world).orElse(null);
			if (spiderman == null)
			{
				return;
			}
			DifficultyRating difficulty = Difficulties.getFor(spiderman);
			if (Math.random() < 0.05)
			{
				spiderman.addPotionEffect(new PotionEffect(PotionEffectType.LEVITATION, 30, 1));
			}
			if (Math.random() < difficulty.asDoubleValue(0.20, 0.25, 0.30))
			{
				spiderman.addPotionEffect(new PotionEffect(PotionEffectType.SPEED, 50, 1));
			}
			if (Math.random() < difficulty.asDoubleValue(0.05, 0.1, 0.25))
			{
				spiderman.addPotionEffect(new PotionEffect(PotionEffectType.REGENERATION, 50, 0));
			}
			if (spiderman.getTarget() == null)
			{
				spiderman.setTarget(getClosestPlayer(spiderman));
			}
		}
	}
	
	private Player getClosestPlayer(Spider spider)
	{
		Player closest = null;
		double distance = 0.0;
		for (Player player : spider.getWorld().getPlayers())
		{
			if (Utility.isInSurvival(player))
			{
				double playerDistance = player.getLocation().distance(spider.getLocation());
				if (closest == null || playerDistance < distance)
				{
					closest = player;
					distance = playerDistance;
				}
			}
		}
		return closest;
	}
	
	@Override
	public void onTick()
	{
		for (World world : server.getWorlds())
		{
			if (world.getPlayerCount() == 0)
			{
				continue;
			}
			Spider manSpider = getSpiderMan(world).orElse(null);
			if (manSpider == null)
			{
				return;
			}
			LivingEntity target = manSpider.getTarget();
			if (target == null)
			{
				return;
			}
			
			DifficultyRating difficulty = Difficulties.getFor(manSpider);
			int tickHit = difficulty.asIntValue(20, 10, 5);
			if (server.getCurrentTick() % tickHit != 0)
			{
				return;
			}
			
			if (manSpider.hasLineOfSight(target) == false)
			{
				return;
			}
			
			manSpiderUpdateFloat(manSpider);
			manSpiderUpdateAttack(manSpider);
		}
	}
	
	private void manSpiderUpdateFloat(Spider manSpider)
	{
		if (manSpider.isInWater() == false)
			return;
		
		Vector motion = new Vector();
		
		LivingEntity target = manSpider.getTarget();
		if (target != null)
		{
			Vector offset = target.getLocation().subtract(manSpider.getLocation()).toVector();
			DifficultyRating difficulty = Difficulties.getFor(manSpider);
			double speed = difficulty.asDoubleValue(1.25, 2.0, 4.0);
			motion.add(offset.normalize().multiply(speed));
		}
		
		manSpider.setVelocity(manSpider.getVelocity().add(motion));
	}
	
	private void manSpiderUpdateAttack(Spider manSpider)
	{
		DifficultyRating difficulty = Difficulties.getFor(manSpider);
		final double projectileSpeed = difficulty.asDoubleValue(2.0, 3.0, 4.0);
		LivingEntity target = manSpider.getTarget();
		Location offsetLocation = target.getEyeLocation().clone().subtract(manSpider.getEyeLocation());
		Vector velocity = new Vector(offsetLocation.getX(), offsetLocation.getY(), offsetLocation.getZ());
		velocity.normalize();
		velocity.multiply(projectileSpeed);
		manSpider.launchProjectile(Arrow.class, velocity, this::configureNewManSpiderArrow);
	}
	
	private void configureNewManSpiderArrow(Arrow arrow)
	{
		arrow.setPickupStatus(PickupStatus.CREATIVE_ONLY);
	}
	
	@EventHandler
	public void onManSpiderDeath(EntityDeathEvent event)
	{
		Entity entity = event.getEntity();
		World world = entity.getWorld();
		Spider spiderman = manSpiders.get(world);
		if (spiderman == null)
		{
			return;
		}
		if (entity != spiderman)
		{
			return;
		}
		Location location = spiderman.getLocation();
		DifficultyRating difficulty = Difficulties.getFor(event);
		
		int eggs = (int) Math.round(difficulty.asDoubleValue(8, 12, 16) * Ender.math.randomRange(0.75, 1.0));
		for (int i = 0; i < eggs; i++)
		{
			Item item = world.dropItemNaturally(location, new ItemStack(Material.SPIDER_SPAWN_EGG, 1));
			double spread = 0.2;
			double up = 1.0;
			double x = Ender.math.randomRange(-spread, spread);
			double y = Ender.math.randomRange(up, up * 2);
			double z = Ender.math.randomRange(-spread, spread);
			Vector motion = new Vector(x, y, z);
			item.setVelocity(motion);
		}
	}
}
