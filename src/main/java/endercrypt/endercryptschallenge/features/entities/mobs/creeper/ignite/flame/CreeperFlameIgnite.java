/**************************************************************************\
* EnderCrypt's Challenge                                                   *
* Copyright (C) 2023 by EnderCrypt                                         *
*                                                                          *
* This program is free software: you can redistribute it and/or modify     *
* it under the terms of the GNU Affero General Public License as           *
* published by the Free Software Foundation, either version 3 of           *
* the License, or (at your option) any later version.                      *
*                                                                          *
* This program is distributed in the hope that it will be useful,          *
* but WITHOUT ANY WARRANTY; without even the implied warranty of           *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
* GNU Affero General Public License for more details.                      *
*                                                                          *
* You should have received a copy of the GNU Affero General Public License *
* along with this program in its root directory.                           *
* If not, see <http://www.gnu.org/licenses/>.                              *
\**************************************************************************/

package endercrypt.endercryptschallenge.features.entities.mobs.creeper.ignite.flame;


import endercrypt.endercryptschallenge.core.Core;
import endercrypt.endercryptschallenge.features.entities.mobs.creeper.cunning.angry.CunningCreeperAngry;
import endercrypt.endercryptschallenge.modules.features.MinecraftFeature;

import org.bukkit.entity.Creeper;
import org.bukkit.event.EventHandler;
import org.bukkit.event.entity.EntityDeathEvent;


/**
 * @author EnderCrypt
 */
public class CreeperFlameIgnite extends MinecraftFeature
{
	public CreeperFlameIgnite(Core core)
	{
		super(core);
	}
	
	@EventHandler
	public void onCreeperDeath(EntityDeathEvent event)
	{
		if (event.getEntity() instanceof Creeper creeper)
		{
			processCreeper(creeper);
		}
	}
	
	public void processCreeper(Creeper creeper)
	{
		if (creeper.getFireTicks() <= 0)
			return;
		
		if (getFeature(CunningCreeperAngry.class).getTemporaryCreepers().contains(creeper))
		{
			return;
		}
		
		creeper.explode();
	}
}
