/**************************************************************************\
* EnderCrypt's Challenge                                                   *
* Copyright (C) 2023 by EnderCrypt                                         *
*                                                                          *
* This program is free software: you can redistribute it and/or modify     *
* it under the terms of the GNU Affero General Public License as           *
* published by the Free Software Foundation, either version 3 of           *
* the License, or (at your option) any later version.                      *
*                                                                          *
* This program is distributed in the hope that it will be useful,          *
* but WITHOUT ANY WARRANTY; without even the implied warranty of           *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
* GNU Affero General Public License for more details.                      *
*                                                                          *
* You should have received a copy of the GNU Affero General Public License *
* along with this program in its root directory.                           *
* If not, see <http://www.gnu.org/licenses/>.                              *
\**************************************************************************/

package endercrypt.endercryptschallenge.features.entities.mobs.creeper.ignite.stuck;


import endercrypt.endercryptschallenge.core.Core;
import endercrypt.endercryptschallenge.features.entities.mobs.creeper.cunning.angry.CunningCreeperAngry;
import endercrypt.endercryptschallenge.modules.features.MinecraftFeature;
import endercrypt.endercryptschallenge.utility.Utility;

import org.bukkit.entity.Creeper;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Spider;
import org.bukkit.event.EventHandler;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;


/**
 * @author EnderCrypt
 */
public class CreeperStuckIgnite extends MinecraftFeature
{
	public CreeperStuckIgnite(Core core)
	{
		super(core);
	}
	
	@EventHandler
	public void onCreeperHurt(EntityDamageEvent event)
	{
		if (event.isCancelled())
			return;
		
		Entity entity = event.getEntity();
		if (entity instanceof Creeper == false)
			return;
		Creeper creeper = (Creeper) entity;
		
		if (creeper.getTicksLived() < 20)
			return;
		
		Entity vehicle = creeper.getVehicle(); // spider jockey
		if (vehicle != null && vehicle instanceof Spider)
		{
			event.setCancelled(true);
			return;
		}
		
		DamageCause cause = event.getCause();
		boolean causedByExplosion = (cause == DamageCause.ENTITY_EXPLOSION) || (cause == DamageCause.BLOCK_EXPLOSION);
		if (Math.random() > 0.25 || causedByExplosion)
			return;
		
		if (Utility.entityStuckBlock(creeper).isEmpty())
			return;
		
		if (getFeature(CunningCreeperAngry.class).getTemporaryCreepers().contains(creeper))
			return;
		
		creeper.ignite();
	}
}
