/**************************************************************************\
* EnderCrypt's Challenge                                                   *
* Copyright (C) 2023 by EnderCrypt                                         *
*                                                                          *
* This program is free software: you can redistribute it and/or modify     *
* it under the terms of the GNU Affero General Public License as           *
* published by the Free Software Foundation, either version 3 of           *
* the License, or (at your option) any later version.                      *
*                                                                          *
* This program is distributed in the hope that it will be useful,          *
* but WITHOUT ANY WARRANTY; without even the implied warranty of           *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
* GNU Affero General Public License for more details.                      *
*                                                                          *
* You should have received a copy of the GNU Affero General Public License *
* along with this program in its root directory.                           *
* If not, see <http://www.gnu.org/licenses/>.                              *
\**************************************************************************/

package endercrypt.endercryptschallenge.features.entities.mobs.creeper.boomer;


import endercrypt.endercryptschallenge.core.Core;
import endercrypt.endercryptschallenge.features.entities.mobs.zombie.hordes.Horde;
import endercrypt.endercryptschallenge.features.entities.mobs.zombie.hordes.HordeManager;
import endercrypt.endercryptschallenge.modules.features.MinecraftFeature;
import endercrypt.endercryptschallenge.modules.worlds.spawner.factory.SpawnAutomator;
import endercrypt.endercryptschallenge.modules.worlds.spawner.factory.SpawnCondition;
import endercrypt.endercryptschallenge.utility.difficulty.Difficulties;
import endercrypt.endercryptschallenge.utility.difficulty.DifficultyRating;
import endercrypt.library.commons.misc.Ender;

import java.util.Collection;

import org.bukkit.World;
import org.bukkit.entity.Creeper;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.entity.Zombie;
import org.bukkit.event.EventHandler;
import org.bukkit.event.entity.EntityDamageByEntityEvent;

import net.md_5.bungee.api.ChatColor;


/**
 * @author EnderCrypt
 */
public class CreeperBoomers extends MinecraftFeature
{
	private final SpawnCondition spawnCondition = new SpawnCondition.Builder()
		.setAnyLightLevel()
		.setHeight(2)
		.build();
	
	private final SpawnAutomator spawnAutomator = new SpawnAutomator.Builder()
		.setCondition(spawnCondition)
		.setRange(20, 25)
		.setChance(1.0)
		.build();
	
	public CreeperBoomers(Core core)
	{
		super(core);
	}
	
	public String getZombieGroupName(int quantity)
	{
		if (quantity == 1)
			return "one nearby zombie";
		if (quantity < 10)
			return "a couple of nearby zombies";
		if (quantity < 25)
			return "a small group of zombies";
		if (quantity < 50)
			return "a group of zombies";
		if (quantity < 75)
			return "a big group of zombies";
		if (quantity < 100)
			return "a really big group of zombies";
		if (quantity < 125)
			return "a massive group of zombies";
		if (quantity < 150)
			return "a humongous group of zombies";
		if (quantity < 200)
			return "an absolute $@^#ton of zombies";
		
		return "an unimaginable amount of zombies, " + ChatColor.RED + "RUN!!!";
	}
	
	@EventHandler
	public void onEntityDamageByEntity(EntityDamageByEntityEvent event)
	{
		Entity target = event.getEntity();
		Entity source = event.getDamager();
		
		World world = target.getWorld();
		if (event.getDamage() >= 0.5 && target instanceof Player player && source instanceof Creeper)
		{
			DifficultyRating difficulty = Difficulties.getFor(player);
			int creeperBoomerRange = difficulty.asIntValue(40, 60, 100);
			int creeperBoomerHordeRange = difficulty.asIntValue(50, 75, 120);
			
			// create hordes
			int hordeSpawnAttempts = (int) Math.round(Ender.math.randomRange(0.5, 1.0) * difficulty.asIntValue(3, 6, 9));
			HordeManager hordes = core.getMinecraftFeatureManager().getFeature(HordeManager.class);
			int hordesCreated = 0;
			for (int i = 0; i < hordeSpawnAttempts; i++)
			{
				Horde horde = hordes.spawnHorde(player, spawnAutomator).orElse(null);
				if (horde != null)
				{
					hordesCreated++;
					horde.setTarget(player);
				}
			}
			if (hordesCreated > 0)
			{
				logger.fine("Created " + hordesCreated + " to target " + player);
			}
			
			// target all hordes
			core.getMinecraftFeatureManager().getFeature(HordeManager.class).targetNearbyHordes(player, creeperBoomerHordeRange);
			
			// target all nearby induvidual zombies
			Collection<Zombie> zombies = world.getNearbyEntitiesByType(Zombie.class, player.getLocation(), creeperBoomerRange);
			if (zombies.isEmpty() == false)
			{
				zombies.forEach(z -> z.setTarget(player));
				
				world.getNearbyPlayers(player.getLocation(), creeperBoomerRange)
					.forEach(p -> p.sendMessage(player.getName() + " attracted " + getZombieGroupName(zombies.size())));
			}
		}
	}
}
