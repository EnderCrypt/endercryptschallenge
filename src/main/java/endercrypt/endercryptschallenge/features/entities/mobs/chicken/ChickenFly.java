/**************************************************************************\
* EnderCrypt's Challenge                                                   *
* Copyright (C) 2023 by EnderCrypt                                         *
*                                                                          *
* This program is free software: you can redistribute it and/or modify     *
* it under the terms of the GNU Affero General Public License as           *
* published by the Free Software Foundation, either version 3 of           *
* the License, or (at your option) any later version.                      *
*                                                                          *
* This program is distributed in the hope that it will be useful,          *
* but WITHOUT ANY WARRANTY; without even the implied warranty of           *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
* GNU Affero General Public License for more details.                      *
*                                                                          *
* You should have received a copy of the GNU Affero General Public License *
* along with this program in its root directory.                           *
* If not, see <http://www.gnu.org/licenses/>.                              *
\**************************************************************************/

package endercrypt.endercryptschallenge.features.entities.mobs.chicken;


import endercrypt.endercryptschallenge.core.Core;
import endercrypt.endercryptschallenge.modules.features.MinecraftFeature;
import endercrypt.endercryptschallenge.utility.Utility;
import endercrypt.endercryptschallenge.utility.tracker.Condition;
import endercrypt.endercryptschallenge.utility.tracker.ObjectTracker;
import endercrypt.endercryptschallenge.utility.tracker.entity.ConditionAliveAndActive;
import endercrypt.endercryptschallenge.utility.tracker.entity.EntityTracker;
import endercrypt.library.commons.structs.range.DoubleRange;
import endercrypt.library.commons.structs.range.Range;

import org.bukkit.entity.Chicken;
import org.bukkit.entity.Entity;
import org.bukkit.event.EventHandler;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.util.Vector;


/**
 * Idea by Father_Lorenzo
 * 
 * @author EnderCrypt
 */
public class ChickenFly extends MinecraftFeature
{
	private static final int CHANCE = 60; // roughly once every 60 seconds
	private static final DoubleRange FLY_TIME = Range.of(2.5, 3.5);
	private static final double FLY_MAX_SPEED = 3;
	private static final double FLY_RANDOMNESS = 0.1;
	private static final double FLY_ACCELERATION = 0.07;
	
	private final ObjectTracker<Chicken> flyingChickens = new EntityTracker<>(Chicken.class)
		.addCondition(new EntityCondition());
	
	public ChickenFly(Core core)
	{
		super(core);
	}
	
	@Override
	public void onCycleEntity(Entity entity)
	{
		if (entity instanceof Chicken chicken)
		{
			if (Math.random() < 1.0 / CHANCE)
			{
				makeFlyingChicken(chicken);
			}
			flyingChickens.add(chicken);
		}
	}
	
	@EventHandler
	public void onHurt(EntityDamageEvent event)
	{
		if (event.getEntity() instanceof Chicken chicken)
		{
			makeFlyingChicken(chicken);
		}
	}
	
	private void makeFlyingChicken(Chicken chicken)
	{
		double seconds = FLY_TIME.getRandom();
		int duration = (int) (seconds * 20);
		int amplifier = 1;
		chicken.addPotionEffect(new PotionEffect(PotionEffectType.LEVITATION, duration, amplifier));
		flyingChickens.add(chicken);
	}
	
	@Override
	public void onTick()
	{
		flyingChickens.forEach(this::process);
	}
	
	private void process(Chicken chicken)
	{
		Vector velocity = chicken.getVelocity();
		
		// add random motion
		Vector randomVelocity = Utility.randomFlatVector(FLY_RANDOMNESS);
		velocity.add(randomVelocity);
		
		// increase speed
		velocity.multiply(1.0 + FLY_ACCELERATION);
		
		// keep below max speed
		if (velocity.length() > FLY_MAX_SPEED)
		{
			double overspeed = 1.0 / FLY_MAX_SPEED * velocity.length();
			velocity.divide(new Vector(overspeed, overspeed, overspeed));
		}
		
		chicken.setVelocity(velocity);
	}
	
	private static class EntityCondition implements Condition<Chicken>
	{
		@Override
		public boolean check(Chicken chicken)
		{
			if (ConditionAliveAndActive.checkEntity(chicken) == false)
				return false;
			
			if (chicken.hasPotionEffect(PotionEffectType.LEVITATION) == false)
				return false;
			
			return true;
		}
	}
}
