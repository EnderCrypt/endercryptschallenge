/**************************************************************************\
* EnderCrypt's Challenge                                                   *
* Copyright (C) 2023 by EnderCrypt                                         *
*                                                                          *
* This program is free software: you can redistribute it and/or modify     *
* it under the terms of the GNU Affero General Public License as           *
* published by the Free Software Foundation, either version 3 of           *
* the License, or (at your option) any later version.                      *
*                                                                          *
* This program is distributed in the hope that it will be useful,          *
* but WITHOUT ANY WARRANTY; without even the implied warranty of           *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
* GNU Affero General Public License for more details.                      *
*                                                                          *
* You should have received a copy of the GNU Affero General Public License *
* along with this program in its root directory.                           *
* If not, see <http://www.gnu.org/licenses/>.                              *
\**************************************************************************/

package endercrypt.endercryptschallenge.features.entities.mobs.skeleton.arrowceiling;


import endercrypt.endercryptschallenge.core.Core;
import endercrypt.endercryptschallenge.modules.features.MinecraftFeature;
import endercrypt.library.commons.misc.Ender;

import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.block.BlockFace;
import org.bukkit.entity.Arrow;
import org.bukkit.entity.Entity;
import org.bukkit.event.EventHandler;
import org.bukkit.event.entity.ProjectileHitEvent;
import org.bukkit.plugin.Plugin;
import org.bukkit.scheduler.BukkitRunnable;


/**
 * @author EnderCrypt
 */
public class ArrowCeiling extends MinecraftFeature
{
	public ArrowCeiling(Core core)
	{
		super(core);
	}
	
	@EventHandler
	public void onProjectileCollide(ProjectileHitEvent event)
	{
		Entity entity = event.getEntity();
		BlockFace blockFace = event.getHitBlockFace();
		if (entity instanceof Arrow arrow && blockFace == BlockFace.DOWN)
		{
			int delay = Ender.math.randomRange(20, 50);
			ArrowDropperTask.perform(plugin, arrow, delay);
		}
	}
	
	private static class ArrowDropperTask extends BukkitRunnable
	{
		public static ArrowDropperTask perform(Plugin plugin, Arrow arrow, int delay)
		{
			ArrowDropperTask task = new ArrowDropperTask(arrow);
			task.runTaskLater(plugin, delay);
			return task;
		}
		
		private final Arrow arrow;
		
		public ArrowDropperTask(Arrow arrow)
		{
			this.arrow = arrow;
		}
		
		@Override
		public void run()
		{
			Location location = arrow.getLocation();
			location.setY(location.getY() - 0.05);
			World world = location.getWorld();
			
			Arrow newArrow = world.spawn(location, Arrow.class);
			newArrow.setPickupStatus(arrow.getPickupStatus());
			
			arrow.remove();
		}
	}
}
