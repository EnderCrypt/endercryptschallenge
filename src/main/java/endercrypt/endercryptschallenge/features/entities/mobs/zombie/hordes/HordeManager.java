/**************************************************************************\
* EnderCrypt's Challenge                                                   *
* Copyright (C) 2023 by EnderCrypt                                         *
*                                                                          *
* This program is free software: you can redistribute it and/or modify     *
* it under the terms of the GNU Affero General Public License as           *
* published by the Free Software Foundation, either version 3 of           *
* the License, or (at your option) any later version.                      *
*                                                                          *
* This program is distributed in the hope that it will be useful,          *
* but WITHOUT ANY WARRANTY; without even the implied warranty of           *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
* GNU Affero General Public License for more details.                      *
*                                                                          *
* You should have received a copy of the GNU Affero General Public License *
* along with this program in its root directory.                           *
* If not, see <http://www.gnu.org/licenses/>.                              *
\**************************************************************************/

package endercrypt.endercryptschallenge.features.entities.mobs.zombie.hordes;


import endercrypt.endercryptschallenge.core.Core;
import endercrypt.endercryptschallenge.modules.features.MinecraftFeature;
import endercrypt.endercryptschallenge.modules.worlds.spawner.factory.SpawnAutomator;
import endercrypt.endercryptschallenge.utility.Utility;
import endercrypt.endercryptschallenge.utility.difficulty.Difficulties;
import endercrypt.endercryptschallenge.utility.difficulty.DifficultyRating;
import endercrypt.library.commons.misc.Ender;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;
import java.util.function.Function;

import org.bukkit.Difficulty;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.Particle;
import org.bukkit.World;
import org.bukkit.entity.Creeper;
import org.bukkit.entity.Monster;
import org.bukkit.entity.Piglin;
import org.bukkit.entity.Player;
import org.bukkit.entity.Projectile;
import org.bukkit.entity.Skeleton;
import org.bukkit.entity.Snowball;
import org.bukkit.entity.WitherSkeleton;
import org.bukkit.entity.Zombie;
import org.bukkit.event.EventHandler;
import org.bukkit.event.entity.ProjectileHitEvent;
import org.bukkit.projectiles.ProjectileSource;


/**
 * @author EnderCrypt
 */
public class HordeManager extends MinecraftFeature
{
	private List<Horde> hordes = new ArrayList<>();
	
	public HordeManager(Core core)
	{
		super(core);
		getDebug().enableDebugSupport();
	}
	
	@EventHandler
	public void onProjectileCollide(ProjectileHitEvent event)
	{
		Projectile projectile = event.getEntity();
		Location location = projectile.getLocation();
		if (projectile instanceof Snowball)
		{
			ProjectileSource source = projectile.getShooter();
			if (source instanceof Player player && player.getGameMode().equals(GameMode.CREATIVE))
			{
				spawnHorde(location, 5);
			}
		}
	}
	
	@Override
	public void onSecond()
	{
		// update
		Iterator<Horde> iterator = hordes.iterator();
		while (iterator.hasNext())
		{
			Horde horde = iterator.next();
			if (horde.isDead())
			{
				logger.fine("despawned dead horde (record size: " + horde.getRecordSize() + ")");
				iterator.remove();
				continue;
			}
			horde.update();
		}
	}
	
	private static List<Class<? extends Monster>> getJoinableSpecies(World world)
	{
		DifficultyRating difficulty = Difficulties.getFor(world);
		
		List<Class<? extends Monster>> joinable = new ArrayList<>();
		
		// always add zombie
		joinable.add(Zombie.class);
		
		// normal and up
		if (difficulty.isHarderThan(Difficulty.EASY))
		{
			joinable.add(WitherSkeleton.class);
			joinable.add(Piglin.class);
		}
		
		// hard
		if (difficulty.isHarderThan(Difficulty.NORMAL))
		{
			joinable.add(Skeleton.class);
			joinable.add(Creeper.class);
		}
		
		return joinable;
	}
	
	@Override
	public void onTick()
	{
		// System.out.println(Horde.t1 + " " + Horde.t2 + " " + Horde.t3 + " hordes: " + hordes.size());
		
		if (isDebug())
		{
			for (Horde horde : hordes)
			{
				World world = horde.getLocator().getWorld();
				world.spawnParticle(Particle.TOTEM_OF_UNDYING, horde.getLocator().getLocation(), 10, 1, 1, 1, 1);
				world.spawnParticle(Particle.HEART, horde.getLocator().target(), (int) (1 + Math.round(horde.getSize() / 5.0)), 1, 1, 1, 1);
			}
		}
	}
	
	public Optional<Horde> spawnHorde(Player player, SpawnAutomator spawnAutomator)
	{
		Location location = spawnAutomator.findSpawnSpot(player).orElse(null);
		if (location == null)
		{
			return Optional.empty();
		}
		
		int monsterCount = location.getWorld().getNearbyEntitiesByType(Monster.class, location, 10).size();
		if (monsterCount > 20)
		{
			return Optional.empty();
		}
		
		return Optional.of(spawnHorde(player, location));
	}
	
	public Horde spawnHorde(Player player, Location location)
	{
		DifficultyRating difficulty = Difficulties.getFor(player);
		int count = (int) Math.ceil(Ender.math.randomRange(0.75, 1.25) * difficulty.asIntValue(3, 5, 7));
		Horde horde = spawnHorde(location, count);
		horde.configuration().setJoinable(true);
		horde.configuration().setJoinableSpecies(getJoinableSpecies(location.getWorld()));
		return horde;
	}
	
	public Horde spawnHorde(Location location, int amount)
	{
		return spawnHorde(location, amount, (loc) -> loc.getWorld().spawn(loc, Zombie.class));
	}
	
	public Horde spawnHorde(Location location, int amount, Function<Location, Monster> spawner)
	{
		// horde
		Horde horde = new Horde(this, location);
		HordeConfiguration configuration = horde.configuration();
		configuration.setJoinable(true);
		configuration.setPriority(Ender.math.randomRange(100, 200));
		
		for (int i = 0; i < amount; i++)
		{
			horde.monsters.add(spawner.apply(location.clone()));
		}
		
		hordes.add(horde);
		
		String amountString = amount
			+ (horde.configuration().isJoinable() ? " +Joinable" : "")
			+ (horde.configuration().isMergeable() ? " +Mergable" : "");
		logger.fine("Spawned horde (" + amountString + ") at: " + location.toBlockLocation());
		
		return horde;
	}
	
	public Optional<Horde> getHorde(Monster monster)
	{
		synchronized (hordes)
		{
			for (Horde horde : hordes)
			{
				if (horde.monsters.contains(monster))
				{
					return Optional.of(horde);
				}
			}
		}
		return Optional.empty();
	}
	
	public List<Horde> getHordes()
	{
		return Collections.unmodifiableList(hordes);
	}
	
	public void targetNearbyHordes(Player player, double radius)
	{
		for (Horde horde : getHordes())
		{
			Location hordeLocation = horde.getLocator().getLocation();
			if (hordeLocation.getWorld().equals(player.getWorld()))
			{
				double distance = hordeLocation.distance(player.getLocation());
				if (distance <= radius)
				{
					Utility.setLocation(horde.getLocator().target(), player.getLocation());
				}
			}
		}
	}
}
