/**************************************************************************\
* EnderCrypt's Challenge                                                   *
* Copyright (C) 2023 by EnderCrypt                                         *
*                                                                          *
* This program is free software: you can redistribute it and/or modify     *
* it under the terms of the GNU Affero General Public License as           *
* published by the Free Software Foundation, either version 3 of           *
* the License, or (at your option) any later version.                      *
*                                                                          *
* This program is distributed in the hope that it will be useful,          *
* but WITHOUT ANY WARRANTY; without even the implied warranty of           *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
* GNU Affero General Public License for more details.                      *
*                                                                          *
* You should have received a copy of the GNU Affero General Public License *
* along with this program in its root directory.                           *
* If not, see <http://www.gnu.org/licenses/>.                              *
\**************************************************************************/

package endercrypt.endercryptschallenge.features.entities.mobs.irongolem.decayed;


import endercrypt.endercryptschallenge.core.Core;
import endercrypt.endercryptschallenge.modules.features.MinecraftFeature;
import endercrypt.endercryptschallenge.utility.difficulty.Difficulties;
import endercrypt.endercryptschallenge.utility.difficulty.DifficultyRating;
import endercrypt.library.commons.misc.Ender;

import java.util.List;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.entity.Entity;
import org.bukkit.entity.IronGolem;
import org.bukkit.entity.Silverfish;
import org.bukkit.entity.Villager;
import org.bukkit.event.EventHandler;
import org.bukkit.event.entity.EntityDeathEvent;
import org.bukkit.inventory.ItemStack;


/**
 * @author EnderCrypt
 */
public class DecayedIronGolems extends MinecraftFeature
{
	public DecayedIronGolems(Core core)
	{
		super(core);
	}
	
	@Override
	public void onCycleEntity(Entity entity)
	{
		if (entity instanceof IronGolem ironGolem && isDecaying(ironGolem))
		{
			ironGolem.damage(0.5);
		}
	}
	
	@EventHandler
	public void onIronGolemDeath(EntityDeathEvent event)
	{
		Entity entity = event.getEntity();
		if (entity instanceof IronGolem ironGolem)
		{
			// drops
			List<ItemStack> drops = event.getDrops();
			drops.clear();
			if (isDecaying(ironGolem) == false)
			{
				drops.add(new ItemStack(Material.IRON_NUGGET, Ender.math.randomRange(5, 10)));
			}
			
			// silver fish
			Location location = ironGolem.getEyeLocation();
			World world = location.getWorld();
			DifficultyRating difficulty = Difficulties.getFor(location);
			int silverFishCount = (int) (difficulty.asDoubleValue(3, 5, 10) * Ender.math.randomRange(0.75, 1.5));
			for (int i = 0; i < silverFishCount; i++)
			{
				world.spawn(location, Silverfish.class);
			}
		}
	}
	
	private boolean isDecaying(IronGolem ironGolem)
	{
		return ironGolem.getLocation().getNearbyEntitiesByType(Villager.class, 100).isEmpty();
	}
}
