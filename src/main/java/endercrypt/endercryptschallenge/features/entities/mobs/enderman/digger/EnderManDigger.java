/**************************************************************************\
* EnderCrypt's Challenge                                                   *
* Copyright (C) 2023 by EnderCrypt                                         *
*                                                                          *
* This program is free software: you can redistribute it and/or modify     *
* it under the terms of the GNU Affero General Public License as           *
* published by the Free Software Foundation, either version 3 of           *
* the License, or (at your option) any later version.                      *
*                                                                          *
* This program is distributed in the hope that it will be useful,          *
* but WITHOUT ANY WARRANTY; without even the implied warranty of           *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
* GNU Affero General Public License for more details.                      *
*                                                                          *
* You should have received a copy of the GNU Affero General Public License *
* along with this program in its root directory.                           *
* If not, see <http://www.gnu.org/licenses/>.                              *
\**************************************************************************/

package endercrypt.endercryptschallenge.features.entities.mobs.enderman.digger;


import endercrypt.endercryptschallenge.core.Core;
import endercrypt.endercryptschallenge.materials.MaterialCollections;
import endercrypt.endercryptschallenge.modules.features.MinecraftFeature;
import endercrypt.endercryptschallenge.utility.Utility;
import endercrypt.endercryptschallenge.utility.difficulty.Difficulties;
import endercrypt.endercryptschallenge.utility.difficulty.DifficultyRating;
import endercrypt.endercryptschallenge.utility.tracker.ObjectTracker;
import endercrypt.endercryptschallenge.utility.tracker.entity.EntityTracker;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.block.data.BlockData;
import org.bukkit.entity.Enderman;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.entity.Vehicle;
import org.bukkit.inventory.ItemStack;
import org.bukkit.util.Vector;


/**
 * @author EnderCrypt
 */
public class EnderManDigger extends MinecraftFeature
{
	private static final double ENDERMAN_BOAT_REACH = 2.5;
	
	private ObjectTracker<Enderman> endermen = new EntityTracker<>(Enderman.class)
		.addCondition((e) -> e.getTarget() instanceof Player);
	
	public EnderManDigger(Core core)
	{
		super(core);
	}
	
	@Override
	public void onCycleEntity(Entity entity)
	{
		endermen.feed(entity);
	}
	
	@Override
	public void onTick()
	{
		endermen.forEach(this::updateEnderMan);
	}
	
	private void updateEnderMan(Enderman enderman)
	{
		jumpyEnderMan(enderman);
		attackAndMassacreBlocks(enderman);
		attackAndPunchPlayerBoats(enderman);
	}
	
	private void jumpyEnderMan(Enderman enderman)
	{
		if (enderman.getTarget() == null)
			return;
		
		if (enderman.isOnGround() == false)
			return;
		
		LivingEntity target = enderman.getTarget();
		if (Math.random() < Difficulties.getFor(enderman).asDoubleValue(0.2, 0.4, 0.6))
		{
			Vector acceleration = target.getLocation().subtract(enderman.getLocation())
				.toVector()
				.normalize()
				.multiply(0.3);
			Vector velocity = enderman.getVelocity();
			velocity.add(acceleration);
			enderman.setVelocity(velocity);
		}
	}
	
	private Optional<Block> findObstructingBlock(Enderman enderman, Location target)
	{
		Block bestBlock = null;
		double bestDistance = 0;
		for (Block block : getSurroundingBlocks(enderman))
		{
			Location blockLocation = block.getLocation().add(0.5, 0.5, 0.5);
			Vector flatOffset = blockLocation.subtract(target).toVector().setY(0);
			double distance = flatOffset.length();
			if (bestBlock == null || distance < bestDistance)
			{
				bestBlock = block;
				bestDistance = distance;
			}
		}
		return Optional.ofNullable(bestBlock);
	}
	
	private List<Block> getSurroundingBlocks(Enderman enderman)
	{
		Location endermanLocation = enderman.getLocation().toBlockLocation();
		List<Block> result = new ArrayList<>();
		for (int i = 0; i < 3; i++)
		{
			int height = 2 - i;
			Location location = endermanLocation.clone().add(0, height, 0);
			for (BlockFace face : Utility.cardinalBlockFaces)
			{
				Block block = location.getBlock().getRelative(face);
				
				if (block.isPassable())
					continue;
				if (MaterialCollections.indestructable.contains(block.getType()))
					continue;
				
				result.add(block);
			}
		}
		
		return result;
	}
	
	private boolean autoDropItems(Enderman enderman)
	{
		BlockData carried = enderman.getCarriedBlock();
		if (carried == null)
		{
			return false;
		}
		Material carriedMaterial = carried.getMaterial();
		if (carriedMaterial == Material.AIR)
		{
			return false;
		}
		World world = enderman.getLocation().getWorld();
		Location eyeLocation = enderman.getEyeLocation();
		ItemStack item = new ItemStack(carriedMaterial, 1);
		world.dropItemNaturally(eyeLocation, item);
		enderman.setCarriedBlock(null);
		return true;
	}
	
	private boolean attackAndMassacreBlocks(Enderman enderman)
	{
		DifficultyRating difficulty = Difficulties.getFor(enderman);
		if (Math.random() > difficulty.asDoubleValue(0.5, 0.75, 1.0))
			return false;
		
		LivingEntity target = enderman.getTarget();
		
		if (target.getWorld().equals(enderman.getWorld()) == false)
		{
			enderman.setTarget(null);
			return false;
		}
		
		Block block = findObstructingBlock(enderman, target.getEyeLocation()).orElse(null);
		if (block == null)
			return false;
		
		if (block.getType() == Material.AIR)
			throw new IllegalArgumentException("cant grab " + block);
		
		double distance = enderman.getEyeLocation().distance(block.getLocation());
		if (distance > 5)
			return false;
		
		if (autoDropItems(enderman))
			return false;
		
		core.getCoreProtectManager().logRemoval("#EnderMan", block);
		
		enderman.setCarriedBlock(block.getBlockData());
		Utility.destroyBlock(block);
		return true;
	}
	
	private void attackAndPunchPlayerBoats(Enderman enderman)
	{
		DifficultyRating difficulty = Difficulties.getFor(enderman);
		if (Math.random() > difficulty.asDoubleValue(0.05, 0.1, 0.5))
		{
			return;
		}
		LivingEntity target = enderman.getTarget();
		if (target == null)
		{
			return;
		}
		if (target.getVehicle() instanceof Vehicle == false)
		{
			return;
		}
		Vehicle vehicle = (Vehicle) target.getVehicle();
		if (enderman.getLocation().distance(vehicle.getLocation()) > ENDERMAN_BOAT_REACH)
		{
			return;
		}
		endermanDestroyVehicle(vehicle);
	}
	
	private void endermanDestroyVehicle(Vehicle vehicle)
	{
		for (Entity passenger : vehicle.getPassengers())
		{
			passenger.sendMessage("The enderman poped your boat");
		}
		Utility.destroyVehicle(vehicle);
	}
}
