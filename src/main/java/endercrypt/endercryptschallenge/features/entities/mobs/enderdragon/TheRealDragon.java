/**************************************************************************\
* EnderCrypt's Challenge                                                   *
* Copyright (C) 2023 by EnderCrypt                                         *
*                                                                          *
* This program is free software: you can redistribute it and/or modify     *
* it under the terms of the GNU Affero General Public License as           *
* published by the Free Software Foundation, either version 3 of           *
* the License, or (at your option) any later version.                      *
*                                                                          *
* This program is distributed in the hope that it will be useful,          *
* but WITHOUT ANY WARRANTY; without even the implied warranty of           *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
* GNU Affero General Public License for more details.                      *
*                                                                          *
* You should have received a copy of the GNU Affero General Public License *
* along with this program in its root directory.                           *
* If not, see <http://www.gnu.org/licenses/>.                              *
\**************************************************************************/

package endercrypt.endercryptschallenge.features.entities.mobs.enderdragon;


import endercrypt.endercryptschallenge.core.Core;
import endercrypt.endercryptschallenge.modules.features.MinecraftFeature;
import endercrypt.endercryptschallenge.utility.Utility;
import endercrypt.endercryptschallenge.utility.difficulty.Difficulties;
import endercrypt.endercryptschallenge.utility.difficulty.DifficultyRating;
import endercrypt.endercryptschallenge.utility.tracker.entity.EntityTracker;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.attribute.Attribute;
import org.bukkit.block.Block;
import org.bukkit.entity.DragonFireball;
import org.bukkit.entity.EnderDragon;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Fireball;
import org.bukkit.entity.Monster;
import org.bukkit.entity.Player;
import org.bukkit.entity.Projectile;
import org.bukkit.event.EventHandler;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;
import org.bukkit.event.entity.EntityTargetEvent;
import org.bukkit.event.entity.ProjectileHitEvent;
import org.bukkit.util.Vector;


/**
 * @author EnderCrypt
 */
public class TheRealDragon extends MinecraftFeature
{
	private EntityTracker<EnderDragon> dragons = new EntityTracker<>(EnderDragon.class);
	
	public TheRealDragon(Core core)
	{
		super(core);
	}
	
	@Override
	public void onCycleEntity(Entity entity)
	{
		if (dragons.feed(entity))
		{
			setupDragon((EnderDragon) entity);
		}
	}
	
	@EventHandler
	public void onTargetEnderDragon(EntityTargetEvent event)
	{
		if (event.getEntity() instanceof Monster monster && event.getTarget() instanceof EnderDragon)
		{
			event.setCancelled(true);
		}
	}
	
	@Override
	public void onTick()
	{
		dragons.forEach(this::processDragon);
	}
	
	private void processDragon(EnderDragon dragon)
	{
		castFireBreath(dragon);
		castFireBall(dragon);
		autoHeal(dragon);
	}
	
	private void autoHeal(EnderDragon dragon)
	{
		Location location = dragon.getLocation();
		Player player = Utility.getClosestSurvivalPlayer(location).orElse(null);
		
		double secondsPerHeal = 10;
		
		if (player == null || location.distance(player.getLocation()) > 1000)
		{
			secondsPerHeal = 1;
		}
		
		double maxHealth = dragon.getAttribute(Attribute.GENERIC_MAX_HEALTH).getBaseValue();
		double health = dragon.getHealth();
		health += 1.0 / (20 * secondsPerHeal);
		health = Math.min(health, maxHealth);
		dragon.setHealth(health);
	}
	
	private void setupDragon(EnderDragon dragon)
	{
		DifficultyRating difficulty = Difficulties.getFor(dragon);
		
		double health = 200 * difficulty.asDoubleValue(2.5, 5.0, 10.0);
		
		dragon.getAttribute(Attribute.GENERIC_MAX_HEALTH).setBaseValue(health);
		dragon.setHealth(health);
		logger.info("dragon configured! (hp: " + ((int) health) + ")");
	}
	
	private Vector generateProjectileVelocity(EnderDragon dragon)
	{
		Vector velocity = dragon.getVelocity().clone();
		velocity.multiply(5.0);
		velocity.add(new Vector(0, -2, 0));
		return velocity;
	}
	
	private void castFireBreath(EnderDragon dragon)
	{
		DifficultyRating difficulty = Difficulties.getFor(dragon);
		if (Math.random() > difficulty.asDoubleValue(0.05, 0.10, 0.20))
			return;
		
		World world = dragon.getWorld();
		DragonFireball projectile = world.spawn(dragon.getLocation(), DragonFireball.class);
		projectile.setShooter(dragon);
		projectile.setVelocity(generateProjectileVelocity(dragon));
	}
	
	private void castFireBall(EnderDragon dragon)
	{
		if (dragon.getWorld().getPlayerCount() == 0)
			return;
		
		DifficultyRating difficulty = Difficulties.getFor(dragon);
		if (Math.random() > difficulty.asDoubleValue(0.2, 0.3, 0.4))
			return;
		
		World world = dragon.getWorld();
		Fireball projectile = world.spawn(dragon.getLocation(), Fireball.class); // this projectile causes error?
		projectile.setShooter(dragon);
		projectile.setVelocity(generateProjectileVelocity(dragon));
	}
	
	private Location getProjectileHitLocation(ProjectileHitEvent event)
	{
		Block block = event.getHitBlock();
		if (block != null)
		{
			return block.getLocation();
		}
		Entity entity = event.getHitEntity();
		if (entity != null)
		{
			return entity.getLocation();
		}
		throw new IllegalArgumentException(event + " did not hit a block or entity");
	}
	
	@EventHandler
	public void onDragonBallHit(ProjectileHitEvent event)
	{
		Projectile projectile = event.getEntity();
		if (projectile.getShooter() instanceof EnderDragon == false)
		{
			return;
		}
		DifficultyRating difficulty = Difficulties.getFor(event);
		Location location = getProjectileHitLocation(event);
		spreadFire(location, difficulty.asIntValue(50, 75, 100));
	}
	
	private void spreadFire(Location location, int amount)
	{
		for (int i = 0; i < amount; i++)
		{
			double mult = Math.random() * 2;
			double distance = mult * mult * mult;
			Location fireLocation = Utility.randomlyShift(location.clone(), distance);
			Block fireBlock = fireLocation.getBlock();
			if (fireBlock.getType() == Material.AIR)
			{
				fireBlock.setType(Material.FIRE);
			}
		}
	}
	
	@EventHandler
	public void onDragonHitByFireball(EntityDamageByEntityEvent event)
	{
		if (event.getDamager() instanceof Fireball fireball || event.getCause() == DamageCause.ENTITY_EXPLOSION || event.getCause() == DamageCause.BLOCK_EXPLOSION)
		{
			if (event.getEntity() instanceof EnderDragon dragon)
			{
				event.setCancelled(true);
			}
		}
	}
}
