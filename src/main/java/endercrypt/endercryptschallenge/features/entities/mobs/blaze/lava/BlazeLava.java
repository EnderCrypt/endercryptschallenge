/**************************************************************************\
* EnderCrypt's Challenge                                                   *
* Copyright (C) 2023 by EnderCrypt                                         *
*                                                                          *
* This program is free software: you can redistribute it and/or modify     *
* it under the terms of the GNU Affero General Public License as           *
* published by the Free Software Foundation, either version 3 of           *
* the License, or (at your option) any later version.                      *
*                                                                          *
* This program is distributed in the hope that it will be useful,          *
* but WITHOUT ANY WARRANTY; without even the implied warranty of           *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
* GNU Affero General Public License for more details.                      *
*                                                                          *
* You should have received a copy of the GNU Affero General Public License *
* along with this program in its root directory.                           *
* If not, see <http://www.gnu.org/licenses/>.                              *
\**************************************************************************/

package endercrypt.endercryptschallenge.features.entities.mobs.blaze.lava;


import endercrypt.endercryptschallenge.core.Core;
import endercrypt.endercryptschallenge.materials.MaterialCollections;
import endercrypt.endercryptschallenge.modules.features.MinecraftFeature;

import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.craftbukkit.v1_21_R1.block.impl.CraftFluids;
import org.bukkit.entity.Blaze;
import org.bukkit.entity.Item;
import org.bukkit.event.EventHandler;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityDeathEvent;


public class BlazeLava extends MinecraftFeature
{
	public BlazeLava(Core core)
	{
		super(core);
	}
	
	@EventHandler
	public void onBlazeItemCombust(EntityDamageEvent event)
	{
		if (event.getEntity() instanceof Item item && item.getItemStack().getType() == Material.BLAZE_ROD)
		{
			event.setCancelled(true);
			item.setFireTicks(0);
		}
	}
	
	@EventHandler
	public void onEntityDeath(EntityDeathEvent event)
	{
		if (event.getEntity() instanceof Blaze blaze)
		{
			onBlazeDeath(blaze);
		}
	}
	
	private void onBlazeDeath(Blaze blaze)
	{
		Block lower = blaze.getLocation().add(0, 0.5, 0).getBlock();
		createBlazeLavaBlock(lower);
		Block upper = lower.getRelative(BlockFace.UP);
		createBlazeLavaBlock(upper);
	}
	
	private void createBlazeLavaBlock(Block block)
	{
		if (MaterialCollections.indestructable.contains(block))
		{
			return;
		}
		
		if (block.getType() != Material.AIR)
		{
			core.getCoreProtectManager().logRemoval("#blaze", block);
			block.breakNaturally();
		}
		
		block.setType(Material.LAVA);
		
		CraftFluids fluids = (CraftFluids) block.getBlockData();
		fluids.setLevel(1);
		block.setBlockData(fluids);
		
		core.getCoreProtectManager().logPlace("#blaze", block);
	}
}
