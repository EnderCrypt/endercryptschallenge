/**************************************************************************\
* EnderCrypt's Challenge                                                   *
* Copyright (C) 2023 by EnderCrypt                                         *
*                                                                          *
* This program is free software: you can redistribute it and/or modify     *
* it under the terms of the GNU Affero General Public License as           *
* published by the Free Software Foundation, either version 3 of           *
* the License, or (at your option) any later version.                      *
*                                                                          *
* This program is distributed in the hope that it will be useful,          *
* but WITHOUT ANY WARRANTY; without even the implied warranty of           *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
* GNU Affero General Public License for more details.                      *
*                                                                          *
* You should have received a copy of the GNU Affero General Public License *
* along with this program in its root directory.                           *
* If not, see <http://www.gnu.org/licenses/>.                              *
\**************************************************************************/

package endercrypt.endercryptschallenge.features.entities.mobs.creeper.speed;


import endercrypt.endercryptschallenge.core.Core;
import endercrypt.endercryptschallenge.modules.features.MinecraftFeature;
import endercrypt.endercryptschallenge.utility.difficulty.Difficulties;
import endercrypt.endercryptschallenge.utility.difficulty.DifficultyRating;

import org.bukkit.attribute.Attribute;
import org.bukkit.attribute.AttributeInstance;
import org.bukkit.entity.Creeper;
import org.bukkit.event.EventHandler;
import org.bukkit.event.entity.EntitySpawnEvent;


/**
 * @author EnderCrypt
 */
public class CreeperBoost extends MinecraftFeature
{
	public CreeperBoost(Core core)
	{
		super(core);
	}
	
	@EventHandler
	public void onCreeperSpawn(EntitySpawnEvent event)
	{
		DifficultyRating difficulty = Difficulties.getFor(event);
		if (event.getEntity() instanceof Creeper creeper)
		{
			double modifier = difficulty.asDoubleValue(1.35, 1.6, 2.25);
			AttributeInstance speed = creeper.getAttribute(Attribute.GENERIC_MOVEMENT_SPEED);
			speed.setBaseValue(speed.getBaseValue() * modifier);
		}
	}
}
