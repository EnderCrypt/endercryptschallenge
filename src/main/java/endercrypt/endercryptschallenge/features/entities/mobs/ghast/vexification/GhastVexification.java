/**************************************************************************\
* EnderCrypt's Challenge                                                   *
* Copyright (C) 2023 by EnderCrypt                                         *
*                                                                          *
* This program is free software: you can redistribute it and/or modify     *
* it under the terms of the GNU Affero General Public License as           *
* published by the Free Software Foundation, either version 3 of           *
* the License, or (at your option) any later version.                      *
*                                                                          *
* This program is distributed in the hope that it will be useful,          *
* but WITHOUT ANY WARRANTY; without even the implied warranty of           *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
* GNU Affero General Public License for more details.                      *
*                                                                          *
* You should have received a copy of the GNU Affero General Public License *
* along with this program in its root directory.                           *
* If not, see <http://www.gnu.org/licenses/>.                              *
\**************************************************************************/

package endercrypt.endercryptschallenge.features.entities.mobs.ghast.vexification;


import endercrypt.endercryptschallenge.core.Core;
import endercrypt.endercryptschallenge.modules.features.MinecraftFeature;
import endercrypt.endercryptschallenge.utility.Utility;
import endercrypt.endercryptschallenge.utility.difficulty.Difficulties;
import endercrypt.endercryptschallenge.utility.difficulty.DifficultyRating;
import endercrypt.library.commons.misc.Ender;

import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.entity.Ghast;
import org.bukkit.entity.Vex;
import org.bukkit.event.EventHandler;
import org.bukkit.event.entity.EntityDeathEvent;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;


/**
 * @author EnderCrypt
 */
public class GhastVexification extends MinecraftFeature
{
	public GhastVexification(Core core)
	{
		super(core);
	}
	
	@EventHandler
	public void onGhastKilled(EntityDeathEvent event)
	{
		if (event.getEntity() instanceof Ghast ghast)
		{
			processGhastKill(ghast);
		}
	}
	
	private void processGhastKill(Ghast ghast)
	{
		Location location = ghast.getLocation();
		World world = location.getWorld();
		
		DifficultyRating difficulty = Difficulties.getFor(world);
		int maxAmount = difficulty.asIntValue(8, 14, 20);
		int amount = (int) Math.round(maxAmount * Ender.math.randomRange(0.5, 1.0));
		for (int i = 0; i < amount; i++)
		{
			Vex vex = world.spawn(location, Vex.class);
			vex.addPotionEffect(new PotionEffect(PotionEffectType.FIRE_RESISTANCE, 20 * 60 * 60, 1));
			vex.setVelocity(Utility.getRandomVector().multiply(0.5));
			vex.setTarget(ghast.getKiller());
		}
	}
}
