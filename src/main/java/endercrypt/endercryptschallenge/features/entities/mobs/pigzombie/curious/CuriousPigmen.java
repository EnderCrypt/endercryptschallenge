/**************************************************************************\
* EnderCrypt's Challenge                                                   *
* Copyright (C) 2023 by EnderCrypt                                         *
*                                                                          *
* This program is free software: you can redistribute it and/or modify     *
* it under the terms of the GNU Affero General Public License as           *
* published by the Free Software Foundation, either version 3 of           *
* the License, or (at your option) any later version.                      *
*                                                                          *
* This program is distributed in the hope that it will be useful,          *
* but WITHOUT ANY WARRANTY; without even the implied warranty of           *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
* GNU Affero General Public License for more details.                      *
*                                                                          *
* You should have received a copy of the GNU Affero General Public License *
* along with this program in its root directory.                           *
* If not, see <http://www.gnu.org/licenses/>.                              *
\**************************************************************************/

package endercrypt.endercryptschallenge.features.entities.mobs.pigzombie.curious;


import endercrypt.endercryptschallenge.core.Core;
import endercrypt.endercryptschallenge.modules.features.MinecraftFeature;
import endercrypt.endercryptschallenge.modules.worlds.list.nether.NetherWorld;
import endercrypt.endercryptschallenge.utility.Utility;
import endercrypt.library.commons.RandomEntry;
import endercrypt.library.commons.structs.range.IntRange;
import endercrypt.library.commons.structs.range.Range;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;

import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.entity.PigZombie;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.block.BlockBreakEvent;


/**
 * @author EnderCrypt
 */
public class CuriousPigmen extends MinecraftFeature
{
	public static final int RETRIES = 10;
	public static final int AREA_ARRIVED = 3;
	public static final IntRange AREA = Range.of(5, 64);
	
	private List<TravellingPigman> travelers = new ArrayList<>();
	
	public CuriousPigmen(Core core)
	{
		super(core);
		getDebug().enableDebugSupport();
	}
	
	@Override
	public void onSecond()
	{
		Iterator<TravellingPigman> iterator = travelers.iterator();
		while (iterator.hasNext())
		{
			TravellingPigman travelling = iterator.next();
			
			if (travelling.isValid() == false)
			{
				iterator.remove();
				continue;
			}
			
			travelling.update();
		}
	}
	
	@EventHandler
	public void onBreakBlock(BlockBreakEvent event)
	{
		Block block = event.getBlock();
		World world = block.getWorld();
		
		if (resolve(world) instanceof NetherWorld == false)
			return;
		
		Player player = event.getPlayer();
		List<PigZombie> pigmen = world.getEntitiesByClass(PigZombie.class)
			.stream()
			.filter(pz -> isPigZombieInRange(player, pz))
			.collect(Collectors.toList());
		
		for (int i = 0; i < RETRIES; i++)
		{
			if (tryAttractPigZombie(player, pigmen))
			{
				return;
			}
		}
	}
	
	private boolean isPigZombieInRange(Player player, PigZombie pigman)
	{
		double distance = pigman.getLocation().distance(player.getLocation());
		return AREA.isInside(distance);
	}
	
	private boolean tryAttractPigZombie(Player player, List<PigZombie> pigmen)
	{
		PigZombie pigZombie = RandomEntry.from(pigmen);
		
		if (pigZombie == null)
			return false;
		
		if (pigZombie.getTarget() != null)
			return false;
		
		Block target = Utility.randomlyShiftOnPlane(player.getLocation(), 1).getBlock();
		
		boolean pathingResult = moveTo(pigZombie, target);
		
		if (pathingResult && isDebug())
		{
			debug(player, "attracted " + pigmen);
		}
		
		return pathingResult;
	}
	
	private boolean moveTo(PigZombie pigZombie, Block target)
	{
		TravellingPigman pigman = new TravellingPigman(pigZombie, target);
		
		if (pigman.path() == false)
			return false;
		
		travelers.add(pigman);
		
		return true;
	}
}
