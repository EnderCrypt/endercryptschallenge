/**************************************************************************\
* EnderCrypt's Challenge                                                   *
* Copyright (C) 2023 by EnderCrypt                                         *
*                                                                          *
* This program is free software: you can redistribute it and/or modify     *
* it under the terms of the GNU Affero General Public License as           *
* published by the Free Software Foundation, either version 3 of           *
* the License, or (at your option) any later version.                      *
*                                                                          *
* This program is distributed in the hope that it will be useful,          *
* but WITHOUT ANY WARRANTY; without even the implied warranty of           *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
* GNU Affero General Public License for more details.                      *
*                                                                          *
* You should have received a copy of the GNU Affero General Public License *
* along with this program in its root directory.                           *
* If not, see <http://www.gnu.org/licenses/>.                              *
\**************************************************************************/

package endercrypt.endercryptschallenge.features.entities.mobs.wither.upgrade;


import endercrypt.endercryptschallenge.materials.MaterialCollections;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.util.Vector;


/**
 * @author EnderCrypt
 */
public class WitherScan
{
	public static WitherScan perform(Location location, int range)
	{
		World world = location.getWorld();
		int lx = location.getBlockX();
		int ly = location.getBlockY();
		int lz = location.getBlockZ();
		
		List<Block> blocks = new ArrayList<>();
		Vector vector = new Vector();
		
		for (int rx = -range; rx <= range; rx++)
		{
			for (int ry = -range; ry <= range; ry++)
			{
				for (int rz = -range; rz <= range; rz++)
				{
					int bx = rx + lx;
					int by = ry + ly;
					int bz = rz + lz;
					Block block = world.getBlockAt(bx, by, bz);
					Material material = block.getType();
					
					if (material == Material.AIR)
						continue;
					
					if (MaterialCollections.indestructable.contains(material))
					{
						vector.add(calculateEscapeVector(location, block));
					}
					else if (block.isPassable() == false)
					{
						blocks.add(block);
					}
				}
			}
		}
		return new WitherScan(blocks, vector);
	}
	
	private static final double blockForce = 0.025;
	
	private static Vector calculateEscapeVector(Location location, Block block)
	{
		Vector offset = block.getLocation().subtract(location).toVector();
		Vector direction = offset.clone().multiply(-1).normalize();
		double distance = offset.length();
		double force = Math.min(blockForce, blockForce / distance);
		return direction.multiply(force);
	}
	
	private Set<Block> blocks = new HashSet<>();
	private Vector vector;
	
	public WitherScan(Collection<Block> blocks, Vector vector)
	{
		this.blocks.addAll(blocks);
		this.vector = vector;
	}
	
	public Set<Block> getBlocks()
	{
		return blocks;
	}
	
	public Vector getVector()
	{
		return vector;
	}
}
