/**************************************************************************\
* EnderCrypt's Challenge                                                   *
* Copyright (C) 2023 by EnderCrypt                                         *
*                                                                          *
* This program is free software: you can redistribute it and/or modify     *
* it under the terms of the GNU Affero General Public License as           *
* published by the Free Software Foundation, either version 3 of           *
* the License, or (at your option) any later version.                      *
*                                                                          *
* This program is distributed in the hope that it will be useful,          *
* but WITHOUT ANY WARRANTY; without even the implied warranty of           *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
* GNU Affero General Public License for more details.                      *
*                                                                          *
* You should have received a copy of the GNU Affero General Public License *
* along with this program in its root directory.                           *
* If not, see <http://www.gnu.org/licenses/>.                              *
\**************************************************************************/

package endercrypt.endercryptschallenge.features.entities.mobs.zombie.hungry;


import endercrypt.endercryptschallenge.core.Core;
import endercrypt.endercryptschallenge.modules.features.MinecraftFeature;
import endercrypt.endercryptschallenge.utility.Utility;
import endercrypt.endercryptschallenge.utility.difficulty.Difficulties;
import endercrypt.endercryptschallenge.utility.difficulty.DifficultyRating;

import org.bukkit.entity.Animals;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Zombie;


/**
 * @author EnderCrypt
 */
public class ZombieHungry extends MinecraftFeature
{
	public ZombieHungry(Core core)
	{
		super(core);
	}
	
	@Override
	public void onCycleEntity(Entity entity)
	{
		if (entity instanceof Zombie zombie)
		{
			aggroAnimals(zombie);
		}
	}
	
	private Animals aggroAnimals(Zombie zombie)
	{
		DifficultyRating difficulty = Difficulties.getFor(zombie);
		double chance = 1.0 / difficulty.asDoubleValue(6, 3, 1);
		if (Math.random() > chance)
			return null;
		
		LivingEntity target = zombie.getTarget();
		if (target instanceof Animals current)
			return current;
		
		double aggroRange = difficulty.asDoubleValue(3, 5, 8);
		Animals nearbyAnimal = Utility.getAnyEntityWhitinRange(zombie.getLocation(), Animals.class, aggroRange);
		if (nearbyAnimal == null)
			return null;
		
		zombie.setTarget(nearbyAnimal);
		return nearbyAnimal;
	}
}
