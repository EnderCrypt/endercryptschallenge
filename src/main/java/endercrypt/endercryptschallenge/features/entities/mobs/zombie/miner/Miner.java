/**************************************************************************\
* EnderCrypt's Challenge                                                   *
* Copyright (C) 2023 by EnderCrypt                                         *
*                                                                          *
* This program is free software: you can redistribute it and/or modify     *
* it under the terms of the GNU Affero General Public License as           *
* published by the Free Software Foundation, either version 3 of           *
* the License, or (at your option) any later version.                      *
*                                                                          *
* This program is distributed in the hope that it will be useful,          *
* but WITHOUT ANY WARRANTY; without even the implied warranty of           *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
* GNU Affero General Public License for more details.                      *
*                                                                          *
* You should have received a copy of the GNU Affero General Public License *
* along with this program in its root directory.                           *
* If not, see <http://www.gnu.org/licenses/>.                              *
\**************************************************************************/

package endercrypt.endercryptschallenge.features.entities.mobs.zombie.miner;


import endercrypt.endercryptschallenge.core.Core;
import endercrypt.endercryptschallenge.core.CoreCollective;
import endercrypt.endercryptschallenge.features.entities.mobs.zombie.miner.techniques.MiningTechnique;
import endercrypt.endercryptschallenge.features.entities.mobs.zombie.tools.ZombieTools;
import endercrypt.endercryptschallenge.materials.MaterialCollections;
import endercrypt.endercryptschallenge.utility.Utility;
import endercrypt.endercryptschallenge.utility.difficulty.Difficulties;
import endercrypt.endercryptschallenge.utility.difficulty.DifficultyRating;
import endercrypt.endercryptschallenge.utility.tracker.entity.ConditionAliveAndActive;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Monster;
import org.bukkit.entity.Player;
import org.bukkit.entity.Zombie;
import org.bukkit.inventory.ItemStack;
import org.bukkit.util.RayTraceResult;
import org.bukkit.util.Vector;


/**
 * @author EnderCrypt
 */
public class Miner extends CoreCollective
{
	private final Monster entity;
	private final List<Block> claustrophobic = new ArrayList<>();
	
	public Miner(Core core, Monster entity)
	{
		super(core);
		this.entity = Objects.requireNonNull(entity, "entity");
	}
	
	public Monster getEntity()
	{
		return entity;
	}
	
	public void update()
	{
		if (mineClaustrophobia())
			return;
		
		if (Math.random() > getDigChance())
			return;
		
		if (mineStuckBlocks())
			return;
		
		if (mineTowardsTarget())
			return;
		
		return;
	}
	
	private boolean mineClaustrophobia()
	{
		if (Math.random() > 1.0 / 20)
			return false;
		
		if (claustrophobic.isEmpty())
			return false;
		
		Block focus = null;
		Iterator<Block> iterator = claustrophobic.iterator();
		while (iterator.hasNext())
		{
			focus = iterator.next();
			iterator.remove();
			entity.getPathfinder().moveTo(Utility.getBlockFloorLocation(focus));
			
			if (tryMineBlock(focus))
			{
				break;
			}
		}
		final Block focusedBlock = focus;
		
		if (claustrophobic.size() <= 3)
		{
			createClaustrophobia(focus, 5);
			
			ZombieMiners miners = core.getMinecraftFeatureManager().getFeature(ZombieMiners.class);
			
			entity.getLocation()
				.getNearbyEntitiesByType(Zombie.class, 5)
				.stream()
				.map(miners::getMiner)
				.filter(Optional::isPresent)
				.map(Optional::get)
				.forEach(m -> m.createClaustrophobia(focusedBlock, 5));
		}
		
		return true;
	}
	
	private boolean createClaustrophobia(Block location, double radius)
	{
		Set<Block> blocks = Utility.getBlockSphere(Utility.getBlockFloorLocation(location), radius)
			.stream()
			.filter(MaterialCollections.halfBlocks::contains)
			.collect(Collectors.toSet());
		
		if (blocks.isEmpty())
			return false;
		
		claustrophobic.addAll(blocks);
		Collections.shuffle(claustrophobic);
		
		return true;
	}
	
	private boolean mineStuckBlocks()
	{
		Block stuck = Utility.entityStuckBlock(entity).orElse(null);
		if (stuck != null)
		{
			claustrophobic.add(stuck);
		}
		
		return false;
	}
	
	private boolean mineTowardsTarget()
	{
		// nearby player
		LivingEntity targetPlayer = entity.getTarget();
		if (targetPlayer == null)
			return false;
		
		// not a player
		if (targetPlayer instanceof Player == false)
			return false;
		
		// wrong world
		if (targetPlayer.getWorld().equals(entity.getWorld()) == false)
		{
			entity.setTarget(null);
			return false;
		}
		
		// player distance
		DifficultyRating difficulty = Difficulties.getFor(entity);
		double distance = entity.getLocation().distance(targetPlayer.getLocation());
		double range = difficulty.asDoubleValue(12, 24, 48);
		if (distance > range)
			return false;
		
		// target block
		Location sourceLocation = entity.getEyeLocation();
		Location targetLocation = targetPlayer.getEyeLocation();
		Block block = MiningTechnique.getMiningTechnique(entity).choose(sourceLocation, targetLocation, 4);
		if (block == null)
			return false;
		
		return tryMineBlock(block);
	}
	
	private boolean tryMineBlock(Block block)
	{
		Location blockLocation = Utility.getBlockCenterLocation(block);
		if (blockLocation.distance(entity.getLocation()) < 3)
		{
			return false;
		}
		
		if (canBreakBlock(block) == false)
		{
			return false;
		}
		
		Vector aim = blockLocation.subtract(entity.getEyeLocation()).toVector();
		double range = aim.length();
		RayTraceResult result = blockLocation.getWorld().rayTraceBlocks(entity.getEyeLocation(), aim, range);
		if (result == null || block.equals(result.getHitBlock()) == false)
		{
			return false;
		}
		
		mineBlock(block);
		return true;
	}
	
	private void mineBlock(Block block)
	{
		// core protect
		core.getCoreProtectManager().logRemoval("#ZombieMiner", block);
		
		// mine!
		entity.swingMainHand();
		ItemStack item = getTool().orElseThrow();
		if (block.getDrops(item).isEmpty())
		{
			block.breakNaturally(true);
		}
		else
		{
			block.breakNaturally(item, true);
		}
	}
	
	public Optional<ItemStack> getTool()
	{
		ItemStack mainItem = entity.getEquipment().getItemInMainHand();
		if (mainItem.getType() == Material.AIR)
		{
			return Optional.empty();
		}
		
		return Optional.of(mainItem);
	}
	
	public double getDigChance()
	{
		DifficultyRating difficulty = Difficulties.getFor(entity);
		if (entity instanceof Zombie && Utility.isSpecialEnemy(entity))
		{
			return 1.0 / difficulty.asDoubleValue(1000, 30, 20, 10);
		}
		
		return 1.0 / difficulty.asDoubleValue(1000, 80, 60, 40);
	}
	
	private boolean canBreakBlock(Block block)
	{
		ItemStack item = getTool().orElse(null);
		
		if (item == null)
			return false;
		
		if (block.getType() == Material.AIR)
			return false;
		
		if (ZombieTools.isValidMiningTool(item.getType()) == false)
			return false;
		
		if (MaterialCollections.alwaysMineable.contains(block))
			return true;
		
		if (block.getDrops(item).isEmpty())
			return false;
		
		return true;
	}
	
	public boolean isAlive()
	{
		if (ConditionAliveAndActive.checkEntity(entity) == false)
			return false;
		
		if (getTool().isEmpty())
			return false;
		
		return true;
	}
	
	@Override
	public int hashCode()
	{
		final int prime = 31;
		int result = 1;
		result = prime * result + ((entity == null) ? 0 : entity.hashCode());
		return result;
	}
	
	@Override
	public boolean equals(Object obj)
	{
		if (this == obj) return true;
		if (!(obj instanceof Miner)) return false;
		Miner other = (Miner) obj;
		if (entity == null)
		{
			if (other.entity != null) return false;
		}
		else if (!entity.equals(other.entity)) return false;
		return true;
	}
}
