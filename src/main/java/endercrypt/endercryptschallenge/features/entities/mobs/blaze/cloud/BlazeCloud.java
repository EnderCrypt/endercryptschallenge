/**************************************************************************\
* EnderCrypt's Challenge                                                   *
* Copyright (C) 2023 by EnderCrypt                                         *
*                                                                          *
* This program is free software: you can redistribute it and/or modify     *
* it under the terms of the GNU Affero General Public License as           *
* published by the Free Software Foundation, either version 3 of           *
* the License, or (at your option) any later version.                      *
*                                                                          *
* This program is distributed in the hope that it will be useful,          *
* but WITHOUT ANY WARRANTY; without even the implied warranty of           *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
* GNU Affero General Public License for more details.                      *
*                                                                          *
* You should have received a copy of the GNU Affero General Public License *
* along with this program in its root directory.                           *
* If not, see <http://www.gnu.org/licenses/>.                              *
\**************************************************************************/

package endercrypt.endercryptschallenge.features.entities.mobs.blaze.cloud;


import endercrypt.endercryptschallenge.core.Core;
import endercrypt.endercryptschallenge.modules.features.MinecraftFeature;
import endercrypt.endercryptschallenge.utility.tracker.ObjectTracker;
import endercrypt.endercryptschallenge.utility.tracker.entity.EntityTracker;

import java.util.Iterator;

import org.bukkit.Color;
import org.bukkit.Location;
import org.bukkit.Particle;
import org.bukkit.entity.AreaEffectCloud;
import org.bukkit.entity.Blaze;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Monster;
import org.bukkit.event.EventHandler;
import org.bukkit.event.entity.AreaEffectCloudApplyEvent;
import org.bukkit.event.entity.EntityDeathEvent;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;


public class BlazeCloud extends MinecraftFeature
{
	private final ObjectTracker<AreaEffectCloud> clouds = new EntityTracker<>(AreaEffectCloud.class);
	
	public BlazeCloud(Core core)
	{
		super(core);
	}
	
	@Override
	public void onSecond()
	{
		clouds.cleanup();
	}
	
	@EventHandler
	public void onEntityDeath(EntityDeathEvent event)
	{
		if (event.getEntity() instanceof Blaze blaze)
		{
			createBlazeCloud(blaze);
		}
	}
	
	@EventHandler
	public void onCloudEffect(AreaEffectCloudApplyEvent event)
	{
		if (clouds.contains(event.getEntity()) == false)
			return;
		
		Iterator<LivingEntity> iterator = event.getAffectedEntities().iterator();
		while (iterator.hasNext())
		{
			LivingEntity entity = iterator.next();
			if (entity instanceof Monster == false)
			{
				iterator.remove();
			}
		}
	}
	
	private void createBlazeCloud(Blaze blaze)
	{
		Location location = blaze.getLocation().add(0, 0.25, 0);
		AreaEffectCloud cloud = location.getWorld().spawn(location, AreaEffectCloud.class);
		cloud.setSource(blaze);
		cloud.setColor(Color.ORANGE);
		cloud.addCustomEffect(new PotionEffect(PotionEffectType.SPEED, 20 * 15, 3), false);
		cloud.addCustomEffect(new PotionEffect(PotionEffectType.HEALTH_BOOST, 20 * 10, 1), false);
		cloud.addCustomEffect(new PotionEffect(PotionEffectType.REGENERATION, 20 * 5, 1), false);
		cloud.setParticle(Particle.FLAME);
		
		float radius = 5;
		int lifetime = 20 * 5;
		
		cloud.setDurationOnUse(10);
		cloud.setDuration(lifetime);
		cloud.setRadius(radius);
		cloud.setRadiusPerTick(-radius / lifetime);
		cloud.setWaitTime(20);
		
		clouds.add(cloud);
	}
}
