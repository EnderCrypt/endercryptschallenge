/**************************************************************************\
* EnderCrypt's Challenge                                                   *
* Copyright (C) 2023 by EnderCrypt                                         *
*                                                                          *
* This program is free software: you can redistribute it and/or modify     *
* it under the terms of the GNU Affero General Public License as           *
* published by the Free Software Foundation, either version 3 of           *
* the License, or (at your option) any later version.                      *
*                                                                          *
* This program is distributed in the hope that it will be useful,          *
* but WITHOUT ANY WARRANTY; without even the implied warranty of           *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
* GNU Affero General Public License for more details.                      *
*                                                                          *
* You should have received a copy of the GNU Affero General Public License *
* along with this program in its root directory.                           *
* If not, see <http://www.gnu.org/licenses/>.                              *
\**************************************************************************/

package endercrypt.endercryptschallenge.features.entities.mobs.zombie.miner.techniques.types;


import endercrypt.endercryptschallenge.features.entities.mobs.zombie.miner.techniques.MiningTechnique;
import endercrypt.endercryptschallenge.utility.Utility;

import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.util.RayTraceResult;
import org.bukkit.util.Vector;

import com.google.common.base.Objects;


/**
 * @author EnderCrypt
 */
public class DirectionalMiningTechnique implements MiningTechnique
{
	@Override
	public Block choose(Location location, Location target, double range)
	{
		if (Objects.equal(location.getWorld(), target.getWorld()) == false)
		{
			throw new IllegalArgumentException("these guys are from diffrent world man");
		}
		World world = location.getWorld();
		
		Vector offsetVector = target.clone().subtract(location).toVector();
		offsetVector.normalize();
		Location baseAim = location.clone().add(offsetVector.clone().multiply(15));
		
		for (int i = 0; i < 20; i++)
		{
			Vector dispersion = Utility.getRandomVector().multiply(i);
			Location aimLocation = baseAim.clone().add(dispersion);
			
			Vector aim = aimLocation.clone().subtract(location).toVector();
			
			RayTraceResult result = world.rayTraceBlocks(location, aim, range);
			if (result != null && result.getHitBlock() != null)
			{
				return result.getHitBlock();
			}
		}
		return null;
	}
}
