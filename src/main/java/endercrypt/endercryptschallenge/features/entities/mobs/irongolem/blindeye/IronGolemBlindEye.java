/**************************************************************************\
* EnderCrypt's Challenge                                                   *
* Copyright (C) 2023 by EnderCrypt                                         *
*                                                                          *
* This program is free software: you can redistribute it and/or modify     *
* it under the terms of the GNU Affero General Public License as           *
* published by the Free Software Foundation, either version 3 of           *
* the License, or (at your option) any later version.                      *
*                                                                          *
* This program is distributed in the hope that it will be useful,          *
* but WITHOUT ANY WARRANTY; without even the implied warranty of           *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
* GNU Affero General Public License for more details.                      *
*                                                                          *
* You should have received a copy of the GNU Affero General Public License *
* along with this program in its root directory.                           *
* If not, see <http://www.gnu.org/licenses/>.                              *
\**************************************************************************/

package endercrypt.endercryptschallenge.features.entities.mobs.irongolem.blindeye;


import endercrypt.endercryptschallenge.core.Core;
import endercrypt.endercryptschallenge.modules.features.MinecraftFeature;

import org.bukkit.entity.IronGolem;
import org.bukkit.entity.Monster;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.entity.EntityTargetEvent.TargetReason;
import org.bukkit.event.entity.EntityTargetLivingEntityEvent;


/**
 * @author EnderCrypt
 */
public class IronGolemBlindEye extends MinecraftFeature
{
	public IronGolemBlindEye(Core core)
	{
		super(core);
	}
	
	@EventHandler
	public void onIronGolemTargetPlayer(EntityTargetLivingEntityEvent event)
	{
		if (event.getEntity() instanceof IronGolem == false)
			return;
		
		if (event.getReason() == TargetReason.TARGET_ATTACKED_ENTITY) // dont ignore if directly attacked
			return;
		
		if (event.getTarget() instanceof Monster == false)
			return;
		Monster targetMonster = (Monster) event.getTarget();
		
		if (targetMonster.getTarget() instanceof Player == false)
			return;
		Player targetPlayer = (Player) targetMonster.getTarget();
		
		if (resolve(targetPlayer).threat().isThreat() == false)
			return;
		
		event.setCancelled(true);
	}
}
