/**************************************************************************\
* EnderCrypt's Challenge                                                   *
* Copyright (C) 2023 by EnderCrypt                                         *
*                                                                          *
* This program is free software: you can redistribute it and/or modify     *
* it under the terms of the GNU Affero General Public License as           *
* published by the Free Software Foundation, either version 3 of           *
* the License, or (at your option) any later version.                      *
*                                                                          *
* This program is distributed in the hope that it will be useful,          *
* but WITHOUT ANY WARRANTY; without even the implied warranty of           *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
* GNU Affero General Public License for more details.                      *
*                                                                          *
* You should have received a copy of the GNU Affero General Public License *
* along with this program in its root directory.                           *
* If not, see <http://www.gnu.org/licenses/>.                              *
\**************************************************************************/

package endercrypt.endercryptschallenge.features.entities.mobs.fox;


import endercrypt.endercryptschallenge.core.Core;
import endercrypt.endercryptschallenge.modules.features.MinecraftFeature;
import endercrypt.endercryptschallenge.utility.Utility;
import endercrypt.library.commons.RandomEntry;

import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Fox;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;


/**
 * idea by Qwuuk (https://www.twitch.tv/qwuuk)
 * 
 * @author EnderCrypt
 */
public class ThievingFox extends MinecraftFeature
{
	public ThievingFox(Core core)
	{
		super(core);
	}
	
	@Override
	public void onCycleEntity(Entity entity)
	{
		if (entity instanceof Fox fox)
		{
			processFox(fox);
		}
	}
	
	private void processFox(Fox fox)
	{
		ItemStack foxItem = fox.getActiveItem();
		if (foxItem.getType() != Material.AIR)
			return;
		
		if (fox.isSleeping())
			return;
		
		Player player = RandomEntry.from(fox.getLocation().getNearbyPlayers(2.5));
		if (player == null)
			return;
		
		if (player.equals(fox.getFirstTrustedPlayer()))
			return;
		
		ItemStack playerItem = player.getInventory().getItemInMainHand();
		if (playerItem.getType() == Material.AIR)
			return;
		
		World world = player.getWorld();
		String plural = playerItem.getAmount() == 1 ? "" : "s";
		player.sendMessage("fox stole your " + Utility.getItemName(playerItem) + plural);
		world.dropItem(fox.getLocation(), playerItem);
		playerItem.setAmount(0);
	}
}
