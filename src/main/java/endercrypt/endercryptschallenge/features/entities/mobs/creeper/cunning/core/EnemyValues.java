/**************************************************************************\
* EnderCrypt's Challenge                                                   *
* Copyright (C) 2023 by EnderCrypt                                         *
*                                                                          *
* This program is free software: you can redistribute it and/or modify     *
* it under the terms of the GNU Affero General Public License as           *
* published by the Free Software Foundation, either version 3 of           *
* the License, or (at your option) any later version.                      *
*                                                                          *
* This program is distributed in the hope that it will be useful,          *
* but WITHOUT ANY WARRANTY; without even the implied warranty of           *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
* GNU Affero General Public License for more details.                      *
*                                                                          *
* You should have received a copy of the GNU Affero General Public License *
* along with this program in its root directory.                           *
* If not, see <http://www.gnu.org/licenses/>.                              *
\**************************************************************************/

package endercrypt.endercryptschallenge.features.entities.mobs.creeper.cunning.core;


import endercrypt.endercryptschallenge.utility.Utility;
import endercrypt.library.commons.entry.Entry;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.entity.Creeper;
import org.bukkit.entity.Enderman;
import org.bukkit.entity.Endermite;
import org.bukkit.entity.Enemy;
import org.bukkit.entity.PigZombie;
import org.bukkit.entity.Piglin;
import org.bukkit.entity.Silverfish;
import org.bukkit.entity.Skeleton;
import org.bukkit.entity.Slime;
import org.bukkit.entity.Spider;
import org.bukkit.entity.Witch;
import org.bukkit.entity.WitherSkeleton;
import org.bukkit.entity.Zombie;


/**
 * @author EnderCrypt
 */
public class EnemyValues
{
	private static final double defaultEnemyValue = 0.15;
	private final List<Entry<Class<? extends Enemy>, Double>> enemyValues = new ArrayList<>();
	
	public EnemyValues()
	{
		register(Enderman.class, 1.5);
		register(WitherSkeleton.class, 0.8);
		register(Witch.class, 0.8);
		register(Piglin.class, 0.4);
		register(Creeper.class, 0.4);
		register(PigZombie.class, 0.4);
		register(Skeleton.class, 0.35);
		register(Endermite.class, 0.3);
		register(Slime.class, 0.3);
		register(Spider.class, 0.2);
		register(Silverfish.class, 0.2);
		register(Zombie.class, 0.075);
	}
	
	private void register(Class<? extends Enemy> enemyClass, double value)
	{
		enemyValues.add(Entry.immutable(enemyClass, value));
	}
	
	public double get(Enemy enemy)
	{
		double value = get(enemy.getClass());
		
		if (Utility.isSpecialEnemy(enemy))
		{
			value *= 3;
		}
		
		return value;
	}
	
	public double get(Class<? extends Enemy> enemyClass)
	{
		for (Entry<Class<? extends Enemy>, Double> entry : enemyValues)
		{
			Class<? extends Enemy> targetClass = entry.getKey();
			double value = entry.getValue();
			if (targetClass.isAssignableFrom(enemyClass))
			{
				return value;
			}
		}
		return defaultEnemyValue;
	}
}
