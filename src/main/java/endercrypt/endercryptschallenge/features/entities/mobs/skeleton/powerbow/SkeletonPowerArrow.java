/**************************************************************************\
* EnderCrypt's Challenge                                                   *
* Copyright (C) 2023 by EnderCrypt                                         *
*                                                                          *
* This program is free software: you can redistribute it and/or modify     *
* it under the terms of the GNU Affero General Public License as           *
* published by the Free Software Foundation, either version 3 of           *
* the License, or (at your option) any later version.                      *
*                                                                          *
* This program is distributed in the hope that it will be useful,          *
* but WITHOUT ANY WARRANTY; without even the implied warranty of           *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
* GNU Affero General Public License for more details.                      *
*                                                                          *
* You should have received a copy of the GNU Affero General Public License *
* along with this program in its root directory.                           *
* If not, see <http://www.gnu.org/licenses/>.                              *
\**************************************************************************/

package endercrypt.endercryptschallenge.features.entities.mobs.skeleton.powerbow;


import endercrypt.endercryptschallenge.core.Core;
import endercrypt.endercryptschallenge.modules.features.MinecraftFeature;
import endercrypt.endercryptschallenge.utility.Utility;
import endercrypt.endercryptschallenge.utility.difficulty.Difficulties;
import endercrypt.endercryptschallenge.utility.difficulty.DifficultyRating;
import endercrypt.library.commons.RandomEntry;

import java.util.List;

import org.bukkit.entity.Arrow;
import org.bukkit.entity.Skeleton;
import org.bukkit.entity.Witch;
import org.bukkit.event.EventHandler;
import org.bukkit.event.entity.EntityShootBowEvent;
import org.bukkit.potion.PotionData;
import org.bukkit.potion.PotionType;


/**
 * @author EnderCrypt
 */
public class SkeletonPowerArrow extends MinecraftFeature
{
	private static final List<PotionType> witchEffects = List.of(PotionType.HARMING, PotionType.POISON, PotionType.WEAKNESS, PotionType.SLOWNESS);
	
	public SkeletonPowerArrow(Core core)
	{
		super(core);
	}
	
	@EventHandler
	public void onShootBow(EntityShootBowEvent event)
	{
		if (event.getEntity() instanceof Skeleton skeleton && event.getProjectile() instanceof Arrow arrow)
		{
			skeletonShootBow(skeleton, arrow);
		}
	}
	
	private void skeletonShootBow(Skeleton skeleton, Arrow arrow)
	{
		DifficultyRating difficulty = Difficulties.getFor(skeleton);
		
		// witch presence
		double witchRadius = difficulty.asIntValue(15, 25, 40);
		if (Utility.getAnyEntityWhitinRange(skeleton.getLocation(), Witch.class, witchRadius) != null)
		{
			PotionType potionType = RandomEntry.from(witchEffects);
			arrow.setBasePotionData(new PotionData(potionType));
		}
		
		// pierce through enemies
		arrow.setPierceLevel(5);
		
		// speed
		double speedMultiplier = difficulty.asDoubleValue(1.5, 2.5, 5);
		arrow.setVelocity(arrow.getVelocity().multiply(speedMultiplier));
		
		// knockback
		int strength = difficulty.asIntValue(5, 8, 12);
		arrow.setKnockbackStrength(strength);
	}
}
