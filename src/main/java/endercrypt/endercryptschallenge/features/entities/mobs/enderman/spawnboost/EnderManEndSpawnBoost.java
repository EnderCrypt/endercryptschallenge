/**************************************************************************\
* EnderCrypt's Challenge                                                   *
* Copyright (C) 2023 by EnderCrypt                                         *
*                                                                          *
* This program is free software: you can redistribute it and/or modify     *
* it under the terms of the GNU Affero General Public License as           *
* published by the Free Software Foundation, either version 3 of           *
* the License, or (at your option) any later version.                      *
*                                                                          *
* This program is distributed in the hope that it will be useful,          *
* but WITHOUT ANY WARRANTY; without even the implied warranty of           *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
* GNU Affero General Public License for more details.                      *
*                                                                          *
* You should have received a copy of the GNU Affero General Public License *
* along with this program in its root directory.                           *
* If not, see <http://www.gnu.org/licenses/>.                              *
\**************************************************************************/

package endercrypt.endercryptschallenge.features.entities.mobs.enderman.spawnboost;


import endercrypt.endercryptschallenge.core.Core;
import endercrypt.endercryptschallenge.modules.features.MinecraftFeature;
import endercrypt.endercryptschallenge.modules.worlds.spawner.factory.SpawnAutomator;
import endercrypt.endercryptschallenge.modules.worlds.spawner.factory.SpawnCondition;

import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.entity.Enderman;
import org.bukkit.entity.Player;


/**
 * @author EnderCrypt
 */
public class EnderManEndSpawnBoost extends MinecraftFeature
{
	private static final SpawnCondition spawnCondition = new SpawnCondition.Builder()
		.setAnyLightLevel()
		.setHeight(3)
		.build();
	
	private static final SpawnAutomator spawnAutomator = new SpawnAutomator.Builder()
		.setCondition(spawnCondition)
		.setChance(1.0)
		.setMinimumDistance(15)
		.setMaximumDistance(25)
		.build();
	
	private static final int prefferedEndermanCount = 10;
	
	public EnderManEndSpawnBoost(Core core)
	{
		super(core);
	}
	
	@Override
	public void onSecond()
	{
		core.getWorldManager()
			.getEndWorld()
			.getRoot()
			.getPlayers()
			.forEach(this::processPlayer);
	}
	
	private void processPlayer(Player player)
	{
		int nearbyEndermen = player.getLocation().getNearbyEntitiesByType(Enderman.class, spawnAutomator.getMaximumDistance()).size();
		if (nearbyEndermen >= prefferedEndermanCount)
			return;
		
		Location spawnLocation = spawnAutomator.findSpawnSpot(player).orElse(null);
		if (spawnLocation == null)
			return;
		
		World world = spawnLocation.getWorld();
		world.spawn(spawnLocation, Enderman.class);
	}
}
