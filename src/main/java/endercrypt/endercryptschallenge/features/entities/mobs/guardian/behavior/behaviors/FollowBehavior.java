/**************************************************************************\
* EnderCrypt's Challenge                                                   *
* Copyright (C) 2023 by EnderCrypt                                         *
*                                                                          *
* This program is free software: you can redistribute it and/or modify     *
* it under the terms of the GNU Affero General Public License as           *
* published by the Free Software Foundation, either version 3 of           *
* the License, or (at your option) any later version.                      *
*                                                                          *
* This program is distributed in the hope that it will be useful,          *
* but WITHOUT ANY WARRANTY; without even the implied warranty of           *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
* GNU Affero General Public License for more details.                      *
*                                                                          *
* You should have received a copy of the GNU Affero General Public License *
* along with this program in its root directory.                           *
* If not, see <http://www.gnu.org/licenses/>.                              *
\**************************************************************************/

package endercrypt.endercryptschallenge.features.entities.mobs.guardian.behavior.behaviors;


import endercrypt.endercryptschallenge.features.entities.mobs.guardian.behavior.StandardGuardianBehavior;

import org.bukkit.Location;
import org.bukkit.entity.Guardian;
import org.bukkit.entity.LivingEntity;
import org.bukkit.util.Vector;


/**
 * @author EnderCrypt
 */
public class FollowBehavior extends StandardGuardianBehavior
{
	@Override
	protected boolean isApplicable(Guardian guardian, LivingEntity target, double distance, int depth)
	{
		return true;
	}
	
	@Override
	protected double getSpeed(Guardian guardian, LivingEntity target, double distance, int depth)
	{
		return 0.02;
	}
	
	@Override
	protected Location getAim(Guardian guardian, LivingEntity target, double distance, int depth)
	{
		return target.getLocation()
			.add(new Vector(0, -2, 0));
	}
	
	@Override
	protected void postAction(Guardian guardian, LivingEntity target, double distance, int depth)
	{
		guardian.lookAt(target, 30f, 30f);
	}
}
