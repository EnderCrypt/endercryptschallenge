/**************************************************************************\
* EnderCrypt's Challenge                                                   *
* Copyright (C) 2023 by EnderCrypt                                         *
*                                                                          *
* This program is free software: you can redistribute it and/or modify     *
* it under the terms of the GNU Affero General Public License as           *
* published by the Free Software Foundation, either version 3 of           *
* the License, or (at your option) any later version.                      *
*                                                                          *
* This program is distributed in the hope that it will be useful,          *
* but WITHOUT ANY WARRANTY; without even the implied warranty of           *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
* GNU Affero General Public License for more details.                      *
*                                                                          *
* You should have received a copy of the GNU Affero General Public License *
* along with this program in its root directory.                           *
* If not, see <http://www.gnu.org/licenses/>.                              *
\**************************************************************************/

package endercrypt.endercryptschallenge.features.entities.mobs.enderman.aggro;


import endercrypt.endercryptschallenge.core.Core;
import endercrypt.endercryptschallenge.modules.features.MinecraftFeature;
import endercrypt.endercryptschallenge.utility.difficulty.Difficulties;
import endercrypt.endercryptschallenge.utility.difficulty.DifficultyRating;

import org.bukkit.Particle;
import org.bukkit.World;
import org.bukkit.entity.Enderman;
import org.bukkit.entity.Player;


/**
 * @author EnderCrypt
 */
public class EnderManEndAggro extends MinecraftFeature
{
	public EnderManEndAggro(Core core)
	{
		super(core);
	}
	
	@Override
	public void onSecond()
	{
		core.getWorldManager()
			.getEndWorld()
			.getRoot()
			.getPlayers()
			.forEach(this::processPlayer);
	}
	
	private void processPlayer(Player player)
	{
		World world = player.getWorld();
		DifficultyRating difficulty = Difficulties.getFor(world);
		int range = difficulty.asIntValue(3, 6, 10);
		
		for (Enderman enderMan : world.getNearbyEntitiesByType(Enderman.class, player.getLocation(), range))
		{
			enderMan.setTarget(player);
			world.spawnParticle(Particle.ANGRY_VILLAGER, enderMan.getEyeLocation(), 5);
		}
	}
}
