/**************************************************************************\
* EnderCrypt's Challenge                                                   *
* Copyright (C) 2023 by EnderCrypt                                         *
*                                                                          *
* This program is free software: you can redistribute it and/or modify     *
* it under the terms of the GNU Affero General Public License as           *
* published by the Free Software Foundation, either version 3 of           *
* the License, or (at your option) any later version.                      *
*                                                                          *
* This program is distributed in the hope that it will be useful,          *
* but WITHOUT ANY WARRANTY; without even the implied warranty of           *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
* GNU Affero General Public License for more details.                      *
*                                                                          *
* You should have received a copy of the GNU Affero General Public License *
* along with this program in its root directory.                           *
* If not, see <http://www.gnu.org/licenses/>.                              *
\**************************************************************************/

package endercrypt.endercryptschallenge.features.entities.mobs.bat.disease;


import endercrypt.endercryptschallenge.core.Core;
import endercrypt.endercryptschallenge.modules.features.MinecraftFeature;
import endercrypt.endercryptschallenge.utility.difficulty.Difficulties;
import endercrypt.endercryptschallenge.utility.difficulty.DifficultyRating;
import endercrypt.library.commons.structs.range.IntRange;
import endercrypt.library.commons.structs.range.Range;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

import org.bukkit.Difficulty;
import org.bukkit.entity.Bat;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;


/**
 * @author EnderCrypt
 */
public class BatDisease extends MinecraftFeature
{
	private static final String MESSAGE = "You catch a cough from the bat.";
	private static final PotionEffectType EFFECT = PotionEffectType.WEAKNESS;
	
	private static final IntRange TIME = Range.of(10, 20);
	private static final int TIME_BONUS_IMMUNITY = 10;
	
	private static final IntRange AMPLIFIER = Range.of(1, 3);
	
	private Map<Player, Integer> cooldown = new HashMap<>();
	
	public BatDisease(Core core)
	{
		super(core);
	}
	
	@Override
	public void onTick()
	{
		Iterator<Entry<Player, Integer>> iterator = cooldown.entrySet().iterator();
		while (iterator.hasNext())
		{
			Entry<Player, Integer> entry = iterator.next();
			int ticks = entry.getValue();
			ticks--;
			entry.setValue(ticks);
			if (ticks <= 0)
			{
				iterator.remove();
			}
		}
	}
	
	@Override
	public void onCycleEntity(Entity entity)
	{
		if (entity instanceof Bat bat)
		{
			processBat(bat);
		}
	}
	
	private void processBat(Bat bat)
	{
		DifficultyRating difficulty = Difficulties.getFor(bat);
		int range = difficulty.asIntValue(2, 4, 8);
		for (Player player : bat.getLocation().getNearbyPlayers(range))
		{
			affectPlayer(player);
		}
	}
	
	private void affectPlayer(Player player)
	{
		if (player.hasPotionEffect(EFFECT))
			return;
		
		DifficultyRating difficulty = Difficulties.getFor(player);
		if (cooldown.containsKey(player) && difficulty.isEasierThan(Difficulty.HARD))
			return;
		
		int diseaseSeconds = TIME.getRandom();
		int diseaseTicks = diseaseSeconds * 20;
		int amplifier = AMPLIFIER.getRandom() - 1;
		player.addPotionEffect(new PotionEffect(EFFECT, diseaseTicks, amplifier));
		player.sendMessage(MESSAGE);
		
		int immunitySeconds = diseaseSeconds + TIME_BONUS_IMMUNITY;
		int immunityTicks = immunitySeconds * 20;
		cooldown.put(player, immunityTicks);
	}
}
