/**************************************************************************\
* EnderCrypt's Challenge                                                   *
* Copyright (C) 2023 by EnderCrypt                                         *
*                                                                          *
* This program is free software: you can redistribute it and/or modify     *
* it under the terms of the GNU Affero General Public License as           *
* published by the Free Software Foundation, either version 3 of           *
* the License, or (at your option) any later version.                      *
*                                                                          *
* This program is distributed in the hope that it will be useful,          *
* but WITHOUT ANY WARRANTY; without even the implied warranty of           *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
* GNU Affero General Public License for more details.                      *
*                                                                          *
* You should have received a copy of the GNU Affero General Public License *
* along with this program in its root directory.                           *
* If not, see <http://www.gnu.org/licenses/>.                              *
\**************************************************************************/

package endercrypt.endercryptschallenge.features.entities.mobs.bat.stealth;


import endercrypt.endercryptschallenge.core.Core;
import endercrypt.endercryptschallenge.modules.features.MinecraftFeature;
import endercrypt.endercryptschallenge.modules.pathfinder.PathfinderConfiguration;
import endercrypt.endercryptschallenge.modules.pathfinder.PathfinderRules;
import endercrypt.endercryptschallenge.utility.Utility;
import endercrypt.endercryptschallenge.utility.difficulty.Difficulties;
import endercrypt.endercryptschallenge.utility.difficulty.DifficultyRating;
import endercrypt.library.commons.structs.range.IntRange;
import endercrypt.library.commons.structs.range.Range;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.logging.Level;

import org.bukkit.FluidCollisionMode;
import org.bukkit.Location;
import org.bukkit.block.Block;
import org.bukkit.entity.Bat;
import org.bukkit.entity.Monster;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.util.RayTraceResult;
import org.bukkit.util.Vector;


/**
 * @author EnderCrypt
 */
public class StealthBat extends MinecraftFeature
{
	// max amounts of a block a bat will attempt to path towards the player
	public static final int PATHFINDING_LIMIT = 500;
	
	// max amount of ticks that pathfinding is allowed to stay in queue before being timedout
	public static final int PATHFINDING_TIMEOUT = 20 * 5;
	
	// bats whitin this range are attracted to the player
	private static final int ATTRACTION_RANGE = 30;
	
	// if no bats were in range, a bar is spawned on a floor whitin this range
	private static final IntRange SPAWN_RANGE = Range.of(20, 25);
	
	// no other players may be closer than this
	private static final int PLAYER_MAX_RANGE = 15;
	
	public StealthBat(Core core)
	{
		super(core);
	}
	
	@EventHandler
	public void onEntityDamageByEntityEvent(EntityDamageByEntityEvent event)
	{
		if (event.getDamager() instanceof Player player && event.getEntity() instanceof Monster)
		{
			onPlayerAttackedMonster(player);
		}
	}
	
	private void onPlayerAttackedMonster(Player player)
	{
		DifficultyRating difficulty = Difficulties.getFor(player);
		if (isShouldAggroStealthBats(difficulty) == false)
			return;
		
		Location target = player.getEyeLocation();
		List<Bat> bats = new ArrayList<>(target.getNearbyEntitiesByType(Bat.class, ATTRACTION_RANGE));
		if (bats.isEmpty())
		{
			attemptSpawnStealthBatNearPlayer(player).ifPresent(bats::add);
		}
		
		for (Bat bat : bats)
		{
			PathfinderConfiguration configuration = new PathfinderConfiguration.Builder()
				.setStart(bat.getLocation().getBlock())
				.setEnd(target.getBlock())
				.setLimit(PATHFINDING_LIMIT)
				.setTimeout(PATHFINDING_TIMEOUT)
				.setRules(PathfinderRules.bat)
				.build();
			
			core.getTravellerManager().submit(bat, configuration);
		}
	}
	
	private boolean isShouldAggroStealthBats(DifficultyRating difficulty)
	{
		return Math.random() < difficulty.asDoubleValue(0.1, 0.25, 0.5);
	}
	
	private Optional<Bat> attemptSpawnStealthBatNearPlayer(Player player)
	{
		final int retries = getSpawningAttemptsInContextOfPlayer(player);
		for (int i = 0; i < retries; i++)
		{
			Location target = player.getEyeLocation();
			Utility.randomlyShift(target, SPAWN_RANGE.getRandom());
			target = Utility.getBlockFloorLocation(target);
			
			if (isSuitableSpawningLocation(target.getBlock()))
			{
				Bat bat = target.getWorld().spawn(target, Bat.class);
				logger.log(Level.FINE, "Spawned stealth bat at " + bat.getLocation());
				return Optional.of(bat);
			}
		}
		
		return Optional.empty();
	}
	
	private boolean isSuitableSpawningLocation(Block block)
	{
		if (block.isPassable() == false)
			return false;
		
		if (isHiddenLocationFromPlayers(Utility.getBlockFloorLocation(block)) == false)
			return false;
		
		return true;
	}
	
	private int getSpawningAttemptsInContextOfPlayer(Player player)
	{
		final int MAX = 10;
		int count = 1;
		for (int i = 0; i < MAX; i++)
		{
			Block block = player.getEyeLocation().add(0, i, 0).getBlock();
			if (block.isSolid())
			{
				count++;
			}
		}
		return count;
	}
	
	private boolean isHiddenLocationFromPlayers(Location location)
	{
		for (Player player : location.getNearbyPlayers(PLAYER_MAX_RANGE))
		{
			Vector offset = player.getLocation().toVector().subtract(location.toVector());
			RayTraceResult result = player.getWorld().rayTraceBlocks(location, offset, PLAYER_MAX_RANGE, FluidCollisionMode.ALWAYS);
			if (result != null)
			{
				return false;
			}
		}
		return true;
	}
}
