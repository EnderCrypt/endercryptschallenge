/**************************************************************************\
* EnderCrypt's Challenge                                                   *
* Copyright (C) 2023 by EnderCrypt                                         *
*                                                                          *
* This program is free software: you can redistribute it and/or modify     *
* it under the terms of the GNU Affero General Public License as           *
* published by the Free Software Foundation, either version 3 of           *
* the License, or (at your option) any later version.                      *
*                                                                          *
* This program is distributed in the hope that it will be useful,          *
* but WITHOUT ANY WARRANTY; without even the implied warranty of           *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
* GNU Affero General Public License for more details.                      *
*                                                                          *
* You should have received a copy of the GNU Affero General Public License *
* along with this program in its root directory.                           *
* If not, see <http://www.gnu.org/licenses/>.                              *
\**************************************************************************/

package endercrypt.endercryptschallenge.features.entities.mobs.vex;


import endercrypt.endercryptschallenge.core.Core;
import endercrypt.endercryptschallenge.modules.features.MinecraftFeature;
import endercrypt.endercryptschallenge.utility.difficulty.Difficulties;
import endercrypt.endercryptschallenge.utility.difficulty.DifficultyRating;

import org.bukkit.entity.Player;
import org.bukkit.entity.Vex;
import org.bukkit.event.EventHandler;
import org.bukkit.event.entity.EntityDamageByEntityEvent;


/**
 * @author EnderCrypt
 */
public class VexStaminaDrain extends MinecraftFeature
{
	public VexStaminaDrain(Core core)
	{
		super(core);
	}
	
	@EventHandler
	public void onVexHit(EntityDamageByEntityEvent event)
	{
		if (event.getEntity() instanceof Player player && event.getDamager() instanceof Vex)
		{
			processVexHit(player);
		}
	}
	
	private void processVexHit(Player player)
	{
		DifficultyRating difficulty = Difficulties.getFor(player);
		final double amount = difficulty.asDoubleValue(0.1, 0.2, 0.5);
		
		resolve(player).stamina().mapStamina(stamina -> stamina - amount);
	}
}
