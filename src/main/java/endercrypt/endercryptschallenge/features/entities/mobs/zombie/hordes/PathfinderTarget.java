/**************************************************************************\
* EnderCrypt's Challenge                                                   *
* Copyright (C) 2023 by EnderCrypt                                         *
*                                                                          *
* This program is free software: you can redistribute it and/or modify     *
* it under the terms of the GNU Affero General Public License as           *
* published by the Free Software Foundation, either version 3 of           *
* the License, or (at your option) any later version.                      *
*                                                                          *
* This program is distributed in the hope that it will be useful,          *
* but WITHOUT ANY WARRANTY; without even the implied warranty of           *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
* GNU Affero General Public License for more details.                      *
*                                                                          *
* You should have received a copy of the GNU Affero General Public License *
* along with this program in its root directory.                           *
* If not, see <http://www.gnu.org/licenses/>.                              *
\**************************************************************************/

package endercrypt.endercryptschallenge.features.entities.mobs.zombie.hordes;


import endercrypt.endercryptschallenge.utility.Utility;

import org.bukkit.Location;
import org.bukkit.block.Block;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Mob;

import com.destroystokyo.paper.entity.Pathfinder;


/**
 * @author EnderCrypt
 */
public abstract class PathfinderTarget
{
	public static PathfinderTarget of(LivingEntity entity)
	{
		return new PathfinderTarget()
		{
			@Override
			public boolean setAsTargetFor(Pathfinder pathfinder)
			{
				return pathfinder.moveTo(entity);
			}
			
			@Override
			public Location getLocation()
			{
				return entity.getLocation();
			}
		};
	}
	
	public static PathfinderTarget of(Block block)
	{
		return of(Utility.getBlockFloorLocation(block));
	}
	
	public static PathfinderTarget of(Location location)
	{
		return new PathfinderTarget()
		{
			@Override
			public boolean setAsTargetFor(Pathfinder pathfinder)
			{
				return pathfinder.moveTo(location);
			}
			
			@Override
			public Location getLocation()
			{
				return location;
			}
		};
	}
	
	public boolean setAsTargetFor(Mob mob)
	{
		return setAsTargetFor(mob.getPathfinder());
	}
	
	public abstract boolean setAsTargetFor(Pathfinder pathfinder);
	
	public abstract Location getLocation();
}
