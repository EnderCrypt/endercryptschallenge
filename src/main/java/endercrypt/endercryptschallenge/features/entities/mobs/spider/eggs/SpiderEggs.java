/**************************************************************************\
* EnderCrypt's Challenge                                                   *
* Copyright (C) 2023 by EnderCrypt                                         *
*                                                                          *
* This program is free software: you can redistribute it and/or modify     *
* it under the terms of the GNU Affero General Public License as           *
* published by the Free Software Foundation, either version 3 of           *
* the License, or (at your option) any later version.                      *
*                                                                          *
* This program is distributed in the hope that it will be useful,          *
* but WITHOUT ANY WARRANTY; without even the implied warranty of           *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
* GNU Affero General Public License for more details.                      *
*                                                                          *
* You should have received a copy of the GNU Affero General Public License *
* along with this program in its root directory.                           *
* If not, see <http://www.gnu.org/licenses/>.                              *
\**************************************************************************/

package endercrypt.endercryptschallenge.features.entities.mobs.spider.eggs;


import endercrypt.endercryptschallenge.core.Core;
import endercrypt.endercryptschallenge.modules.features.MinecraftFeature;
import endercrypt.endercryptschallenge.utility.difficulty.Difficulties;
import endercrypt.endercryptschallenge.utility.difficulty.DifficultyRating;
import endercrypt.library.commons.misc.Ender;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Item;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.entity.Spider;
import org.bukkit.event.EventHandler;
import org.bukkit.event.entity.EntityPickupItemEvent;
import org.bukkit.event.entity.ItemDespawnEvent;
import org.bukkit.event.entity.ItemMergeEvent;
import org.bukkit.inventory.ItemStack;


/**
 * @author EnderCrypt
 */
public class SpiderEggs extends MinecraftFeature
{
	public static final Material REPRODUCTION_ITEM = Material.SPIDER_SPAWN_EGG;
	
	public SpiderEggs(Core core)
	{
		super(core);
	}
	
	@EventHandler
	public void onItemPickup(EntityPickupItemEvent event)
	{
		LivingEntity entity = event.getEntity();
		if (entity instanceof Player player)
		{
			Item item = event.getItem();
			ItemStack itemStack = item.getItemStack();
			if (itemStack.getType() == REPRODUCTION_ITEM)
			{
				// spawn spiders
				for (int i = 0; i < itemStack.getAmount(); i++)
				{
					Spider spider = item.getWorld().spawn(item.getLocation(), Spider.class);
					spider.setTarget(player);
					spider.getPassengers().forEach(spider::removePassenger);
				}
				
				// cancel event
				event.setCancelled(true);
				item.remove();
			}
		}
	}
	
	@EventHandler
	public void onItemDespawn(ItemDespawnEvent event)
	{
		ItemStack itemStack = event.getEntity().getItemStack();
		if (itemStack.getType() == REPRODUCTION_ITEM)
		{
			Location location = event.getEntity().getLocation();
			World world = location.getWorld();
			for (int i = 0; i < itemStack.getAmount(); i++)
			{
				world.spawn(location, Spider.class);
			}
		}
	}
	
	@EventHandler
	public void onItemMerge(ItemMergeEvent event)
	{
		Item source = event.getEntity();
		ItemStack sourceStack = source.getItemStack();
		Item target = event.getTarget();
		ItemStack targetStack = target.getItemStack();
		if (sourceStack.getType() == REPRODUCTION_ITEM && targetStack.getType() == REPRODUCTION_ITEM)
		{
			event.setCancelled(true);
		}
	}
	
	@Override
	public void onCycleEntity(Entity entity)
	{
		if (entity instanceof Spider spider)
		{
			processSpider(spider);
		}
	}
	
	private void processSpider(Spider spider)
	{
		DifficultyRating difficulty = Difficulties.getFor(spider);
		if (Math.random() < difficulty.asDoubleValue(0.02, 0.04, 0.06))
		{
			World world = spider.getWorld();
			int nearbySpiders = world.getNearbyEntitiesByType(Spider.class, spider.getLocation(), 50).size();
			if (nearbySpiders < 16)
			{
				int maxEggs = difficulty.asIntValue(3, 4, 5);
				world.dropItemNaturally(spider.getLocation(), new ItemStack(REPRODUCTION_ITEM, Ender.math.randomRange(1, maxEggs)));
			}
		}
	}
}
