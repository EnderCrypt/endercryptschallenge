/**************************************************************************\
* EnderCrypt's Challenge                                                   *
* Copyright (C) 2023 by EnderCrypt                                         *
*                                                                          *
* This program is free software: you can redistribute it and/or modify     *
* it under the terms of the GNU Affero General Public License as           *
* published by the Free Software Foundation, either version 3 of           *
* the License, or (at your option) any later version.                      *
*                                                                          *
* This program is distributed in the hope that it will be useful,          *
* but WITHOUT ANY WARRANTY; without even the implied warranty of           *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
* GNU Affero General Public License for more details.                      *
*                                                                          *
* You should have received a copy of the GNU Affero General Public License *
* along with this program in its root directory.                           *
* If not, see <http://www.gnu.org/licenses/>.                              *
\**************************************************************************/

package endercrypt.endercryptschallenge.features.entities.mobs.skeleton.arrowexplosion;


import endercrypt.endercryptschallenge.core.Core;
import endercrypt.endercryptschallenge.modules.features.MinecraftFeature;
import endercrypt.endercryptschallenge.utility.difficulty.Difficulties;
import endercrypt.library.commons.misc.Ender;

import org.bukkit.entity.AbstractArrow.PickupStatus;
import org.bukkit.entity.Arrow;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Skeleton;
import org.bukkit.event.EventHandler;
import org.bukkit.event.entity.EntityDeathEvent;
import org.bukkit.util.Vector;


/**
 * @author EnderCrypt
 */
public class SkeletonArrowExplosion extends MinecraftFeature
{
	public SkeletonArrowExplosion(Core core)
	{
		super(core);
	}
	
	@EventHandler
	public void onEntityDeath(EntityDeathEvent event)
	{
		Entity entity = event.getEntity();
		if (entity instanceof Skeleton skeleton)
		{
			spawnSkeletonArrows(skeleton);
		}
	}
	
	private void spawnSkeletonArrows(Skeleton skeleton)
	{
		int arrows = (int) Math.round(Ender.math.randomRange(0.75, 1) * Difficulties.getFor(skeleton).asIntValue(30, 40, 60));
		for (int i = 0; i < arrows; i++)
		{
			Arrow arrow = skeleton.getWorld().spawnArrow(skeleton.getEyeLocation(), new Vector(0, 10, 0), 1.5f, 32f);
			arrow.setPickupStatus(PickupStatus.CREATIVE_ONLY);
			arrow.setShooter(skeleton);
			if (skeleton.getFireTicks() > 0)
			{
				int ticks = skeleton.getFireTicks() + 20;
				ticks *= Ender.math.randomRange(0.75, 1.25);
				arrow.setFireTicks(ticks);
			}
		}
	}
}
