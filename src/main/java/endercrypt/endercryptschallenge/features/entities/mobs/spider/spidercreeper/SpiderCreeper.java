/**************************************************************************\
* EnderCrypt's Challenge                                                   *
* Copyright (C) 2023 by EnderCrypt                                         *
*                                                                          *
* This program is free software: you can redistribute it and/or modify     *
* it under the terms of the GNU Affero General Public License as           *
* published by the Free Software Foundation, either version 3 of           *
* the License, or (at your option) any later version.                      *
*                                                                          *
* This program is distributed in the hope that it will be useful,          *
* but WITHOUT ANY WARRANTY; without even the implied warranty of           *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
* GNU Affero General Public License for more details.                      *
*                                                                          *
* You should have received a copy of the GNU Affero General Public License *
* along with this program in its root directory.                           *
* If not, see <http://www.gnu.org/licenses/>.                              *
\**************************************************************************/

package endercrypt.endercryptschallenge.features.entities.mobs.spider.spidercreeper;


import endercrypt.endercryptschallenge.core.Core;
import endercrypt.endercryptschallenge.modules.features.MinecraftFeature;

import java.util.List;
import java.util.Optional;

import org.bukkit.entity.Creeper;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Spider;
import org.bukkit.event.EventHandler;
import org.bukkit.event.entity.CreatureSpawnEvent;
import org.bukkit.event.entity.CreatureSpawnEvent.SpawnReason;


/**
 * @author EnderCrypt
 */
public class SpiderCreeper extends MinecraftFeature
{
	public static final int CHANCE = 100; // 1 out of 100
	
	public SpiderCreeper(Core core)
	{
		super(core);
	}
	
	@Override
	public void onCycleEntity(Entity entity)
	{
		if (entity instanceof Spider == false)
			return;
		Spider spider = (Spider) entity;
		
		Creeper creeper = getCreeperRider(spider).orElse(null);
		if (creeper == null)
			return;
		
		spider.setTarget(creeper.getTarget());
	}
	
	@EventHandler
	public void onSpawn(CreatureSpawnEvent event)
	{
		if (event.getSpawnReason() != SpawnReason.NATURAL)
			return;
		
		if (Math.random() > 1.0 / CHANCE)
			return;
		
		if (event.getEntity() instanceof Spider == false)
			return;
		Spider spider = (Spider) event.getEntity();
		
		Creeper creeper = spider.getWorld().spawn(spider.getLocation(), Creeper.class);
		spider.addPassenger(creeper);
	}
	
	public Optional<Creeper> getCreeperRider(Spider spider)
	{
		List<Entity> passengers = spider.getPassengers();
		
		return passengers.stream()
			.filter(Creeper.class::isInstance)
			.map(Creeper.class::cast)
			.findFirst();
	}
}
