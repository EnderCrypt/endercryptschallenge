/**************************************************************************\
* EnderCrypt's Challenge                                                   *
* Copyright (C) 2023 by EnderCrypt                                         *
*                                                                          *
* This program is free software: you can redistribute it and/or modify     *
* it under the terms of the GNU Affero General Public License as           *
* published by the Free Software Foundation, either version 3 of           *
* the License, or (at your option) any later version.                      *
*                                                                          *
* This program is distributed in the hope that it will be useful,          *
* but WITHOUT ANY WARRANTY; without even the implied warranty of           *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
* GNU Affero General Public License for more details.                      *
*                                                                          *
* You should have received a copy of the GNU Affero General Public License *
* along with this program in its root directory.                           *
* If not, see <http://www.gnu.org/licenses/>.                              *
\**************************************************************************/

package endercrypt.endercryptschallenge.features.entities.mobs.zombie.hordes;


import java.util.List;

import org.bukkit.entity.Creeper;
import org.bukkit.entity.Monster;
import org.bukkit.entity.Skeleton;
import org.bukkit.entity.Spider;
import org.bukkit.entity.Zombie;


/**
 * @author EnderCrypt
 */
public class HordeConfiguration
{
	// if other entities are allowed to join this horde
	
	private boolean joinable = false;
	
	public void setJoinable(boolean joinable)
	{
		this.joinable = joinable;
	}
	
	public boolean isJoinable()
	{
		return joinable;
	}
	
	// what monster types are allowed to join the horde
	
	private List<Class<? extends Monster>> joinableSpecies = List.of(Zombie.class, Creeper.class, Skeleton.class, Spider.class);
	
	public void setJoinableSpecies(List<Class<? extends Monster>> joinableSpecies)
	{
		this.joinableSpecies = joinableSpecies;
	}
	
	public List<Class<? extends Monster>> joinableSpecies()
	{
		return joinableSpecies;
	}
	
	// if this horde is allowed to join with another merge-able horde
	
	private boolean mergeable = true;
	
	public void setMergeable(boolean mergeable)
	{
		this.mergeable = mergeable;
	}
	
	public boolean isMergeable()
	{
		return mergeable;
	}
	
	// entity join priority (may recruit from other hordes with lower priority)
	// 0 = non horde mobs
	// 100 = default
	
	private int priority = 100;
	
	public void setPriority(int priority)
	{
		this.priority = priority;
	}
	
	public int getPriority()
	{
		return priority;
	}
	
	// how far away the horde needs to be from a player do despawn (one monster at a time)
	
	private int despawnRange = 64;
	
	public void setDespawnRange(int despawnRange)
	{
		this.despawnRange = despawnRange;
	}
	
	public int getDespawnRange()
	{
		return despawnRange;
	}
}
