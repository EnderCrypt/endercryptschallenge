/**************************************************************************\
* EnderCrypt's Challenge                                                   *
* Copyright (C) 2023 by EnderCrypt                                         *
*                                                                          *
* This program is free software: you can redistribute it and/or modify     *
* it under the terms of the GNU Affero General Public License as           *
* published by the Free Software Foundation, either version 3 of           *
* the License, or (at your option) any later version.                      *
*                                                                          *
* This program is distributed in the hope that it will be useful,          *
* but WITHOUT ANY WARRANTY; without even the implied warranty of           *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
* GNU Affero General Public License for more details.                      *
*                                                                          *
* You should have received a copy of the GNU Affero General Public License *
* along with this program in its root directory.                           *
* If not, see <http://www.gnu.org/licenses/>.                              *
\**************************************************************************/

package endercrypt.endercryptschallenge.features.entities.mobs.creeper.cunning.chest;


import endercrypt.endercryptschallenge.core.Core;
import endercrypt.endercryptschallenge.features.entities.mobs.creeper.cunning.core.CunningCreeperCore;
import endercrypt.endercryptschallenge.materials.MaterialCollections;
import endercrypt.endercryptschallenge.modules.features.MinecraftFeature;
import endercrypt.endercryptschallenge.utility.ItemValues;
import endercrypt.endercryptschallenge.utility.Utility;
import endercrypt.library.commons.misc.Ender;

import org.bukkit.Location;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.entity.Minecart;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.InventoryHolder;


/**
 * @author EnderCrypt
 */
public class CunningCreeperChest extends MinecraftFeature
{
	public CunningCreeperChest(Core core)
	{
		super(core);
		getDebug().enableDebugSupport();
	}
	
	@EventHandler
	public void onClosingChest(InventoryCloseEvent event)
	{
		Inventory inventory = event.getInventory();
		Location location = Utility.getBlockFloorLocation(inventory.getLocation());
		
		if (event.getPlayer() instanceof Player player)
		{
			double base = ItemValues.get(inventory) * (1.0 / 10_000);
			double multiplier = 1.0 + getChestValueBoost(inventory);
			double punch = base * multiplier;
			
			debug(player, "Created chest-punch: " + Ender.math.round(punch, 3) + " due to the chest having a (base) value of " + Ender.math.round(base, 3) + " and getting a " + multiplier + " surroundings boost");
			getFeature(CunningCreeperCore.class).getTriggers().punch(location, punch);
		}
	}
	
	private double getChestValueBoost(Inventory inventory)
	{
		InventoryHolder inventoryHolder = inventory.getHolder();
		if (inventoryHolder instanceof Minecart)
		{
			return 16;
		}
		
		Location location = inventory.getLocation();
		Block center = location.getBlock();
		int count = 0;
		for (BlockFace direction : Utility.allblockFaces)
		{
			Block focus = center.getRelative(direction);
			if (MaterialCollections.storage.contains(focus))
			{
				count++;
			}
		}
		return count;
	}
}
