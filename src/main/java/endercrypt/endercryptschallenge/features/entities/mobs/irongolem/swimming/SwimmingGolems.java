/**************************************************************************\
* EnderCrypt's Challenge                                                   *
* Copyright (C) 2023 by EnderCrypt                                         *
*                                                                          *
* This program is free software: you can redistribute it and/or modify     *
* it under the terms of the GNU Affero General Public License as           *
* published by the Free Software Foundation, either version 3 of           *
* the License, or (at your option) any later version.                      *
*                                                                          *
* This program is distributed in the hope that it will be useful,          *
* but WITHOUT ANY WARRANTY; without even the implied warranty of           *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
* GNU Affero General Public License for more details.                      *
*                                                                          *
* You should have received a copy of the GNU Affero General Public License *
* along with this program in its root directory.                           *
* If not, see <http://www.gnu.org/licenses/>.                              *
\**************************************************************************/

package endercrypt.endercryptschallenge.features.entities.mobs.irongolem.swimming;


import endercrypt.endercryptschallenge.core.Core;
import endercrypt.endercryptschallenge.modules.features.MinecraftFeature;
import endercrypt.endercryptschallenge.utility.tracker.ObjectTracker;
import endercrypt.endercryptschallenge.utility.tracker.entity.ConditionAliveAndActive;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Entity;
import org.bukkit.entity.IronGolem;
import org.bukkit.util.Vector;


/**
 * idea by AtomicWarlock
 * 
 * @author EnderCrypt
 */
public class SwimmingGolems extends MinecraftFeature
{
	public static boolean isSwimmingGolem(IronGolem golem)
	{
		Location buoyancyLocation = golem.getLocation().add(0, 1, 0);
		return ConditionAliveAndActive.checkEntity(golem) && buoyancyLocation.getBlock().getType() == Material.WATER;
	}
	
	private final ObjectTracker<IronGolem> swimming = new ObjectTracker<>(IronGolem.class)
		.addCondition(SwimmingGolems::isSwimmingGolem);
	
	public SwimmingGolems(Core core)
	{
		super(core);
	}
	
	@Override
	public void onCycleEntity(Entity entity)
	{
		swimming.feed(entity);
	}
	
	@Override
	public void onTick()
	{
		for (IronGolem golem : swimming)
		{
			Vector motion = new Vector(0, 0.02, 0);
			golem.setVelocity(golem.getVelocity().add(motion));
		}
	}
}
