/**************************************************************************\
* EnderCrypt's Challenge                                                   *
* Copyright (C) 2023 by EnderCrypt                                         *
*                                                                          *
* This program is free software: you can redistribute it and/or modify     *
* it under the terms of the GNU Affero General Public License as           *
* published by the Free Software Foundation, either version 3 of           *
* the License, or (at your option) any later version.                      *
*                                                                          *
* This program is distributed in the hope that it will be useful,          *
* but WITHOUT ANY WARRANTY; without even the implied warranty of           *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
* GNU Affero General Public License for more details.                      *
*                                                                          *
* You should have received a copy of the GNU Affero General Public License *
* along with this program in its root directory.                           *
* If not, see <http://www.gnu.org/licenses/>.                              *
\**************************************************************************/

package endercrypt.endercryptschallenge.features.entities.mobs.creeper.cunning.core;


import endercrypt.endercryptschallenge.core.Core;
import endercrypt.endercryptschallenge.features.entities.mobs.creeper.cunning.core.triggers.TriggerSpot;
import endercrypt.endercryptschallenge.features.entities.mobs.creeper.cunning.core.triggers.TriggerSpotManager;
import endercrypt.endercryptschallenge.modules.features.MinecraftFeature;
import endercrypt.endercryptschallenge.utility.Utility;

import org.bukkit.Location;
import org.bukkit.entity.Creeper;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Monster;
import org.bukkit.event.EventHandler;
import org.bukkit.event.entity.EntityDeathEvent;
import org.bukkit.event.entity.EntityExplodeEvent;

import com.destroystokyo.paper.entity.Pathfinder;
import com.destroystokyo.paper.entity.Pathfinder.PathResult;


/**
 * @author EnderCrypt
 */
public class CunningCreeperCore extends MinecraftFeature
{
	private static final double CREEPER_EXPLOSION_DEFUSE = 2.0;
	
	private final TriggerSpotManager triggers = new TriggerSpotManager();
	
	public CunningCreeperCore(Core core)
	{
		super(core);
		getDebug().enableDebugSupport();
	}
	
	public TriggerSpotManager getTriggers()
	{
		return triggers;
	}
	
	@Override
	public void onTick()
	{
		triggers.updateDebug(isDebug());
	}
	
	@Override
	public void onSecond()
	{
		triggers.update();
	}
	
	@EventHandler
	public void onMonsterDeath(EntityDeathEvent event)
	{
		if (event.getEntity() instanceof Monster monster)
		{
			triggers.punch(monster);
		}
	}
	
	@EventHandler
	public void onCreeperExplode(EntityExplodeEvent event)
	{
		if (event.getEntity() instanceof Creeper == false)
		{
			return;
		}
		Creeper creeper = (Creeper) event.getEntity();
		
		if (creeper.isInWater())
			return;
		
		TriggerSpot spot = triggers.getTriggerSpot(creeper.getLocation()).orElse(null);
		if (spot == null)
		{
			return;
		}
		spot.punch(-CREEPER_EXPLOSION_DEFUSE);
	}
	
	@Override
	public void onCycleEntity(Entity entity)
	{
		if (entity instanceof Creeper creeper)
		{
			processCreeper(creeper);
		}
	}
	
	private void processCreeper(Creeper creeper)
	{
		// ignore busy creepers
		if (creeper.getTarget() != null)
		{
			return;
		}
		
		// target
		Location creeperLocation = creeper.getLocation();
		TriggerSpot spot = triggers.getAggroSpot(creeperLocation).orElse(null);
		if (spot == null)
		{
			return;
		}
		
		if (spot.isAngry() == false)
			return;
		
		if (isDebug())
		{
			Utility.debugParticles(creeperLocation);
		}
		
		// pathfind to spot
		Location target = spot.getLocation();
		Pathfinder pathfinder = creeper.getPathfinder();
		PathResult path = pathfinder.findPath(target);
		if (path != null)
		{
			double missing = path.getFinalPoint().distance(target); // TODO: solve unexpected exception:  java.lang.IllegalArgumentException: Cannot measure distance between world and world_nether
			if (missing <= spot.getAggroRange())
			{
				pathfinder.moveTo(path);
			}
		}
		
		// explode
		if (spot.isAngry() && spot.isWhitinTriggerRange(creeperLocation))
		{
			creeper.ignite();
		}
	}
}
