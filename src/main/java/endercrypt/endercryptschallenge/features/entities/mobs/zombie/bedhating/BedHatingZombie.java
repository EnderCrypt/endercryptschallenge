/**************************************************************************\
* EnderCrypt's Challenge                                                   *
* Copyright (C) 2023 by EnderCrypt                                         *
*                                                                          *
* This program is free software: you can redistribute it and/or modify     *
* it under the terms of the GNU Affero General Public License as           *
* published by the Free Software Foundation, either version 3 of           *
* the License, or (at your option) any later version.                      *
*                                                                          *
* This program is distributed in the hope that it will be useful,          *
* but WITHOUT ANY WARRANTY; without even the implied warranty of           *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
* GNU Affero General Public License for more details.                      *
*                                                                          *
* You should have received a copy of the GNU Affero General Public License *
* along with this program in its root directory.                           *
* If not, see <http://www.gnu.org/licenses/>.                              *
\**************************************************************************/

package endercrypt.endercryptschallenge.features.entities.mobs.zombie.bedhating;


import endercrypt.endercryptschallenge.core.Core;
import endercrypt.endercryptschallenge.modules.features.MinecraftFeature;

import org.bukkit.block.Block;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Monster;
import org.bukkit.entity.Player;


/**
 * @author EnderCrypt
 */
public class BedHatingZombie extends MinecraftFeature
{
	public BedHatingZombie(Core core)
	{
		super(core);
	}
	
	@Override
	public void onCycleEntity(Entity entity)
	{
		if (entity instanceof Monster monster)
		{
			processZombie(monster);
		}
	}
	
	private void processZombie(Monster monster)
	{
		LivingEntity target = monster.getTarget();
		if (target == null)
			return;
		
		if (target instanceof Player == false)
			return;
		
		Player player = (Player) target;
		if (player.getWorld().equals(monster.getWorld()) == false)
			return;
		
		if (player.isSleeping() == false)
			return;
		
		if (monster.getLocation().distance(player.getLocation()) > 3)
			return;
		
		Block block = player.getLocation().getBlock();
		block.breakNaturally();
		player.sendMessage("The " + monster.getName() + " rips out the bed from under you");
	}
}
