/**************************************************************************\
* EnderCrypt's Challenge                                                   *
* Copyright (C) 2023 by EnderCrypt                                         *
*                                                                          *
* This program is free software: you can redistribute it and/or modify     *
* it under the terms of the GNU Affero General Public License as           *
* published by the Free Software Foundation, either version 3 of           *
* the License, or (at your option) any later version.                      *
*                                                                          *
* This program is distributed in the hope that it will be useful,          *
* but WITHOUT ANY WARRANTY; without even the implied warranty of           *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
* GNU Affero General Public License for more details.                      *
*                                                                          *
* You should have received a copy of the GNU Affero General Public License *
* along with this program in its root directory.                           *
* If not, see <http://www.gnu.org/licenses/>.                              *
\**************************************************************************/

package endercrypt.endercryptschallenge.features.entities.mobs.bee.worm;


import endercrypt.endercryptschallenge.core.Core;
import endercrypt.endercryptschallenge.features.entities.mobs.bee.worm.creature.Segment;
import endercrypt.endercryptschallenge.features.entities.mobs.bee.worm.creature.Worm;
import endercrypt.endercryptschallenge.modules.features.DisabledFeature;
import endercrypt.endercryptschallenge.modules.features.MinecraftFeature;
import endercrypt.endercryptschallenge.utility.Utility;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Optional;
import java.util.Set;

import org.bukkit.Location;
import org.bukkit.entity.Bee;
import org.bukkit.event.EventHandler;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;


/**
 * @author EnderCrypt
 */
@DisabledFeature
public class BeeWorms extends MinecraftFeature
{
	private final Set<Worm> worms = new HashSet<>();
	
	public BeeWorms(Core core)
	{
		super(core);
	}
	
	public Optional<Segment> getSegment(Bee bee)
	{
		for (Worm worm : worms)
		{
			for (Segment segment : worm)
			{
				if (segment.getEntity().equals(bee))
				{
					return Optional.of(segment);
				}
			}
		}
		return Optional.empty();
	}
	
	public Optional<Worm> getWorm(Bee bee)
	{
		return getSegment(bee).map(Segment::getWorm);
	}
	
	public Worm spawn(Location location, int size)
	{
		Worm worm = new Worm(location, size);
		worms.add(worm);
		return worm;
	}
	
	@EventHandler
	public void temp(BlockBreakEvent event)
	{
		// TODO: remove testing code
		spawn(Utility.getBlockFloorLocation(event.getBlock()), 5);
	}
	
	@Override
	public void onTick()
	{
		Iterator<Worm> iterator = worms.iterator();
		while (iterator.hasNext())
		{
			Worm worm = iterator.next();
			worm.update();
			if (worm.isDead())
			{
				iterator.remove();
			}
		}
	}
	
	@EventHandler
	public void onBeeHurt(EntityDamageEvent event)
	{
		if (event.getEntity() instanceof Bee bee)
		{
			Segment segment = getSegment(bee).orElse(null);
			if (segment != null)
			{
				if (event.getCause() != DamageCause.SUFFOCATION)
				{
					segment.damage(event.getDamage());
				}
				event.setCancelled(true);
			}
		}
	}
}
