/**************************************************************************\
* EnderCrypt's Challenge                                                   *
* Copyright (C) 2023 by EnderCrypt                                         *
*                                                                          *
* This program is free software: you can redistribute it and/or modify     *
* it under the terms of the GNU Affero General Public License as           *
* published by the Free Software Foundation, either version 3 of           *
* the License, or (at your option) any later version.                      *
*                                                                          *
* This program is distributed in the hope that it will be useful,          *
* but WITHOUT ANY WARRANTY; without even the implied warranty of           *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
* GNU Affero General Public License for more details.                      *
*                                                                          *
* You should have received a copy of the GNU Affero General Public License *
* along with this program in its root directory.                           *
* If not, see <http://www.gnu.org/licenses/>.                              *
\**************************************************************************/

package endercrypt.endercryptschallenge.features.entities.mobs.creeper.cunning.core.triggers;


import endercrypt.endercryptschallenge.utility.Utility;

import java.util.Objects;
import java.util.Optional;

import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.util.Vector;


/**
 * @author EnderCrypt
 */
public abstract class TriggerSpot
{
	private final Location location;
	
	protected TriggerSpot(Location location)
	{
		this.location = Objects.requireNonNull(location, "location");
	}
	
	public World getWorld()
	{
		return getLocation().getWorld();
	}
	
	public Location getLocation()
	{
		return location;
	}
	
	protected abstract TriggerSpot createMergedTriggerSpot(Location location);
	
	public abstract void punch(double strength);
	
	public abstract double getStrength();
	
	public abstract boolean isDecayed();
	
	public abstract void updateDebug(boolean enabled);
	
	public abstract void update();
	
	public Optional<TriggerSpot> attemptMerge(TriggerSpot other)
	{
		if (getWorld() != other.getWorld())
		{
			throw new IllegalArgumentException(other + " is not in a compatible world");
		}
		if (other == this)
		{
			return Optional.empty();
		}
		
		double range = getTriggerRange() + other.getTriggerRange();
		double distance = Utility.roughDistance(getLocation(), other.getLocation());
		if (distance > range)
		{
			return Optional.empty();
		}
		
		Vector middle = other.getLocation().toVector().subtract(getLocation().toVector());
		double totalStrength = getStrength() + other.getStrength();
		double push = (1.0 / totalStrength) * getStrength();
		middle.multiply(1.0 - push);
		
		Location mergeLocation = getLocation().clone().add(middle);
		
		TriggerSpot merged = createMergedTriggerSpot(mergeLocation);
		merged.punch(totalStrength);
		return Optional.of(merged);
	}
	
	public abstract boolean isSuperAngry();
	
	public abstract boolean isAngry();
	
	public abstract double getTriggerRange();
	
	public final boolean isWhitinTriggerRange(Location location)
	{
		return Utility.roughDistance(getLocation(), location) <= getTriggerRange();
	}
	
	public abstract double getAggroRange();
	
	public final boolean isWhitinAggroRange(Location location)
	{
		return Utility.roughDistance(getLocation(), location) <= getAggroRange();
	}
	
	public abstract void delete();
	
	@Override
	public String toString()
	{
		Vector vector = getLocation().toVector();
		int x = (int) vector.getX();
		int y = (int) vector.getY();
		int z = (int) vector.getZ();
		return getStrength() + " -> " + x + "," + y + "," + z;
	}
}
