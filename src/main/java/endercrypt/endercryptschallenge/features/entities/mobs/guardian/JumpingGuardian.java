/**************************************************************************\
* EnderCrypt's Challenge                                                   *
* Copyright (C) 2023 by EnderCrypt                                         *
*                                                                          *
* This program is free software: you can redistribute it and/or modify     *
* it under the terms of the GNU Affero General Public License as           *
* published by the Free Software Foundation, either version 3 of           *
* the License, or (at your option) any later version.                      *
*                                                                          *
* This program is distributed in the hope that it will be useful,          *
* but WITHOUT ANY WARRANTY; without even the implied warranty of           *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
* GNU Affero General Public License for more details.                      *
*                                                                          *
* You should have received a copy of the GNU Affero General Public License *
* along with this program in its root directory.                           *
* If not, see <http://www.gnu.org/licenses/>.                              *
\**************************************************************************/

package endercrypt.endercryptschallenge.features.entities.mobs.guardian;


import endercrypt.endercryptschallenge.core.Core;
import endercrypt.endercryptschallenge.features.entities.mobs.guardian.behavior.GuardianBehaviorSystem;
import endercrypt.endercryptschallenge.modules.features.MinecraftFeature;
import endercrypt.endercryptschallenge.utility.Utility;
import endercrypt.endercryptschallenge.utility.tracker.ObjectTracker;

import java.util.Optional;

import org.bukkit.Material;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Guardian;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;

import lombok.Getter;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;


/**
 * @author EnderCrypt
 */
public class JumpingGuardian extends MinecraftFeature
{
	public static final double DETECTION_RANGE = 30;
	public static final double FREQUENCY_SECONDS = 7.5; // chance of 1 per x seconds
	
	private final ObjectTracker<ControlledGuardian> entities = new ObjectTracker<>(ControlledGuardian.class)
		.addCondition(ControlledGuardian::isRelevant);
	
	public JumpingGuardian(Core core)
	{
		super(core);
	}
	
	@Override
	public void onCycleEntity(Entity entity)
	{
		if (Math.random() < 1.0 / FREQUENCY_SECONDS && entity instanceof Guardian guardian)
		{
			entities.feed(new ControlledGuardian(guardian));
		}
	}
	
	@Override
	public void onTick()
	{
		entities.forEach(ControlledGuardian::update);
	}
	
	private static Optional<? extends Player> getVictim(Guardian guardian)
	{
		return guardian
			.getWorld()
			.getPlayers()
			.stream()
			.filter(player -> isEligebleVictim(guardian, player))
			.findFirst();
	}
	
	private static boolean isEligebleVictim(Guardian guardian, Player player)
	{
		if (Utility.isInSurvival(player) == false)
			return false;
		
		if (Utility.roughDistance(player.getLocation(), guardian.getLocation()) > DETECTION_RANGE)
			return false;
		
		if (player.getEyeLocation().getBlock().getType() == Material.WATER)
			return false;
		
		if (Utility.hasWaterUnderEntity(player, 3) == false)
			return false;
		
		return true;
	}
	
	@RequiredArgsConstructor
	@Getter
	private static class ControlledGuardian
	{
		private static final GuardianBehaviorSystem guardianBehaviorSystem = new GuardianBehaviorSystem();
		
		@NonNull
		private final Guardian guardian;
		private int lifetime = 0;
		private LivingEntity target;
		
		public void update()
		{
			lifetime++;
			target = getVictim(guardian).orElse(null);
			if (target == null)
				return;
			guardianBehaviorSystem.execute(guardian, target);
		}
		
		public boolean isRelevant()
		{
			if (target == null && lifetime > 0)
				return false;
			
			if (target != null && guardian.getLocation().getY() > target.getLocation().getY() + 1)
				return false;
			
			return true;
		}
	}
}
