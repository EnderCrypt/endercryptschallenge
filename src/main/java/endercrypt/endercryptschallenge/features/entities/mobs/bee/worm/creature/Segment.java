/**************************************************************************\
* EnderCrypt's Challenge                                                   *
* Copyright (C) 2023 by EnderCrypt                                         *
*                                                                          *
* This program is free software: you can redistribute it and/or modify     *
* it under the terms of the GNU Affero General Public License as           *
* published by the Free Software Foundation, either version 3 of           *
* the License, or (at your option) any later version.                      *
*                                                                          *
* This program is distributed in the hope that it will be useful,          *
* but WITHOUT ANY WARRANTY; without even the implied warranty of           *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
* GNU Affero General Public License for more details.                      *
*                                                                          *
* You should have received a copy of the GNU Affero General Public License *
* along with this program in its root directory.                           *
* If not, see <http://www.gnu.org/licenses/>.                              *
\**************************************************************************/

package endercrypt.endercryptschallenge.features.entities.mobs.bee.worm.creature;


import endercrypt.endercryptschallenge.utility.Utility;

import java.util.Objects;
import java.util.Optional;

import org.bukkit.Location;
import org.bukkit.entity.Bee;
import org.bukkit.entity.LivingEntity;
import org.bukkit.util.Vector;


/**
 * @author EnderCrypt
 */
public class Segment
{
	public static final String NAME = "Bee Worm";
	public static final Class<? extends LivingEntity> ENTITY_CLASS = Bee.class;
	private static final double MAX_SPEED = 0.25;
	
	private final Worm worm;
	private final int id;
	private final LivingEntity entity;
	private Vector motion = new Vector(0, 0, 0);
	
	protected Segment(Worm worm, int id, Location location)
	{
		if (id < 0)
		{
			throw new IllegalArgumentException("id cannot be " + id);
		}
		
		this.worm = Objects.requireNonNull(worm, "worm");
		this.id = id;
		this.entity = location.getWorld().spawn(location, ENTITY_CLASS);
		this.entity.setAI(false);
		
		if (isHead())
		{
			this.entity.setCustomName(NAME);
			this.entity.setCustomNameVisible(true);
		}
	}
	
	public Worm getWorm()
	{
		return worm;
	}
	
	public LivingEntity getEntity()
	{
		return entity;
	}
	
	public boolean isHead()
	{
		return id == 0;
	}
	
	public Optional<Segment> getNextSegment()
	{
		if (isHead())
			return Optional.empty();
		
		return Optional.of(worm.segments.get(id - 1));
	}
	
	public void damage(double damage)
	{
		getWorm().damage(damage);
	}
	
	public boolean isOnGround()
	{
		return entity.getLocation()
			.getBlock()
			.isSolid();
	}
	
	public void update()
	{
		// main
		if (getWorm().isDead())
		{
			entity.setHealth(0);
		}
		
		// body
		getNextSegment().ifPresentOrElse(this::updateBody, this::updateHead);
	}
	
	private void updateHead()
	{
		// random movement
		motion.add(Utility.getRandomVector().multiply(0.05));
		
		// gravity
		double gravity = -0.075 * (1 - getWorm().getGroundTouch());
		// entity.getWorld().getPlayers().forEach(p -> p.sendMessage("gravity: " + gravity));
		motion.add(new Vector(0, gravity, 0));
		if (getWorm().getGroundTouch() > 0 && motion.getY() < 0.1)
		{
			motion.setY(0.1);
		}
		
		// friction
		motion.multiply(0.975);
		
		// clamp
		if (motion.length() > MAX_SPEED)
		{
			motion.multiply(1.0 / motion.length() * MAX_SPEED);
		}
		
		// update motion
		Location targetLocation = entity.getLocation().add(motion);
		targetLocation.setPitch((float) Math.toDegrees(Utility.getPitch(motion)) - 90);
		targetLocation.setYaw((float) Math.toDegrees(Utility.getFlatAngle(motion)) - 90);
		entity.teleport(targetLocation);
	}
	
	private void updateBody(Segment next)
	{
		Vector offset = next.entity.getLocation().subtract(entity.getLocation()).toVector();
		final double distance = 0.75;
		double change = offset.length() - distance;
		Vector expected = offset.clone().multiply(1.0 / distance * change);
		Location targetLocation = entity.getLocation();
		targetLocation.add(expected);
		targetLocation.setPitch((float) Math.toDegrees(Utility.getPitch(offset)) - 90f);
		targetLocation.setYaw((float) Math.toDegrees(Utility.getFlatAngle(offset)) - 90f);
		entity.teleport(targetLocation);
	}
}
