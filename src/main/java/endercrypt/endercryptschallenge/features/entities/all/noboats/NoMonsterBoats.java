/**************************************************************************\
* EnderCrypt's Challenge                                                   *
* Copyright (C) 2023 by EnderCrypt                                         *
*                                                                          *
* This program is free software: you can redistribute it and/or modify     *
* it under the terms of the GNU Affero General Public License as           *
* published by the Free Software Foundation, either version 3 of           *
* the License, or (at your option) any later version.                      *
*                                                                          *
* This program is distributed in the hope that it will be useful,          *
* but WITHOUT ANY WARRANTY; without even the implied warranty of           *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
* GNU Affero General Public License for more details.                      *
*                                                                          *
* You should have received a copy of the GNU Affero General Public License *
* along with this program in its root directory.                           *
* If not, see <http://www.gnu.org/licenses/>.                              *
\**************************************************************************/

package endercrypt.endercryptschallenge.features.entities.all.noboats;


import endercrypt.endercryptschallenge.core.Core;
import endercrypt.endercryptschallenge.features.entities.all.boatdriver.BoatDriver;
import endercrypt.endercryptschallenge.modules.features.MinecraftFeature;
import endercrypt.endercryptschallenge.utility.difficulty.Difficulties;
import endercrypt.endercryptschallenge.utility.difficulty.DifficultyRating;

import org.bukkit.entity.Boat;
import org.bukkit.entity.Enderman;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Monster;
import org.bukkit.entity.Vehicle;
import org.bukkit.event.EventHandler;
import org.bukkit.event.entity.EntityDamageByEntityEvent;


/**
 * @author EnderCrypt
 */
public class NoMonsterBoats extends MinecraftFeature
{
	public NoMonsterBoats(Core core)
	{
		super(core);
	}
	
	@Override
	public void onCycleEntity(Entity entity)
	{
		if (entity instanceof Vehicle vehicle)
		{
			processVehicle(vehicle);
		}
	}
	
	private void processVehicle(Vehicle vehicle)
	{
		for (Entity entity : vehicle.getPassengers())
		{
			if (entity instanceof Monster monster)
			{
				processMonster(monster);
			}
		}
	}
	
	private void processMonster(Monster monster)
	{
		DifficultyRating difficulty = Difficulties.getFor(monster);
		boolean isDriver = monster.getVehicle() instanceof Boat boat && BoatDriver.isDrivableBoat(boat);
		double chance = difficulty.asDoubleValue(0.1, 0.2, 0.3) * (isDriver ? 0.25 : 1.0);
		if (Math.random() > chance)
		{
			return;
		}
		ejectMonster(monster);
	}
	
	private static boolean ejectMonster(Monster monster)
	{
		if (monster.isInsideVehicle() == false)
		{
			return false;
		}
		if (monster.getVehicle() instanceof Vehicle == false)
		{
			return false;
		}
		Vehicle vehicle = (Vehicle) monster.getVehicle();
		vehicle.removePassenger(monster);
		return true;
	}
	
	@EventHandler
	public void onEntityDamageByEntity(EntityDamageByEntityEvent event)
	{
		Entity defender = event.getEntity();
		if (defender instanceof Monster monster && ejectMonster(monster))
		{
			// retaliate
			if (event.getDamager() instanceof LivingEntity livingAttacker)
			{
				monster.setTarget(livingAttacker);
				
				// teleport enderman randomly
				if (defender instanceof Enderman enderman)
				{
					enderman.teleportRandomly();
				}
			}
		}
	}
}
