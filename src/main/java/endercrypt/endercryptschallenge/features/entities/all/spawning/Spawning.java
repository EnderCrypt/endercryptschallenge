/**************************************************************************\
* EnderCrypt's Challenge                                                   *
* Copyright (C) 2023 by EnderCrypt                                         *
*                                                                          *
* This program is free software: you can redistribute it and/or modify     *
* it under the terms of the GNU Affero General Public License as           *
* published by the Free Software Foundation, either version 3 of           *
* the License, or (at your option) any later version.                      *
*                                                                          *
* This program is distributed in the hope that it will be useful,          *
* but WITHOUT ANY WARRANTY; without even the implied warranty of           *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
* GNU Affero General Public License for more details.                      *
*                                                                          *
* You should have received a copy of the GNU Affero General Public License *
* along with this program in its root directory.                           *
* If not, see <http://www.gnu.org/licenses/>.                              *
\**************************************************************************/

package endercrypt.endercryptschallenge.features.entities.all.spawning;


import endercrypt.endercryptschallenge.core.Core;
import endercrypt.endercryptschallenge.modules.features.MinecraftFeature;
import endercrypt.endercryptschallenge.modules.worlds.WorldProfile;
import endercrypt.endercryptschallenge.modules.worlds.spawner.factory.SpawnAutomator;
import endercrypt.endercryptschallenge.modules.worlds.spawner.factory.factories.SpawnerFactory;
import endercrypt.endercryptschallenge.utility.CodeTimer;

import java.util.Set;

import org.bukkit.Location;
import org.bukkit.entity.Player;


/**
 * @author EnderCrypt
 */
public class Spawning extends MinecraftFeature
{
	public Spawning(Core core)
	{
		super(core);
		getDebug().enableDebugSupport();
	}
	
	@Override
	public void onSecond()
	{
		debug(this::printDebug);
		
		CodeTimer timer = new CodeTimer("Spawning", logger);
		getCore().getWorldManager().forEach(this::processWorld);
		timer.check(20);
	}
	
	private String printDebug(Player player)
	{
		return "Spawners: " + resolve(player.getWorld()).getSpawners();
	}
	
	private void processWorld(WorldProfile world)
	{
		Set<SpawnerFactory> spawnerFactories = world.getSpawners();
		for (Player player : world.getPlayers())
		{
			for (SpawnerFactory spawnerFactory : spawnerFactories)
			{
				spawn(player, spawnerFactory);
			}
		}
	}
	
	private boolean spawn(Player player, SpawnerFactory spawnerFactory)
	{
		SpawnAutomator spawnAutomator = spawnerFactory.generate(player);
		
		Location location = spawnAutomator.findSpawnSpot(player).orElse(null);
		if (location == null)
			return false;
		
		spawnerFactory.spawn(player, location);
		return true;
	}
}
