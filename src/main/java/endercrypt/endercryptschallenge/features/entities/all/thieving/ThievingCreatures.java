/**************************************************************************\
* EnderCrypt's Challenge                                                   *
* Copyright (C) 2023 by EnderCrypt                                         *
*                                                                          *
* This program is free software: you can redistribute it and/or modify     *
* it under the terms of the GNU Affero General Public License as           *
* published by the Free Software Foundation, either version 3 of           *
* the License, or (at your option) any later version.                      *
*                                                                          *
* This program is distributed in the hope that it will be useful,          *
* but WITHOUT ANY WARRANTY; without even the implied warranty of           *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
* GNU Affero General Public License for more details.                      *
*                                                                          *
* You should have received a copy of the GNU Affero General Public License *
* along with this program in its root directory.                           *
* If not, see <http://www.gnu.org/licenses/>.                              *
\**************************************************************************/

package endercrypt.endercryptschallenge.features.entities.all.thieving;


import endercrypt.endercryptschallenge.core.Core;
import endercrypt.endercryptschallenge.modules.features.MinecraftFeature;
import endercrypt.endercryptschallenge.utility.Utility;
import endercrypt.library.commons.RandomEntry;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.block.BlockState;
import org.bukkit.block.Chest;
import org.bukkit.block.Container;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Mob;
import org.bukkit.entity.Pillager;
import org.bukkit.entity.Skeleton;
import org.bukkit.entity.WitherSkeleton;
import org.bukkit.entity.Zombie;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;


/**
 * @author EnderCrypt
 */
public class ThievingCreatures extends MinecraftFeature
{
	private static final Set<Class<?>> thieves = Set.of(Zombie.class, Skeleton.class, Pillager.class, WitherSkeleton.class);
	
	private static final List<BlockFace> grabDirections = Utility.cartesianBlockFaces
		.stream()
		.filter(d -> d.getModY() <= 0)
		.collect(Collectors.toUnmodifiableList());
	
	public ThievingCreatures(Core core)
	{
		super(core);
		
		List<Class<?>> invalidClasses = thieves.stream()
			.filter(c -> Mob.class.isAssignableFrom(c) == false)
			.collect(Collectors.toUnmodifiableList());
		
		if (invalidClasses.isEmpty() == false)
		{
			throw new IllegalArgumentException("cannot cast to non-mob class: " + invalidClasses);
		}
	}
	
	@Override
	public void onCycleEntity(Entity entity)
	{
		if (Utility.isOfClass(entity.getClass(), thieves))
		{
			processThief((Mob) entity);
		}
	}
	
	private void processThief(Mob mob)
	{
		Block blockLocation = mob.getLocation().add(0, 0.5, 0).getBlock();
		
		for (BlockFace face : grabDirections)
		{
			Block target = blockLocation.getRelative(face);
			if (thiefTrySteal(mob, target))
			{
				return;
			}
		}
	}
	
	private boolean thiefTrySteal(Mob mob, Block target)
	{
		// block
		BlockState blockState = target.getState();
		if (blockState instanceof Container == false)
			return false;
		
		Container container = (Container) blockState;
		
		// inventory
		HandSlots handSlots = new HandSlots(mob.getEquipment());
		if (handSlots.hasOpenSlots() == false)
			return true;
		
		boolean shouldClose = container instanceof Chest chest && chest.isOpen() == false;
		
		boolean reachedEnd = tryTakeFromContainer(container, handSlots);
		
		if (container instanceof Chest chest && chest.isOpen() && shouldClose)
		{
			schedule(10, chest::close);
		}
		
		return reachedEnd;
	}
	
	private boolean tryTakeFromContainer(Container container, HandSlots handSlots)
	{
		Inventory inventory = container.getInventory();
		ItemStack stack = RandomEntry.from(inventory.getContents());
		
		if (stack == null || stack.getType() == Material.AIR)
		{
			return false;
		}
		
		if (handSlots.hasOpenSlots() == false)
		{
			return true;
		}
		
		handSlots.putItem(stack.clone());
		stack.setAmount(0);
		if (container instanceof Chest chest)
		{
			chest.open();
		}
		
		return false;
	}
}
