/**************************************************************************\
* EnderCrypt's Challenge                                                   *
* Copyright (C) 2023 by EnderCrypt                                         *
*                                                                          *
* This program is free software: you can redistribute it and/or modify     *
* it under the terms of the GNU Affero General Public License as           *
* published by the Free Software Foundation, either version 3 of           *
* the License, or (at your option) any later version.                      *
*                                                                          *
* This program is distributed in the hope that it will be useful,          *
* but WITHOUT ANY WARRANTY; without even the implied warranty of           *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
* GNU Affero General Public License for more details.                      *
*                                                                          *
* You should have received a copy of the GNU Affero General Public License *
* along with this program in its root directory.                           *
* If not, see <http://www.gnu.org/licenses/>.                              *
\**************************************************************************/

package endercrypt.endercryptschallenge.features.entities.all.growing;


import endercrypt.endercryptschallenge.utility.Utility;

import java.util.Objects;

import org.bukkit.Location;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.entity.Mob;
import org.bukkit.util.Vector;


/**
 * @author EnderCrypt
 */
public class Grower
{
	private final Mob mob;
	private final Block targetBlock;
	private final double growthTime;
	private final Location start;
	private final Location end;
	
	private double time = 0;
	
	public Grower(Class<? extends Mob> mob, Block targetBlock, double growthTime)
	{
		this.targetBlock = Objects.requireNonNull(targetBlock, "targetBlock");
		this.growthTime = growthTime;
		if (growthTime <= 0)
		{
			throw new IllegalArgumentException("growth time cannot be " + growthTime);
		}
		
		int monsterHeight = 2; // (int) Math.ceil(mob.getHeight());
		this.end = Utility.getBlockFloorLocation(targetBlock);
		this.start = Utility.getBlockFloorLocation(end.getBlock().getRelative(BlockFace.DOWN, monsterHeight));
		this.mob = targetBlock.getWorld().spawn(start, mob);
		
		update();
	}
	
	public Mob getMob()
	{
		return mob;
	}
	
	public Block getTargetBlock()
	{
		return targetBlock;
	}
	
	public double getGrowthTime()
	{
		return growthTime;
	}
	
	public boolean isDone()
	{
		return time >= growthTime || mob.isDead() || mob.isValid() == false;
	}
	
	protected void update()
	{
		time += (1.0 / 20);
		Vector offset = end.toVector().subtract(start.toVector());
		double progress = 1.0 / growthTime * time;
		Vector slice = new Vector(offset.getX() * progress, offset.getY() * progress, offset.getZ() * progress);
		Vector current = slice.clone().multiply(progress);
		mob.teleport(start.clone().add(current));
	}
}
