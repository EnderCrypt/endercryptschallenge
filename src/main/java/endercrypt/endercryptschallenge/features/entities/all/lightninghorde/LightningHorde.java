/**************************************************************************\
* EnderCrypt's Challenge                                                   *
* Copyright (C) 2023 by EnderCrypt                                         *
*                                                                          *
* This program is free software: you can redistribute it and/or modify     *
* it under the terms of the GNU Affero General Public License as           *
* published by the Free Software Foundation, either version 3 of           *
* the License, or (at your option) any later version.                      *
*                                                                          *
* This program is distributed in the hope that it will be useful,          *
* but WITHOUT ANY WARRANTY; without even the implied warranty of           *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
* GNU Affero General Public License for more details.                      *
*                                                                          *
* You should have received a copy of the GNU Affero General Public License *
* along with this program in its root directory.                           *
* If not, see <http://www.gnu.org/licenses/>.                              *
\**************************************************************************/

package endercrypt.endercryptschallenge.features.entities.all.lightninghorde;


import endercrypt.endercryptschallenge.core.Core;
import endercrypt.endercryptschallenge.features.entities.mobs.zombie.hordes.Horde;
import endercrypt.endercryptschallenge.features.entities.mobs.zombie.hordes.HordeManager;
import endercrypt.endercryptschallenge.modules.features.MinecraftFeature;
import endercrypt.endercryptschallenge.utility.Utility;
import endercrypt.endercryptschallenge.utility.difficulty.Difficulties;
import endercrypt.endercryptschallenge.utility.difficulty.DifficultyRating;
import endercrypt.library.commons.misc.Ender;

import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.entity.Creeper;
import org.bukkit.entity.Monster;
import org.bukkit.entity.Player;
import org.bukkit.entity.Skeleton;
import org.bukkit.entity.WitherSkeleton;
import org.bukkit.entity.Zombie;
import org.bukkit.event.EventHandler;
import org.bukkit.event.weather.LightningStrikeEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.util.Vector;


/**
 * @author EnderCrypt
 */
public class LightningHorde extends MinecraftFeature
{
	public LightningHorde(Core core)
	{
		super(core);
	}
	
	@EventHandler
	public void onLightningStrike(LightningStrikeEvent event)
	{
		Location location = event.getLightning().getLocation();
		DifficultyRating difficulty = Difficulties.getFor(location);
		int enemies = (int) (Ender.math.randomRange(0.5, 1.0) * difficulty.asDoubleValue(30, 50, 70));
		
		// spawn
		Horde horde = core.getMinecraftFeatureManager().getFeature(HordeManager.class).spawnHorde(location, enemies, this::generateMonster);
		horde.configuration().setJoinable(true);
		
		// processM
		for (Monster monster : horde.getMonsters())
		{
			monster.addPotionEffect(new PotionEffect(PotionEffectType.SLOW_FALLING, 20 * 30, 3));
			monster.addPotionEffect(new PotionEffect(PotionEffectType.FIRE_RESISTANCE, 20 * 15, 3));
			monster.addPotionEffect(new PotionEffect(PotionEffectType.RESISTANCE, 20 * 1, 10));
			
			double rx = Ender.math.randomRange(-0.1, 0.1);
			double ry = Ender.math.randomRange(-0.1, 0.1);
			double rz = Ender.math.randomRange(-0.1, 0.1);
			
			Location offset = location.clone()
				.subtract(monster.getLocation())
				.add(rx, ry, rz);
			
			Vector velocity = Utility.superNormalize(offset.toVector())
				.multiply(1.5)
				.add(new Vector(0, 0.5, 0));
			monster.setVelocity(velocity);
		}
		
		// alert
		int radius = difficulty.asIntValue(50, 30, 20, 10);
		for (Player player : location.getWorld().getNearbyPlayers(location, radius))
		{
			double distance = Utility.roughDistance(player.getLocation(), location);
			String distanceMessaage = distance <= 5 ? ChatColor.RED + "right next to you, run!!!" : "nearby, be cautious!";
			player.sendMessage(ChatColor.DARK_RED + "WARNING:" + ChatColor.RESET + " A horde of monsters have appeared " + distanceMessaage);
		}
	}
	
	private Monster generateMonster(Location location)
	{
		World world = location.getWorld();
		
		location = generateOffsetLocation(location);
		
		if (Math.random() < 0.1)
		{
			return world.spawn(location, Skeleton.class);
		}
		if (Math.random() < 0.1)
		{
			return world.spawn(location, Creeper.class);
		}
		if (Math.random() < 0.1)
		{
			return world.spawn(location, WitherSkeleton.class);
		}
		
		Zombie zombie = world.spawn(location, Zombie.class);
		if (Math.random() < 0.5)
		{
			zombie.getEquipment().setItemInMainHand(new ItemStack(Material.STONE_PICKAXE));
		}
		return zombie;
	}
	
	private Location generateOffsetLocation(Location location)
	{
		for (int i = 0; i < 5; i++)
		{
			Location potential = location.clone().add(
				Ender.math.randomRange(-5, 5),
				Ender.math.randomRange(-2, 3),
				Ender.math.randomRange(-5, 5));
			if (potential.getBlock().getType() == Material.AIR)
			{
				return potential;
			}
		}
		return location;
	}
}
