/**************************************************************************\
* EnderCrypt's Challenge                                                   *
* Copyright (C) 2023 by EnderCrypt                                         *
*                                                                          *
* This program is free software: you can redistribute it and/or modify     *
* it under the terms of the GNU Affero General Public License as           *
* published by the Free Software Foundation, either version 3 of           *
* the License, or (at your option) any later version.                      *
*                                                                          *
* This program is distributed in the hope that it will be useful,          *
* but WITHOUT ANY WARRANTY; without even the implied warranty of           *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
* GNU Affero General Public License for more details.                      *
*                                                                          *
* You should have received a copy of the GNU Affero General Public License *
* along with this program in its root directory.                           *
* If not, see <http://www.gnu.org/licenses/>.                              *
\**************************************************************************/

package endercrypt.endercryptschallenge.features.entities.all.mushroomloot;


import endercrypt.endercryptschallenge.core.Core;
import endercrypt.endercryptschallenge.materials.MaterialCollections;
import endercrypt.endercryptschallenge.modules.features.MinecraftFeature;

import java.util.List;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Biome;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Monster;
import org.bukkit.event.EventHandler;
import org.bukkit.event.entity.EntityDeathEvent;
import org.bukkit.inventory.ItemStack;


/**
 * @author EnderCrypt
 */
public class MushroomLoot extends MinecraftFeature
{
	public MushroomLoot(Core core)
	{
		super(core);
	}
	
	@EventHandler
	public void onEntityDeath(EntityDeathEvent event)
	{
		LivingEntity entity = event.getEntity();
		if (entity instanceof Monster == false)
			return;
		
		Location location = entity.getLocation();
		Biome biome = location.getWorld().getBiome(location);
		if (biome != Biome.MUSHROOM_FIELDS)
			return;
		
		event.setDroppedExp(event.getDroppedExp() / 2);
		
		List<ItemStack> drops = event.getDrops();
		Material dropMaterial = MaterialCollections.mushrooms.getRandom();
		int dropAmount = drops
			.stream()
			.mapToInt(ItemStack::getAmount)
			.sum();
		
		drops.clear();
		drops.add(new ItemStack(dropMaterial, dropAmount));
	}
}
