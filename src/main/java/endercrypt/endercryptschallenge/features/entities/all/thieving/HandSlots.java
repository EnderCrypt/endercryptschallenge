/**************************************************************************\
* EnderCrypt's Challenge                                                   *
* Copyright (C) 2023 by EnderCrypt                                         *
*                                                                          *
* This program is free software: you can redistribute it and/or modify     *
* it under the terms of the GNU Affero General Public License as           *
* published by the Free Software Foundation, either version 3 of           *
* the License, or (at your option) any later version.                      *
*                                                                          *
* This program is distributed in the hope that it will be useful,          *
* but WITHOUT ANY WARRANTY; without even the implied warranty of           *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
* GNU Affero General Public License for more details.                      *
*                                                                          *
* You should have received a copy of the GNU Affero General Public License *
* along with this program in its root directory.                           *
* If not, see <http://www.gnu.org/licenses/>.                              *
\**************************************************************************/

package endercrypt.endercryptschallenge.features.entities.all.thieving;


import java.util.List;
import java.util.Objects;
import java.util.Optional;

import org.bukkit.Material;
import org.bukkit.inventory.EntityEquipment;
import org.bukkit.inventory.EquipmentSlot;
import org.bukkit.inventory.ItemStack;


/**
 * @author EnderCrypt
 */
public class HandSlots
{
	private static final List<EquipmentSlot> slots = List.of(EquipmentSlot.HAND, EquipmentSlot.OFF_HAND);
	
	private final EntityEquipment equipment;
	
	public HandSlots(EntityEquipment equipment)
	{
		this.equipment = Objects.requireNonNull(equipment, "equipment");
	}
	
	public Optional<EquipmentSlot> getNextAvailableEquipmetSlot()
	{
		for (EquipmentSlot slot : slots)
		{
			ItemStack stack = equipment.getItem(slot);
			if (stack.getType() == Material.AIR)
			{
				return Optional.of(slot);
			}
		}
		return Optional.empty();
	}
	
	public boolean hasOpenSlots()
	{
		return getNextAvailableEquipmetSlot().isPresent();
	}
	
	public void putItem(ItemStack stack)
	{
		EquipmentSlot slot = getNextAvailableEquipmetSlot().orElseThrow();
		equipment.setItem(slot, stack);
		equipment.setDropChance(slot, 1f);
	}
}
