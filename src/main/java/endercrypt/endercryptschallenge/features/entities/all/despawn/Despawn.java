/**************************************************************************\
* EnderCrypt's Challenge                                                   *
* Copyright (C) 2023 by EnderCrypt                                         *
*                                                                          *
* This program is free software: you can redistribute it and/or modify     *
* it under the terms of the GNU Affero General Public License as           *
* published by the Free Software Foundation, either version 3 of           *
* the License, or (at your option) any later version.                      *
*                                                                          *
* This program is distributed in the hope that it will be useful,          *
* but WITHOUT ANY WARRANTY; without even the implied warranty of           *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
* GNU Affero General Public License for more details.                      *
*                                                                          *
* You should have received a copy of the GNU Affero General Public License *
* along with this program in its root directory.                           *
* If not, see <http://www.gnu.org/licenses/>.                              *
\**************************************************************************/

package endercrypt.endercryptschallenge.features.entities.all.despawn;


import endercrypt.endercryptschallenge.core.Core;
import endercrypt.endercryptschallenge.modules.features.MinecraftFeature;
import endercrypt.endercryptschallenge.utility.Utility;
import endercrypt.library.commons.misc.Ender;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.entity.Monster;
import org.bukkit.entity.Player;


/**
 * @author EnderCrypt
 */
public class Despawn extends MinecraftFeature
{
	private static final int MONSTER_COUNT_STABLE = 200;
	private static final int MONSTER_COUNT_CAP = 300;
	private static final int MONSTER_HARD_CAP = 1000;
	
	public Despawn(Core core)
	{
		super(core);
	}
	
	@Override
	public void onSecond()
	{
		server.getWorlds().forEach(this::processWorld);
	}
	
	private void processWorld(World world)
	{
		Collection<Monster> monsters = world.getEntitiesByClass(Monster.class)
			.stream()
			.filter(Utility::isEligableForDeletion)
			.collect(Collectors.toList());
		double chance = Ender.math.remap(monsters.size(), MONSTER_COUNT_STABLE, MONSTER_COUNT_CAP, 0.0, 1.0);
		while (Math.random() < chance || monsters.size() > MONSTER_HARD_CAP)
		{
			chance -= 1.0;
			Monster monster = findMostDistantMonster(world, monsters);
			if (monster == null)
			{
				return;
			}
			logger.fine("Despawned distant monster: " + monster);
			monster.remove();
			monsters.remove(monster);
		}
	}
	
	private static Monster findMostDistantMonster(World world, Collection<Monster> monsters)
	{
		List<Location> playerLocations = world.getPlayers()
			.stream()
			.map(Player::getLocation)
			.collect(Collectors.toList());
		
		Monster targetMonster = null;
		double targetDistance = 0.0;
		for (Monster monster : monsters)
		{
			Location monsterLocation = monster.getLocation();
			double playerDistance = -1;
			for (Location playerLocation : playerLocations)
			{
				double distance = Utility.roughDistance(playerLocation, monsterLocation);
				if (playerDistance < 0 || distance < playerDistance)
				{
					playerDistance = distance;
				}
			}
			if (targetMonster == null || playerDistance > targetDistance)
			{
				targetMonster = monster;
				targetDistance = playerDistance;
			}
		}
		
		return targetMonster;
	}
}
