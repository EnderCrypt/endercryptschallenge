/**************************************************************************\
* EnderCrypt's Challenge                                                   *
* Copyright (C) 2023 by EnderCrypt                                         *
*                                                                          *
* This program is free software: you can redistribute it and/or modify     *
* it under the terms of the GNU Affero General Public License as           *
* published by the Free Software Foundation, either version 3 of           *
* the License, or (at your option) any later version.                      *
*                                                                          *
* This program is distributed in the hope that it will be useful,          *
* but WITHOUT ANY WARRANTY; without even the implied warranty of           *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
* GNU Affero General Public License for more details.                      *
*                                                                          *
* You should have received a copy of the GNU Affero General Public License *
* along with this program in its root directory.                           *
* If not, see <http://www.gnu.org/licenses/>.                              *
\**************************************************************************/

package endercrypt.endercryptschallenge.features.entities.all.inthewalls;


import endercrypt.endercryptschallenge.core.Core;
import endercrypt.endercryptschallenge.materials.MaterialCollections;
import endercrypt.endercryptschallenge.modules.features.MinecraftFeature;
import endercrypt.endercryptschallenge.modules.worlds.list.over.OverWorld;
import endercrypt.endercryptschallenge.utility.Utility;
import endercrypt.library.commons.structs.range.IntRange;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.entity.Player;
import org.bukkit.entity.Zombie;
import org.bukkit.inventory.ItemStack;

import lombok.Getter;


/**
 * @author EnderCrypt
 */
public class TheyAreInTheWallsFeature extends MinecraftFeature
{
	private static final int CHANCE = 20_000; // one in x chance
	private static final int MAX_SEARCH_DISTANCE = 10;
	
	public TheyAreInTheWallsFeature(Core core)
	{
		super(core);
	}
	
	@Override
	public void onSecond()
	{
		server.getOnlinePlayers().forEach(this::processPlayer);
	}
	
	private void processPlayer(Player player)
	{
		double depth = getDepthValue(player);
		double strike = Math.random() * CHANCE;
		boolean spawn = depth > strike;
		if (spawn)
		{
			trySpawnForPlayer(player);
		}
	}
	
	private double getDepthValue(Player player)
	{
		if (resolve(player.getWorld()) instanceof OverWorld)
		{
			return Utility.getCanonicalDepth(player);
		}
		return 200;
	}
	
	private boolean trySpawnForPlayer(Player player)
	{
		SpawnRequest spawnRequest = new SpawnRequest(player);
		Checker checker = new Checker(spawnRequest);
		while (checker.getDistance() < MAX_SEARCH_DISTANCE)
		{
			checker.advance();
			Block block = checker.findSolidWall().orElse(null);
			if (block != null)
			{
				spawnRequest.trigger(block);
				return true;
			}
		}
		return false;
	}
	
	private static class Checker
	{
		private static final boolean wallMustBeFilled = true;
		private static final List<BlockFace> directions = Arrays.stream(BlockFace.values())
			.filter(bf -> bf.getModY() >= 0) // all, except those that go downwards
			.toList();
		
		private final Map<BlockFace, Block> locations = new HashMap<>();
		
		private final SpawnRequest spawnRequest;
		
		@Getter
		private int distance = 0;
		
		private Checker(SpawnRequest spawnRequest)
		{
			this.spawnRequest = spawnRequest;
			for (BlockFace direction : directions)
			{
				locations.put(direction, spawnRequest.getSourceLocation());
			}
		}
		
		public void advance()
		{
			locations.replaceAll(this::advance);
			distance++;
		}
		
		private Block advance(BlockFace blockFace, Block block)
		{
			return block.getRelative(blockFace);
		}
		
		public Optional<Block> findSolidWall()
		{
			for (Block block : locations.values())
			{
				if (isSolidWall(block))
				{
					return Optional.of(block);
				}
			}
			return Optional.empty();
		}
		
		private boolean isSolidWall(Block floor)
		{
			List<Block> blocks = new ArrayList<>();
			
			// above & below
			blocks.add(floor.getRelative(BlockFace.DOWN));
			blocks.add(floor.getRelative(BlockFace.UP, spawnRequest.getHeight()));
			
			// inside
			if (wallMustBeFilled)
			{
				for (int i = 0; i < spawnRequest.getHeight(); i++)
				{
					blocks.add(floor.getRelative(BlockFace.UP, i));
				}
			}
			
			// around
			for (int i = 0; i < spawnRequest.getHeight(); i++)
			{
				Block centerBlock = floor.getRelative(BlockFace.UP, i);
				
				Utility.cardinalBlockFaces.stream()
					.map(centerBlock::getRelative)
					.forEach(blocks::add);
			}
			
			return blocks.stream()
				.allMatch(this::isBlockQualifiedAsWall);
		}
		
		private boolean isBlockQualifiedAsWall(Block block)
		{
			return MaterialCollections.naturalBlocks.contains(block);
		}
	}
	
	@Getter
	private static class SpawnRequest
	{
		private static final IntRange spawnAmount = IntRange.of(1, 1);
		
		private final Player target;
		private final Block sourceLocation;
		private final boolean baby;
		private final int height;
		
		public SpawnRequest(Player target)
		{
			this.target = target;
			sourceLocation = target.getLocation().getBlock();
			baby = Utility.isPlayerProne(target);
			height = baby ? 1 : 2;
		}
		
		public void trigger(Block block)
		{
			for (int i = 0; i < height; i++)
			{
				block.getRelative(BlockFace.UP, i).setType(Material.AIR);
			}
			
			Location location = Utility.getBlockFloorLocation(block);
			int count = spawnAmount.getRandom();
			for (int i = 0; i < count; i++)
			{
				Zombie zombie = block.getWorld().spawn(location, Zombie.class);
				zombie.getEquipment().setItemInMainHand(new ItemStack(Material.STONE_PICKAXE));
				zombie.getEquipment().setItemInMainHandDropChance(0.1f);
				if (target != null)
				{
					zombie.setTarget(target);
				}
				if (baby)
				{
					zombie.setBaby();
				}
			}
		}
	}
}
