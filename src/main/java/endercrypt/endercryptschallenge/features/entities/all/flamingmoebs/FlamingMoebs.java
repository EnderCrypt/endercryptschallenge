/**************************************************************************\
* EnderCrypt's Challenge                                                   *
* Copyright (C) 2023 by EnderCrypt                                         *
*                                                                          *
* This program is free software: you can redistribute it and/or modify     *
* it under the terms of the GNU Affero General Public License as           *
* published by the Free Software Foundation, either version 3 of           *
* the License, or (at your option) any later version.                      *
*                                                                          *
* This program is distributed in the hope that it will be useful,          *
* but WITHOUT ANY WARRANTY; without even the implied warranty of           *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
* GNU Affero General Public License for more details.                      *
*                                                                          *
* You should have received a copy of the GNU Affero General Public License *
* along with this program in its root directory.                           *
* If not, see <http://www.gnu.org/licenses/>.                              *
\**************************************************************************/

package endercrypt.endercryptschallenge.features.entities.all.flamingmoebs;


import endercrypt.endercryptschallenge.core.Core;
import endercrypt.endercryptschallenge.materials.MaterialCollections;
import endercrypt.endercryptschallenge.modules.features.MinecraftFeature;
import endercrypt.endercryptschallenge.utility.Utility;

import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Mob;


/**
 * @author EnderCrypt
 */
public class FlamingMoebs extends MinecraftFeature
{
	public FlamingMoebs(Core core)
	{
		super(core);
	}
	
	@Override
	public void onCycleEntity(Entity entity)
	{
		if (entity instanceof Mob mob)
		{
			updateMob(mob);
		}
	}
	
	private void updateMob(Mob mob)
	{
		if (mob.getFireTicks() <= 0)
		{
			return;
		}
		Block block = mob.getLocation().getBlock();
		if (isBurnable(block) == false)
		{
			return;
		}
		
		block.setType(Material.FIRE);
	}
	
	private boolean isBurnable(Block block)
	{
		Material material = block.getType();
		if (material != Material.AIR)
		{
			return false;
		}
		
		for (BlockFace face : Utility.cartesianBlockFaces)
		{
			Block connectedBlock = block.getRelative(face);
			Material connectedMaterial = connectedBlock.getType();
			if (MaterialCollections.flammables.contains(connectedMaterial))
			{
				return true;
			}
		}
		
		return false;
	}
}
