/**************************************************************************\
* EnderCrypt's Challenge                                                   *
* Copyright (C) 2023 by EnderCrypt                                         *
*                                                                          *
* This program is free software: you can redistribute it and/or modify     *
* it under the terms of the GNU Affero General Public License as           *
* published by the Free Software Foundation, either version 3 of           *
* the License, or (at your option) any later version.                      *
*                                                                          *
* This program is distributed in the hope that it will be useful,          *
* but WITHOUT ANY WARRANTY; without even the implied warranty of           *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
* GNU Affero General Public License for more details.                      *
*                                                                          *
* You should have received a copy of the GNU Affero General Public License *
* along with this program in its root directory.                           *
* If not, see <http://www.gnu.org/licenses/>.                              *
\**************************************************************************/

package endercrypt.endercryptschallenge.features.entities.all.jumpplants;


import endercrypt.endercryptschallenge.core.Core;
import endercrypt.endercryptschallenge.materials.MaterialCollections;
import endercrypt.endercryptschallenge.modules.features.MinecraftFeature;

import java.util.Set;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.BlockFace;
import org.bukkit.entity.Cow;
import org.bukkit.entity.Creeper;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Goat;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Mob;
import org.bukkit.entity.Pig;
import org.bukkit.entity.Pillager;
import org.bukkit.entity.Sheep;
import org.bukkit.entity.Skeleton;
import org.bukkit.entity.Vindicator;
import org.bukkit.entity.Witch;
import org.bukkit.entity.WitherSkeleton;
import org.bukkit.entity.Zombie;
import org.bukkit.util.Vector;


/**
 * @author EnderCrypt
 */
public class JumpPlants extends MinecraftFeature
{
	private final Set<Class<? extends Mob>> jumpers = Set.of(
		Pig.class,
		Cow.class,
		Sheep.class,
		Zombie.class,
		Creeper.class,
		Skeleton.class,
		Goat.class,
		Pillager.class,
		Vindicator.class,
		Witch.class,
		WitherSkeleton.class);
	
	public JumpPlants(Core core)
	{
		super(core);
	}
	
	@Override
	public void onCycleEntity(Entity entity)
	{
		if (entity instanceof Mob mob)
		{
			processMob(mob);
		}
	}
	
	private void processMob(Mob mob)
	{
		if (isJumperMob(mob) == false)
			return;
		
		Location location = mob.getLocation().add(0, 0.1, 0);
		Material type = location.getBlock().getType();
		if (MaterialCollections.crops.contains(type) == false)
			return;
		
		if (isTouchingCeiling(mob))
			return;
		
		trample(mob);
	}
	
	private void trample(Mob mob)
	{
		mob.getPathfinder().stopPathfinding();
		mob.getPathfinder().findPath(mob.getLocation().toBlockLocation());
		mob.setVelocity(new Vector(0, 0, 0));
		mob.setJumping(true);
	}
	
	private boolean isJumperMob(Mob mob)
	{
		for (Class<? extends LivingEntity> jumper : jumpers)
		{
			if (jumper.isAssignableFrom(mob.getClass()))
			{
				return true;
			}
		}
		return false;
	}
	
	private static boolean isTouchingCeiling(LivingEntity entity)
	{
		return entity
			.getLocation()
			.add(0, entity.getEyeHeight(), 0)
			.getBlock()
			.getRelative(BlockFace.UP)
			.isPassable() == false;
	}
}
