/**************************************************************************\
* EnderCrypt's Challenge                                                   *
* Copyright (C) 2023 by EnderCrypt                                         *
*                                                                          *
* This program is free software: you can redistribute it and/or modify     *
* it under the terms of the GNU Affero General Public License as           *
* published by the Free Software Foundation, either version 3 of           *
* the License, or (at your option) any later version.                      *
*                                                                          *
* This program is distributed in the hope that it will be useful,          *
* but WITHOUT ANY WARRANTY; without even the implied warranty of           *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
* GNU Affero General Public License for more details.                      *
*                                                                          *
* You should have received a copy of the GNU Affero General Public License *
* along with this program in its root directory.                           *
* If not, see <http://www.gnu.org/licenses/>.                              *
\**************************************************************************/

package endercrypt.endercryptschallenge.features.entities.all.boatdriver;


import endercrypt.endercryptschallenge.core.Core;
import endercrypt.endercryptschallenge.modules.features.MinecraftFeature;
import endercrypt.endercryptschallenge.utility.Utility;
import endercrypt.endercryptschallenge.utility.difficulty.Difficulties;
import endercrypt.endercryptschallenge.utility.difficulty.DifficultyRating;
import endercrypt.endercryptschallenge.utility.tracker.ObjectTracker;
import endercrypt.library.commons.misc.Ender;

import java.util.Set;

import org.bukkit.entity.Boat;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Piglin;
import org.bukkit.entity.Player;
import org.bukkit.entity.Skeleton;
import org.bukkit.entity.Villager;
import org.bukkit.entity.Zombie;
import org.bukkit.entity.ZombieVillager;
import org.bukkit.util.Vector;


/**
 * @author EnderCrypt
 */
public class BoatDriver extends MinecraftFeature
{
	private static final double turnSpeed = 0.75;
	
	private static final Set<Class<? extends Entity>> drivers = Set.of(Skeleton.class, Piglin.class, Zombie.class, ZombieVillager.class, Villager.class);
	private ObjectTracker<Boat> drivenBoats = new ObjectTracker<>(Boat.class)
		.addCondition(BoatDriver::isDrivableBoat);
	
	public BoatDriver(Core core)
	{
		super(core);
	}
	
	@Override
	public void onCycleEntity(Entity entity)
	{
		if (entity instanceof Boat boat && isDrivableBoat(boat))
		{
			drivenBoats.add(boat);
		}
	}
	
	public static boolean isDrivableBoat(Boat boat)
	{
		boolean hasMonster = false;
		for (Entity passanger : boat.getPassengers())
		{
			if (passanger instanceof Player)
				return false;
			
			if (hasMonster == false)
			{
				hasMonster = drivers.stream().anyMatch(c -> c.isAssignableFrom(passanger.getClass()));
			}
		}
		return hasMonster;
	}
	
	@Override
	public void onTick()
	{
		for (Boat boat : drivenBoats)
		{
			DifficultyRating difficulty = Difficulties.getFor(boat);
			double acceleration = difficulty.asDoubleValue(0.05, 0.075, 0.15);
			double yaw = boat.getLocation().getYaw() + Ender.math.randomRange(-turnSpeed, turnSpeed);
			
			boat.setRotation((float) yaw, 0f);
			Vector motion = Utility.createVector(yaw, acceleration);
			
			boat.setVelocity(boat.getVelocity().add(motion));
		}
	}
}
