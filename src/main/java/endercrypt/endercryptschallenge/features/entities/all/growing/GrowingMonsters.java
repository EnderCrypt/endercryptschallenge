/**************************************************************************\
* EnderCrypt's Challenge                                                   *
* Copyright (C) 2023 by EnderCrypt                                         *
*                                                                          *
* This program is free software: you can redistribute it and/or modify     *
* it under the terms of the GNU Affero General Public License as           *
* published by the Free Software Foundation, either version 3 of           *
* the License, or (at your option) any later version.                      *
*                                                                          *
* This program is distributed in the hope that it will be useful,          *
* but WITHOUT ANY WARRANTY; without even the implied warranty of           *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
* GNU Affero General Public License for more details.                      *
*                                                                          *
* You should have received a copy of the GNU Affero General Public License *
* along with this program in its root directory.                           *
* If not, see <http://www.gnu.org/licenses/>.                              *
\**************************************************************************/

package endercrypt.endercryptschallenge.features.entities.all.growing;


import endercrypt.endercryptschallenge.core.Core;
import endercrypt.endercryptschallenge.modules.features.MinecraftFeature;

import java.util.EnumSet;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Optional;

import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Mob;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;


/**
 * @author EnderCrypt
 */
public class GrowingMonsters extends MinecraftFeature
{
	private static final EnumSet<DamageCause> ignoredDamageCauses = EnumSet.of(DamageCause.SUFFOCATION, DamageCause.CRAMMING);
	private Map<Mob, Grower> growers = new HashMap<>();
	
	public GrowingMonsters(Core core)
	{
		super(core);
	}
	
	@Override
	public void onTick()
	{
		Iterator<Grower> iterator = growers.values().iterator();
		while (iterator.hasNext())
		{
			Grower grower = iterator.next();
			grower.update();
			if (grower.isDone())
			{
				iterator.remove();
			}
		}
	}
	
	@EventHandler(priority = EventPriority.LOW)
	public void onGrowerHurt(EntityDamageEvent event)
	{
		if (get(event.getEntity()).isPresent() && ignoredDamageCauses.contains(event.getCause()))
		{
			event.setCancelled(true);
		}
	}
	
	public Grower create(Class<? extends Mob> mob, Block block, double growthTime)
	{
		if (isValidGrowingBlock(block, false, 2) == false)
		{
			throw new IllegalArgumentException(block + " is not a legal growing block");
		}
		Grower grower = new Grower(mob, block, growthTime);
		growers.put(grower.getMob(), grower);
		return grower;
	}
	
	public Optional<Grower> get(Entity entity)
	{
		return Optional.ofNullable(growers.get(entity));
	}
	
	public static boolean isValidGrowingBlock(Block block, boolean complete, int height)
	{
		for (int i = 0; i < height; i++)
		{
			if (block.getRelative(BlockFace.UP, i).getType() != Material.AIR)
			{
				return false;
			}
		}
		
		if (block.getRelative(BlockFace.DOWN, 1).isSolid() == false)
		{
			return false;
		}
		
		if (complete)
		{
			for (int i = 1; i <= height; i++)
			{
				if (block.getRelative(BlockFace.DOWN, i).isSolid() == false)
				{
					return false;
				}
			}
		}
		
		return true;
	}
}
