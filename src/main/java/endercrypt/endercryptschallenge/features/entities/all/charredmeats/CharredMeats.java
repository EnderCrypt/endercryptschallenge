/**************************************************************************\
* EnderCrypt's Challenge                                                   *
* Copyright (C) 2023 by EnderCrypt                                         *
*                                                                          *
* This program is free software: you can redistribute it and/or modify     *
* it under the terms of the GNU Affero General Public License as           *
* published by the Free Software Foundation, either version 3 of           *
* the License, or (at your option) any later version.                      *
*                                                                          *
* This program is distributed in the hope that it will be useful,          *
* but WITHOUT ANY WARRANTY; without even the implied warranty of           *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
* GNU Affero General Public License for more details.                      *
*                                                                          *
* You should have received a copy of the GNU Affero General Public License *
* along with this program in its root directory.                           *
* If not, see <http://www.gnu.org/licenses/>.                              *
\**************************************************************************/

package endercrypt.endercryptschallenge.features.entities.all.charredmeats;


import endercrypt.endercryptschallenge.core.Core;
import endercrypt.endercryptschallenge.materials.MaterialCollections;
import endercrypt.endercryptschallenge.modules.features.MinecraftFeature;
import endercrypt.endercryptschallenge.utility.ItemStackLore;
import endercrypt.library.commons.structs.range.DoubleRange;
import endercrypt.library.commons.structs.range.Range;

import java.util.List;

import org.bukkit.entity.Creature;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.entity.EntityDeathEvent;
import org.bukkit.event.player.PlayerItemConsumeEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import net.kyori.adventure.text.Component;
import net.kyori.adventure.text.TextComponent;
import net.kyori.adventure.text.format.TextColor;


/**
 * @author EnderCrypt
 */
public class CharredMeats extends MinecraftFeature
{
	private static final TextComponent charredText = Component.text("Charred").color(TextColor.color(130, 10, 20));
	
	public static final DoubleRange POISON_DURATION = Range.of(5.0, 10.0);
	
	public CharredMeats(Core core)
	{
		super(core);
	}
	
	@EventHandler
	public void onEntityDeath(EntityDeathEvent event)
	{
		LivingEntity entity = event.getEntity();
		if (entity.getFireTicks() <= 0)
			return;
		
		if (entity instanceof Creature == false)
			return;
		
		List<ItemStack> drops = event.getDrops();
		for (ItemStack item : drops)
		{
			if (MaterialCollections.cookedMeats.contains(item))
			{
				ItemStackLore.of(item).add(charredText);
			}
		}
	}
	
	@EventHandler
	public void onEat(PlayerItemConsumeEvent event)
	{
		ItemStack itemStack = event.getItem();
		if (isCharred(itemStack) == false)
			return;
		
		Player player = event.getPlayer();
		double poisonDurationSeconds = POISON_DURATION.getRandom();
		int poisonDurationTicks = (int) (poisonDurationSeconds * 20);
		player.addPotionEffect(new PotionEffect(PotionEffectType.POISON, poisonDurationTicks, 0));
	}
	
	public boolean isCharred(ItemStack itemStack)
	{
		return ItemStackLore.of(itemStack).contains(charredText);
	}
}
