/**************************************************************************\
* EnderCrypt's Challenge                                                   *
* Copyright (C) 2023 by EnderCrypt                                         *
*                                                                          *
* This program is free software: you can redistribute it and/or modify     *
* it under the terms of the GNU Affero General Public License as           *
* published by the Free Software Foundation, either version 3 of           *
* the License, or (at your option) any later version.                      *
*                                                                          *
* This program is distributed in the hope that it will be useful,          *
* but WITHOUT ANY WARRANTY; without even the implied warranty of           *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
* GNU Affero General Public License for more details.                      *
*                                                                          *
* You should have received a copy of the GNU Affero General Public License *
* along with this program in its root directory.                           *
* If not, see <http://www.gnu.org/licenses/>.                              *
\**************************************************************************/

package endercrypt.endercryptschallenge.features.entities.player.fishing.funny.entity.item.factory;


import endercrypt.endercryptschallenge.materials.structure.MaterialCollection;
import endercrypt.library.commons.misc.Ender;

import java.util.Objects;

import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.inventory.ItemStack;


/**
 * @author EnderCrypt
 */
public interface ItemStackFactory
{
	public ItemStack produce(World world);
	
	public static class Builder
	{
		// OTHER
		
		public Builder of(ItemStack itemStack)
		{
			setMaterial(itemStack.getType());
			setAmount(itemStack.getAmount());
			return this;
		}
		
		// MATERIAL
		
		private ItemStackMaterialFactory materialFunction;
		
		public Builder setMaterial(ItemStackMaterialFactory materialFunction)
		{
			this.materialFunction = materialFunction;
			return this;
		}
		
		public Builder setMaterial(Material material)
		{
			return setMaterial((w) -> material);
		}
		
		public Builder setMaterial(MaterialCollection collection)
		{
			return setMaterial((w) -> collection.getRandom());
		}
		
		// AMOUNT
		
		private ItemStackAmountFactory amountFunction = (w) -> 1;
		
		public Builder setAmount(ItemStackAmountFactory amountFunction)
		{
			this.amountFunction = amountFunction;
			return this;
		}
		
		public Builder setAmount(int amount)
		{
			return setAmount((w) -> amount);
		}
		
		public Builder setRandomAmount(int minimum, int maximum)
		{
			return setAmount((w) -> Ender.math.randomRange(minimum, maximum));
		}
		
		// BUILD
		
		public ItemStackFactory build()
		{
			return new ItemStackFactory()
			{
				@Override
				public ItemStack produce(World world)
				{
					Objects.requireNonNull(materialFunction, "materialFunction");
					Objects.requireNonNull(amountFunction, "amountFunction");
					return new ItemStack(materialFunction.produce(world), amountFunction.produce(world));
				}
			};
		}
	}
}
