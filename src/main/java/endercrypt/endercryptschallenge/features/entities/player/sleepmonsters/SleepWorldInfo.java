/**************************************************************************\
* EnderCrypt's Challenge                                                   *
* Copyright (C) 2023 by EnderCrypt                                         *
*                                                                          *
* This program is free software: you can redistribute it and/or modify     *
* it under the terms of the GNU Affero General Public License as           *
* published by the Free Software Foundation, either version 3 of           *
* the License, or (at your option) any later version.                      *
*                                                                          *
* This program is distributed in the hope that it will be useful,          *
* but WITHOUT ANY WARRANTY; without even the implied warranty of           *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
* GNU Affero General Public License for more details.                      *
*                                                                          *
* You should have received a copy of the GNU Affero General Public License *
* along with this program in its root directory.                           *
* If not, see <http://www.gnu.org/licenses/>.                              *
\**************************************************************************/

package endercrypt.endercryptschallenge.features.entities.player.sleepmonsters;


import endercrypt.endercryptschallenge.core.CoreCollective;

import java.util.Objects;

import org.bukkit.World;
import org.bukkit.entity.Player;
import org.bukkit.event.player.PlayerBedLeaveEvent;


/**
 * @author EnderCrypt
 */
public class SleepWorldInfo extends CoreCollective
{
	private final SleepMonsters sleepMonsters;
	private final World world;
	private long time = 0;
	
	public SleepWorldInfo(SleepMonsters sleepMonsters, World world)
	{
		super(sleepMonsters.getCore());
		this.sleepMonsters = Objects.requireNonNull(sleepMonsters, "sleepMonsters");
		this.world = Objects.requireNonNull(world, "world");
	}
	
	public void onTick()
	{
		time = world.getTime();
	}
	
	public boolean onSleep(PlayerBedLeaveEvent event)
	{
		Player player = event.getPlayer();
		if (this.world.equals(player.getWorld()) == false)
			throw new IllegalArgumentException(event + " triggered for wrong world");
		
		long newTime = world.getTime();
		if (newTime != 0)
		{
			return false;
		}
		long oldTime = time;
		long amount = 24_000 - oldTime;
		processWorldSleep(player, amount);
		return true;
	}
	
	private void processWorldSleep(Player player, long amount)
	{
		final int nightTime = 12_000;
		if (amount > nightTime)
		{
			throw new IllegalArgumentException("player couldnt have slept for " + amount + " time");
		}
		double chance = 1.0 / nightTime * amount;
		sleepMonsters.createSleepHordes(player, chance);
	}
}
