/**************************************************************************\
* EnderCrypt's Challenge                                                   *
* Copyright (C) 2023 by EnderCrypt                                         *
*                                                                          *
* This program is free software: you can redistribute it and/or modify     *
* it under the terms of the GNU Affero General Public License as           *
* published by the Free Software Foundation, either version 3 of           *
* the License, or (at your option) any later version.                      *
*                                                                          *
* This program is distributed in the hope that it will be useful,          *
* but WITHOUT ANY WARRANTY; without even the implied warranty of           *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
* GNU Affero General Public License for more details.                      *
*                                                                          *
* You should have received a copy of the GNU Affero General Public License *
* along with this program in its root directory.                           *
* If not, see <http://www.gnu.org/licenses/>.                              *
\**************************************************************************/

package endercrypt.endercryptschallenge.features.entities.player.playerzombie;


import endercrypt.endercryptschallenge.core.Core;
import endercrypt.endercryptschallenge.modules.features.MinecraftFeature;
import endercrypt.endercryptschallenge.modules.worlds.list.nether.NetherWorld;
import endercrypt.endercryptschallenge.utility.Utility;
import endercrypt.endercryptschallenge.utility.difficulty.Difficulties;
import endercrypt.endercryptschallenge.utility.difficulty.DifficultyRating;

import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.entity.Ageable;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Monster;
import org.bukkit.entity.PigZombie;
import org.bukkit.entity.Player;
import org.bukkit.entity.Zombie;
import org.bukkit.event.EventHandler;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.inventory.EntityEquipment;
import org.bukkit.inventory.EquipmentSlot;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;


/**
 * @author EnderCrypt
 */
public class PlayerZombie extends MinecraftFeature
{
	public PlayerZombie(Core core)
	{
		super(core);
	}
	
	private Class<? extends Monster> getPlayerMonsterClass(Player player)
	{
		if (resolve(player.getWorld()) instanceof NetherWorld)
		{
			return PigZombie.class;
		}
		return Zombie.class;
	}
	
	@EventHandler
	public void onPlayerDeath(PlayerDeathEvent event)
	{
		Player player = event.getEntity();
		Location location = player.getLocation();
		World world = location.getWorld();
		Monster playerZombie = world.spawn(location, getPlayerMonsterClass(player));
		addEffect(playerZombie, 10, PotionEffectType.RESISTANCE, 1, 2, 3);
		addEffect(playerZombie, 20, PotionEffectType.SPEED, 1, 2, 3);
		addEffect(playerZombie, 30, PotionEffectType.REGENERATION, 1, 2, 3);
		addEffect(playerZombie, 60, PotionEffectType.HEALTH_BOOST, 3, 4, 5);
		addEffect(playerZombie, 60 * 30, PotionEffectType.GLOWING, 1, 2, 3);
		playerZombie.setCanPickupItems(false);
		if (playerZombie instanceof Ageable ageable)
		{
			ageable.setAdult();
		}
		playerZombie.setPersistent(true);
		playerZombie.setRemoveWhenFarAway(false);
		playerZombie.setCustomName("Fallen " + player.getName());
		playerZombie.setCustomNameVisible(true);
		
		// equipment
		transferEquipment(event, player, playerZombie, EquipmentSlot.HAND);
		transferEquipment(event, player, playerZombie, EquipmentSlot.OFF_HAND);
		transferEquipment(event, player, playerZombie, EquipmentSlot.HEAD);
		transferEquipment(event, player, playerZombie, EquipmentSlot.CHEST);
		transferEquipment(event, player, playerZombie, EquipmentSlot.LEGS);
		transferEquipment(event, player, playerZombie, EquipmentSlot.FEET);
		
		if (Utility.isPlayerProne(player) && playerZombie instanceof Ageable ageablePlayerZombie)
		{
			ageablePlayerZombie.setBaby();
		}
		
		Entity vehicle = player.getVehicle();
		if (vehicle != null)
		{
			vehicle.addPassenger(playerZombie);
		}
	}
	
	private static void transferEquipment(PlayerDeathEvent event, Player player, Monster playerZombie, EquipmentSlot slot)
	{
		EntityEquipment playerEquipment = player.getEquipment();
		EntityEquipment playerZombieEquipment = playerZombie.getEquipment();
		ItemStack item = playerEquipment.getItem(slot);
		playerZombieEquipment.setItem(slot, item);
		event.getDrops().remove(item);
		playerZombieEquipment.setDropChance(slot, 1.0f);
	}
	
	private static void addEffect(Monster playerZombie, int time, PotionEffectType effect, int easy, int normal, int hard)
	{
		DifficultyRating difficuty = Difficulties.getFor(playerZombie);
		int duration = (int) (20 * time * difficuty.asDoubleModifier());
		int amplifier = difficuty.asIntValue(easy, normal, hard);
		playerZombie.addPotionEffect(new PotionEffect(effect, duration, amplifier));
	}
}
