/**************************************************************************\
* EnderCrypt's Challenge                                                   *
* Copyright (C) 2023 by EnderCrypt                                         *
*                                                                          *
* This program is free software: you can redistribute it and/or modify     *
* it under the terms of the GNU Affero General Public License as           *
* published by the Free Software Foundation, either version 3 of           *
* the License, or (at your option) any later version.                      *
*                                                                          *
* This program is distributed in the hope that it will be useful,          *
* but WITHOUT ANY WARRANTY; without even the implied warranty of           *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
* GNU Affero General Public License for more details.                      *
*                                                                          *
* You should have received a copy of the GNU Affero General Public License *
* along with this program in its root directory.                           *
* If not, see <http://www.gnu.org/licenses/>.                              *
\**************************************************************************/

package endercrypt.endercryptschallenge.features.entities.player.nauseous;


import endercrypt.endercryptschallenge.core.Core;
import endercrypt.endercryptschallenge.core.CoreCollective;
import endercrypt.endercryptschallenge.features.popup.deathmessage.DeathMessage;
import endercrypt.endercryptschallenge.utility.NumberWaves;

import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;


/**
 * @author EnderCrypt
 */
public class PlayerHead extends CoreCollective
{
	private static final double VALUE_MINIMUM = 750;
	private static final double VALUE_STEP = 50;
	private static final PotionEffectType CAUSE_EFFECT = PotionEffectType.NAUSEA;
	
	private final Player player;
	private float yaw;
	private float pitch;
	private NumberWaves nauseaWaves = new NumberWaves(20);
	
	public PlayerHead(Core core, Player player)
	{
		super(core);
		this.player = player;
		
		Location face = player.getEyeLocation();
		this.yaw = face.getYaw();
		this.pitch = face.getPitch();
	}
	
	public Player getPlayer()
	{
		return player;
	}
	
	public NumberWaves getNauseaWaves()
	{
		return nauseaWaves;
	}
	
	public void update()
	{
		Location face = player.getEyeLocation();
		float newYaw = face.getYaw();
		float newPitch = face.getPitch();
		
		double effect = (Math.abs(yaw - newYaw) + Math.abs(pitch - newPitch));
		effect = Math.min(100, effect);
		effect = effect * effect;
		
		if (core.getSettingsManager().getGameplay().getNausea().get() == false)
		{
			effect = 0;
		}
		
		nauseaWaves.carry(0.1);
		nauseaWaves.decay(0.1);
		nauseaWaves.push(effect);
		
		double nauseaValue = nauseaWaves.get();
		// player.sendMessage("nauseaWaves: " + nauseaValue);
		if (nauseaValue > VALUE_MINIMUM)
		{
			int nauseaCount = (int) Math.floor((nauseaValue - VALUE_MINIMUM) / VALUE_STEP);
			createNausea(nauseaCount);
		}
		
		yaw = newYaw;
		pitch = newPitch;
	}
	
	private void createNausea(int intensity)
	{
		if (intensity <= 0)
			return;
		int duration = intensity * 20;
		
		PotionEffect effect = player.getPotionEffect(CAUSE_EFFECT);
		if (effect != null && effect.getDuration() >= duration)
			return;
		
		player.addPotionEffect(new PotionEffect(CAUSE_EFFECT, duration, 0));
		if (effect == null)
		{
			player.sendMessage("You feel nauseous from all the fast movements ...");
		}
		
		if (intensity > 100)
		{
			DeathMessage deathMessage = core.getMinecraftFeatureManager().getFeature(DeathMessage.class);
			deathMessage.setKilledBy(player, "brain damage");
			player.damage(100);
		}
	}
}
