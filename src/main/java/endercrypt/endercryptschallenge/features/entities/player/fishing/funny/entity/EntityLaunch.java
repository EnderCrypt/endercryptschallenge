/**************************************************************************\
* EnderCrypt's Challenge                                                   *
* Copyright (C) 2023 by EnderCrypt                                         *
*                                                                          *
* This program is free software: you can redistribute it and/or modify     *
* it under the terms of the GNU Affero General Public License as           *
* published by the Free Software Foundation, either version 3 of           *
* the License, or (at your option) any later version.                      *
*                                                                          *
* This program is distributed in the hope that it will be useful,          *
* but WITHOUT ANY WARRANTY; without even the implied warranty of           *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
* GNU Affero General Public License for more details.                      *
*                                                                          *
* You should have received a copy of the GNU Affero General Public License *
* along with this program in its root directory.                           *
* If not, see <http://www.gnu.org/licenses/>.                              *
\**************************************************************************/

package endercrypt.endercryptschallenge.features.entities.player.fishing.funny.entity;


import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Monster;
import org.bukkit.event.player.PlayerFishEvent;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.util.Vector;


/**
 * @author EnderCrypt
 */
public abstract class EntityLaunch implements FunnyFish
{
	@Override
	public final boolean pull(PlayerFishEvent event)
	{
		Location playerLocation = event.getPlayer().getEyeLocation().add(0, 1, 0);
		Location location = event.getHook().getLocation();
		Entity entity = spawn(event, location.getWorld(), location);
		if (entity == null)
		{
			return false;
		}
		
		if (entity instanceof Monster monster)
		{
			monster.addPotionEffect(new PotionEffect(PotionEffectType.SLOW_FALLING, 20 * 10, 2));
		}
		
		double x = playerLocation.getX() - location.getX();
		double y = playerLocation.getY() - location.getY();
		double z = playerLocation.getZ() - location.getZ();
		double speed = 0.15;
		Vector velocity = new Vector(x * speed, y * speed, z * speed);
		entity.setVelocity(velocity);
		return true;
	}
	
	protected abstract Entity spawn(PlayerFishEvent event, World world, Location location);
}
