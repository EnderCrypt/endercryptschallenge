/**************************************************************************\
* EnderCrypt's Challenge                                                   *
* Copyright (C) 2023 by EnderCrypt                                         *
*                                                                          *
* This program is free software: you can redistribute it and/or modify     *
* it under the terms of the GNU Affero General Public License as           *
* published by the Free Software Foundation, either version 3 of           *
* the License, or (at your option) any later version.                      *
*                                                                          *
* This program is distributed in the hope that it will be useful,          *
* but WITHOUT ANY WARRANTY; without even the implied warranty of           *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
* GNU Affero General Public License for more details.                      *
*                                                                          *
* You should have received a copy of the GNU Affero General Public License *
* along with this program in its root directory.                           *
* If not, see <http://www.gnu.org/licenses/>.                              *
\**************************************************************************/

package endercrypt.endercryptschallenge.features.entities.player.nauseous;


import endercrypt.endercryptschallenge.core.Core;
import endercrypt.endercryptschallenge.modules.features.MinecraftFeature;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.entity.PlayerDeathEvent;


/**
 * @author EnderCrypt
 */
public class NauseousPlayer extends MinecraftFeature
{
	private Map<Player, PlayerHead> players = new HashMap<>();
	
	public NauseousPlayer(Core core)
	{
		super(core);
		getDebug().enableDebugSupport();
	}
	
	@Override
	public void onCycleEntity(Entity entity)
	{
		if (entity instanceof Player player)
		{
			players.computeIfAbsent(player, (p) -> new PlayerHead(core, player));
		}
	}
	
	@Override
	public void onTick()
	{
		Iterator<PlayerHead> iterator = players.values().iterator();
		while (iterator.hasNext())
		{
			PlayerHead playerHead = iterator.next();
			Player player = playerHead.getPlayer();
			if (player.isOnline() == false)
			{
				iterator.remove();
				continue;
			}
			playerHead.update();
		}
	}
	
	@Override
	public void onSecond()
	{
		debug(p -> players.get(p).getNauseaWaves().toStringColored());
	}
	
	@EventHandler
	public void onPlayerDeath(PlayerDeathEvent event)
	{
		Player player = event.getPlayer();
		players.remove(player);
	}
}
