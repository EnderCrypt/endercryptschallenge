/**************************************************************************\
* EnderCrypt's Challenge                                                   *
* Copyright (C) 2023 by EnderCrypt                                         *
*                                                                          *
* This program is free software: you can redistribute it and/or modify     *
* it under the terms of the GNU Affero General Public License as           *
* published by the Free Software Foundation, either version 3 of           *
* the License, or (at your option) any later version.                      *
*                                                                          *
* This program is distributed in the hope that it will be useful,          *
* but WITHOUT ANY WARRANTY; without even the implied warranty of           *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
* GNU Affero General Public License for more details.                      *
*                                                                          *
* You should have received a copy of the GNU Affero General Public License *
* along with this program in its root directory.                           *
* If not, see <http://www.gnu.org/licenses/>.                              *
\**************************************************************************/

package endercrypt.endercryptschallenge.features.entities.player.breakbones;


import endercrypt.endercryptschallenge.core.Core;
import endercrypt.endercryptschallenge.modules.features.MinecraftFeature;
import endercrypt.endercryptschallenge.utility.difficulty.Difficulties;
import endercrypt.endercryptschallenge.utility.difficulty.DifficultyRating;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;


/**
 * @author EnderCrypt
 */
public class BreakBones extends MinecraftFeature
{
	public static final PotionEffectType effect = PotionEffectType.SLOWNESS;
	public static final int amplifier = 3;
	
	public BreakBones(Core core)
	{
		super(core);
	}
	
	@EventHandler(priority = EventPriority.LOW)
	public void breakBones(EntityDamageEvent event)
	{
		if (event.isCancelled())
			return;
		
		if (event.getCause() != DamageCause.FALL)
			return;
		
		if (event.getEntity() instanceof Player == false)
			return;
		
		Player player = (Player) event.getEntity();
		if (player.hasPotionEffect(PotionEffectType.SLOW_FALLING))
			return;
		
		DifficultyRating difficulty = Difficulties.getFor(event);
		double fallDistance = player.getFallDistance();
		double breakLegDistance = difficulty.asDoubleValue(8, 7, 6);
		if (fallDistance < breakLegDistance)
			return;
		
		breakPlayerLeg(player, fallDistance, breakLegDistance);
	}
	
	private void breakPlayerLeg(Player player, double fallDistance, double breakLegDistance)
	{
		DifficultyRating difficulty = Difficulties.getFor(player);
		double pain = 5 + (fallDistance - breakLegDistance);
		double duration = pain * difficulty.asDoubleValue(20, 30, 40);
		player.addPotionEffect(new PotionEffect(effect, (int) (20 * duration), amplifier, false, false));
		player.sendMessage("Your fall caused you to " + ChatColor.DARK_RED + "break a leg!");
	}
}
