/**************************************************************************\
* EnderCrypt's Challenge                                                   *
* Copyright (C) 2023 by EnderCrypt                                         *
*                                                                          *
* This program is free software: you can redistribute it and/or modify     *
* it under the terms of the GNU Affero General Public License as           *
* published by the Free Software Foundation, either version 3 of           *
* the License, or (at your option) any later version.                      *
*                                                                          *
* This program is distributed in the hope that it will be useful,          *
* but WITHOUT ANY WARRANTY; without even the implied warranty of           *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
* GNU Affero General Public License for more details.                      *
*                                                                          *
* You should have received a copy of the GNU Affero General Public License *
* along with this program in its root directory.                           *
* If not, see <http://www.gnu.org/licenses/>.                              *
\**************************************************************************/

package endercrypt.endercryptschallenge.features.entities.player.sleepmonsters;


import endercrypt.endercryptschallenge.core.Core;
import endercrypt.endercryptschallenge.features.entities.mobs.zombie.hordes.Horde;
import endercrypt.endercryptschallenge.features.entities.mobs.zombie.hordes.HordeManager;
import endercrypt.endercryptschallenge.modules.features.MinecraftFeature;
import endercrypt.endercryptschallenge.modules.worlds.spawner.factory.SpawnAutomator;
import endercrypt.endercryptschallenge.modules.worlds.spawner.factory.SpawnCondition;
import endercrypt.endercryptschallenge.utility.Utility;
import endercrypt.endercryptschallenge.utility.difficulty.Difficulties;
import endercrypt.endercryptschallenge.utility.difficulty.DifficultyRating;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.bukkit.World;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.player.PlayerBedLeaveEvent;


/**
 * @author EnderCrypt
 */
public class SleepMonsters extends MinecraftFeature
{
	public static final SpawnCondition spawnCondition = new SpawnCondition.Builder()
		.setAnyLightLevel()
		.setHeight(2)
		.build();
	
	public static final SpawnAutomator spawnAutomator = new SpawnAutomator.Builder()
		.setCondition(spawnCondition)
		.setChance(1.0)
		.setMinimumDistance(15)
		.setMaximumDistance(20)
		.build();
	
	private Map<World, SleepWorldInfo> worlds = new HashMap<>();
	
	public SleepMonsters(Core core)
	{
		super(core);
		getDebug().enableDebugSupport();
	}
	
	public synchronized SleepWorldInfo getWorld(World world)
	{
		return worlds.computeIfAbsent(world, (w) -> new SleepWorldInfo(this, world));
	}
	
	@Override
	public void onTick()
	{
		for (World world : server.getWorlds())
		{
			getWorld(world).onTick();
		}
	}
	
	@EventHandler
	public void onPlayerSleep(PlayerBedLeaveEvent event)
	{
		if (Utility.isFullSleep(event))
		{
			getWorld(event.getPlayer().getWorld()).onSleep(event);
		}
	}
	
	public List<Horde> createSleepHordes(Player player, double chance)
	{
		HordeManager hordeManager = core.getMinecraftFeatureManager().getFeature(HordeManager.class);
		DifficultyRating difficulty = Difficulties.getFor(player);
		
		List<Horde> hordes = new ArrayList<>();
		int count = Math.round(difficulty.asIntValue(5, 10, 20));
		for (int i = 0; i < count; i++)
		{
			if (Math.random() < chance)
			{
				Horde horde = hordeManager.spawnHorde(player, SleepMonsters.spawnAutomator).orElse(null);
				if (horde != null)
				{
					horde.setTarget(player);
					hordes.add(horde);
				}
			}
		}
		
		debug(player, "spwned " + hordes.size() + " on you (chance was " + chance + " on " + count + " attempts)");
		return hordes;
	}
}
