/**************************************************************************\
* EnderCrypt's Challenge                                                   *
* Copyright (C) 2023 by EnderCrypt                                         *
*                                                                          *
* This program is free software: you can redistribute it and/or modify     *
* it under the terms of the GNU Affero General Public License as           *
* published by the Free Software Foundation, either version 3 of           *
* the License, or (at your option) any later version.                      *
*                                                                          *
* This program is distributed in the hope that it will be useful,          *
* but WITHOUT ANY WARRANTY; without even the implied warranty of           *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
* GNU Affero General Public License for more details.                      *
*                                                                          *
* You should have received a copy of the GNU Affero General Public License *
* along with this program in its root directory.                           *
* If not, see <http://www.gnu.org/licenses/>.                              *
\**************************************************************************/

package endercrypt.endercryptschallenge.features.entities.player.fishing.funny.entity.other;


import endercrypt.endercryptschallenge.features.entities.mobs.spider.manspider.ManSpider;
import endercrypt.endercryptschallenge.features.entities.player.fishing.funny.entity.EntityLaunch;

import java.util.Objects;

import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Spider;
import org.bukkit.event.player.PlayerFishEvent;


/**
 * @author EnderCrypt
 */
public class ManSpiderCatch extends EntityLaunch
{
	private final ManSpider manSpider;
	
	public ManSpiderCatch(ManSpider manSpider)
	{
		this.manSpider = Objects.requireNonNull(manSpider, "manSpider");
	}
	
	@Override
	protected Entity spawn(PlayerFishEvent event, World world, Location location)
	{
		Spider existingManSpider = manSpider.getSpiderMan(world).orElse(null);
		if (existingManSpider != null && existingManSpider.getLocation().getNearbyPlayers(10).isEmpty() == false)
			return null;
		
		Spider spider = world.spawn(location, Spider.class);
		manSpider.createSpiderMan(spider);
		spider.setTarget(event.getPlayer());
		return spider;
	}
}
