/**************************************************************************\
* EnderCrypt's Challenge                                                   *
* Copyright (C) 2023 by EnderCrypt                                         *
*                                                                          *
* This program is free software: you can redistribute it and/or modify     *
* it under the terms of the GNU Affero General Public License as           *
* published by the Free Software Foundation, either version 3 of           *
* the License, or (at your option) any later version.                      *
*                                                                          *
* This program is distributed in the hope that it will be useful,          *
* but WITHOUT ANY WARRANTY; without even the implied warranty of           *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
* GNU Affero General Public License for more details.                      *
*                                                                          *
* You should have received a copy of the GNU Affero General Public License *
* along with this program in its root directory.                           *
* If not, see <http://www.gnu.org/licenses/>.                              *
\**************************************************************************/

package endercrypt.endercryptschallenge.features.entities.player.fishing.entity;


import endercrypt.endercryptschallenge.core.Core;
import endercrypt.endercryptschallenge.modules.features.MinecraftFeature;

import org.bukkit.World;
import org.bukkit.entity.Entity;
import org.bukkit.entity.FishHook;
import org.bukkit.entity.LivingEntity;

import com.google.common.base.Objects;


/**
 * @author EnderCrypt
 */
public class EntityFishing extends MinecraftFeature
{
	public static final double REACH = 1.5;
	public static final Class<? extends Entity> TARGET_CLASS = LivingEntity.class;
	
	public EntityFishing(Core core)
	{
		super(core);
	}
	
	@Override
	public void onSecond()
	{
		for (World world : server.getWorlds())
		{
			world.getEntitiesByClass(FishHook.class).forEach(this::processFishHook);
		}
	}
	
	private void processFishHook(FishHook fishHook)
	{
		if (fishHook.getHookedEntity() != null)
			return;
		
		Entity hooked = fishHook.getLocation().getNearbyEntitiesByType(TARGET_CLASS, REACH).stream().findAny().orElse(null);
		if (hooked == null)
			return;
		
		if (Objects.equal(fishHook.getShooter(), hooked))
			return;
		
		if (Math.random() < 0.8)
			return;
		
		fishHook.setHookedEntity(hooked);
	}
}
