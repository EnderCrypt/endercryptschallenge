/**************************************************************************\
* EnderCrypt's Challenge                                                   *
* Copyright (C) 2023 by EnderCrypt                                         *
*                                                                          *
* This program is free software: you can redistribute it and/or modify     *
* it under the terms of the GNU Affero General Public License as           *
* published by the Free Software Foundation, either version 3 of           *
* the License, or (at your option) any later version.                      *
*                                                                          *
* This program is distributed in the hope that it will be useful,          *
* but WITHOUT ANY WARRANTY; without even the implied warranty of           *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
* GNU Affero General Public License for more details.                      *
*                                                                          *
* You should have received a copy of the GNU Affero General Public License *
* along with this program in its root directory.                           *
* If not, see <http://www.gnu.org/licenses/>.                              *
\**************************************************************************/

package endercrypt.endercryptschallenge.features.entities.player.sleepmonsterdisconnect;


import org.bukkit.entity.Player;


/**
 * @author EnderCrypt
 */
public class FakeSleeper
{
	public static final double DISCONNECT_RATE = 1.25; // chance at which zombie chance starts counting at
	public static final double DECAY_TIME = 60 * 2; // seconds
	
	private final Player player;
	private double multiplier = DISCONNECT_RATE;
	private boolean primed = false;
	
	public FakeSleeper(Player player)
	{
		this.player = player;
	}
	
	public Player getPlayer()
	{
		return player;
	}
	
	public double getMultiplier()
	{
		return multiplier;
	}
	
	public boolean isPrimed()
	{
		return primed;
	}
	
	protected void prime()
	{
		primed = true;
	}
	
	protected void update()
	{
		multiplier -= (1.0 / DECAY_TIME) * DISCONNECT_RATE;
	}
	
	public boolean isDecayed()
	{
		return getMultiplier() <= 0;
	}
	
	@Override
	public String toString()
	{
		return "FakeSleeper [player=" + player.getUniqueId() + ", multiplier=" + multiplier + ", primed=" + primed + "]";
	}
}
