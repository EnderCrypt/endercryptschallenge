/**************************************************************************\
* EnderCrypt's Challenge                                                   *
* Copyright (C) 2023 by EnderCrypt                                         *
*                                                                          *
* This program is free software: you can redistribute it and/or modify     *
* it under the terms of the GNU Affero General Public License as           *
* published by the Free Software Foundation, either version 3 of           *
* the License, or (at your option) any later version.                      *
*                                                                          *
* This program is distributed in the hope that it will be useful,          *
* but WITHOUT ANY WARRANTY; without even the implied warranty of           *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
* GNU Affero General Public License for more details.                      *
*                                                                          *
* You should have received a copy of the GNU Affero General Public License *
* along with this program in its root directory.                           *
* If not, see <http://www.gnu.org/licenses/>.                              *
\**************************************************************************/

package endercrypt.endercryptschallenge.features.entities.player.fishing.funny.entity.mob;


import endercrypt.endercryptschallenge.features.entities.player.fishing.funny.entity.EntityLaunch;

import java.util.Objects;

import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.event.player.PlayerFishEvent;


/**
 * @author EnderCrypt
 */
public class MobLaunch extends EntityLaunch
{
	private final Class<? extends LivingEntity> entityClass;
	
	public MobLaunch(Class<? extends LivingEntity> entityClass)
	{
		this.entityClass = Objects.requireNonNull(entityClass, "ENTITY_CLASS");
	}
	
	@Override
	protected Entity spawn(PlayerFishEvent event, World world, Location location)
	{
		return world.spawn(location, entityClass);
	}
}
