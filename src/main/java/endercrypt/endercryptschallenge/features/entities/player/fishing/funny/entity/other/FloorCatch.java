/**************************************************************************\
* EnderCrypt's Challenge                                                   *
* Copyright (C) 2023 by EnderCrypt                                         *
*                                                                          *
* This program is free software: you can redistribute it and/or modify     *
* it under the terms of the GNU Affero General Public License as           *
* published by the Free Software Foundation, either version 3 of           *
* the License, or (at your option) any later version.                      *
*                                                                          *
* This program is distributed in the hope that it will be useful,          *
* but WITHOUT ANY WARRANTY; without even the implied warranty of           *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
* GNU Affero General Public License for more details.                      *
*                                                                          *
* You should have received a copy of the GNU Affero General Public License *
* along with this program in its root directory.                           *
* If not, see <http://www.gnu.org/licenses/>.                              *
\**************************************************************************/

package endercrypt.endercryptschallenge.features.entities.player.fishing.funny.entity.other;


import endercrypt.endercryptschallenge.features.entities.player.fishing.funny.entity.EntityLaunch;
import endercrypt.endercryptschallenge.materials.MaterialCollections;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.entity.Entity;
import org.bukkit.event.player.PlayerFishEvent;
import org.bukkit.inventory.ItemStack;


/**
 * @author EnderCrypt
 */
public class FloorCatch extends EntityLaunch
{
	@Override
	protected Entity spawn(PlayerFishEvent event, World world, Location location)
	{
		Block block = getGround(location);
		if (block == null)
		{
			return null;
		}
		Material material = block.getType();
		block.setType(Material.AIR);
		return world.dropItem(location, new ItemStack(material));
	}
	
	protected Block getGround(Location location)
	{
		Block focus = location.getBlock();
		
		while (focus.getY() > 0)
		{
			if (focus.isSolid())
			{
				if (MaterialCollections.indestructable.contains(focus))
				{
					return null;
				}
				return focus;
			}
			focus = focus.getRelative(BlockFace.DOWN);
		}
		
		return null;
	}
}
