/**************************************************************************\
* EnderCrypt's Challenge                                                   *
* Copyright (C) 2023 by EnderCrypt                                         *
*                                                                          *
* This program is free software: you can redistribute it and/or modify     *
* it under the terms of the GNU Affero General Public License as           *
* published by the Free Software Foundation, either version 3 of           *
* the License, or (at your option) any later version.                      *
*                                                                          *
* This program is distributed in the hope that it will be useful,          *
* but WITHOUT ANY WARRANTY; without even the implied warranty of           *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
* GNU Affero General Public License for more details.                      *
*                                                                          *
* You should have received a copy of the GNU Affero General Public License *
* along with this program in its root directory.                           *
* If not, see <http://www.gnu.org/licenses/>.                              *
\**************************************************************************/

package endercrypt.endercryptschallenge.features.entities.player.fishing.funny;


import endercrypt.endercryptschallenge.core.Core;
import endercrypt.endercryptschallenge.features.entities.mobs.creeper.cunning.core.CunningCreeperCore;
import endercrypt.endercryptschallenge.features.entities.mobs.spider.manspider.ManSpider;
import endercrypt.endercryptschallenge.features.entities.player.fishing.funny.entity.FunnyFish;
import endercrypt.endercryptschallenge.features.entities.player.fishing.funny.entity.item.ItemLaunch;
import endercrypt.endercryptschallenge.features.entities.player.fishing.funny.entity.item.factory.ItemStackAmountFactory;
import endercrypt.endercryptschallenge.features.entities.player.fishing.funny.entity.item.factory.ItemStackFactory;
import endercrypt.endercryptschallenge.features.entities.player.fishing.funny.entity.mob.MobLaunch;
import endercrypt.endercryptschallenge.features.entities.player.fishing.funny.entity.other.FloorCatch;
import endercrypt.endercryptschallenge.features.entities.player.fishing.funny.entity.other.ManSpiderCatch;
import endercrypt.endercryptschallenge.materials.MaterialCollections;
import endercrypt.endercryptschallenge.modules.features.MinecraftFeature;
import endercrypt.endercryptschallenge.utility.difficulty.Difficulties;
import endercrypt.endercryptschallenge.utility.difficulty.DifficultyRating;
import endercrypt.library.commons.randomizer.Randomizer;

import org.bukkit.Material;
import org.bukkit.entity.Creeper;
import org.bukkit.entity.Dolphin;
import org.bukkit.entity.Drowned;
import org.bukkit.entity.Guardian;
import org.bukkit.entity.Player;
import org.bukkit.entity.PufferFish;
import org.bukkit.entity.Skeleton;
import org.bukkit.entity.Turtle;
import org.bukkit.event.EventHandler;
import org.bukkit.event.player.PlayerFishEvent;
import org.bukkit.event.player.PlayerFishEvent.State;


/**
 * @author EnderCrypt
 */
public class FunnyFishing extends MinecraftFeature
{
	private final Randomizer<FunnyFish> fishes = new Randomizer<>();
	
	public FunnyFishing(Core core)
	{
		super(core);
	}
	
	@Override
	public void onReady()
	{
		super.onReady();
		
		//mobs
		fishes.enter(1000, new MobLaunch(Dolphin.class));
		fishes.enter(750, new MobLaunch(Turtle.class));
		fishes.enter(750, new MobLaunch(PufferFish.class));
		fishes.enter(600, new MobLaunch(Drowned.class));
		fishes.enter(500, new MobLaunch(Creeper.class));
		fishes.enter(400, new MobLaunch(Guardian.class));
		fishes.enter(250, new MobLaunch(Skeleton.class));
		
		// items
		fishes.enter(500, new ItemLaunch(new ItemStackFactory.Builder()
			.setMaterial(Material.KELP)
			.setRandomAmount(1, 3)
			.build()));
		
		fishes.enter(500, new ItemLaunch(new ItemStackFactory.Builder()
			.setMaterial(Material.SPIDER_SPAWN_EGG)
			.setAmount(ItemStackAmountFactory.from(2, 4, 6).to(4, 8, 12))
			.build()));
		
		fishes.enter(100, new ItemLaunch(new ItemStackFactory.Builder()
			.setMaterial(MaterialCollections.boats)
			.build()));
		
		// other
		fishes.enter(1000, new FloorCatch());
		fishes.enter(500, new ManSpiderCatch(getFeature(ManSpider.class)));
	}
	
	@EventHandler
	public void onPlayerFish(PlayerFishEvent event)
	{
		if (event.getState() != State.CAUGHT_FISH)
			return;
		
		// cunningness
		Player player = event.getPlayer();
		getFeature(CunningCreeperCore.class).getTriggers().punch(player.getLocation(), 0.25);
		
		// chance
		DifficultyRating difficulty = Difficulties.getFor(event);
		double chance = 1.0 / difficulty.asDoubleValue(4, 3, 2);
		if (Math.random() > chance)
			return;
		
		// success
		event.setCancelled(true);
		event.getHook().remove();
		
		for (int i = 0; i < 5; i++)
		{
			FunnyFish funny = fishes.pull();
			if (funny.pull(event))
			{
				return;
			}
		}
	}
}
