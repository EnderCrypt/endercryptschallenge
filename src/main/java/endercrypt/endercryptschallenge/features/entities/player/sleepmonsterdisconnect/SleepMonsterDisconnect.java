/**************************************************************************\
* EnderCrypt's Challenge                                                   *
* Copyright (C) 2023 by EnderCrypt                                         *
*                                                                          *
* This program is free software: you can redistribute it and/or modify     *
* it under the terms of the GNU Affero General Public License as           *
* published by the Free Software Foundation, either version 3 of           *
* the License, or (at your option) any later version.                      *
*                                                                          *
* This program is distributed in the hope that it will be useful,          *
* but WITHOUT ANY WARRANTY; without even the implied warranty of           *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
* GNU Affero General Public License for more details.                      *
*                                                                          *
* You should have received a copy of the GNU Affero General Public License *
* along with this program in its root directory.                           *
* If not, see <http://www.gnu.org/licenses/>.                              *
\**************************************************************************/

package endercrypt.endercryptschallenge.features.entities.player.sleepmonsterdisconnect;


import endercrypt.endercryptschallenge.core.Core;
import endercrypt.endercryptschallenge.features.entities.player.sleepmonsters.SleepMonsters;
import endercrypt.endercryptschallenge.modules.features.MinecraftFeature;
import endercrypt.endercryptschallenge.utility.Utility;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.UUID;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.player.PlayerBedLeaveEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;


/**
 * @author EnderCrypt
 */
public class SleepMonsterDisconnect extends MinecraftFeature
{
	private Map<UUID, FakeSleeper> fakeSleepers = new HashMap<>();
	
	public SleepMonsterDisconnect(Core core)
	{
		super(core);
		getDebug().enableDebugSupport();
	}
	
	@Override
	public void onSecond()
	{
		Iterator<Entry<UUID, FakeSleeper>> iterator = fakeSleepers.entrySet().iterator();
		while (iterator.hasNext())
		{
			Entry<UUID, FakeSleeper> entry = iterator.next();
			FakeSleeper fakeSleeper = entry.getValue();
			fakeSleeper.update();
			if (fakeSleeper.isDecayed())
			{
				Player player = fakeSleeper.getPlayer();
				debug(player, "Sleep monster disconnect multiplier is now gone");
				iterator.remove();
			}
		}
	}
	
	@EventHandler
	public void onPlayerSleep(PlayerBedLeaveEvent event)
	{
		if (Utility.isFullSleep(event))
		{
			fakeSleepers.values().forEach(FakeSleeper::prime);
		}
	}
	
	@EventHandler
	public void onPlayerDisconnect(PlayerQuitEvent event)
	{
		Player player = event.getPlayer();
		fakeSleepers.put(player.getUniqueId(), new FakeSleeper(player));
	}
	
	@EventHandler
	public void onPlayerConnect(PlayerJoinEvent event)
	{
		Player player = event.getPlayer();
		
		FakeSleeper fakeSleeper = fakeSleepers.remove(player.getUniqueId());
		
		if (fakeSleeper == null)
			return;
		
		if (fakeSleeper.isPrimed() == false)
			return;
		
		double multiplier = fakeSleeper.getMultiplier();
		debug(player, "rejoined with " + multiplier + " sleep monster multiplier (primed)");
		getFeature(SleepMonsters.class).createSleepHordes(player, multiplier);
	}
}
