/**************************************************************************\
* EnderCrypt's Challenge                                                   *
* Copyright (C) 2023 by EnderCrypt                                         *
*                                                                          *
* This program is free software: you can redistribute it and/or modify     *
* it under the terms of the GNU Affero General Public License as           *
* published by the Free Software Foundation, either version 3 of           *
* the License, or (at your option) any later version.                      *
*                                                                          *
* This program is distributed in the hope that it will be useful,          *
* but WITHOUT ANY WARRANTY; without even the implied warranty of           *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
* GNU Affero General Public License for more details.                      *
*                                                                          *
* You should have received a copy of the GNU Affero General Public License *
* along with this program in its root directory.                           *
* If not, see <http://www.gnu.org/licenses/>.                              *
\**************************************************************************/

package endercrypt.endercryptschallenge.features.entities.player.fishing.funny.entity.item;


import endercrypt.endercryptschallenge.features.entities.player.fishing.funny.entity.EntityLaunch;
import endercrypt.endercryptschallenge.features.entities.player.fishing.funny.entity.item.factory.ItemStackFactory;

import java.util.Objects;

import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.entity.Entity;
import org.bukkit.event.player.PlayerFishEvent;


/**
 * @author EnderCrypt
 */
public class ItemLaunch extends EntityLaunch
{
	private final ItemStackFactory itemStackFactory;
	
	public ItemLaunch(ItemStackFactory itemStackSupplier)
	{
		this.itemStackFactory = Objects.requireNonNull(itemStackSupplier, "itemStackFactory");
	}
	
	@Override
	protected Entity spawn(PlayerFishEvent event, World world, Location location)
	{
		return world.dropItem(location, itemStackFactory.produce(world));
	}
}
