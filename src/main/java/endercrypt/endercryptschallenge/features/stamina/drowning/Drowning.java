/**************************************************************************\
* EnderCrypt's Challenge                                                   *
* Copyright (C) 2023 by EnderCrypt                                         *
*                                                                          *
* This program is free software: you can redistribute it and/or modify     *
* it under the terms of the GNU Affero General Public License as           *
* published by the Free Software Foundation, either version 3 of           *
* the License, or (at your option) any later version.                      *
*                                                                          *
* This program is distributed in the hope that it will be useful,          *
* but WITHOUT ANY WARRANTY; without even the implied warranty of           *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
* GNU Affero General Public License for more details.                      *
*                                                                          *
* You should have received a copy of the GNU Affero General Public License *
* along with this program in its root directory.                           *
* If not, see <http://www.gnu.org/licenses/>.                              *
\**************************************************************************/

package endercrypt.endercryptschallenge.features.stamina.drowning;


import endercrypt.endercryptschallenge.core.Core;
import endercrypt.endercryptschallenge.modules.features.MinecraftFeature;
import endercrypt.endercryptschallenge.utility.Utility;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.util.Vector;


/**
 * @author EnderCrypt
 */
public class Drowning extends MinecraftFeature
{
	public Drowning(Core core)
	{
		super(core);
	}
	
	@Override
	public void onTick()
	{
		for (World world : server.getWorlds())
		{
			for (Player player : world.getPlayers())
			{
				updatePlayer(player);
			}
		}
	}
	
	private void updatePlayer(Player player)
	{
		if (player.isInWater() == false)
		{
			return;
		}
		Location standOnLocation = player.getLocation().add(0, -0.01, 0);
		Block standOnBlock = standOnLocation.getBlock();
		// player.sendMessage("Standing on: " + standOnBlock.getType().name());
		if (standOnBlock.getType() != Material.WATER)
		{
			return;
		}
		
		if (resolve(player).stamina().burnStaminaByWeight(0.00055, 0.0006, 0.0007) == false)
		{
			drownPlayer(player);
		}
	}
	
	private void drownPlayer(Player player)
	{
		double weight = Utility.getPlayerWeight(player);
		double pull = weight * 0.0002;
		
		Vector velocity = player.getVelocity();
		velocity.add(new Vector(0, -pull, 0));
		player.setVelocity(velocity);
	}
}
