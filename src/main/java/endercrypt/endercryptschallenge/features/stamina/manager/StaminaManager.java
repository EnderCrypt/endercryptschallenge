/**************************************************************************\
* EnderCrypt's Challenge                                                   *
* Copyright (C) 2023 by EnderCrypt                                         *
*                                                                          *
* This program is free software: you can redistribute it and/or modify     *
* it under the terms of the GNU Affero General Public License as           *
* published by the Free Software Foundation, either version 3 of           *
* the License, or (at your option) any later version.                      *
*                                                                          *
* This program is distributed in the hope that it will be useful,          *
* but WITHOUT ANY WARRANTY; without even the implied warranty of           *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
* GNU Affero General Public License for more details.                      *
*                                                                          *
* You should have received a copy of the GNU Affero General Public License *
* along with this program in its root directory.                           *
* If not, see <http://www.gnu.org/licenses/>.                              *
\**************************************************************************/

package endercrypt.endercryptschallenge.features.stamina.manager;


import endercrypt.endercryptschallenge.core.Core;
import endercrypt.endercryptschallenge.modules.features.MinecraftFeature;
import endercrypt.endercryptschallenge.modules.players.values.stamina.StaminaPlayerValue;
import endercrypt.library.commons.misc.Ender;

import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.PlayerAnimationType;
import org.bukkit.event.player.PlayerBedLeaveEvent;
import org.bukkit.event.player.PlayerMoveEvent;

import com.destroystokyo.paper.event.player.PlayerJumpEvent;

import io.papermc.paper.event.player.PlayerArmSwingEvent;


/**
 * @author EnderCrypt
 */
public class StaminaManager extends MinecraftFeature
{
	public StaminaManager(Core core)
	{
		super(core);
		getDebug().enableDebugSupport();
	}
	
	private final PlayerDebug debugPlayer = new PlayerDebug()
	{
		@Override
		public String log(Player player)
		{
			StaminaPlayerValue stamina = resolve(player).stamina();
			StringBuilder sb = new StringBuilder();
			sb.append(stamina.getPlayerActivity());
			if (stamina.isWeak())
			{
				sb.append(" [weak]");
			}
			if (stamina.getSugar() > 0.1)
			{
				sb.append(" [sugar: " + Ender.math.round(stamina.getSugar(), 1) + "]");
			}
			sb.append(" " + Ender.math.round(stamina.getStamina(), 1) + "/100");
			return sb.toString();
		}
	};
	
	@Override
	public void onSecond()
	{
		debug(debugPlayer);
	}
	
	@EventHandler
	public void onPlayerMove(PlayerMoveEvent event)
	{
		if (event.getFrom().distance(event.getTo()) > 0.05)
		{
			Player player = event.getPlayer();
			resolve(player).stamina().onPlayerMove();
		}
	}
	
	@EventHandler
	public void onPlayerDeath(PlayerDeathEvent event)
	{
		Player player = event.getPlayer();
		resolve(player).stamina().setStamina(1.0);
	}
	
	@EventHandler
	public void onJump(PlayerJumpEvent event)
	{
		Player player = event.getPlayer();
		resolve(player).stamina().onPlayerJump();
	}
	
	@EventHandler
	public void onPlayerAttack(EntityDamageByEntityEvent event)
	{
		if (event.getDamager() instanceof Player player && event.getCause() == DamageCause.ENTITY_ATTACK && event.getEntity() instanceof LivingEntity target)
		{
			if (resolve(player).stamina().onSwingArm(true) == false)
			{
				event.setCancelled(true);
			}
		}
	}
	
	@EventHandler
	public void onPlayerWork(PlayerArmSwingEvent event)
	{
		if (event.getAnimationType() == PlayerAnimationType.ARM_SWING)
		{
			if (resolve(event.getPlayer()).stamina().onSwingArm(false) == false)
			{
				event.setCancelled(true);
			}
		}
	}
	
	@EventHandler
	public void onPlayerSleep(PlayerBedLeaveEvent event)
	{
		Player player = event.getPlayer();
		if (player.getWorld().getTime() != 0)
		{
			return;
		}
		resolve(player).stamina().setStamina(1.0);
	}
}
