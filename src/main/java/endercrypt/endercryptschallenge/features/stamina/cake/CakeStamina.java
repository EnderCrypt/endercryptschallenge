/**************************************************************************\
* EnderCrypt's Challenge                                                   *
* Copyright (C) 2023 by EnderCrypt                                         *
*                                                                          *
* This program is free software: you can redistribute it and/or modify     *
* it under the terms of the GNU Affero General Public License as           *
* published by the Free Software Foundation, either version 3 of           *
* the License, or (at your option) any later version.                      *
*                                                                          *
* This program is distributed in the hope that it will be useful,          *
* but WITHOUT ANY WARRANTY; without even the implied warranty of           *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
* GNU Affero General Public License for more details.                      *
*                                                                          *
* You should have received a copy of the GNU Affero General Public License *
* along with this program in its root directory.                           *
* If not, see <http://www.gnu.org/licenses/>.                              *
\**************************************************************************/

package endercrypt.endercryptschallenge.features.stamina.cake;


import endercrypt.endercryptschallenge.core.Core;
import endercrypt.endercryptschallenge.modules.features.MinecraftFeature;

import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.craftbukkit.v1_21_R1.block.impl.CraftCake;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;


/**
 * @author EnderCrypt
 */
public class CakeStamina extends MinecraftFeature
{
	public CakeStamina(Core core)
	{
		super(core);
	}
	
	@EventHandler
	public void onEatCake(PlayerInteractEvent event)
	{
		Player player = event.getPlayer();
		if (event.getAction() != Action.RIGHT_CLICK_BLOCK)
			return;
		
		Block block = event.getClickedBlock();
		if (block.getType() != Material.CAKE)
			return;
		
		if (player.getFoodLevel() >= 20)
		{
			CraftCake cakeData = (CraftCake) block.getBlockData();
			int bites = cakeData.getBites();
			bites++;
			if (bites > 6)
			{
				block.breakNaturally(false);
				return;
			}
			cakeData.setBites(bites);
			block.setBlockData(cakeData);
		}
		
		resolve(player).stamina().addSugar(400.0);
	}
}
