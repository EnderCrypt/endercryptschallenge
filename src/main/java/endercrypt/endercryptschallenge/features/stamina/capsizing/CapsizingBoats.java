/**************************************************************************\
* EnderCrypt's Challenge                                                   *
* Copyright (C) 2023 by EnderCrypt                                         *
*                                                                          *
* This program is free software: you can redistribute it and/or modify     *
* it under the terms of the GNU Affero General Public License as           *
* published by the Free Software Foundation, either version 3 of           *
* the License, or (at your option) any later version.                      *
*                                                                          *
* This program is distributed in the hope that it will be useful,          *
* but WITHOUT ANY WARRANTY; without even the implied warranty of           *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
* GNU Affero General Public License for more details.                      *
*                                                                          *
* You should have received a copy of the GNU Affero General Public License *
* along with this program in its root directory.                           *
* If not, see <http://www.gnu.org/licenses/>.                              *
\**************************************************************************/

package endercrypt.endercryptschallenge.features.stamina.capsizing;


import endercrypt.endercryptschallenge.core.Core;
import endercrypt.endercryptschallenge.modules.features.MinecraftFeature;
import endercrypt.endercryptschallenge.utility.Utility;
import endercrypt.endercryptschallenge.utility.difficulty.Difficulties;
import endercrypt.endercryptschallenge.utility.difficulty.DifficultyRating;

import org.bukkit.entity.Boat;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;


/**
 * @author EnderCrypt
 */
public class CapsizingBoats extends MinecraftFeature
{
	public CapsizingBoats(Core core)
	{
		super(core);
	}
	
	@Override
	public void onCycleEntity(Entity entity)
	{
		if (entity instanceof Player player)
		{
			processPlayer(player);
		}
	}
	
	private void processPlayer(Player player)
	{
		DifficultyRating difficulty = Difficulties.getFor(player);
		if (Math.random() > 1.0 / difficulty.asIntValue(9, 6, 3))
		{
			return;
		}
		if (player.getVehicle() instanceof Boat == false)
		{
			return;
		}
		Boat boat = (Boat) player.getVehicle();
		if (resolve(player).stamina().isWeak() == false)
		{
			return;
		}
		capsize(player, boat);
	}
	
	private void capsize(Player cause, Boat boat)
	{
		for (Entity entity : boat.getPassengers())
		{
			entity.sendMessage(getMessage(cause, entity));
		}
		Utility.destroyVehicle(boat);
	}
	
	private String getMessage(Player cause, Entity target)
	{
		if (cause.equals(target))
		{
			return "In your exhaustion, you manage to completly capsize the boat!";
		}
		else
		{
			return "In their exhaustion, " + cause.getName() + " manages to somehow capsize the boat!";
		}
	}
}
