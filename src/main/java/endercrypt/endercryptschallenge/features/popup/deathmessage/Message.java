/**************************************************************************\
* EnderCrypt's Challenge                                                   *
* Copyright (C) 2023 by EnderCrypt                                         *
*                                                                          *
* This program is free software: you can redistribute it and/or modify     *
* it under the terms of the GNU Affero General Public License as           *
* published by the Free Software Foundation, either version 3 of           *
* the License, or (at your option) any later version.                      *
*                                                                          *
* This program is distributed in the hope that it will be useful,          *
* but WITHOUT ANY WARRANTY; without even the implied warranty of           *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
* GNU Affero General Public License for more details.                      *
*                                                                          *
* You should have received a copy of the GNU Affero General Public License *
* along with this program in its root directory.                           *
* If not, see <http://www.gnu.org/licenses/>.                              *
\**************************************************************************/

package endercrypt.endercryptschallenge.features.popup.deathmessage;


import org.bukkit.entity.Player;

import net.kyori.adventure.text.Component;


/**
 * @author EnderCrypt
 */
public class Message
{
	private final Player player;
	private final Component text;
	private int ticks;
	
	public Message(Player player, Component text, int ticks)
	{
		this.player = player;
		this.text = text;
		this.ticks = ticks;
	}
	
	public Player getPlayer()
	{
		return player;
	}
	
	public Component getText()
	{
		return text;
	}
	
	public int getTicks()
	{
		return ticks;
	}
	
	public void tick()
	{
		ticks--;
	}
}
