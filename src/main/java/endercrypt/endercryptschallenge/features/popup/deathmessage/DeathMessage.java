/**************************************************************************\
* EnderCrypt's Challenge                                                   *
* Copyright (C) 2023 by EnderCrypt                                         *
*                                                                          *
* This program is free software: you can redistribute it and/or modify     *
* it under the terms of the GNU Affero General Public License as           *
* published by the Free Software Foundation, either version 3 of           *
* the License, or (at your option) any later version.                      *
*                                                                          *
* This program is distributed in the hope that it will be useful,          *
* but WITHOUT ANY WARRANTY; without even the implied warranty of           *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
* GNU Affero General Public License for more details.                      *
*                                                                          *
* You should have received a copy of the GNU Affero General Public License *
* along with this program in its root directory.                           *
* If not, see <http://www.gnu.org/licenses/>.                              *
\**************************************************************************/

package endercrypt.endercryptschallenge.features.popup.deathmessage;


import endercrypt.endercryptschallenge.core.Core;
import endercrypt.endercryptschallenge.modules.features.MinecraftFeature;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.entity.PlayerDeathEvent;

import net.kyori.adventure.text.Component;


/**
 * @author EnderCrypt
 */
public class DeathMessage extends MinecraftFeature
{
	private Map<Player, Message> messages = new HashMap<>();
	
	public DeathMessage(Core core)
	{
		super(core);
	}
	
	public void setKilledBy(Player player, String text)
	{
		set(player, player.getName() + " was killed by " + text);
	}
	
	public void set(Player player, String text)
	{
		set(player, Component.text(text));
	}
	
	public void set(Player player, Component text)
	{
		set(player, text, 1);
	}
	
	public void set(Player player, Component text, int ticks)
	{
		if (ticks < 1)
		{
			throw new IllegalArgumentException("cant be " + ticks + " ticks");
		}
		Message message = new Message(player, text, ticks);
		messages.put(player, message);
	}
	
	@Override
	public void onTick()
	{
		Iterator<Entry<Player, Message>> iterator = messages.entrySet().iterator();
		while (iterator.hasNext())
		{
			Entry<Player, Message> entry = iterator.next();
			Message message = entry.getValue();
			message.tick();
			if (message.getTicks() <= 0)
			{
				iterator.remove();
			}
		}
	}
	
	@EventHandler
	public void onPlayerDeath(PlayerDeathEvent event)
	{
		Player player = event.getPlayer();
		Message message = messages.remove(player);
		if (message == null)
		{
			return;
		}
		event.deathMessage(message.getText());
	}
}
