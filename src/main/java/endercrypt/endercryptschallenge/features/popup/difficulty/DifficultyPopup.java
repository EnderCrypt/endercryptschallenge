/**************************************************************************\
* EnderCrypt's Challenge                                                   *
* Copyright (C) 2023 by EnderCrypt                                         *
*                                                                          *
* This program is free software: you can redistribute it and/or modify     *
* it under the terms of the GNU Affero General Public License as           *
* published by the Free Software Foundation, either version 3 of           *
* the License, or (at your option) any later version.                      *
*                                                                          *
* This program is distributed in the hope that it will be useful,          *
* but WITHOUT ANY WARRANTY; without even the implied warranty of           *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
* GNU Affero General Public License for more details.                      *
*                                                                          *
* You should have received a copy of the GNU Affero General Public License *
* along with this program in its root directory.                           *
* If not, see <http://www.gnu.org/licenses/>.                              *
\**************************************************************************/

package endercrypt.endercryptschallenge.features.popup.difficulty;


import endercrypt.endercryptschallenge.core.Core;
import endercrypt.endercryptschallenge.modules.features.MinecraftFeature;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import org.bukkit.ChatColor;
import org.bukkit.Difficulty;
import org.bukkit.World;


/**
 * @author EnderCrypt
 */
public class DifficultyPopup extends MinecraftFeature
{
	private static final String defaultDifficultyEffect = ChatColor.AQUA.toString() + ChatColor.ITALIC.toString();
	private static final String reset = ChatColor.RESET + defaultDifficultyEffect;
	
	private final Map<World, Difficulty> worldDifficulties = new HashMap<>();
	
	private final Map<Difficulty, String> descriptions = new HashMap<>();
	
	public DifficultyPopup(Core core)
	{
		super(core);
		final String bad = ChatColor.RED.toString() + ChatColor.UNDERLINE.toString();
		descriptions.put(Difficulty.PEACEFUL, ChatColor.BOLD + "Not playing" + reset + " EnderCrypt's Challenge");
		descriptions.put(Difficulty.EASY, "A proper challenge for the worthy!");
		descriptions.put(Difficulty.NORMAL, "A " + bad + "nightmare" + reset + ", for the foolish!");
		descriptions.put(Difficulty.HARD, bad + "Surrounded by perpetual death!");
	}
	
	@Override
	public void onTick()
	{
		// add worlds
		server.getWorlds().forEach(world -> worldDifficulties.computeIfAbsent(world, World::getDifficulty));
		
		// check difficulty
		for (Entry<World, Difficulty> entry : worldDifficulties.entrySet())
		{
			World world = entry.getKey();
			Difficulty oldDifficulty = entry.getValue();
			Difficulty newDifficulty = world.getDifficulty();
			if (oldDifficulty != newDifficulty)
			{
				worldDifficulties.put(world, newDifficulty);
				
				String difficultyMessage = "Difficulty has been updated: " + oldDifficulty + " -> " + getDifficultyText(newDifficulty);
				
				world.getPlayers().forEach(p -> {
					p.sendMessage(difficultyMessage);
				});
			}
		}
	}
	
	public String getDifficultyText(Difficulty difficulty)
	{
		String difficultyDescription = descriptions.get(difficulty);
		return difficulty + "\n> " + reset + difficultyDescription + ChatColor.RESET + " <";
	}
}
