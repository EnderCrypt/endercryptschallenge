/**************************************************************************\
* EnderCrypt's Challenge                                                   *
* Copyright (C) 2023 by EnderCrypt                                         *
*                                                                          *
* This program is free software: you can redistribute it and/or modify     *
* it under the terms of the GNU Affero General Public License as           *
* published by the Free Software Foundation, either version 3 of           *
* the License, or (at your option) any later version.                      *
*                                                                          *
* This program is distributed in the hope that it will be useful,          *
* but WITHOUT ANY WARRANTY; without even the implied warranty of           *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
* GNU Affero General Public License for more details.                      *
*                                                                          *
* You should have received a copy of the GNU Affero General Public License *
* along with this program in its root directory.                           *
* If not, see <http://www.gnu.org/licenses/>.                              *
\**************************************************************************/

package endercrypt.endercryptschallenge.features.village.land.destruction;


import endercrypt.endercryptschallenge.features.village.land.VillageLand;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.Chest;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;


/**
 * @author EnderCrypt
 */
public class ChestDestructionCalculator implements BlockDestructionCalculator
{
	private static final Map<String, Double> itemValues = new HashMap<>();
	
	static
	{
		itemValues.put("DIAMOND", 100.0);
		itemValues.put("EMERALD", 100.0);
		itemValues.put("IRON", 20.0);
		itemValues.put("BREAD", 5.0);
	}
	
	@Override
	public double getThreat(Block block, Player player)
	{
		if (block.getState()instanceof Chest chest)
		{
			ItemStack[] items = chest.getBlockInventory().getStorageContents();
			return getItemThreat(items);
		}
		return 0;
	}
	
	public static double getItemThreat(ItemStack... items)
	{
		double threat = 0;
		int itemCount = 0;
		for (ItemStack item : items)
		{
			if (item != null)
			{
				itemCount++;
				threat += getItemValue(item.getType()) * item.getAmount() * VillageLand.ITEM_THEFT_THREAT_MULTIPLIER;
			}
		}
		return threat + (itemCount > 0 ? VillageLand.ITEM_THEFT_THREAT_BASE : 0);
	}
	
	public static double getItemValue(Material material)
	{
		String id = material.toString();
		
		for (Entry<String, Double> entry : itemValues.entrySet())
		{
			String name = entry.getKey();
			if (id.contains(name))
			{
				return entry.getValue();
			}
		}
		
		return 1.0;
	}
}
