/**************************************************************************\
* EnderCrypt's Challenge                                                   *
* Copyright (C) 2023 by EnderCrypt                                         *
*                                                                          *
* This program is free software: you can redistribute it and/or modify     *
* it under the terms of the GNU Affero General Public License as           *
* published by the Free Software Foundation, either version 3 of           *
* the License, or (at your option) any later version.                      *
*                                                                          *
* This program is distributed in the hope that it will be useful,          *
* but WITHOUT ANY WARRANTY; without even the implied warranty of           *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
* GNU Affero General Public License for more details.                      *
*                                                                          *
* You should have received a copy of the GNU Affero General Public License *
* along with this program in its root directory.                           *
* If not, see <http://www.gnu.org/licenses/>.                              *
\**************************************************************************/

package endercrypt.endercryptschallenge.features.village.land;


import endercrypt.endercryptschallenge.core.Core;
import endercrypt.endercryptschallenge.features.village.land.destruction.BedDestructionCalculator;
import endercrypt.endercryptschallenge.features.village.land.destruction.BlockDestructionCalculator;
import endercrypt.endercryptschallenge.features.village.land.destruction.ChestDestructionCalculator;
import endercrypt.endercryptschallenge.features.village.land.destruction.MaterialBlockDestruction;
import endercrypt.endercryptschallenge.modules.features.MinecraftFeature;
import endercrypt.endercryptschallenge.utility.Utility;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.bukkit.Material;
import org.bukkit.Particle;
import org.bukkit.block.Block;
import org.bukkit.block.Chest;
import org.bukkit.entity.Player;
import org.bukkit.entity.Villager;
import org.bukkit.event.EventHandler;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.inventory.InventoryAction;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.InventoryHolder;
import org.bukkit.inventory.ItemStack;


/**
 * @author EnderCrypt
 */
public class VillageLand extends MinecraftFeature
{
	public static final int VILLAGER_SIZE = 2;
	public static final int VILLAGER_RANGE = 25;
	public static final int OWNERSHIP_CHECK_TIMEOUT = 20 * 60 * 1; // 1 minutes
	
	public static final double ITEM_THEFT_THREAT_BASE = 10.0;
	public static final double ITEM_THEFT_THREAT_MULTIPLIER = 1.0;
	
	private final Map<Block, BlockOwnership> blocks = new HashMap<>();
	
	private final List<BlockDestructionCalculator> blockCalculators = new ArrayList<>();
	
	public VillageLand(Core core)
	{
		super(core);
		blockCalculators.add(new ChestDestructionCalculator());
		blockCalculators.add(new BedDestructionCalculator(20));
		blockCalculators.add(new MaterialBlockDestruction(5.0, Material.MELON_STEM, Material.ATTACHED_MELON_STEM, Material.PUMPKIN, Material.PUMPKIN_STEM));
		blockCalculators.add(new MaterialBlockDestruction(5.0, Material.WHEAT, Material.CARROTS, Material.POTATOES));
		blockCalculators.add(new MaterialBlockDestruction(10.0, Material.GLASS_PANE));
		blockCalculators.add(new MaterialBlockDestruction(10.0, Material.HAY_BLOCK));
		blockCalculators.add(new MaterialBlockDestruction(15.0, Material.BOOKSHELF));
		blockCalculators.add(new MaterialBlockDestruction(20.0, Material.GRINDSTONE, Material.LECTERN, Material.SMOKER, Material.COMPOSTER, Material.LOOM, Material.BLAST_FURNACE, Material.CAULDRON));
		blockCalculators.add(new MaterialBlockDestruction(30.0, Material.BREWING_STAND));
		blockCalculators.add(new MaterialBlockDestruction(50.0, Material.BELL));
	}
	
	@EventHandler
	public void onPlayerDestroyChest(BlockBreakEvent event)
	{
		Block block = event.getBlock();
		Player player = event.getPlayer();
		
		if (isLegalBlock(block))
		{
			return;
		}
		
		double threat = 0.0;
		for (BlockDestructionCalculator blockDestructionCalculator : blockCalculators)
		{
			threat += blockDestructionCalculator.getThreat(block, player);
		}
		
		if (threat > 0)
		{
			Collection<ItemStack> drops = block.getDrops();
			String itemNames = drops.stream().map(ItemStack::getI18NDisplayName).collect(Collectors.joining(","));
			String blockName = drops.isEmpty() ? "" : ": " + itemNames;
			resolve(player).threat().add(threat, "Destroyed village property" + blockName);
			
			int particleAmount = (int) Math.ceil(threat / 5);
			block.getWorld().spawnParticle(Particle.ANGRY_VILLAGER, block.getLocation(), particleAmount, 1, 1, 1, 1);
		}
	}
	
	@EventHandler
	public void onInteractWithChest(InventoryClickEvent event)
	{
		InventoryAction action = event.getAction();
		if (action.name().startsWith("PICKUP_") == false && action != InventoryAction.MOVE_TO_OTHER_INVENTORY)
		{
			return;
		}
		ItemStack item = event.getCurrentItem();
		if (item.getType() == Material.AIR)
		{
			return;
		}
		Inventory inventory = event.getClickedInventory();
		InventoryHolder holder = inventory.getHolder();
		if (holder instanceof Chest == false)
		{
			return;
		}
		Chest chest = (Chest) holder;
		if (isLegalBlock(chest.getBlock()))
		{
			return;
		}
		if (event.getWhoClicked() instanceof Player player)
		{
			double threat = ChestDestructionCalculator.getItemThreat(item);
			resolve(player).threat().add(threat, "Stole " + item.getAmount() + " " + Utility.getItemName(item));
		}
	}
	
	private boolean isInVillage(Block block)
	{
		int villagers = block.getLocation().getNearbyEntitiesByType(Villager.class, VILLAGER_RANGE).size();
		return villagers >= VILLAGER_SIZE;
	}
	
	private BlockOwnership getBlockInformation(Block block)
	{
		BlockOwnership chestInformation = blocks.get(block);
		if (chestInformation == null || chestInformation.isExpired())
		{
			boolean legal = isInVillage(block) == false;
			chestInformation = new BlockOwnership(server, block, legal);
			blocks.put(block, chestInformation);
		}
		return chestInformation;
	}
	
	private boolean isLegalBlock(Block block)
	{
		return getBlockInformation(block).isLegal();
	}
}
