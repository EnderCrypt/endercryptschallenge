/**************************************************************************\
* EnderCrypt's Challenge                                                   *
* Copyright (C) 2023 by EnderCrypt                                         *
*                                                                          *
* This program is free software: you can redistribute it and/or modify     *
* it under the terms of the GNU Affero General Public License as           *
* published by the Free Software Foundation, either version 3 of           *
* the License, or (at your option) any later version.                      *
*                                                                          *
* This program is distributed in the hope that it will be useful,          *
* but WITHOUT ANY WARRANTY; without even the implied warranty of           *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
* GNU Affero General Public License for more details.                      *
*                                                                          *
* You should have received a copy of the GNU Affero General Public License *
* along with this program in its root directory.                           *
* If not, see <http://www.gnu.org/licenses/>.                              *
\**************************************************************************/

package endercrypt.endercryptschallenge.features.village.land.destruction;


import org.bukkit.block.Block;
import org.bukkit.entity.Player;


/**
 * @author EnderCrypt
 */
public class BedDestructionCalculator extends BaseBlockDestructionCalculator
{
	public BedDestructionCalculator(double threat)
	{
		super(threat);
	}
	
	@Override
	public double getThreat(Block block, Player player)
	{
		if (isBed(block) == false)
		{
			return 0.0;
		}
		return super.getThreat(block, player);
	}
	
	private boolean isBed(Block block)
	{
		return block.getType().name().endsWith("_BED");
	}
}
