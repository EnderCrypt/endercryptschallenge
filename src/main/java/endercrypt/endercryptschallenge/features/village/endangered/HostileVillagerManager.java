/**************************************************************************\
* EnderCrypt's Challenge                                                   *
* Copyright (C) 2023 by EnderCrypt                                         *
*                                                                          *
* This program is free software: you can redistribute it and/or modify     *
* it under the terms of the GNU Affero General Public License as           *
* published by the Free Software Foundation, either version 3 of           *
* the License, or (at your option) any later version.                      *
*                                                                          *
* This program is distributed in the hope that it will be useful,          *
* but WITHOUT ANY WARRANTY; without even the implied warranty of           *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
* GNU Affero General Public License for more details.                      *
*                                                                          *
* You should have received a copy of the GNU Affero General Public License *
* along with this program in its root directory.                           *
* If not, see <http://www.gnu.org/licenses/>.                              *
\**************************************************************************/

package endercrypt.endercryptschallenge.features.village.endangered;


import endercrypt.endercryptschallenge.core.Core;
import endercrypt.endercryptschallenge.modules.features.MinecraftFeature;
import endercrypt.endercryptschallenge.modules.players.PlayerProfile;
import endercrypt.endercryptschallenge.utility.difficulty.Difficulties;
import endercrypt.endercryptschallenge.utility.difficulty.DifficultyRating;
import endercrypt.endercryptschallenge.utility.tracker.entity.EntityTracker;

import java.awt.geom.Point2D;
import java.util.Collection;
import java.util.Optional;

import org.bukkit.Location;
import org.bukkit.Particle;
import org.bukkit.World;
import org.bukkit.entity.Entity;
import org.bukkit.entity.IronGolem;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.entity.Projectile;
import org.bukkit.entity.Villager;
import org.bukkit.event.EventHandler;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.util.Vector;


/**
 * @author EnderCrypt
 */
public class HostileVillagerManager extends MinecraftFeature
{
	private final EntityTracker<IronGolem> ironGolems = new EntityTracker<>(IronGolem.class);
	private final EndangeredCollection endangered = new EndangeredCollection();
	
	public HostileVillagerManager(Core core)
	{
		super(core);
	}
	
	@EventHandler
	public void onVillageEntityAttacked(EntityDamageByEntityEvent event)
	{
		Player player = getDamageSource(event.getDamager());
		Entity defender = event.getEntity();
		double entityValue = getEntityValue(defender);
		if (player != null && entityValue > 0)
		{
			// threat
			DifficultyRating difficulty = Difficulties.getFor(event);
			double damage = event.getFinalDamage();
			double threat = (5 + (damage * (difficulty.asDoubleModifier() * 5))) * entityValue;
			resolve(player).threat().add(threat, "Attacked " + defender.getName());
			
			// target
			if (defender instanceof IronGolem ironGolem)
			{
				ironGolem.setTarget(player);
			}
		}
	}
	
	public double getEntityValue(Entity entity)
	{
		if (entity instanceof IronGolem)
		{
			return 1.0;
		}
		if (entity instanceof Villager)
		{
			return 5.0;
		}
		return 0.0;
	}
	
	private Player getDamageSource(Entity damager)
	{
		if (damager instanceof Player player)
		{
			return player;
		}
		if (damager instanceof Projectile projectile && projectile.getShooter() instanceof Player player)
		{
			return player;
		}
		return null;
	}
	
	@Override
	public void onCycleEntity(Entity entity)
	{
		if (entity instanceof Villager villager)
		{
			endangered.sync(villager);
		}
		if (entity instanceof IronGolem ironGolem)
		{
			ironGolems.add(ironGolem);
		}
	}
	
	@Override
	public void onTick()
	{
		for (IronGolem ironGolem : ironGolems)
		{
			LivingEntity target = ironGolem.getTarget();
			if (target != null && shouldGolemFly(ironGolem, target))
			{
				ironGolem.addPotionEffect(new PotionEffect(PotionEffectType.LEVITATION, 5, 2));
				double x = target.getLocation().getX() - ironGolem.getLocation().getX();
				double z = target.getLocation().getZ() - ironGolem.getLocation().getZ();
				Vector motion = new Vector(x, 0, z).normalize().multiply(0.025);
				ironGolem.setVelocity(ironGolem.getVelocity().add(motion));
				ironGolem.getWorld().spawnParticle(Particle.FLAME, ironGolem.getLocation(), 5, 0.5, 0, 0.5, 0.05);
			}
		}
	}
	
	@Override
	public void onSecond()
	{
		// update villagers
		endangered.update();
		
		// update heat
		for (PlayerProfile profile : core.getPlayerManager())
		{
			profile.threat().apply(endangered);
		}
		
		// update golems
		updateGolems();
	}
	
	private void updateGolems()
	{
		for (World world : server.getWorlds())
		{
			for (IronGolem ironGolem : world.getEntitiesByClass(IronGolem.class))
			{
				getPriorityTarget(ironGolem).ifPresent(ironGolem::setTarget);
			}
		}
	}
	
	public boolean shouldGolemFly(IronGolem ironGolem, LivingEntity target)
	{
		if (target instanceof Player == false)
		{
			return false;
		}
		Location location = ironGolem.getLocation();
		Location targetLocation = target.getLocation();
		double altitude = targetLocation.getY() - location.getY() - 2;
		double distance = Point2D.distance(location.getX(), location.getZ(), targetLocation.getX(), targetLocation.getZ());
		return distance * 0.5 < altitude;
	}
	
	private Optional<Player> getPriorityTarget(IronGolem ironGolem)
	{
		Collection<Player> players = ironGolem.getLocation().getWorld().getPlayers();
		Player target = null;
		double targetValue = 1.0; // minimum target valu
		
		for (Player player : players)
		{
			double heat = resolve(player).threat().get();
			double distance = ironGolem.getLocation().distance(player.getLocation());
			double value = heat / distance;
			if (distance < 100 && value > targetValue)
			{
				target = player;
				targetValue = value;
			}
		}
		
		return Optional.ofNullable(target);
	}
}
