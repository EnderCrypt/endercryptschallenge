/**************************************************************************\
* EnderCrypt's Challenge                                                   *
* Copyright (C) 2023 by EnderCrypt                                         *
*                                                                          *
* This program is free software: you can redistribute it and/or modify     *
* it under the terms of the GNU Affero General Public License as           *
* published by the Free Software Foundation, either version 3 of           *
* the License, or (at your option) any later version.                      *
*                                                                          *
* This program is distributed in the hope that it will be useful,          *
* but WITHOUT ANY WARRANTY; without even the implied warranty of           *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
* GNU Affero General Public License for more details.                      *
*                                                                          *
* You should have received a copy of the GNU Affero General Public License *
* along with this program in its root directory.                           *
* If not, see <http://www.gnu.org/licenses/>.                              *
\**************************************************************************/

package endercrypt.endercryptschallenge.features.village.endangered;


import endercrypt.endercryptschallenge.utility.difficulty.Difficulties;
import endercrypt.endercryptschallenge.utility.difficulty.DifficultyRating;
import endercrypt.library.commons.misc.Ender;

import java.util.HashMap;
import java.util.Map;

import org.bukkit.Location;
import org.bukkit.Particle;
import org.bukkit.entity.Boat;
import org.bukkit.entity.Player;
import org.bukkit.entity.Vehicle;
import org.bukkit.entity.Villager;


/**
 * @author EnderCrypt
 */
public class EndangeredVillager
{
	public static final double ENGANGERED_DISTANCE = 32;
	
	public static boolean isEndangered(Villager villager)
	{
		Location origin = villager.getOrigin();
		Location location = villager.getLocation();
		if (origin.getWorld().equals(location.getWorld()) == false)
		{
			return true;
		}
		double distance = origin.distance(location);
		return (distance >= ENGANGERED_DISTANCE);
	}
	
	private final Villager villager;
	private int time = 0;
	
	public EndangeredVillager(Villager villager)
	{
		this.villager = villager;
	}
	
	public Villager getVillager()
	{
		return villager;
	}
	
	public void update()
	{
		time++;
		particleEffect();
		getOutOfVehicle();
		pathBackHome();
	}
	
	private void particleEffect()
	{
		Location location = villager.getLocation().add(0, 1.25, 0);
		villager.getWorld().spawnParticle(Particle.ANGRY_VILLAGER, location, Ender.math.randomRange(3, 5));
	}
	
	private void getOutOfVehicle()
	{
		DifficultyRating difficulty = Difficulties.getFor(villager);
		if (Math.random() > 1.0 / difficulty.asIntValue(30, 20, 10))
		{
			return;
		}
		if (villager.getVehicle() instanceof Vehicle == false)
		{
			return;
		}
		if (villager.getVehicle() instanceof Boat boat && boat.getPassengers().size() == 1) // villager driving boat, dont exit it
		{
			return;
		}
		
		villager.leaveVehicle();
	}
	
	private void pathBackHome()
	{
		if (Math.random() > 0.1)
		{
			return;
		}
		if (villager.isInsideVehicle())
		{
			return;
		}
		villager.getPathfinder().moveTo(villager.getOrigin());
	}
	
	public int getThreatRange()
	{
		int range = time - 5;
		if (range < 0)
		{
			range = 0;
		}
		if (range > 16)
		{
			range = 16;
		}
		return range;
	}
	
	public Map<Player, Double> getActiveThreats()
	{
		final int range = getThreatRange();
		Map<Player, Double> threats = new HashMap<>();
		if (range <= 0)
		{
			return threats;
		}
		Location location = villager.getLocation();
		for (Player player : location.getNearbyPlayers(range))
		{
			double distance = location.distance(player.getLocation());
			double threat = range - distance;
			threats.put(player, threat);
		}
		return threats;
	}
	
	public boolean isIrrelevant()
	{
		return villager.isDead() || EndangeredVillager.isEndangered(villager) == false;
	}
}
