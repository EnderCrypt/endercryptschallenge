/**************************************************************************\
* EnderCrypt's Challenge                                                   *
* Copyright (C) 2023 by EnderCrypt                                         *
*                                                                          *
* This program is free software: you can redistribute it and/or modify     *
* it under the terms of the GNU Affero General Public License as           *
* published by the Free Software Foundation, either version 3 of           *
* the License, or (at your option) any later version.                      *
*                                                                          *
* This program is distributed in the hope that it will be useful,          *
* but WITHOUT ANY WARRANTY; without even the implied warranty of           *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
* GNU Affero General Public License for more details.                      *
*                                                                          *
* You should have received a copy of the GNU Affero General Public License *
* along with this program in its root directory.                           *
* If not, see <http://www.gnu.org/licenses/>.                              *
\**************************************************************************/

package endercrypt.endercryptschallenge.features.village.endangered;


import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.bukkit.entity.Villager;


/**
 * @author EnderCrypt
 */
public class EndangeredCollection implements Iterable<EndangeredVillager>
{
	private final Map<Villager, EndangeredVillager> endangered = new HashMap<>();
	
	public void add(Villager villager)
	{
		if (has(villager))
		{
			return;
		}
		endangered.put(villager, new EndangeredVillager(villager));
	}
	
	public void remove(Villager villager)
	{
		endangered.remove(villager);
	}
	
	public boolean has(Villager villager)
	{
		return endangered.containsKey(villager);
	}
	
	public void syncAll(Collection<Villager> villagers)
	{
		for (Villager villager : villagers)
		{
			sync(villager);
		}
	}
	
	public void sync(Villager villager)
	{
		if (EndangeredVillager.isEndangered(villager))
		{
			add(villager);
		}
	}
	
	public void update()
	{
		Iterator<EndangeredVillager> iterator = iterator();
		while (iterator.hasNext())
		{
			EndangeredVillager villager = iterator.next();
			villager.update();
			if (villager.isIrrelevant())
			{
				iterator.remove();
			}
		}
	}
	
	@Override
	public Iterator<EndangeredVillager> iterator()
	{
		return endangered.values().iterator();
	}
}
