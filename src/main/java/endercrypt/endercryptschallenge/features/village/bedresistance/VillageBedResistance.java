/**************************************************************************\
* EnderCrypt's Challenge                                                   *
* Copyright (C) 2023 by EnderCrypt                                         *
*                                                                          *
* This program is free software: you can redistribute it and/or modify     *
* it under the terms of the GNU Affero General Public License as           *
* published by the Free Software Foundation, either version 3 of           *
* the License, or (at your option) any later version.                      *
*                                                                          *
* This program is distributed in the hope that it will be useful,          *
* but WITHOUT ANY WARRANTY; without even the implied warranty of           *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
* GNU Affero General Public License for more details.                      *
*                                                                          *
* You should have received a copy of the GNU Affero General Public License *
* along with this program in its root directory.                           *
* If not, see <http://www.gnu.org/licenses/>.                              *
\**************************************************************************/

package endercrypt.endercryptschallenge.features.village.bedresistance;


import endercrypt.endercryptschallenge.core.Core;
import endercrypt.endercryptschallenge.materials.MaterialCollections;
import endercrypt.endercryptschallenge.modules.features.MinecraftFeature;
import endercrypt.endercryptschallenge.utility.Utility;

import java.util.List;

import org.bukkit.Location;
import org.bukkit.block.Block;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.entity.Villager;


/**
 * @author EnderCrypt
 */
public class VillageBedResistance extends MinecraftFeature
{
	private static final int VILLAGER_DISTANCE = 15;
	private static final int PLAYER_DISTANCE = 15;
	
	public VillageBedResistance(Core core)
	{
		super(core);
	}
	
	@Override
	public void onCycleEntity(Entity entity)
	{
		if (entity instanceof Player player)
		{
			processPlayer(player);
		}
	}
	
	private void processPlayer(Player player)
	{
		Location bedLocation = player.getBedSpawnLocation();
		if (bedLocation == null)
			return;
		
		if (player.getWorld().equals(bedLocation.getWorld()))
		{
			double playerDistance = player.getLocation().distance(bedLocation);
			if (playerDistance < PLAYER_DISTANCE)
			{
				return;
			}
		}
		
		double playerThreat = resolve(player).threat().get();
		if (playerThreat < 5)
			return;
		
		int villagers = bedLocation.getNearbyEntitiesByType(Villager.class, VILLAGER_DISTANCE).size();
		if (villagers == 0)
			return;
		
		player.sendMessage("A villager has broken your bed due to your recent aggressions!");
		destroyBed(bedLocation);
	}
	
	private void destroyBed(Location location)
	{
		Block locationBlock = location.getBlock();
		List<Block> blocks = Utility.getBlockQube(locationBlock, 1);
		blocks.remove(locationBlock);
		for (Block block : blocks)
		{
			if (MaterialCollections.beds.contains(block))
			{
				block.breakNaturally();
			}
		}
	}
}
